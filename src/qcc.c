/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "libqcc/qcc.h"

#include <stdarg.h>
#include <stdio.h>


static FILE *logfile = NULL;


static unsigned char *QCC_ReadFile (const char *fname, void *buffer, int len) {
  long length;
  FILE *f = fopen(fname, "rb");
  if (!f) return NULL;
  length = fread(buffer, 1, len, f);
  fclose(f);
  if (length != len) return NULL;
  return buffer;
}


static int QCC_FileSize (const char *fname) {
  long length;
  FILE *f = fopen(fname, "rb");
  if (!f) return -1;
  fseek(f, 0, SEEK_END);
  length = ftell(f);
  fclose(f);
  return length;
}


static qccbool QCC_WriteFile (const char *name, const void *data, int len) {
  long length;
  FILE *f = fopen(name, "wb");
  if (!f) return qcc_false;
  length = fwrite(data, 1, len, f);
  fclose(f);
  if (length != len) return qcc_false;
  return qcc_true;
}


static void Sys_Error (const char *text, ...) {
  va_list argptr;
  static char msg[2048];
  va_start(argptr,text);
  vsnprintf(msg,sizeof(msg)-1, text,argptr);
  va_end(argptr);
  QCC_Error(ERR_INTERNAL, "%s", msg);
}


static __attribute__((format(printf,1,2))) int logprintf (const char *format, ...) {
  va_list argptr, a2;
  va_start(argptr, format);
  va_copy(a2, argptr);
  vfprintf(stderr, format, a2);
  va_end(a2);
  if (logfile) vfprintf(logfile, format, argptr);
  va_end(argptr);
  return 0;
}


int main (int argc, char *argv[]) {
  int sucess;
  progfuncs_t funcs;
  qccprogfuncs = &funcs;
  memset(&funcs, 0, sizeof(funcs));
  funcs.ReadFile = QCC_ReadFile;
  funcs.FileSize = QCC_FileSize;
  funcs.WriteFile = QCC_WriteFile;
  funcs.printf = logprintf;
  funcs.Sys_Error = Sys_Error;
  funcs.memalloc = malloc;
  funcs.memfree = free;
  funcs.memrealloc = realloc;
  for (int f = 1; f < argc; ++f) {
    const char *a = argv[f];
    if (strcmp(a, "--") == 0) break;
    if (strcmp(a, "-log") == 0 || strcmp(a, "--log") == 0) {
      if (logfile) fclose(logfile);
      logfile = fopen("d2dqcc.log", "w");
      for (int c = f; c < argc; ++c) argv[c] = argv[c+1];
      --argc;
      --f;
    }
  }
  sucess = CompileParams(&funcs, qcc_true, argc, argv);
  qccClearHunk();
  if (logfile) fclose(logfile);
  return (sucess == 0);
}
