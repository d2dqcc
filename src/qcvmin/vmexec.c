/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013  Wolfgang Bumiller, Dale Weiler
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
////////////////////////////////////////////////////////////////////////////////
static __attribute__((format(printf,3,4))) void qcvmerror (qc_program *prog, const prog_section_function *func, const char *fmt, ...) {
  va_list ap;
  /* */
  ++prog->vmerror;
  fprintf(stderr, "*** VM ERROR *** (%s in %s): ", prog_getstring(prog, func->name), prog->filename);
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fputc('\n', stderr);
}


////////////////////////////////////////////////////////////////////////////////
static qcint prog_enterfunction (qc_program *prog, prog_section_function *func) {
  qc_exec_stack st;
  size_t parampos;
  qcint *globals;
  /* back up locals */
  st.localsp = vec_size(prog->localstack);
  st.stmt = prog->statement;
  st.function = func;
  globals = prog->globals+func->firstlocal;
  vec_append(prog->localstack, func->locals, globals);
  /* copy parameters */
  parampos = func->firstlocal;
  for (int32_t p = 0; p < func->nargs; ++p) {
    for (size_t s = 0; s < (func->argsize[p]&0xff); ++s) {
      prog->globals[parampos] = prog->globals[OFS_PARM0+3*p+s];
      ++parampos;
    }
  }
  vec_push(prog->stack, st);
  return func->entry;
}


static qcint prog_leavefunction (qc_program *prog, prog_section_function **pfunc) {
  prog_section_function *prev = NULL;
  size_t oldsp;
  qc_exec_stack st = vec_last(prog->stack);
  prev = prog->stack[vec_size(prog->stack)-1].function;
  oldsp = prog->stack[vec_size(prog->stack)-1].localsp;
  if (prev) {
    qcint *globals = prog->globals+prev->firstlocal;
    memcpy(globals, prog->localstack+oldsp, prev->locals*sizeof(prog->localstack[0]));
    /* vec_remove(prog->localstack, oldsp, vec_size(prog->localstack)-oldsp); */
    vec_shrinkto(prog->localstack, oldsp);
  }
  vec_pop(prog->stack);
  if (pfunc != NULL) *pfunc = prev;
  return st.stmt-1;
}


////////////////////////////////////////////////////////////////////////////////
#define OPA ((qcany *)(prog->globals+st->o1.u1))
#define OPB ((qcany *)(prog->globals+st->o2.u1))
#define OPC ((qcany *)(prog->globals+st->o3.u1))

#define GLOBAL(x) ((qcany *)(prog->globals+(x)))


/* to be consistent with current darkplaces behaviour */
/* float 'zero' and 'negative zero' is actually 'int zero' with possible 31th bit set */
#if !defined(FLOAT_IS_TRUE_FOR_INT)
# define FLOAT_IS_TRUE_FOR_INT(x)  ((x)&0x7FFFFFFF)
#endif


#if !defined(FLOAT_IS_ZERO)
# define FLOAT_IS_ZERO(x)  (((x)&0x7FFFFFFF) == 0)
#endif


#define INC_JUMP_COUNT()  do { \
  if (++jumpcount == maxjumps) { \
    qcvmerror(prog, func, "runaway loop counter limit of %lli jumps exceeded", jumpcount); \
    goto cleanup; \
  } \
} while (0)


////////////////////////////////////////////////////////////////////////////////
static inline qcany *QCP2CP (qc_program *prog, const prog_section_function *func, qcuint pp, int ofs) {
  if ((pp&0x80000000U) == 0) {
    // field
    pp += ofs;
    if (pp >= (qcuint)vec_size(prog->entitydata)) {
      qcvmerror(prog, func, "attempted to address something out of entity (%u)", pp);
      return NULL;
    }
    return (qcany *)(prog->entitydata+pp);
  } else {
    // either string or global
    if (pp&0x40000000U) {
      // global
      pp &= QCP_MASK;
      pp += ofs;
      if (pp >= (qcuint)vec_size(prog->globals)) {
        qcvmerror(prog, func, "attempted to address something out of globals (%u)", pp);
        return NULL;
      }
      return (qcany *)(prog->globals+pp);
    } else {
      // string
      pp &= QCP_MASK;
      pp += ofs;
      if (pp >= (qcuint)vec_size(prog->strings)) {
        qcvmerror(prog, func, "attempted to address something out of globals (%u)", pp);
        return NULL;
      }
      return (qcany *)(prog->strings+pp);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#include "qcopnames.c"


////////////////////////////////////////////////////////////////////////////////
qcbool prog_exec (qc_program *prog, prog_section_function *func, int64_t maxjumps) {
  int64_t jumpcount = 0;
  prog_section_statement *st;
  prog->vmerror = 0;
  st = prog->code+prog_enterfunction(prog, func);
  --st;
  while (prog->vmerror == 0) {
    char buffer[128];
    prog_section_function *newf;
    qcany *ed;
    qcany *ptr;
    qcfloat f;
    qcint i;
    ++st;
    //fprintf(stderr, "%d: %s\n", (int)(st-prog->code+prog_enterfunction(prog, func)), opnames[st->opcode]);
    switch (st->opcode) {
      case OP_DONE:
      case OP_RETURN:
        GLOBAL(OFS_RETURN)->ivector[0] = OPA->ivector[0];
        GLOBAL(OFS_RETURN)->ivector[1] = OPA->ivector[1];
        GLOBAL(OFS_RETURN)->ivector[2] = OPA->ivector[2];
        st = prog->code+prog_leavefunction(prog, &newf);
        //fprintf(stderr, "O: [%s -> %s]\n", prog_getstring(prog, func->name), prog_getstring(prog, newf->name));
        if (!vec_size(prog->stack)) goto cleanup;
        func = newf;
        break;
      case OP_MUL_F: OPC->fnum = OPA->fnum*OPB->fnum; break;
      case OP_MUL_V:
        OPC->fnum = OPA->vector[0]*OPB->vector[0]+OPA->vector[1]*OPB->vector[1]+OPA->vector[2]*OPB->vector[2];
        break;
      case OP_MUL_FV:
        f = OPA->fnum;
        OPC->vector[0] = f*OPB->vector[0];
        OPC->vector[1] = f*OPB->vector[1];
        OPC->vector[2] = f*OPB->vector[2];
        break;
      case OP_MUL_VF:
        f = OPB->fnum;
        OPC->vector[0] = f*OPA->vector[0];
        OPC->vector[1] = f*OPA->vector[1];
        OPC->vector[2] = f*OPA->vector[2];
        break;
      case OP_DIV_F:
        if (FLOAT_IS_ZERO(OPB->inum)) {
          qcvmerror(prog, func, "divided by zero");
          goto cleanup;
        }
        OPC->fnum = OPA->fnum/OPB->fnum;
        break;
      case OP_ADD_F: OPC->fnum = OPA->fnum+OPB->fnum; break;
      case OP_ADD_V:
        OPC->vector[0] = OPA->vector[0]+OPB->vector[0];
        OPC->vector[1] = OPA->vector[1]+OPB->vector[1];
        OPC->vector[2] = OPA->vector[2]+OPB->vector[2];
        break;
      case OP_SUB_F: OPC->fnum = OPA->fnum-OPB->fnum; break;
      case OP_SUB_V:
        OPC->vector[0] = OPA->vector[0]-OPB->vector[0];
        OPC->vector[1] = OPA->vector[1]-OPB->vector[1];
        OPC->vector[2] = OPA->vector[2]-OPB->vector[2];
        break;
      case OP_EQ_F: OPC->fnum = (OPA->fnum == OPB->fnum); break;
      case OP_EQ_I: OPC->inum = (OPA->inum == OPB->inum); break;
      case OP_EQ_IF: OPC->fnum = (OPA->inum == OPB->fnum); break;
      case OP_EQ_FI: OPC->fnum = (OPA->fnum == OPB->inum); break;
      case OP_EQ_V:
        OPC->fnum = ((OPA->vector[0] == OPB->vector[0]) &&
                     (OPA->vector[1] == OPB->vector[1]) &&
                     (OPA->vector[2] == OPB->vector[2]));
        break;
      case OP_EQ_S: OPC->fnum = (strcmp(prog_getstring(prog, OPA->string&QCP_MASK), prog_getstring(prog, OPB->string&QCP_MASK)) == 0); break;
      case OP_EQ_E: OPC->fnum = (OPA->inum == OPB->inum); break;
      case OP_EQ_FNC: OPC->fnum = (OPA->function == OPB->function); break;
      case OP_NE_F: OPC->fnum = (OPA->fnum != OPB->fnum); break;
      case OP_NE_I: OPC->inum = (OPA->inum != OPB->inum); break;
      case OP_NE_V:
        OPC->fnum = ((OPA->vector[0] != OPB->vector[0]) ||
                     (OPA->vector[1] != OPB->vector[1]) ||
                     (OPA->vector[2] != OPB->vector[2]));
          break;
      case OP_NE_S: OPC->fnum = (strcmp(prog_getstring(prog, OPA->string&QCP_MASK), prog_getstring(prog, OPB->string&QCP_MASK)) != 0); break;
      case OP_NE_E: OPC->fnum = (OPA->inum != OPB->inum); break;
      case OP_NE_FNC: OPC->fnum = (OPA->function != OPB->function); break;
      case OP_LE_F: OPC->fnum = (OPA->fnum <= OPB->fnum); break;
      case OP_GE_F: OPC->fnum = (OPA->fnum >= OPB->fnum); break;
      case OP_LT_F: OPC->fnum = (OPA->fnum < OPB->fnum); break;
      case OP_GT_F: OPC->fnum = (OPA->fnum > OPB->fnum); break;
      case OP_LE_I: OPC->fnum = (OPA->inum <= OPB->inum); break;
      case OP_GE_I: OPC->fnum = (OPA->inum >= OPB->inum); break;
      case OP_LT_I: OPC->fnum = (OPA->inum < OPB->inum); break;
      case OP_GT_I: OPC->fnum = (OPA->inum > OPB->inum); break;
      case OP_LOAD_I:
      case OP_LOAD_F:
      case OP_LOAD_S:
      case OP_LOAD_FLD:
      case OP_LOAD_ENT:
      case OP_LOAD_FNC:
        if (OPA->edict < 0 || OPA->edict >= prog->entities) {
          qcvmerror(prog, func, "attempted to read an out of bounds entity");
          goto cleanup;
        }
        if (OPB->ptr >= (qcuint)prog->entityfields) {
          qcvmerror(prog, func, "attempted to read an invalid field from entity (%i)", OPB->inum);
          goto cleanup;
        }
        ed = prog_getedict(prog, OPA->edict);
        OPC->inum = ((const qcany *)(((const qcint *)ed)+OPB->inum))->inum;
        break;
      case OP_LOAD_V:
        if (OPA->edict < 0 || OPA->edict >= prog->entities) {
          qcvmerror(prog, func, "attempted to read an out of bounds entity");
          goto cleanup;
        }
        if (OPB->inum < 0 || OPB->inum+3 > (qcint)prog->entityfields) {
          qcvmerror(prog, func, "attempted to read an invalid field from entity (%i)", OPB->inum+2);
          goto cleanup;
        }
        ed = prog_getedict(prog, OPA->edict);
        ptr = (qcany *)(((qcint *)ed)+OPB->inum);
        OPC->ivector[0] = ptr->ivector[0];
        OPC->ivector[1] = ptr->ivector[1];
        OPC->ivector[2] = ptr->ivector[2];
        break;
      case OP_ADDRESS:
        if (OPA->edict < 0 || OPA->edict >= prog->entities) {
          qcvmerror(prog, func, "attempted to address an out of bounds entity %i", OPA->edict);
          goto cleanup;
        }
        if (OPB->ptr >= (qcuint)prog->entityfields) {
          qcvmerror(prog, func, "attempted to read an invalid field from entity (%i)", OPB->inum);
          goto cleanup;
        }
        ed = prog_getedict(prog, OPA->edict);
        OPC->inum = ((const qcint *)ed)-prog->entitydata+OPB->inum;
        break;
      case OP_STORE_P:
      case OP_STORE_I:
      case OP_STORE_F:
      case OP_STORE_S:
      case OP_STORE_ENT:
      case OP_STORE_FLD:
      case OP_STORE_FNC:
        OPB->inum = OPA->inum;
        break;
      case OP_STORE_V:
        OPB->ivector[0] = OPA->ivector[0];
        OPB->ivector[1] = OPA->ivector[1];
        OPB->ivector[2] = OPA->ivector[2];
        break;
      // store a value to a pointer
      case OP_STOREP_IF:
        if ((ptr = QCP2CP(prog, func, OPB->inum, 0)) == NULL) goto cleanup;
        ptr->fnum = (qcfloat)OPA->inum;
        break;
      case OP_STOREP_FI:
        if ((ptr = QCP2CP(prog, func, OPB->inum, 0)) == NULL) goto cleanup;
        ptr->inum = (qcint)OPA->fnum;
        break;
      case OP_STOREP_I:
        if ((ptr = QCP2CP(prog, func, OPB->inum, 0)) == NULL) goto cleanup;
        ptr->inum = OPA->inum;
        break;
      case OP_STOREP_F:
      case OP_STOREP_S:
      case OP_STOREP_ENT:
      case OP_STOREP_FLD:
      case OP_STOREP_FNC:
        if ((ptr = QCP2CP(prog, func, OPB->inum, 0)) == NULL) goto cleanup;
        ptr->inum = OPA->inum;
        break;
      case OP_STOREP_V:
        if ((ptr = QCP2CP(prog, func, OPB->inum, 0)) == NULL) goto cleanup;
        ptr->ivector[0] = OPA->ivector[0];
        ptr->ivector[1] = OPA->ivector[1];
        ptr->ivector[2] = OPA->ivector[2];
        break;
      case OP_NOT_F: OPC->fnum = !FLOAT_IS_TRUE_FOR_INT(OPA->inum); break;
      case OP_NOT_V: OPC->fnum = (!OPA->vector[0] && !OPA->vector[1] && !OPA->vector[2]); break;
      case OP_NOT_S: OPC->fnum = (!OPA->string || (prog_getstring(prog, OPA->string))[0] == 0); break;
      case OP_NOT_ENT: OPC->fnum = (OPA->edict == 0); break;
      case OP_NOT_FNC: OPC->fnum = !OPA->function; break;
      case OP_IF_I: if (OPA->inum) { st += st->o2.s1-1; INC_JUMP_COUNT(); } break;
      case OP_IFNOT_I: if (!OPA->inum) { st += st->o2.s1-1; INC_JUMP_COUNT(); } break;
      case OP_CALL0:
      case OP_CALL1:
      case OP_CALL2:
      case OP_CALL3:
      case OP_CALL4:
      case OP_CALL5:
      case OP_CALL6:
      case OP_CALL7:
      case OP_CALL8:
        prog->argc = st->opcode-OP_CALL0;
        if (!OPA->function) qcvmerror(prog, func, "NULL function call");
        if (!OPA->function || OPA->function >= (qcint)vec_size(prog->functions)) {
          qcvmerror(prog, func, "CALL outside the program");
          goto cleanup;
        }
        newf = &prog->functions[OPA->function];
        prog->statement = (st-prog->code)+1;
        if (newf->entry < 0) {
          // negative statements are built in functions
          qcint builtinnumber = -newf->entry;
          if (builtinnumber == 666) {
            qcvmerror(prog, func, "*** aborted! ***");
            goto cleanup;
          }
          if (builtinnumber < (qcint)prog->builtins_count && prog->builtins[builtinnumber].fn) {
            if (prog->builtins[builtinnumber].fn(prog) < 0) {
              qcvmerror(prog, func, "builtin #%d failed", builtinnumber);
              goto cleanup;
            }
          } else {
            qcvmerror(prog, func, "no builtin #%i", builtinnumber);
            goto cleanup;
          }
        } else {
          //fprintf(stderr, "I: [%s -> %s]\n", prog_getstring(prog, func->name), prog_getstring(prog, newf->name));
          st = prog->code+prog_enterfunction(prog, newf)-1; // offset st++
          func = newf;
        }
        if (prog->vmerror) goto cleanup;
        INC_JUMP_COUNT();
        break;
      case OP_STATE:
        qcvmerror(prog, func, "tried to execute a STATE operation");
        break;
      case OP_GOTO:
        st += st->o1.s1-1; // offset the s++
        INC_JUMP_COUNT();
        break;
      case OP_AND_F:
        OPC->fnum = FLOAT_IS_TRUE_FOR_INT(OPA->inum) && FLOAT_IS_TRUE_FOR_INT(OPB->inum);
        break;
      case OP_OR_F:
        OPC->fnum = FLOAT_IS_TRUE_FOR_INT(OPA->inum) || FLOAT_IS_TRUE_FOR_INT(OPB->inum);
        break;
      case OP_BITAND_F: OPC->fnum = ((int)OPA->fnum)&((int)OPB->fnum); break;
      case OP_BITOR_F: OPC->fnum = ((int)OPA->fnum)|((int)OPB->fnum); break;
      case OP_FETCH_GBL_F:
      case OP_FETCH_GBL_S:
      case OP_FETCH_GBL_E:
      case OP_FETCH_GBL_FNC:
        if (st->o1.u1 < 1 || st->o1.u1 >= vec_size(prog->globals)) {
          qcvmerror(prog, func, "invalid array address (%u)", st->o1.u1);
          goto cleanup;
        }
        i = (qcint)OPB->fnum;
        ptr = (qcany *)(prog->globals+st->o1.u1-1);
        if (i < 0 || i+st->o1.s1 >= (qcint)vec_size(prog->globals) || i > ptr->inum) {
          qcvmerror(prog, func, "array index %d out of bounds", i);
          goto cleanup;
        }
        ptr += i+1;
        OPC->inum = ptr->inum;
        break;
      case OP_FETCH_GBL_V:
        if (st->o1.u1 < 1 || st->o1.u1 >= vec_size(prog->globals)) {
          qcvmerror(prog, func, "invalid array address (%u(", st->o1.u1);
          goto cleanup;
        }
        i = (qcint)OPB->fnum;
        ptr = (qcany *)(prog->globals+st->o1.u1-1);
        if (i < 0 || i*3+st->o1.s1 >= (qcint)vec_size(prog->globals) || i > ptr->inum) {
          qcvmerror(prog, func, "array index %d out of bounds", i);
          goto cleanup;
        }
        ptr += i*3+1;
        OPC->vector[0] = ptr->vector[0];
        OPC->vector[1] = ptr->vector[1];
        OPC->vector[2] = ptr->vector[2];
        break;
      case OP_BOUNDCHECK:
        if (OPA->ptr < st->o3.u1 || OPA->ptr >= st->o2.u1) {
          qcvmerror(prog, func, "boundcheck failed");
          goto cleanup;
        }
        break;
      case OP_CONV_ITOF: OPC->fnum = (qcfloat)OPA->inum; break;
      case OP_CONV_FTOI: OPC->inum = (qcint)OPA->fnum; break;
      case OP_CP_ITOF: //FIXME: process other pointer types?
        ptr = (qcany *)(((uint8_t *)prog->entitydata)+OPA->inum);
        OPC->fnum = (qcfloat)ptr->inum;
        break;
      case OP_CP_FTOI: //FIXME: process other pointer types?
        ptr = (qcany *)(((uint8_t *)prog->entitydata)+OPA->inum);
        OPC->inum = (qcint)ptr->fnum;
        break;
      case OP_BITAND_IF: OPC->inum = (OPA->inum&(qcint)OPB->fnum); break;
      case OP_BITOR_IF: OPC->inum = (OPA->inum|(qcint)OPB->fnum); break;
      case OP_BITAND_FI: OPC->inum = ((qcint)OPA->fnum&OPB->inum); break;
      case OP_BITOR_FI: OPC->inum = ((qcint)OPA->fnum|OPB->inum); break;
      case OP_BITSET: OPB->fnum = (qcint)OPB->fnum|(qcint)OPA->fnum; break; // b (+) a
      case OP_BITCLR: OPB->fnum = (qcint)OPB->fnum&~((qcint)OPA->fnum); break; // b (-) a
      case OP_STORE_IF: OPB->fnum = OPA->inum; break;
      case OP_STORE_FI: OPB->inum = (qcint)OPA->fnum; break;
      case OP_ADD_I: OPC->inum = OPA->inum+OPB->inum; break;
      case OP_ADD_FI: OPC->fnum = OPA->fnum+(qcfloat)OPB->inum; break;
      case OP_ADD_IF: OPC->fnum = (qcfloat)OPA->inum+OPB->fnum; break;
      case OP_SUB_I: OPC->inum = OPA->inum-OPB->inum; break;
      case OP_SUB_FI: OPC->fnum = OPA->fnum-(qcfloat)OPB->inum; break;
      case OP_SUB_IF: OPC->fnum = (qcfloat)OPA->inum-OPB->fnum; break;
      case OP_BITAND_I: OPC->inum = (OPA->inum&OPB->inum); break;
      case OP_BITOR_I: OPC->inum = (OPA->inum|OPB->inum); break;
      case OP_MUL_I: OPC->inum = OPA->inum*OPB->inum; break;
      case OP_DIV_I:
        if (OPB->inum == 0) {
          qcvmerror(prog, func, "divided by zero");
          goto cleanup;
        }
        OPC->inum = OPA->inum/OPB->inum;
        break;
      case OP_IFNOT_S: if (!OPA->string || !prog_getstring(prog, OPA->string&QCP_MASK)[0]) { st += st->o2.s1-1; INC_JUMP_COUNT(); } break;
      case OP_IF_S: if (OPA->string && prog_getstring(prog, OPA->string&QCP_MASK)[0]) { st += st->o2.s1-1; INC_JUMP_COUNT(); } break;
      case OP_IF_F: if (FLOAT_IS_TRUE_FOR_INT(OPA->inum)) { st += st->o2.s1-1; INC_JUMP_COUNT(); } break; // this is consistent with darkplaces' behaviour
      case OP_IFNOT_F: if (!FLOAT_IS_TRUE_FOR_INT(OPA->inum)) { st += st->o2.s1-1; INC_JUMP_COUNT(); } break; // this is consistent with darkplaces' behaviour
      case OP_XOR_I: OPC->inum = OPA->inum^OPB->inum; break;
      case OP_RSHIFT_I: OPC->inum = OPA->inum>>OPB->inum; break;
      case OP_LSHIFT_I: OPC->inum = OPA->inum<<OPB->inum; break;
      // this always load from globals
      case OP_LOADA_I:
      case OP_LOADA_F:
      case OP_LOADA_FLD:
      case OP_LOADA_ENT:
      case OP_LOADA_S:
      case OP_LOADA_FNC:
        ptr = (qcany *)(&OPA->inum+OPB->inum); // pointer arithmetic
        OPC->inum = ptr->inum;
        break;
      case OP_LOADA_V:
        ptr = (qcany *)(&OPA->inum+OPB->inum);
        OPC->vector[0] = ptr->vector[0];
        OPC->vector[1] = ptr->vector[1];
        OPC->vector[2] = ptr->vector[2];
        break;
      case OP_GLOBALADDRESS:
        OPC->ptr = QCGlobalPtr(&OPA->inum-prog->globals+OPB->inum); // pointer arithmetic
        break;
      case OP_POINTER_ADD: // pointer to 32 bit (remember to *3 for vectors)
        OPC->ptr = OPA->ptr+OPB->inum*4;
        break;
      case OP_LOADP_I:
      case OP_LOADP_F:
      case OP_LOADP_FLD:
      case OP_LOADP_ENT:
      case OP_LOADP_S:
      case OP_LOADP_FNC:
        if ((ptr = QCP2CP(prog, func, OPA->inum, OPB->inum*4)) == NULL) goto cleanup;
        OPC->inum = ptr->inum;
        break;
      case OP_LOADP_V:
        if ((ptr = QCP2CP(prog, func, OPA->inum, OPB->inum)) == NULL) goto cleanup;
        OPC->vector[0] = ptr->vector[0];
        OPC->vector[1] = ptr->vector[1];
        OPC->vector[2] = ptr->vector[2];
        break;
      case OP_ADD_SF: // (char*)c = (char*)a + (qcfloat)b
        OPC->inum = OPA->inum+(qcint)OPB->fnum;
        break;
      case OP_SUB_S:  // (qcfloat)c = (char*)a - (char*)b
        OPC->inum = OPA->inum-OPB->inum;
        break;
      case OP_STOREP_C: // store character in a string; A: char(f), B: stidx(i)
        if (OPB->unum >= (qcint)vec_size(prog->strings)) {
          qcvmerror(prog, func, "string out of bounds");
          goto cleanup;
        }
        if (OPB->unum < prog->tempstring_start) {
          qcvmerror(prog, func, "can't change read-only string");
          goto cleanup;
        }
        prog->strings[OPB->inum] = (unsigned char)OPA->fnum;
        break;
      case OP_LOADP_C: // load character from a string; A: stidx(i); B: stofs(f); C: dest
        OPC->fnum = *(prog_getstring(prog, OPA->inum+(qcint)OPB->fnum));
        break;
      case OP_BITSETP: // .b (+) a
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        ptr->fnum = ((qcint)ptr->fnum|(qcint)OPA->fnum);
        break;
      case OP_BITCLRP: // .b (-) a
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        ptr->fnum = ((qcint)ptr->fnum&~((qcint)OPA->fnum));
        break;
      case OP_MULSTORE_F: // f *= f
        OPB->fnum *= OPA->fnum;
        break;
      case OP_MULSTORE_V: // v *= f
        OPB->vector[0] *= OPA->fnum;
        OPB->vector[1] *= OPA->fnum;
        OPB->vector[2] *= OPA->fnum;
        break;
      case OP_MULSTOREP_F: // e.f *= f
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->fnum = (ptr->fnum *= OPA->fnum);
        break;
      case OP_MULSTOREP_V: // e.v *= f
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->vector[0] = (ptr->vector[0] *= OPA->fnum);
        OPC->vector[0] = (ptr->vector[1] *= OPA->fnum);
        OPC->vector[0] = (ptr->vector[2] *= OPA->fnum);
        break;
      case OP_DIVSTORE_F: // f /= f
        if (FLOAT_IS_ZERO(OPA->inum)) {
          qcvmerror(prog, func, "divided by zero");
          goto cleanup;
        }
        OPB->fnum /= OPA->fnum;
        break;
      case OP_DIVSTOREP_F: // e.f /= f
        if (FLOAT_IS_ZERO(OPA->inum)) {
          qcvmerror(prog, func, "divided by zero");
          goto cleanup;
        }
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->fnum = (ptr->fnum /= OPA->fnum);
        break;
      case OP_ADDSTORE_F: // f += f
        OPB->fnum += OPA->fnum;
        break;
      case OP_ADDSTORE_V: // v += v
        OPB->vector[0] += OPA->vector[0];
        OPB->vector[1] += OPA->vector[1];
        OPB->vector[2] += OPA->vector[2];
        break;
      case OP_ADDSTOREP_F: // e.f += f
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->fnum = (ptr->fnum += OPA->fnum);
        break;
      case OP_ADDSTOREP_V: // e.v += v
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->vector[0] = (ptr->vector[0] += OPA->vector[0]);
        OPC->vector[1] = (ptr->vector[1] += OPA->vector[1]);
        OPC->vector[2] = (ptr->vector[2] += OPA->vector[2]);
        break;
      case OP_SUBSTORE_F: // f -= f
        OPB->fnum -= OPA->fnum;
        break;
      case OP_SUBSTORE_V: // v -= v
        OPB->vector[0] -= OPA->vector[0];
        OPB->vector[1] -= OPA->vector[1];
        OPB->vector[2] -= OPA->vector[2];
        break;
      case OP_SUBSTOREP_F: // e.f -= f
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->fnum = (ptr->fnum -= OPA->fnum);
        break;
      case OP_SUBSTOREP_V: // e.v -= v
        ptr = QCP2CP(prog, func, OPB->inum, 0);
        OPC->vector[0] = (ptr->vector[0] -= OPA->vector[0]);
        OPC->vector[1] = (ptr->vector[1] -= OPA->vector[1]);
        OPC->vector[2] = (ptr->vector[2] -= OPA->vector[2]);
        break;
      case OP_LE_IF: OPC->fnum = (OPA->inum <= OPB->fnum); break;
      case OP_GE_IF: OPC->fnum = (OPA->inum >= OPB->fnum); break;
      case OP_LT_IF: OPC->fnum = (OPA->inum < OPB->fnum); break;
      case OP_GT_IF: OPC->fnum = (OPA->inum > OPB->fnum); break;
      case OP_LE_FI: OPC->fnum = (OPA->fnum <= OPB->inum); break;
      case OP_GE_FI: OPC->fnum = (OPA->fnum >= OPB->inum); break;
      case OP_LT_FI: OPC->fnum = (OPA->fnum < OPB->inum); break;
      case OP_GT_FI: OPC->fnum = (OPA->fnum > OPB->inum); break;
      case OP_MUL_IF: OPC->fnum = (OPA->inum*OPB->fnum); break;
      case OP_MUL_FI: OPC->fnum = (OPA->fnum*OPB->inum); break;
      case OP_MUL_VI:
        OPC->vector[0] = OPA->vector[0]*OPB->inum;
        OPC->vector[1] = OPA->vector[0]*OPB->inum;
        OPC->vector[2] = OPA->vector[0]*OPB->inum;
        break;
      case OP_MUL_IV:
        OPC->vector[0] = OPB->inum*OPA->vector[0];
        OPC->vector[1] = OPB->inum*OPA->vector[1];
        OPC->vector[2] = OPB->inum*OPA->vector[2];
        break;
      case OP_DIV_IF:
        if (FLOAT_IS_ZERO(OPB->inum)) {
          qcvmerror(prog, func, "divided by zero");
          goto cleanup;
        }
        OPC->fnum = (OPA->inum/OPB->fnum);
        break;
      case OP_DIV_FI:
        if (OPA->inum == 0) {
          qcvmerror(prog, func, "divided by zero");
          goto cleanup;
        }
        OPC->fnum = (OPA->fnum/OPB->inum);
        break;
      case OP_AND_I: OPC->inum = (OPA->inum && OPB->inum); break;
      case OP_OR_I: OPC->inum = (OPA->inum || OPB->inum); break;
      case OP_AND_IF: OPC->inum = (OPA->inum && OPB->fnum); break;
      case OP_OR_IF: OPC->inum = (OPA->inum || OPB->fnum); break;
      case OP_AND_FI: OPC->inum = (OPA->fnum && OPB->inum); break;
      case OP_OR_FI: OPC->inum = (OPA->fnum || OPB->inum); break;
      case OP_NE_IF: OPC->inum = (OPA->inum != OPB->fnum); break;
      case OP_NE_FI: OPC->inum = (OPA->fnum != OPB->inum); break;
      case OP_CONV_FTOS:
        snprintf(buffer, sizeof(buffer), "%.15g", OPA->fnum);
        OPC->string = prog_tempstring(prog, buffer);
        break;
      case OP_CONV_ITOS:
        snprintf(buffer, sizeof(buffer), "%d", OPA->inum);
        OPC->string = prog_tempstring(prog, buffer);
        break;
      case OP_CONV_VTOS:
        snprintf(buffer, sizeof(buffer), "'%.15g %.15g %.15g'", OPA->vector[0], OPA->vector[1], OPA->vector[2]);
        OPC->string = prog_tempstring(prog, buffer);
        break;
      case OP_CONV_ETOS:
        snprintf(buffer, sizeof(buffer), "<e:%d>", OPA->inum);
        OPC->string = prog_tempstring(prog, buffer);
        break;
      default:
        qcvmerror(prog, func, "illegal instruction");
        goto cleanup;
    }
  }
cleanup:
  vec_free(prog->localstack);
  vec_free(prog->stack);
  return (prog->vmerror ? qcfalse : qctrue);
}


#undef FLOAT_IS_TRUE_FORinum
#undef GLOBAL
#undef OPC
#undef OPB
#undef OPA
