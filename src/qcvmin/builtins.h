/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013  Wolfgang Bumiller, Dale Weiler
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _BUILTINS_H_
#define _BUILTINS_H_


#define qcvm_check_argc(num) do { \
  if (prog->argc != (num)) { \
    ++prog->vmerror; \
    fprintf(stderr, "ERROR: invalid number of arguments for %s: %i, expected %i\n", __FUNCTION__, prog->argc, (num));                                      \
    return -1; \
  } \
} while (0)

#define qcvm_get_global(idx)  ((qcany *)(prog->globals+(idx)))
#define qcvm_get_arg(num)     qcvm_get_global(OFS_PARM0+3*(num))
#define qcvm_set_res(any)     (*(qcvm_get_global(OFS_RETURN)) = (any))


#endif
