/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013  Wolfgang Bumiller, Dale Weiler
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _QCEXEC_H_
#define _QCEXEC_H_

#include <stdint.h>

/*#include "qcvmopcodes.h"*/
#include "../libqcc/qcc_vmop.h"


#define QVM_GCC_PACKED  __attribute__((packed,gcc_struct))

typedef enum { qcfalse, qctrue } qcbool;
typedef int32_t qcint;
typedef uint32_t qcuint;
typedef float qcfloat;


typedef struct QVM_GCC_PACKED {
  uint32_t offset; /* Offset in file of where data begins */
  uint32_t length; /* Length of section (how many of) */
} prog_section;


typedef struct QVM_GCC_PACKED {
  uint32_t version; /* Program version (6) */
  uint16_t crc16;
  uint16_t skip;
  /* */
  prog_section statements; /* prog_section_statement  */
  prog_section defs; /* prog_section_def */
  prog_section fields; /* prog_section_field */
  prog_section functions; /* prog_section_function */
  prog_section strings;
  prog_section globals;
  uint32_t entfield; /* number of entity fields */
} prog_header;


/*
 * Each paramater incerements by 3 since vector types hold
 * 3 components (x,y,z).
 */
#define OFS_NULL      (0)
#define OFS_RETURN    (1)
#define OFS_PARM0     (OFS_RETURN+3)
#define OFS_PARM1     (OFS_PARM0+3)
#define OFS_PARM2     (OFS_PARM1+3)
#define OFS_PARM3     (OFS_PARM2+3)
#define OFS_PARM4     (OFS_PARM3+3)
#define OFS_PARM5     (OFS_PARM4+3)
#define OFS_PARM6     (OFS_PARM5+3)
#define OFS_PARM7     (OFS_PARM6+3)
#define OFS_PARMTHIS  (OFS_PARM7+3)  // class id


typedef struct QVM_GCC_PACKED {
  uint16_t opcode;
  union {
    struct {
      union { int16_t s1; uint16_t u1; } o1;
      union { int16_t s1; uint16_t u1; } o2;
      union { int16_t s1; uint16_t u1; } o3;
    };
    struct {
      union { int16_t s; uint16_t u; } o[3];
    };
  };
} prog_section_statement;


typedef struct QVM_GCC_PACKED {
  /*
   * The types:
   * 0 = ev_void
   * 1 = ev_string
   * 2 = ev_float
   * 3 = ev_vector
   * 4 = ev_entity
   * 5 = ev_field
   * 6 = ev_function
   * 7 = ev_pointer
   * 8 = ev_integer
   */
  uint16_t type;
  uint16_t offset;
  uint32_t name;
} prog_section_both;

typedef prog_section_both prog_section_def;
typedef prog_section_both prog_section_field;


/* this is ORed to the type */
#define DEF_SAVEGLOBAL  (1<<15)
#define DEF_SHARED      (1<<15)
#define DEF_TYPEMASK    ((1<<8)-1)


typedef struct QVM_GCC_PACKED {
  int32_t entry;       /* in statement table for instructions */
  uint32_t firstlocal; /* First local in local table          */
  uint32_t locals;     /* Total ints of params + locals       */
  uint32_t name;       /* name of function in string table    */
  uint32_t file;       /* file of the source file             */
  int32_t nargs;       /* number of arguments                 */
  uint32_t argsize[8]; /* size of argument(0b), type(1b), fieldtype(2b) */
} prog_section_function;


/*
 * Darkplaces has (or will have) a 64 bit prog loader
 * where the 32 bit qc program is autoconverted on load.
 * Since we may want to support that as well, let's redefine
 * float and int here.
 */
typedef union QVM_GCC_PACKED {
  qcint inum;
  qcuint unum;
  qcint string;
  qcint function;
  qcint edict;
  qcfloat fnum;
  qcfloat vector[3];
  qcint ivector[3];
  qcuint ptr;
} qcany;

typedef char qcfloat_size_is_correct [sizeof(qcfloat) == 4 ? 1 : -1];
typedef char qcint_size_is_correct [sizeof(qcint) == 4 ? 1 : -1];


enum {
  VMERR_OK,
  VMERR_TEMPSTRING_ALLOC,
  /* */
  VMERR_END
};


#define VM_JUMPS_DEFAULT  (1000000)

/* execute-flags */
#define VMXF_DEFAULT  (0x0000)  /* default flags - nothing */
#define VMXF_TRACE    (0x0001)  /* trace: print statements before executing */

struct qc_program_s;

typedef int (*prog_builtin_fn) (struct qc_program_s *prog);

typedef struct {
  const char *name;
  prog_builtin_fn fn;
} qc_builtin;


typedef struct {
  qcint stmt;
  size_t localsp;
  prog_section_function *function;
} qc_exec_stack;


typedef struct qc_program_s {
  char *filename;
  /* */
  prog_section_statement *code;
  prog_section_def *defs;
  prog_section_def *fields;
  prog_section_function *functions;
  char *strings;
  qcint *globals;
  qcint *entitydata;
  qcbool *entitypool;
  /* */
  const char **function_stack;
  /* */
  uint16_t crc16;
  /* */
  size_t tempstring_start;
  size_t tempstring_at;
  /* */
  qcint vmerror;
  /* */
  const qc_builtin *builtins;
  size_t builtins_count;
  /* */
  /* size_t ip; */
  qcint entities;
  size_t entityfields;
  /* */
  qcint *localstack;
  qc_exec_stack *stack;
  size_t statement;
  /* */
  int argc; /* current arg count for debugging */
} qc_program;


extern qc_program *prog_load (const char *filename, qcbool ignoreversion);
extern void prog_delete(qc_program *prog);

extern qcbool prog_exec (qc_program *prog, prog_section_function *func, int64_t maxjumps);

extern const char *prog_getstring (qc_program *prog, qcint str);
extern prog_section_def *prog_entfield (qc_program *prog, qcint off);
extern prog_section_def *prog_getdef (qc_program *prog, qcint off);
extern qcany *prog_getedict (qc_program *prog, qcint e);
extern qcint prog_tempstring (qc_program *prog, const char *_str);


/* QC pointers are 32-bit unsigned numbers (qcuint)
 *  bit 31:
 *    0: this is pointer to field
 *    1: this is pointer to either string or global:
 *      bit 30:
 *       0: this is pointer to string
 *       1: this is pointer to global
 */
static inline qcuint QCFieldPtr (qcint ofs) { return ofs; }
static inline qcuint QCGlobalPtr (qcint ofs) { return (ofs|0xc0000000U); }
static inline qcuint QCStringPtr (qcint ofs) { return (ofs|0x80000000U); }

static inline qcbool isQCFieldPtr (qcuint ofs) { return ((ofs&0x80000000U) == 0); }
static inline qcbool isQCGlobalPtr (qcuint ofs) { return ((ofs&0xc0000000U) == 0xc0000000U); }
static inline qcbool isQCStringPtr (qcuint ofs) { return ((ofs&0xc0000000U) == 0x80000000U); }

#define QCP_MASK  (0x3fffffffU)


#endif
