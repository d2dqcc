/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013
 *     Dale Weiler
 *     Wolfgang Bumiller
 * Copyright (C) 2013  Ketmar Dark
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef _QC_VECTOR_H_
#define _QC_VECTOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>


/*
 * A flexible vector implementation: all vector pointers contain some
 * data about themselfs exactly - sizeof(vector_t) behind the pointer
 * this data is represented in the structure below. Doing this allows
 * us to use the array [] to access individual elements from the vector
 * opposed to using set/get methods.
 */
typedef struct {
  size_t allocated;
  size_t used;
  /* can be extended now! whoot */
} vector_t;


#define GMQCC_VEC_WILLGROW(X,Y) ( \
  ((!(X) || vec_meta(X)->used+(Y) >= vec_meta(X)->allocated)) ? \
    (void)_gmqcc_util_vec_grow(((void**)&(X)), (Y), sizeof(*(X))) : \
    (void)0 \
)


/* exposed interface */
#define vec_meta(A)        (((vector_t*)((void*)(A)))-1)
#define vec_free(A)        ((void)((A) ? (free((void *)vec_meta(A)), (A) = NULL) : 0))
#define vec_push(A,V)      (GMQCC_VEC_WILLGROW((A), 1), (A)[vec_meta(A)->used++] = (V))
#define vec_size(A)        ((A) ? vec_meta(A)->used : 0)
#define vec_add(A,N)       (GMQCC_VEC_WILLGROW((A), (N)), vec_meta(A)->used += (N), &(A)[vec_meta(A)->used-(N)])
#define vec_last(A)        ((A)[vec_meta(A)->used-1])
#define vec_pop(A)         ((void)(vec_meta(A)->used -= 1))
#define vec_shrinkto(A,N)  ((void)(vec_meta(A)->used = (N)))
#define vec_shrinkby(A,N)  ((void)(vec_meta(A)->used -= (N)))
#define vec_append(A,N,S)  ((void)(memcpy(vec_add((A), (N)), (S), (N) * sizeof(*(S)))))
#define vec_upload(X,Y,S)  ((void)(memcpy(vec_add((X), (S)*sizeof(*(Y))), (Y), (S)*sizeof(*(Y)))))
#define vec_remove(A,I,N)  ((void)(memmove((A)+(I), (A)+((I)+(N)), sizeof(*(A))*(vec_meta(A)->used-(I)-(N))), vec_meta(A)->used -= (N)))


/* hidden interface */
static inline void _gmqcc_util_vec_grow (void **a, size_t i, size_t s) {
  vector_t *d = vec_meta(*a);
  size_t m = 0;
  void *p = NULL;
  if (*a) {
    m = 2*d->allocated+i;
    p = realloc(d, s*m+sizeof(vector_t));
  } else {
    m = i+1;
    p = malloc(s*m+sizeof(vector_t));
    ((vector_t*)p)->used = 0;
  }
  *a = (vector_t*)p+1;
  vec_meta(*a)->allocated = m;
}


#ifdef __cplusplus
}
#endif
#endif
