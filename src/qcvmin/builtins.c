/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013  Wolfgang Bumiller, Dale Weiler
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
static int qc_print (qc_program *prog) {
  for (int f = 0; f < prog->argc; ++f) {
    const qcany *str = (const qcany *)(prog->globals+OFS_PARM0+3*f);
    const char *laststr = prog_getstring(prog, str->string);
    printf("%s", laststr);
  }
  return 0;
}


static int qc_writeln (qc_program *prog) {
  for (size_t f = 0; f < (size_t)prog->argc; ++f) {
    const qcany *str = (const qcany *)(prog->globals+OFS_PARM0+3*f);
    const char *laststr = prog_getstring(prog, str->string);
    printf("%s", laststr);
  }
  printf("\n");
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_error (qc_program *prog) {
  fprintf(stderr, "*** VM raised an error:\n");
  qc_print(prog);
  ++prog->vmerror;
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
//TODO: pass arguments
static int qc_spawn (qc_program *prog) {
  qcany ent;
  qcvm_check_argc(0);
  ent.edict = prog_spawn_entity(prog);
  qcvm_set_res(ent);
  return (ent.edict ? 0 : -1);
}


static int qc_kill (qc_program *prog) {
  qcany *ent;
  qcvm_check_argc(1);
  ent = qcvm_get_arg(0);
  prog_free_entity(prog, ent->edict);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_itos (qc_program *prog) {
  char buffer[512];
  qcany *num;
  qcany str;
  qcvm_check_argc(1);
  num = qcvm_get_arg(0);
  snprintf(buffer, sizeof(buffer), "%d", num->inum);
  str.string = prog_tempstring(prog, buffer);
  qcvm_set_res(str);
  return 0;
}


static int qc_ftos (qc_program *prog) {
  char buffer[512];
  qcany *num;
  qcany str;
  qcvm_check_argc(1);
  num = qcvm_get_arg(0);
  snprintf(buffer, sizeof(buffer), "%.15g", num->fnum);
  str.string = prog_tempstring(prog, buffer);
  qcvm_set_res(str);
  return 0;
}


static int qc_vtos (qc_program *prog) {
  char buffer[512];
  qcany *num;
  qcany str;
  qcvm_check_argc(1);
  num = qcvm_get_arg(0);
  snprintf(buffer, sizeof(buffer), "'%.15g %.15g %.15g'", num->vector[0], num->vector[1], num->vector[2]);
  str.string = prog_tempstring(prog, buffer);
  qcvm_set_res(str);
  return 0;
}


static int qc_etos (qc_program *prog) {
  char buffer[512];
  qcany *num;
  qcany str;
  qcvm_check_argc(1);
  num = qcvm_get_arg(0);
  snprintf(buffer, sizeof(buffer), "<e:%d>", num->inum);
  str.string = prog_tempstring(prog, buffer);
  qcvm_set_res(str);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_stoi (qc_program *prog) {
  qcany *str;
  qcany num;
  qcvm_check_argc(1);
  str = qcvm_get_arg(0);
  num.inum = (qcint)strtol(prog_getstring(prog, str->string), NULL, 10);
  qcvm_set_res(num);
  return 0;
}


static int qc_stof (qc_program *prog) {
  qcany *str;
  qcany num;
  qcvm_check_argc(1);
  str = qcvm_get_arg(0);
  num.fnum = (qcfloat)strtof(prog_getstring(prog, str->string), NULL);
  qcvm_set_res(num);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_strcat (qc_program *prog) {
  char *buffer;
  size_t len1, len2;
  qcany *str1, *str2;
  qcany out;
  const char *cstr1, *cstr2;
  qcvm_check_argc(2);
  str1 = qcvm_get_arg(0);
  str2 = qcvm_get_arg(1);
  cstr1 = prog_getstring(prog, str1->string);
  cstr2 = prog_getstring(prog, str2->string);
  len1 = strlen(cstr1);
  len2 = strlen(cstr2);
  buffer = (char *)malloc(len1+len2+1);
  if (len1 > 0) memcpy(buffer, cstr1, len1);
  memcpy(buffer+len1, cstr2, len2+1);
  out.string = prog_tempstring(prog, buffer);
  free(buffer);
  qcvm_set_res(out);
  return 0;
}


static int qc_strcmp (qc_program *prog) {
  qcany *str1, *str2;
  qcany out;
  const char *cstr1, *cstr2;
  if (prog->argc != 2 && prog->argc != 3) {
    fprintf(stderr, "ERROR: invalid number of arguments for strcmp/strncmp: %d, expected 2 or 3\n", prog->argc);
    return -1;
  }
  str1 = qcvm_get_arg(0);
  str2 = qcvm_get_arg(1);
  cstr1 = prog_getstring(prog, str1->string);
  cstr2 = prog_getstring(prog, str2->string);
  if (prog->argc == 3) {
    out.fnum = strncmp(cstr1, cstr2, qcvm_get_arg(2)->fnum);
  } else {
    out.fnum = strcmp(cstr1, cstr2);
  }
  qcvm_set_res(out);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_vlen (qc_program *prog) {
  qcany *vec, len;
  qcvm_check_argc(1);
  vec = qcvm_get_arg(0);
  len.fnum = sqrt(vec->vector[0]*vec->vector[0]+vec->vector[1]*vec->vector[1]+vec->vector[2]*vec->vector[2]);
  qcvm_set_res(len);
  return 0;
}


static int qc_normalize (qc_program *prog) {
  double len;
  qcany *vec;
  qcany out;
  qcvm_check_argc(1);
  vec = qcvm_get_arg(0);
  len = sqrt(vec->vector[0]*vec->vector[0]+vec->vector[1]*vec->vector[1]+vec->vector[2]*vec->vector[2]);
  len = (len ? 1.0f/len : 0.0f);
  out.vector[0] = len*vec->vector[0];
  out.vector[1] = len*vec->vector[1];
  out.vector[2] = len*vec->vector[2];
  qcvm_set_res(out);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_sqrt (qc_program *prog) {
  qcany *num, out;
  qcvm_check_argc(1);
  num = qcvm_get_arg(0);
  out.fnum = sqrt(num->fnum);
  qcvm_set_res(out);
  return 0;
}


static int qc_floor (qc_program *prog) {
  qcany *num, out;
  qcvm_check_argc(1);
  num = qcvm_get_arg(0);
  out.fnum = floor(num->fnum);
  qcvm_set_res(out);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int qc_frand (qc_program *prog) {
  qcany num;
  qcvm_check_argc(0);
  num.fnum = ((qcfloat)rand())/(((qcfloat)RAND_MAX)+1.0f);
  qcvm_set_res(num);
  return 0;
}


// irand(max)
static int qc_irand (qc_program *prog) {
  qcany *a0;
  qcany num;
  qcvm_check_argc(1);
  a0 = qcvm_get_arg(0);
  num.inum = (rand()%a0->inum);
  qcvm_set_res(num);
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static const qc_builtin qc_builtins[] = {
  {.name=NULL, .fn=NULL},
  {.name="print", .fn=&qc_print},
  {.name="writeln", .fn=&qc_writeln},

  {.name="error", .fn=&qc_error},

  {.name="spawn", .fn=&qc_spawn},
  {.name="kill", .fn=&qc_kill},

  {.name="ftos", .fn=&qc_ftos},
  {.name="itos", .fn=&qc_itos},
  {.name="vtos", .fn=&qc_vtos},
  {.name="etos", .fn=&qc_etos},

  {.name="stof", .fn=&qc_stof},
  {.name="stoi", .fn=&qc_stoi},

  {.name="strcat", .fn=&qc_strcat},
  {.name="strcmp", .fn=&qc_strcmp},

  {.name="vlen", .fn=&qc_vlen},
  {.name="normalize", .fn=&qc_normalize},

  {.name="sqrt", .fn=&qc_sqrt},
  {.name="floor", .fn=&qc_floor},

  {.name="frand", .fn=&qc_frand},
  {.name="irand", .fn=&qc_irand},
};
static size_t qc_builtins_count = sizeof(qc_builtins)/sizeof(qc_builtins[0]);
