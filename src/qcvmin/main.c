/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013  Wolfgang Bumiller, Dale Weiler
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "builtins.h"
#include "qcvector.h"
#include "qcexec.h"


#include "vmutil.c"
#include "vmexec.c"
#include "builtins.c"


/******************************************************************************/
static const char *arg0 = NULL;


static __attribute__((noreturn)) void usage (void) {
  printf("usage: %s [options] [parameters] file\n", arg0);
  printf(
    "options:\n"
    "  -h, --help         print this message\n"
    "parameters:\n"
    "  -vector <V>   pass a vector parameter to main()\n"
    "  -float  <f>   pass a float parameter to main()\n"
    "  -string <s>   pass a string parameter to main() \n"
  "");
  exit(EXIT_FAILURE);
}


/******************************************************************************/
typedef struct {
  int vtype;
  const char *value;
} qcvm_parameter;


static qcvm_parameter *main_params = NULL;


static void prog_main_setparams (qc_program *prog) {
  for (size_t i = 0; i < vec_size(main_params); ++i) {
    qcany *arg = qcvm_get_arg(i);
    arg->vector[0] = 0.0f;
    arg->vector[1] = 0.0f;
    arg->vector[2] = 0.0f;
    switch (main_params[i].vtype) {
      case ev_vector:
        sscanf(main_params[i].value, " %f %f %f ", &arg->vector[0], &arg->vector[1], &arg->vector[2]);
        break;
      case ev_float:
        arg->fnum = atof(main_params[i].value);
        break;
      case ev_string:
        arg->string = prog_tempstring(prog, main_params[i].value);
        break;
      default:
        fprintf(stderr, "FATAL: unhandled parameter type: %i\n", main_params[i].vtype);
        exit(EXIT_FAILURE);
        break;
    }
  }
}


/******************************************************************************/
int main (int argc, char *argv[]) {
  qcint fnmain = -1;
  qc_program *prog;
  const char *progsfile = NULL;
  int exitcode = EXIT_FAILURE;
  /* */
  srand(time(NULL));
  arg0 = argv[0];
  if (argc < 2) usage();
  /* */
  while (argc > 1) {
    const char *aa = argv[1];
    if (!strcmp(aa, "-h") || !strcmp(aa, "-help") || !strcmp(aa, "--help")) usage();
    if (!strcmp(aa, "--vector") || !strcmp(aa, "--string") || !strcmp(aa, "--float")) ++aa;
    if (!strcmp(aa, "-vector") || !strcmp(aa, "-string") || !strcmp(aa, "-float")) {
      qcvm_parameter p;
      switch (aa[1]) {
        case 'f': p.vtype = ev_float; break;
        case 's': p.vtype = ev_string; break;
        case 'v': p.vtype = ev_vector; break;
        default: abort();
      }
      --argc;
      ++argv;
      if (argc < 2) usage();
      p.value = argv[1];
      vec_push(main_params, p);
      --argc;
      ++argv;
      continue;
    }
    if (!strcmp(aa, "--")) { --argc; ++argv; break; }
    if (aa[0] != '-') {
      if (progsfile != NULL) {
        fprintf(stderr, "only 1 program file may be specified\n");
        usage();
      }
      progsfile = aa;
      --argc;
      ++argv;
    } else {
      fprintf(stderr, "unknown parameter: %s\n", aa);
      usage();
    }
  }
  /* */
  if (argc == 2 && progsfile == NULL) {
    progsfile = argv[1];
    --argc;
    ++argv;
  }
  /* */
  if (progsfile == NULL) {
    fprintf(stderr, "must specify a program to execute\n");
    usage();
    exit(EXIT_FAILURE);
  }
  /* */
  prog = prog_load(progsfile, qcfalse);
  if (prog == NULL) {
    fprintf(stderr, "FATAL: failed to load program '%s'\n", progsfile);
    exit(EXIT_FAILURE);
  }
  /* */
  prog->builtins = qc_builtins;
  prog->builtins_count = qc_builtins_count;
  prog_fixbuiltins(prog);
  /* look for main() */
  for (size_t i = 1; i < vec_size(prog->functions); ++i) {
    const char *name = prog_getstring(prog, prog->functions[i].name);
    if (!strcmp(name, "main")) fnmain = (qcint)i;
  }
  if (fnmain > 0) {
    prog_main_setparams(prog);
    if (prog_exec(prog, &prog->functions[fnmain], VM_JUMPS_DEFAULT)) exitcode = EXIT_SUCCESS;
  } else {
    fprintf(stderr, "FATAL: no main function found\n");
  }
  prog_delete(prog);
  exit(exitcode);
}
