/*
 * Minimalistic QuakeC virtual machine
 * Copyright (C) 2012, 2013  Wolfgang Bumiller, Dale Weiler
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
static __attribute__((format(printf,1,2))) void loaderror (const char *fmt, ...) {
  int err = errno;
  va_list ap;
  /* */
  fprintf(stderr, "FATAL: ");
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, ": %s\n", strerror(err));
}


qc_program *prog_load (const char *filename, qcbool skipversion) {
  qc_program *prog;
  prog_header header;
  FILE *file = fopen(filename, "rb");
  /* */
  if (!file) return NULL;
  if (fread(&header, sizeof(header), 1, file) != 1) {
    loaderror("failed to read header from '%s'", filename);
    fclose(file);
    return NULL;
  }
  /* */
  if (!skipversion && header.version != 6 && header.version != 0x80000006u &&
      header.version != 7 /*hack for fteqcc8*/) {
    loaderror("header says this is a version %i progs, we need version 6 or 7\n", header.version);
    fclose(file);
    return NULL;
  }
  /* */
  prog = (qc_program *)malloc(sizeof(qc_program));
  if (!prog) {
    fclose(file);
    fprintf(stderr, "failed to allocate program data\n");
    return NULL;
  }
  memset(prog, 0, sizeof(*prog));
  /* */
  prog->entityfields = header.entfield;
  prog->crc16 = header.crc16;
  /* */
  prog->filename = strdup(filename);
  if (!prog->filename) {
    loaderror("failed to store program name");
    goto error;
  }
#define read_data(hdrvar, progvar, reserved)  do { \
  if (fseek(file, header.hdrvar.offset, SEEK_SET) != 0) { \
    loaderror("seek failed"); \
    goto error; \
  } \
  if (fread(vec_add(prog->progvar, header.hdrvar.length+reserved), sizeof(*prog->progvar), header.hdrvar.length, file ) != header.hdrvar.length) { \
    loaderror("read failed"); \
    goto error; \
  } \
} while (0)
#define read_data1(x)     read_data(x, x, 0)
#define read_data2(x, y)  read_data(x, x, y)
  read_data(statements, code, 0);
  read_data1(defs);
  read_data1(fields);
  read_data1(functions);
  read_data1(strings);
  read_data2(globals, 2); /* reserve more in case a RETURN using with the global at "the end" exists */
#undef read_data2
#undef read_data1
#undef read_data
  fclose(file);
  /* add tempstring area */
  prog->tempstring_start = vec_size(prog->strings);
  prog->tempstring_at = vec_size(prog->strings);
  memset(vec_add(prog->strings, 256*1024), 0, 256*1024);
  /* spawn the world entity */
  vec_push(prog->entitypool, qctrue);
  memset(vec_add(prog->entitydata, prog->entityfields), 0, prog->entityfields * sizeof(prog->entitydata[0]));
  prog->entities = 1;
  /* */
  return prog;
error:
  if (prog->filename) free(prog->filename);
  vec_free(prog->code);
  vec_free(prog->defs);
  vec_free(prog->fields);
  vec_free(prog->functions);
  vec_free(prog->strings);
  vec_free(prog->globals);
  vec_free(prog->entitydata);
  vec_free(prog->entitypool);
  free(prog);
  return NULL;
}


void prog_delete (qc_program *prog) {
  if (prog->filename) free(prog->filename);
  vec_free(prog->code);
  vec_free(prog->defs);
  vec_free(prog->fields);
  vec_free(prog->functions);
  vec_free(prog->strings);
  vec_free(prog->globals);
  vec_free(prog->entitydata);
  vec_free(prog->entitypool);
  vec_free(prog->localstack);
  vec_free(prog->stack);
  free(prog);
}


/***********************************************************************
 * VM code
 */
const char *prog_getstring (qc_program *prog, qcint str) {
  /* cast for return required for C++ */
  if (str < 0 || str >= (qcint)vec_size(prog->strings)) return "\0<<<invalid string>>>";
  return prog->strings+str;
}


prog_section_def *prog_entfield (qc_program *prog, qcint off) {
  for (size_t i = 0; i < vec_size(prog->fields); ++i) {
    if (prog->fields[i].offset == off) return (prog->fields+i);
  }
  return NULL;
}


prog_section_def *prog_getdef (qc_program *prog, qcint off) {
  for (size_t i = 0; i < vec_size(prog->defs); ++i) {
    if (prog->defs[i].offset == off) return (prog->defs+i);
  }
  return NULL;
}


qcany *prog_getedict (qc_program *prog, qcint e) {
  if (e >= (qcint)vec_size(prog->entitypool)) {
    ++prog->vmerror;
    fprintf(stderr, "Accessing out of bounds edict %i\n", (int)e);
    e = 0;
  }
  return (qcany *)(prog->entitydata+(prog->entityfields*e));
}


qcint prog_spawn_entity (qc_program *prog) {
  char *data;
  qcint e;
  for (e = 0; e < (qcint)vec_size(prog->entitypool); ++e) {
    if (!prog->entitypool[e]) {
      data = (char *)(prog->entitydata+(prog->entityfields*e));
      memset(data, 0, prog->entityfields*sizeof(qcint));
      return e;
    }
  }
  vec_push(prog->entitypool, qctrue);
  ++prog->entities;
  data = (char *)vec_add(prog->entitydata, prog->entityfields);
  memset(data, 0, prog->entityfields*sizeof(qcint));
  return e;
}


void prog_free_entity (qc_program *prog, qcint e) {
  if (!e) {
    ++prog->vmerror;
    fprintf(stderr, "Trying to free world entity\n");
    return;
  }
  if (e >= (qcint)vec_size(prog->entitypool)) {
    ++prog->vmerror;
    fprintf(stderr, "Trying to free out of bounds entity\n");
    return;
  }
  if (!prog->entitypool[e]) {
    ++prog->vmerror;
    fprintf(stderr, "Double free on entity\n");
    return;
  }
  prog->entitypool[e] = qcfalse;
}


qcint prog_tempstring (qc_program *prog, const char *str) {
  size_t len = strlen(str);
  size_t at = prog->tempstring_at;
  /* when we reach the end we start over */
  if (at+len >= vec_size(prog->strings)) at = prog->tempstring_start;
  /* when it doesn't fit, reallocate */
  if (at+len >= vec_size(prog->strings)) {
    (void)vec_add(prog->strings, len+1);
    memcpy(prog->strings+at, str, len+1);
    return at;
  }
  /* when it fits, just copy */
  memcpy(prog->strings+at, str, len+1);
  prog->tempstring_at += len+1;
  return at;
}


void prog_fixbuiltins (qc_program *prog) {
  for (int f = 0; f < vec_size(prog->functions); ++f) {
    prog_section_function *fn = &prog->functions[f];
    //fprintf(stderr, "#%d: %d\n", f, fn->name);
    if (fn->entry < 0) {
      int found = 0;
      const char *name = prog_getstring(prog, prog->functions[f].name);
      if (!name[0]) continue; // assume that unnamed builtin is correctly numbered
      if (strcmp("abort", name) == 0) { fn->entry = -666; continue; }
      for (size_t c = 1; c < prog->builtins_count; ++c) {
        if (strcmp(prog->builtins[c].name, name) == 0) {
          found = 1;
          fn->entry = -((int)c);
          break;
        }
      }
      if (!found) {
        fprintf(stderr, "WARNING: unknown built-in '%s'!\n", prog_getstring(prog, fn->name));
      } else {
        //fprintf(stderr, "built-in '%s' fixed.\n", prog_getstring(prog, fn->name));
      }
    }
  }
}
