static __attribute__((unused)) const char *opnames[] = {
  "DONE",
  "MUL_F",
  "MUL_V",
  "MUL_FV",
  "MUL_VF",
  "DIV_F",
  "ADD_F",
  "ADD_V",
  "SUB_F",
  "SUB_V",

  "EQ_F",
  "EQ_V",
  "EQ_S",
  "EQ_E",
  "EQ_FNC",

  "NE_F",
  "NE_V",
  "NE_S",
  "NE_E",
  "NE_FNC",

  "LE_F",
  "GE_F",
  "LT_F",
  "GT_F",

  "LOAD_F",
  "LOAD_V",
  "LOAD_S",
  "LOAD_ENT",
  "LOAD_FLD",
  "LOAD_FNC",

  "ADDRESS",

  "STORE_F",
  "STORE_V",
  "STORE_S",
  "STORE_ENT",
  "STORE_FLD",
  "STORE_FNC",

  "STOREP_F",
  "STOREP_V",
  "STOREP_S",
  "STOREP_ENT",
  "STOREP_FLD",
  "STOREP_FNC",

  "RETURN",
  "NOT_F",
  "NOT_V",
  "NOT_S",
  "NOT_ENT",
  "NOT_FNC",
  "IF_I",
  "IFNOT_I",
  "CALL0",
  "CALL1",
  "CALL2",
  "CALL3",
  "CALL4",
  "CALL5",
  "CALL6",
  "CALL7",
  "CALL8",
  "STATE",
  "GOTO",
  "AND_F",
  "OR_F",

  "BITAND_F",
  "BITOR_F",

  // end of Quake opcodes

  "FETCH_GBL_F",
  "FETCH_GBL_V",
  "FETCH_GBL_S",
  "FETCH_GBL_E",
  "FETCH_GBL_FNC",

  "CONV_ITOF",
  "CONV_FTOI",
  "CP_ITOF",
  "CP_FTOI",

  "LOAD_I",
  "STOREP_I",
  "STOREP_IF",
  "STOREP_FI",

  "LE_I",
  "GE_I",
  "LT_I",
  "GT_I",
  "EQ_I",
  "EQ_IF",
  "EQ_FI",
  "NE_I",

  "BITSET",
  "BITCLR",

  "BOUNDCHECK",

  "STORE_I",
  "STORE_IF",
  "STORE_FI",

  "ADD_I",
  "ADD_FI",
  "ADD_IF",

  "SUB_I",
  "SUB_FI",
  "SUB_IF",

  "BITAND_I",
  "BITOR_I",

  "MUL_I",
  "DIV_I",

  "IFNOT_S",
  "IF_S",

  "XOR_I",
  "RSHIFT_I",
  "LSHIFT_I",

  "IF_F",
  "IFNOT_F",

  "LOADA_F",
  "LOADA_V",
  "LOADA_S",
  "LOADA_ENT",
  "LOADA_FLD",
  "LOADA_FNC",
  "LOADA_I",

  "GLOBALADDRESS",
  "POINTER_ADD",

  "STORE_P",
  //"LOAD_P",

  "LOADP_F",
  "LOADP_V",
  "LOADP_S",
  "LOADP_ENT",
  "LOADP_FLD",
  "LOADP_FNC",
  "LOADP_I",

  //-------------------------------------
  //string manipulation.
  "ADD_SF",
  "SUB_S",
  "STOREP_C",
  "LOADP_C",
  //-------------------------------------

  "BITAND_IF",
  "BITOR_IF",
  "BITAND_FI",
  "BITOR_FI",

  "BITSETP",
  "BITCLRP",

  "MULSTORE_F",
  "MULSTORE_V",
  "MULSTOREP_F",
  "MULSTOREP_V",

  "DIVSTORE_F",
  "DIVSTOREP_F",

  "ADDSTORE_F",
  "ADDSTORE_V",
  "ADDSTOREP_F",
  "ADDSTOREP_V",

  "SUBSTORE_F",
  "SUBSTORE_V",
  "SUBSTOREP_F",
  "SUBSTOREP_V",

  "LE_IF",
  "GE_IF",
  "LT_IF",
  "GT_IF",

  "LE_FI",
  "GE_FI",
  "LT_FI",
  "GT_FI",

  "MUL_IF",
  "MUL_FI",
  "MUL_VI",
  "MUL_IV",
  "DIV_IF",
  "DIV_FI",
  "AND_I",
  "OR_I",
  "AND_IF",
  "OR_IF",
  "AND_FI",
  "OR_FI",
  "NE_IF",
  "NE_FI",

  "CONV_FTOS",
  "CONV_ITOS",
  "CONV_VTOS",
  "CONV_ETOS",
};
