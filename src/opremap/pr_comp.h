//
  OP_DONE,  //0
  OP_MUL_F,
  OP_MUL_V,
  OP_MUL_FV,
  OP_MUL_VF,
  OP_DIV_F,
  OP_ADD_F,
  OP_ADD_V,
  OP_SUB_F,
  OP_SUB_V,

  OP_EQ_F,  //10
  OP_EQ_V,
  OP_EQ_S,
  OP_EQ_E,
  OP_EQ_FNC,

  OP_NE_F,
  OP_NE_V,
  OP_NE_S,
  OP_NE_E,
  OP_NE_FNC,

  OP_LE_F,  //20
  OP_GE_F,
  OP_LT_F,
  OP_GT_F,

  OP_LOAD_F,
  OP_LOAD_V,
  OP_LOAD_S,
  OP_LOAD_ENT,
  OP_LOAD_FLD,
  OP_LOAD_FNC,

  OP_ADDRESS, //30

  OP_STORE_F,
  OP_STORE_V,
  OP_STORE_S,
  OP_STORE_ENT,
  OP_STORE_FLD,
  OP_STORE_FNC,

  OP_STOREP_F,
  OP_STOREP_V,
  OP_STOREP_S,
  OP_STOREP_ENT,  //40
  OP_STOREP_FLD,
  OP_STOREP_FNC,

  OP_RETURN,
  OP_NOT_F,
  OP_NOT_V,
  OP_NOT_S,
  OP_NOT_ENT,
  OP_NOT_FNC,
  OP_IF_I,
  OP_IFNOT_I,   //50
  OP_CALL0,   //careful... hexen2 and q1 have different calling conventions
  OP_CALL1,   //remap hexen2 calls to OP_CALL2H
  OP_CALL2,
  OP_CALL3,
  OP_CALL4,
  OP_CALL5,
  OP_CALL6,
  OP_CALL7,
  OP_CALL8,
  OP_STATE,   //60
  OP_GOTO,
  OP_AND_F,
  OP_OR_F,

  OP_BITAND_F,
  OP_BITOR_F,


  //these following ones are Hexen 2 constants.

  OP_MULSTORE_F,
  OP_MULSTORE_V,
  OP_MULSTOREP_F,
  OP_MULSTOREP_V,

  OP_DIVSTORE_F,  //70
  OP_DIVSTOREP_F,

  OP_ADDSTORE_F,
  OP_ADDSTORE_V,
  OP_ADDSTOREP_F,
  OP_ADDSTOREP_V,

  OP_SUBSTORE_F,
  OP_SUBSTORE_V,
  OP_SUBSTOREP_F,
  OP_SUBSTOREP_V,

  OP_FETCH_GBL_F, //80
  OP_FETCH_GBL_V,
  OP_FETCH_GBL_S,
  OP_FETCH_GBL_E,
  OP_FETCH_GBL_FNC,

  OP_CSTATE,
  OP_CWSTATE,

  OP_THINKTIME,

  OP_BITSET,
  OP_BITSETP,
  OP_BITCLR,    //90
  OP_BITCLRP,

  OP_RAND0,
  OP_RAND1,
  OP_RAND2,
  OP_RANDV0,
  OP_RANDV1,
  OP_RANDV2,

  OP_SWITCH_F,
  OP_SWITCH_V,
  OP_SWITCH_S,  //100
  OP_SWITCH_E,
  OP_SWITCH_FNC,

  OP_CASE,
  OP_CASERANGE,





  //the rest are added
  //mostly they are various different ways of adding two vars with conversions.

  OP_CALL1H,
  OP_CALL2H,
  OP_CALL3H,
  OP_CALL4H,
  OP_CALL5H,
  OP_CALL6H,    //110
  OP_CALL7H,
  OP_CALL8H,


  OP_STORE_I,
  OP_STORE_IF,
  OP_STORE_FI,

  OP_ADD_I,
  OP_ADD_FI,
  OP_ADD_IF,

  OP_SUB_I,
  OP_SUB_FI,    //120
  OP_SUB_IF,

  OP_CONV_ITOF,
  OP_CONV_FTOI,
  OP_CP_ITOF,
  OP_CP_FTOI,
  OP_LOAD_I,
  OP_STOREP_I,
  OP_STOREP_IF,
  OP_STOREP_FI,

  OP_BITAND_I,  //130
  OP_BITOR_I,

  OP_MUL_I,
  OP_DIV_I,
  OP_EQ_I,
  OP_NE_I,

  OP_IFNOT_S,
  OP_IF_S,

  OP_NOT_I,

  OP_DIV_VF,

  OP_XOR_I,   //140
  OP_RSHIFT_I,
  OP_LSHIFT_I,

  OP_GLOBALADDRESS,
  OP_POINTER_ADD, //32 bit pointers

  OP_LOADA_F,
  OP_LOADA_V,
  OP_LOADA_S,
  OP_LOADA_ENT,
  OP_LOADA_FLD,
  OP_LOADA_FNC, //150
  OP_LOADA_I,

  OP_STORE_P, //152... erm.. wait...
  OP_LOAD_P,

  OP_LOADP_F,
  OP_LOADP_V,
  OP_LOADP_S,
  OP_LOADP_ENT,
  OP_LOADP_FLD,
  OP_LOADP_FNC,
  OP_LOADP_I,   //160

  OP_LE_I,
  OP_GE_I,
  OP_LT_I,
  OP_GT_I,

  OP_LE_IF,
  OP_GE_IF,
  OP_LT_IF,
  OP_GT_IF,

  OP_LE_FI,
  OP_GE_FI,   //170
  OP_LT_FI,
  OP_GT_FI,

  OP_EQ_IF,
  OP_EQ_FI,

  //-------------------------------------
  //string manipulation.
  OP_ADD_SF,  //(char*)c = (char*)a + (float)b
  OP_SUB_S, //(float)c = (char*)a - (char*)b
  OP_STOREP_C,//(float)c = *(char*)b = (float)a
  OP_LOADP_C, //(float)c = *(char*)
  //-------------------------------------


  OP_MUL_IF,
  OP_MUL_FI,    //180
  OP_MUL_VI,
  OP_MUL_IV,
  OP_DIV_IF,
  OP_DIV_FI,
  OP_BITAND_IF,
  OP_BITOR_IF,
  OP_BITAND_FI,
  OP_BITOR_FI,
  OP_AND_I,
  OP_OR_I,    //190
  OP_AND_IF,
  OP_OR_IF,
  OP_AND_FI,
  OP_OR_FI,
  OP_NE_IF,
  OP_NE_FI,

//erm... FTEQCC doesn't make use of these... These are for DP.
  OP_GSTOREP_I,
  OP_GSTOREP_F,
  OP_GSTOREP_ENT,
  OP_GSTOREP_FLD,   // integers   //200
  OP_GSTOREP_S,
  OP_GSTOREP_FNC,   // pointers
  OP_GSTOREP_V,
  OP_GADDRESS,
  OP_GLOAD_I,
  OP_GLOAD_F,
  OP_GLOAD_FLD,
  OP_GLOAD_ENT,
  OP_GLOAD_S,
  OP_GLOAD_FNC,   //210
  OP_BOUNDCHECK,

//back to ones that we do use.
  OP_STOREP_P,
  OP_PUSH,  //push 4octets onto the local-stack (which is ALWAYS poped on function return). Returns a pointer.
  OP_POP,   //pop those ones that were pushed (don't over do it). Needs assembler.

  OP_SWITCH_I,//hmm.
  OP_GLOAD_V,

  OP_IF_F,
  OP_IFNOT_F,

  OP_NUMREALOPS,

  /*
  These ops are emulated out, always, and are only present in the compiler.
  */

  OP_BITSET_I,  //220
  OP_BITSETP_I,

  OP_MULSTORE_I,
  OP_DIVSTORE_I,
  OP_ADDSTORE_I,
  OP_SUBSTORE_I,
  OP_MULSTOREP_I,
  OP_DIVSTOREP_I,
  OP_ADDSTOREP_I,
  OP_SUBSTOREP_I,

  OP_MULSTORE_IF, //230
  OP_MULSTOREP_IF,
  OP_DIVSTORE_IF,
  OP_DIVSTOREP_IF,
  OP_ADDSTORE_IF,
  OP_ADDSTOREP_IF,
  OP_SUBSTORE_IF,
  OP_SUBSTOREP_IF,

  OP_MULSTORE_FI,
  OP_MULSTOREP_FI,
  OP_DIVSTORE_FI,   //240
  OP_DIVSTOREP_FI,
  OP_ADDSTORE_FI,
  OP_ADDSTOREP_FI,
  OP_SUBSTORE_FI,
  OP_SUBSTOREP_FI,

  OP_CONV_FTOS,
  OP_CONV_ITOS,
  OP_CONV_VTOS,
  OP_CONV_ETOS,

  OP_NUMOPS     //246
};


@@
 {6, "<DONE>", "DONE", -1, ASSOC_LEFT,      &type_void, &type_void, &type_void},

 {6, "*", "MUL_F",      3, ASSOC_LEFT,        &type_float, &type_float, &type_float},
 {6, "*", "MUL_V",      3, ASSOC_LEFT,        &type_vector, &type_vector, &type_float},
 {6, "*", "MUL_FV",     3, ASSOC_LEFT,        &type_float, &type_vector, &type_vector},
 {6, "*", "MUL_VF",     3, ASSOC_LEFT,        &type_vector, &type_float, &type_vector},

 {6, "/", "DIV_F",      3, ASSOC_LEFT,        &type_float, &type_float, &type_float},

 {6, "+", "ADD_F",      4, ASSOC_LEFT,        &type_float, &type_float, &type_float},
 {6, "+", "ADD_V",      4, ASSOC_LEFT,        &type_vector, &type_vector, &type_vector},

 {6, "-", "SUB_F",      4, ASSOC_LEFT,        &type_float, &type_float, &type_float},
 {6, "-", "SUB_V",      4, ASSOC_LEFT,        &type_vector, &type_vector, &type_vector},

 {6, "==", "EQ_F",      5, ASSOC_LEFT,        &type_float, &type_float, &type_float},
 {6, "==", "EQ_V",      5, ASSOC_LEFT,        &type_vector, &type_vector, &type_float},
 {6, "==", "EQ_S",      5, ASSOC_LEFT,        &type_string, &type_string, &type_float},
 {6, "==", "EQ_E",      5, ASSOC_LEFT,        &type_entity, &type_entity, &type_float},
 {6, "==", "EQ_FNC",    5, ASSOC_LEFT,        &type_function, &type_function, &type_float},

 {6, "!=", "NE_F",      5, ASSOC_LEFT,        &type_float, &type_float, &type_float},
 {6, "!=", "NE_V",      5, ASSOC_LEFT,        &type_vector, &type_vector, &type_float},
 {6, "!=", "NE_S",      5, ASSOC_LEFT,        &type_string, &type_string, &type_float},
 {6, "!=", "NE_E",      5, ASSOC_LEFT,        &type_entity, &type_entity, &type_float},
 {6, "!=", "NE_FNC",    5, ASSOC_LEFT,        &type_function, &type_function, &type_float},

 {6, "<=", "LE",      5, ASSOC_LEFT,          &type_float, &type_float, &type_float},
 {6, ">=", "GE",      5, ASSOC_LEFT,          &type_float, &type_float, &type_float},
 {6, "<", "LT",       5, ASSOC_LEFT,          &type_float, &type_float, &type_float},
 {6, ">", "GT",       5, ASSOC_LEFT,          &type_float, &type_float, &type_float},

 {6, ".", "INDIRECT_F",   1, ASSOC_LEFT,      &type_entity, &type_field, &type_float},
 {6, ".", "INDIRECT_V",   1, ASSOC_LEFT,      &type_entity, &type_field, &type_vector},
 {6, ".", "INDIRECT_S",   1, ASSOC_LEFT,      &type_entity, &type_field, &type_string},
 {6, ".", "INDIRECT_E",   1, ASSOC_LEFT,      &type_entity, &type_field, &type_entity},
 {6, ".", "INDIRECT_FI",  1, ASSOC_LEFT,      &type_entity, &type_field, &type_field},
 {6, ".", "INDIRECT_FU",  1, ASSOC_LEFT,      &type_entity, &type_field, &type_function},

 {6, ".", "ADDRESS",    1, ASSOC_LEFT,        &type_entity, &type_field, &type_pointer},

 {6, "=", "STORE_F",    6, ASSOC_RIGHT,       &type_float, &type_float, &type_float},
 {6, "=", "STORE_V",    6, ASSOC_RIGHT,       &type_vector, &type_vector, &type_vector},
 {6, "=", "STORE_S",    6, ASSOC_RIGHT,       &type_string, &type_string, &type_string},
 {6, "=", "STORE_ENT",    6, ASSOC_RIGHT,       &type_entity, &type_entity, &type_entity},
 {6, "=", "STORE_FLD",    6, ASSOC_RIGHT,       &type_field, &type_field, &type_field},
 {6, "=", "STORE_FNC",    6, ASSOC_RIGHT,       &type_function, &type_function, &type_function},

 {6, "=", "STOREP_F",   6, ASSOC_RIGHT,       &type_pointer, &type_float, &type_float},
 {6, "=", "STOREP_V",   6, ASSOC_RIGHT,       &type_pointer, &type_vector, &type_vector},
 {6, "=", "STOREP_S",   6, ASSOC_RIGHT,       &type_pointer, &type_string, &type_string},
 {6, "=", "STOREP_ENT",   6, ASSOC_RIGHT,     &type_pointer, &type_entity, &type_entity},
 {6, "=", "STOREP_FLD",   6, ASSOC_RIGHT,     &type_pointer, &type_field, &type_field},
 {6, "=", "STOREP_FNC",   6, ASSOC_RIGHT,     &type_pointer, &type_function, &type_function},

 {6, "<RETURN>", "RETURN",  -1, ASSOC_LEFT,   &type_float, &type_void, &type_void},

 {6, "!", "NOT_F",      -1, ASSOC_LEFT,       &type_float, &type_void, &type_float},
 {6, "!", "NOT_V",      -1, ASSOC_LEFT,       &type_vector, &type_void, &type_float},
 {6, "!", "NOT_S",      -1, ASSOC_LEFT,       &type_vector, &type_void, &type_float},
 {6, "!", "NOT_ENT",    -1, ASSOC_LEFT,       &type_entity, &type_void, &type_float},
 {6, "!", "NOT_FNC",    -1, ASSOC_LEFT,       &type_function, &type_void, &type_float},

  {6, "<IF>", "IF",     -1, ASSOC_RIGHT,        &type_float, NULL, &type_void},
  {6, "<IFNOT>", "IFNOT", -1, ASSOC_RIGHT,      &type_float, NULL, &type_void},

// calls returns REG_RETURN
 {6, "<CALL0>", "CALL0",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL1>", "CALL1",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL2>", "CALL2",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL3>", "CALL3",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL4>", "CALL4",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL5>", "CALL5",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL6>", "CALL6",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL7>", "CALL7",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},
 {6, "<CALL8>", "CALL8",  -1, ASSOC_LEFT,     &type_function, &type_void, &type_void},

 {6, "<STATE>", "STATE",  -1, ASSOC_LEFT,     &type_float, &type_float, &type_void},

 {6, "<GOTO>", "GOTO",    -1, ASSOC_RIGHT,      NULL, &type_void, &type_void},

 {6, "&&", "AND",     7, ASSOC_LEFT,          &type_float,  &type_float, &type_float},
 {6, "||", "OR",      7, ASSOC_LEFT,          &type_float,  &type_float, &type_float},

 {6, "&", "BITAND",     3, ASSOC_LEFT,        &type_float, &type_float, &type_float},
 {6, "|", "BITOR",      3, ASSOC_LEFT,        &type_float, &type_float, &type_float},

 //version 6 are in normal progs.



//these are hexen2
 {7, "*=", "MULSTORE_F",  6, ASSOC_RIGHT_RESULT,        &type_float, &type_float, &type_float},
 {7, "*=", "MULSTORE_V",  6, ASSOC_RIGHT_RESULT,        &type_vector, &type_float, &type_vector},
 {7, "*=", "MULSTOREP_F", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_float, &type_float},
 {7, "*=", "MULSTOREP_V", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_float, &type_vector},

 {7, "/=", "DIVSTORE_F",  6, ASSOC_RIGHT_RESULT,        &type_float, &type_float, &type_float},
 {7, "/=", "DIVSTOREP_F", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_float, &type_float},

 {7, "+=", "ADDSTORE_F",  6, ASSOC_RIGHT_RESULT,        &type_float, &type_float, &type_float},
 {7, "+=", "ADDSTORE_V",  6, ASSOC_RIGHT_RESULT,        &type_vector, &type_vector, &type_vector},
 {7, "+=", "ADDSTOREP_F", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_float, &type_float},
 {7, "+=", "ADDSTOREP_V", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_vector, &type_vector},

 {7, "-=", "SUBSTORE_F",  6, ASSOC_RIGHT_RESULT,        &type_float, &type_float, &type_float},
 {7, "-=", "SUBSTORE_V",  6, ASSOC_RIGHT_RESULT,        &type_vector, &type_vector, &type_vector},
 {7, "-=", "SUBSTOREP_F", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_float, &type_float},
 {7, "-=", "SUBSTOREP_V", 6, ASSOC_RIGHT_RESULT,        &type_pointer, &type_vector, &type_vector},

 {7, "<FETCH_GBL_F>", "FETCH_GBL_F",    -1, ASSOC_LEFT, &type_float, &type_float, &type_float},
 {7, "<FETCH_GBL_V>", "FETCH_GBL_V",    -1, ASSOC_LEFT, &type_vector, &type_float, &type_vector},
 {7, "<FETCH_GBL_S>", "FETCH_GBL_S",    -1, ASSOC_LEFT, &type_string, &type_float, &type_string},
 {7, "<FETCH_GBL_E>", "FETCH_GBL_E",    -1, ASSOC_LEFT, &type_entity, &type_float, &type_entity},
 {7, "<FETCH_GBL_FNC>", "FETCH_GBL_FNC",  -1, ASSOC_LEFT, &type_function, &type_float, &type_function},

 {7, "<CSTATE>", "CSTATE",          -1, ASSOC_LEFT, &type_float, &type_float, &type_void},

 {7, "<CWSTATE>", "CWSTATE",        -1, ASSOC_LEFT, &type_float, &type_float, &type_void},

 {7, "<THINKTIME>", "THINKTIME",      -1, ASSOC_LEFT, &type_entity, &type_float, &type_void},

 {7, "|=", "BITSET_F",            6,  ASSOC_RIGHT,  &type_float, &type_float, &type_float},
 {7, "|=", "BITSETP_F",           6,  ASSOC_RIGHT,  &type_pointer, &type_float, &type_float},
 {7, "&~=", "BITCLR_F",           6,  ASSOC_RIGHT,  &type_float, &type_float, &type_float},
 {7, "&~=", "BITCLRP_F",          6,  ASSOC_RIGHT,  &type_pointer, &type_float, &type_float},

 {7, "<RAND0>", "RAND0",          -1, ASSOC_LEFT, &type_void, &type_void, &type_float},
 {7, "<RAND1>", "RAND1",          -1, ASSOC_LEFT, &type_float, &type_void, &type_float},
 {7, "<RAND2>", "RAND2",          -1, ASSOC_LEFT, &type_float, &type_float, &type_float},
 {7, "<RANDV0>", "RANDV0",          -1, ASSOC_LEFT, &type_void, &type_void, &type_vector},
 {7, "<RANDV1>", "RANDV1",          -1, ASSOC_LEFT, &type_vector, &type_void, &type_vector},
 {7, "<RANDV2>", "RANDV2",          -1, ASSOC_LEFT, &type_vector, &type_vector, &type_vector},

 {7, "<SWITCH_F>", "SWITCH_F",        -1, ASSOC_LEFT, &type_void, NULL, &type_void},
 {7, "<SWITCH_V>", "SWITCH_V",        -1, ASSOC_LEFT, &type_void, NULL, &type_void},
 {7, "<SWITCH_S>", "SWITCH_S",        -1, ASSOC_LEFT, &type_void, NULL, &type_void},
 {7, "<SWITCH_E>", "SWITCH_E",        -1, ASSOC_LEFT, &type_void, NULL, &type_void},
 {7, "<SWITCH_FNC>", "SWITCH_FNC",      -1, ASSOC_LEFT, &type_void, NULL, &type_void},

 {7, "<CASE>", "CASE",            -1, ASSOC_LEFT, &type_void, NULL, &type_void},
 {7, "<CASERANGE>", "CASERANGE",      -1, ASSOC_LEFT, &type_void, &type_void, NULL},


//Later are additions by DMW.

 {7, "<CALL1H>", "CALL1H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_void},
 {7, "<CALL2H>", "CALL2H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},
 {7, "<CALL3H>", "CALL3H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},
 {7, "<CALL4H>", "CALL4H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},
 {7, "<CALL5H>", "CALL5H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},
 {7, "<CALL6H>", "CALL6H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},
 {7, "<CALL7H>", "CALL7H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},
 {7, "<CALL8H>", "CALL8H",  -1, ASSOC_LEFT,     &type_function, &type_vector, &type_vector},

 {7, "=", "STORE_I", 6, ASSOC_RIGHT,        &type_integer, &type_integer, &type_integer},
 {7, "=", "STORE_IF", 6, ASSOC_RIGHT,     &type_float, &type_integer, &type_integer},
 {7, "=", "STORE_FI", 6, ASSOC_RIGHT,     &type_integer, &type_float, &type_float},

 {7, "+", "ADD_I", 4, ASSOC_LEFT,       &type_integer, &type_integer, &type_integer},
 {7, "+", "ADD_FI", 4, ASSOC_LEFT,        &type_float, &type_integer, &type_float},
 {7, "+", "ADD_IF", 4, ASSOC_LEFT,        &type_integer, &type_float, &type_float},

 {7, "-", "SUB_I", 4, ASSOC_LEFT,       &type_integer, &type_integer, &type_integer},
 {7, "-", "SUB_FI", 4, ASSOC_LEFT,        &type_float, &type_integer, &type_float},
 {7, "-", "SUB_IF", 4, ASSOC_LEFT,        &type_integer, &type_float, &type_float},

 {7, "<CIF>", "C_ITOF", -1, ASSOC_LEFT,       &type_integer, &type_void, &type_float},
 {7, "<CFI>", "C_FTOI", -1, ASSOC_LEFT,       &type_float, &type_void, &type_integer},
 {7, "<CPIF>", "CP_ITOF", -1, ASSOC_LEFT,     &type_pointer, &type_integer, &type_float},
 {7, "<CPFI>", "CP_FTOI", -1, ASSOC_LEFT,     &type_pointer, &type_float, &type_integer},

 {7, ".", "INDIRECT", 1, ASSOC_LEFT,        &type_entity, &type_field, &type_integer},
 {7, "=", "STOREP_I", 6, ASSOC_RIGHT,       &type_pointer,  &type_integer, &type_integer},
 {7, "=", "STOREP_IF", 6, ASSOC_RIGHT,        &type_pointer,  &type_float, &type_integer},
 {7, "=", "STOREP_FI", 6, ASSOC_RIGHT,        &type_pointer,  &type_integer, &type_float},

 {7, "&", "BITAND_I", 3, ASSOC_LEFT,        &type_integer,  &type_integer, &type_integer},
 {7, "|", "BITOR_I", 3, ASSOC_LEFT,       &type_integer,  &type_integer, &type_integer},

 {7, "*", "MUL_I", 3, ASSOC_LEFT,       &type_integer,  &type_integer, &type_integer},
 {7, "/", "DIV_I", 3, ASSOC_LEFT,       &type_integer,  &type_integer, &type_integer},
 {7, "==", "EQ_I", 5, ASSOC_LEFT,       &type_integer,  &type_integer, &type_integer},
 {7, "!=", "NE_I", 5, ASSOC_LEFT,       &type_integer,  &type_integer, &type_integer},

 {7, "<IFNOTS>", "IFNOTS", -1, ASSOC_RIGHT,   &type_string, NULL, &type_void},
 {7, "<IFS>", "IFS", -1, ASSOC_RIGHT,       &type_string, NULL, &type_void},

 {7, "!", "NOT_I", -1, ASSOC_LEFT,        &type_integer,  &type_void, &type_integer},

 {7, "/", "DIV_VF", 3, ASSOC_LEFT,        &type_vector, &type_float, &type_float},

 {7, "^", "XOR_I", 3, ASSOC_LEFT,       &type_integer,  &type_integer, &type_integer},
 {7, ">>", "RSHIFT_I", 3, ASSOC_LEFT,     &type_integer,  &type_integer, &type_integer},
 {7, "<<", "LSHIFT_I", 3, ASSOC_LEFT,     &type_integer,  &type_integer, &type_integer},

                    //var,    offset      return
 {7, "<ARRAY>", "GET_POINTER", -1, ASSOC_LEFT,  &type_float,    &type_integer, &type_pointer},
 {7, "<ARRAY>", "MUL4ADD_I", -1, ASSOC_LEFT,    &type_pointer,  &type_integer, &type_pointer},

 {7, "=", "LOADA_F", 6, ASSOC_LEFT,     &type_float,  &type_integer, &type_float},
 {7, "=", "LOADA_V", 6, ASSOC_LEFT,     &type_vector, &type_integer, &type_vector},
 {7, "=", "LOADA_S", 6, ASSOC_LEFT,     &type_string, &type_integer, &type_string},
 {7, "=", "LOADA_ENT", 6, ASSOC_LEFT,   &type_entity, &type_integer, &type_entity},
 {7, "=", "LOADA_FLD", 6, ASSOC_LEFT,   &type_field,  &type_integer, &type_field},
 {7, "=", "LOADA_FNC", 6, ASSOC_LEFT,   &type_function, &type_integer, &type_function},
 {7, "=", "LOADA_I", 6, ASSOC_LEFT,     &type_integer,  &type_integer, &type_integer},

 {7, "=", "STORE_P", 6, ASSOC_RIGHT,      &type_pointer,  &type_pointer, &type_void},
 {7, ".", "INDIRECT_P", 1, ASSOC_LEFT,      &type_entity, &type_field, &type_pointer},

 {7, "=", "LOADP_F", 6, ASSOC_LEFT,     &type_pointer,  &type_integer, &type_float},
 {7, "=", "LOADP_V", 6, ASSOC_LEFT,     &type_pointer,  &type_integer, &type_vector},
 {7, "=", "LOADP_S", 6, ASSOC_LEFT,     &type_pointer,  &type_integer, &type_string},
 {7, "=", "LOADP_ENT", 6, ASSOC_LEFT,   &type_pointer,  &type_integer, &type_entity},
 {7, "=", "LOADP_FLD", 6, ASSOC_LEFT,   &type_pointer,  &type_integer, &type_field},
 {7, "=", "LOADP_FNC", 6, ASSOC_LEFT,   &type_pointer,  &type_integer, &type_function},
 {7, "=", "LOADP_I", 6, ASSOC_LEFT,     &type_pointer,  &type_integer, &type_integer},


 {7, "<=", "LE_I", 5, ASSOC_LEFT,         &type_integer, &type_integer, &type_integer},
 {7, ">=", "GE_I", 5, ASSOC_LEFT,         &type_integer, &type_integer, &type_integer},
 {7, "<", "LT_I", 5, ASSOC_LEFT,          &type_integer, &type_integer, &type_integer},
 {7, ">", "GT_I", 5, ASSOC_LEFT,          &type_integer, &type_integer, &type_integer},

 {7, "<=", "LE_IF", 5, ASSOC_LEFT,          &type_integer, &type_float, &type_integer},
 {7, ">=", "GE_IF", 5, ASSOC_LEFT,          &type_integer, &type_float, &type_integer},
 {7, "<", "LT_IF", 5, ASSOC_LEFT,         &type_integer, &type_float, &type_integer},
 {7, ">", "GT_IF", 5, ASSOC_LEFT,         &type_integer, &type_float, &type_integer},

 {7, "<=", "LE_FI", 5, ASSOC_LEFT,          &type_float, &type_integer, &type_integer},
 {7, ">=", "GE_FI", 5, ASSOC_LEFT,          &type_float, &type_integer, &type_integer},
 {7, "<", "LT_FI", 5, ASSOC_LEFT,         &type_float, &type_integer, &type_integer},
 {7, ">", "GT_FI", 5, ASSOC_LEFT,         &type_float, &type_integer, &type_integer},

 {7, "==", "EQ_IF", 5, ASSOC_LEFT,        &type_integer,  &type_float, &type_integer},
 {7, "==", "EQ_FI", 5, ASSOC_LEFT,        &type_float,  &type_integer, &type_float},

  //-------------------------------------
  //string manipulation.
 {7, "+", "ADD_SF", 4, ASSOC_LEFT,        &type_string, &type_float, &type_string},
 {7, "-", "SUB_S",  4, ASSOC_LEFT,        &type_string, &type_string, &type_float},
 {7, "<STOREP_C>", "STOREP_C",  1, ASSOC_RIGHT, &type_float, &type_string, &type_void},
 {7, "<LOADP_C>", "LOADP_C",  1, ASSOC_LEFT,  &type_string, &type_float, &type_float},
  //-------------------------------------



{7, "*", "MUL_IF", 5, ASSOC_LEFT,       &type_integer,  &type_float,  &type_integer},
{7, "*", "MUL_FI", 5, ASSOC_LEFT,       &type_float,  &type_integer,  &type_float},
{7, "*", "MUL_VI", 5, ASSOC_LEFT,       &type_vector, &type_integer,  &type_vector},
{7, "*", "MUL_IV", 5, ASSOC_LEFT,       &type_integer,  &type_vector, &type_vector},

{7, "/", "DIV_IF", 5, ASSOC_LEFT,       &type_integer,  &type_float,  &type_integer},
{7, "/", "DIV_FI", 5, ASSOC_LEFT,       &type_float,  &type_integer,  &type_float},

{7, "&", "BITAND_IF", 5, ASSOC_LEFT,      &type_integer,  &type_float,  &type_integer},
{7, "|", "BITOR_IF", 5, ASSOC_LEFT,       &type_integer,  &type_float,  &type_integer},
{7, "&", "BITAND_FI", 5, ASSOC_LEFT,      &type_float,  &type_integer,  &type_float},
{7, "|", "BITOR_FI", 5, ASSOC_LEFT,       &type_float,  &type_integer,  &type_float},

{7, "&&", "AND_I", 7, ASSOC_LEFT,       &type_integer,  &type_integer,  &type_integer},
{7, "||", "OR_I", 7, ASSOC_LEFT,        &type_integer,  &type_integer,  &type_integer},
{7, "&&", "AND_IF", 7, ASSOC_LEFT,        &type_integer,  &type_float,  &type_integer},
{7, "||", "OR_IF", 7, ASSOC_LEFT,       &type_integer,  &type_float,  &type_integer},
{7, "&&", "AND_FI", 7, ASSOC_LEFT,        &type_float,  &type_integer,  &type_integer},
{7, "||", "OR_FI", 7, ASSOC_LEFT,       &type_float,  &type_integer,  &type_integer},
{7, "!=", "NE_IF", 5, ASSOC_LEFT,       &type_integer,  &type_float,  &type_integer},
{7, "!=", "NE_FI", 5, ASSOC_LEFT,       &type_float,  &type_float,  &type_integer},






{7, "<>", "GSTOREP_I", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},
{7, "<>", "GSTOREP_F", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},
{7, "<>", "GSTOREP_ENT", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},
{7, "<>", "GSTOREP_FLD", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},
{7, "<>", "GSTOREP_S", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},
{7, "<>", "GSTOREP_FNC", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},
{7, "<>", "GSTOREP_V", -1, ASSOC_LEFT,      &type_float,  &type_float,  &type_float},

{7, "<>", "GADDRESS", -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},

{7, "<>", "GLOAD_I",    -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},
{7, "<>", "GLOAD_F",    -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},
{7, "<>", "GLOAD_FLD",  -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},
{7, "<>", "GLOAD_ENT",  -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},
{7, "<>", "GLOAD_S",    -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},
{7, "<>", "GLOAD_FNC",  -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},

{7, "<>", "BOUNDCHECK", -1, ASSOC_LEFT,       &type_integer,  NULL, NULL},

{7, "=",  "STOREP_P",   6,  ASSOC_RIGHT,        &type_pointer,  &type_pointer,  &type_void},
{7, "<PUSH>", "PUSH",   -1, ASSOC_RIGHT,      &type_float,  &type_void,   &type_pointer},
{7, "<POP>",  "POP",    -1, ASSOC_RIGHT,      &type_float,  &type_void,   &type_void},

{7, "<SWITCH_I>", "SWITCH_I",       -1, ASSOC_LEFT, &type_void, NULL, &type_void},
{7, "<>", "GLOAD_S",    -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},

{6, "<IF_F>", "IF_F",   -1, ASSOC_RIGHT,        &type_float, NULL, &type_void},
{6, "<IFNOT_F>","IFNOT_F",  -1, ASSOC_RIGHT,        &type_float, NULL, &type_void},

/* emulated ops begin here */
 {7, "<>",  "OP_EMULATED",    -1, ASSOC_LEFT,       &type_float,  &type_float,  &type_float},


 {7, "|=", "BITSET_I",    6,  ASSOC_RIGHT,        &type_integer, &type_integer, &type_integer},
 {7, "|=", "BITSETP_I",   6,  ASSOC_RIGHT,        &type_pointer, &type_integer, &type_integer},


 {7, "*=", "MULSTORE_I",  6,  ASSOC_RIGHT_RESULT,   &type_integer, &type_integer, &type_integer},
 {7, "/=", "DIVSTORE_I",  6,  ASSOC_RIGHT_RESULT,   &type_integer, &type_integer, &type_integer},
 {7, "+=", "ADDSTORE_I",  6,  ASSOC_RIGHT_RESULT,   &type_integer, &type_integer, &type_integer},
 {7, "-=", "SUBSTORE_I",  6,  ASSOC_RIGHT_RESULT,   &type_integer, &type_integer, &type_integer},

 {7, "*=", "MULSTOREP_I", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_integer},
 {7, "/=", "DIVSTOREP_I", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_integer},
 {7, "+=", "ADDSTOREP_I", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_integer},
 {7, "-=", "SUBSTOREP_I", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_integer},

 {7, "*=", "OP_MULSTORE_IF",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "*=", "OP_MULSTOREP_IF", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "/=", "OP_DIVSTORE_IF",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "/=", "OP_DIVSTOREP_IF", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "+=", "OP_ADDSTORE_IF",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "+=", "OP_ADDSTOREP_IF", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "-=", "OP_SUBSTORE_IF",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},
 {7, "-=", "OP_SUBSTOREP_IF", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_float, &type_integer},

 {7, "*=", "OP_MULSTORE_FI",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "*=", "OP_MULSTOREP_FI", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "/=", "OP_DIVSTORE_FI",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "/=", "OP_DIVSTOREP_FI", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "+=", "OP_ADDSTORE_FI",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "+=", "OP_ADDSTOREP_FI", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "-=", "OP_SUBSTORE_FI",  6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},
 {7, "-=", "OP_SUBSTOREP_FI", 6,  ASSOC_RIGHT_RESULT,   &type_pointer, &type_integer, &type_float},

{7,"<CONV_FTOS>","C_FTOS",-1,ASSOC_LEFT,&type_float,&type_void,&type_string}, //OP_CONV_FTOS
{7,"<CONV_ITOS>","C_ITOS",-1,ASSOC_LEFT,&type_integer,&type_void,&type_string}, //OP_CONV_FTOS
{7,"<CONV_VTOS>","C_VTOS",-1,ASSOC_LEFT,&type_vector,&type_void,&type_string}, //OP_CONV_VTOS
{7,"<CONV_ETOS>","C_ETOS",-1,ASSOC_LEFT,&type_entity,&type_void,&type_string}, //OP_CONV_ETOS
