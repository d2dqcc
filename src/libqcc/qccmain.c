/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "qcc.h"
#ifdef _WIN32
# include <direct.h>
#endif

#include "errno.h"

#ifdef QCC_WRITEASM
FILE *asmfile;
#endif

static void StartNewStyleCompile (void);

qccbool writeasm;
static qccbool pr_werror;
qccbool verbose;

int MAX_REGS;
int MAX_STRINGS;
int MAX_GLOBALS;
int MAX_FIELDS;
int MAX_STATEMENTS;
int MAX_FUNCTIONS;
int MAX_CONSTANTS;
int MAX_TEMPS;

int *qcc_tempofs;
int tempsstart;
int numtemps;

char sourcefileslist[MAXSOURCEFILESLIST][1024];
int currentsourcefile;
int numsourcefiles;

qccbool newstylesource;
char destfile[1024];

float *qcc_pr_globals;
unsigned int numpr_globals;

char *strings;
int strofs;

QCC_dstatement_t *statements;
int numstatements;
int *statement_linenums;

QCC_dfunction_t *functions;
int numfunctions;

QCC_ddef_t *qcc_globals;
int numglobaldefs;

QCC_ddef_t *fields;
int numfielddefs;

int dotranslate_count;
hashtable_t compconstantstable;
hashtable_t globalstable;
hashtable_t localstable;
hashtable_t floatconstdefstable;
hashtable_t stringconstdefstable;
hashtable_t stringconstdefstable_trans;

qccbool qccwarningdisabled[WARN_MAX];

qccbool bodylessfuncs;

QCC_type_t *qcc_typeinfo;
int numtypeinfos;
int MAX_TYPEINFOS;


static const struct {
  const char *name;
  int index;
} warningnames[] =
{
  {"Q302", WARN_NOTREFERENCED},
  //{"", WARN_NOTREFERENCEDCONST},
  //{"", WARN_CONFLICTINGRETURNS},
  {"Q105", WARN_TOOFEWPARAMS},
  {"Q101", WARN_TOOMANYPARAMS},
  //{"", WARN_UNEXPECTEDPUNCT},
  {"Q106", WARN_ASSIGNMENTTOCONSTANT},
  {"Q203", WARN_MISSINGRETURNVALUE},
  {"Q204", WARN_WRONGRETURNTYPE},
  {"Q205", WARN_POINTLESSSTATEMENT},
  {"Q206", WARN_MISSINGRETURN},
  {"Q207", WARN_DUPLICATEDEFINITION},
  {"Q100", WARN_PRECOMPILERMESSAGE},
  //{"", WARN_STRINGTOOLONG},
  {"Q120", WARN_BADPRAGMA},
  //{"", WARN_HANGINGSLASHR},
  //{"", WARN_NOTDEFINED},
  //{"", WARN_SWITCHTYPEMISMATCH},
  //{"", WARN_CONFLICTINGUNIONMEMBER},
  //{"", WARN_KEYWORDDISABLED},
  //{"", WARN_ENUMFLAGS_NOTINTEGER},
  //{"", WARN_ENUMFLAGS_NOTBINARY},
  {"Q111", WARN_DUPLICATELABEL},
  {"Q201", WARN_ASSIGNMENTINCONDITIONAL},
  {"F300", WARN_DEADCODE},
  {NULL}
};


int QCC_WarningForName (const char *name) {
  for (int i = 0; warningnames[i].name; ++i) {
    if (!strcasecmp(name, warningnames[i].name)) return warningnames[i].index;
  }
  return -1;
}

optimisations_t optimisations[] = {
  //level 0 = no optimisations
  //level 1 = size optimisations
  //level 2 = speed optimisations
  //level 3 = dodgy optimisations.
  //level 4 = experimental...
  {&opt_shortenifnots,          "i",  1,  FLAG_ASDEFAULT,     "shortenifs",
    "if (!a) was traditionally compiled in two statements. This optimisation does it in one, but can "
    "cause some decompilers to get confused."},
  {&opt_nonvec_parms,           "p",  1,  FLAG_ASDEFAULT,     "nonvec_parms",
    "In the original qcc, function parameters were specified as a vector store even for floats. This fixes that."},
  {&opt_constant_names,         "c",  2,  FLAG_KILLSDEBUGGERS,"constant_names",
    "This optimisation strips out the names of constants (but not strings) from your progs, resulting in "
    "smaller files. It makes decompilers leave out names or fabricate numerical ones."},
  {&opt_constant_names_strings, "cs", 3,  FLAG_KILLSDEBUGGERS,"constant_names_strings",
    "This optimisation strips out the names of string constants from your progs. However, this can break "
    "addons, so don't use it in those cases."},
/*
  {&opt_locals,                 "l",  1,  FLAG_KILLSDEBUGGERS,"locals",
    "Strips out local names and definitions. This makes it REALLY hard to decompile"},
*/
  {&opt_unreferenced,           "u",  1,  FLAG_ASDEFAULT,     "unreferenced",
    "Removes the entries of unreferenced variables. Doesn't make a difference in well maintained code."},
  {&opt_overlaptemps,           "r",  1,  FLAG_ASDEFAULT,     "overlaptemps",
    "Optimises the pr_globals count by overlapping temporaries. In QC, every multiplication, division or "
    "operation in general produces a temporary variable. This optimisation prevents excess. The most "
    "important optimisation, ever."},
  {&opt_compound_jumps,         "cj", 3,  FLAG_KILLSDEBUGGERS,"compound_jumps",
    "This optimisation plays an effect mostly with nested if/else statements, instead of jumping to an "
    "unconditional jump statement, it'll jump to the final destination instead. This will bewilder decompilers."},
  {&opt_locals_marshalling,     "lm", 4,  FLAG_KILLSDEBUGGERS,"locals_marshalling",
    "Store all locals in one section of the pr_globals. Vastly reducing it. This effectivly does the job of "
    "overlaptemps. It's been noticed as buggy by a few, however, and the curcumstances where it causes problems "
    "are not yet known."},
  {&opt_vectorcalls,            "vc", 4,  FLAG_KILLSDEBUGGERS,"vectorcalls",
    "Where a function is called with just a vector, this causes the function call to store three floats instead of "
    "one vector. This can save a good number of pr_globals where those vectors contain many duplicate coordinates "
    "but do not match entirly."},
  {NULL}
};


//global to store useage to, flags, codename, human-readable name, help text
compiler_flag_t compiler_flag[] = {
  //options
  //spit out a qc.asm file, containing an assembler dump of the ENTIRE progs. (Doesn't include initialisation of constants)
  {&writeasm,             0,              "wasm",       "Dump Assembler",
    "Writes out a qc.asm which contains all your functions but in assembler. This is a great way to look for "
    "bugs in d2dqcc, but can also be used to see exactly what your functions turn into, and thus how to "
    "optimise statements better."},
  {&flag_assume_integer,  FLAG_MIDCOMPILE,"assumeint",  "Assume Integers",
    "Numerical constants are assumed to be integers instead of floats."},
  {&verbose,              FLAG_MIDCOMPILE,"verbose",    "Verbose",
    "Lots of extra compiler messages."},
  {NULL}
};


// CopyString returns an offset from the string heap
int QCC_CopyString (const char *str) {
  int old;
  if (!str || !*str) return 0;
  for (const char *s = strings; s < strings+strofs; ++s) {
    if (strcmp(s, str) == 0) {
      optres_noduplicatestrings += strlen(str);
      return s-strings;
    }
  }
  old = strofs;
  strcpy (strings+strofs, str);
  strofs += strlen(str)+1;
  return old;
}


int QCC_CopyDupBackString (const char *str) {
  int old;
  for (const char *s = strings+strofs-1; s > strings; --s) if (!strcmp(s, str)) return s-strings;
  old = strofs;
  strcpy (strings+strofs, str);
  strofs += strlen(str)+1;
  return old;
}


static void QCC_InitData (void) {
  static char parmname[12][MAX_PARMS];
  static temp_t ret_temp;
  qcc_sourcefile = NULL;
  numstatements = 1;
  strofs = 1;
  numfunctions = 1;
  numglobaldefs = 1;
  numfielddefs = 1;
  memset(&ret_temp, 0, sizeof(ret_temp));
  def_ret.ofs = OFS_RETURN;
  def_ret.name = "return";
  def_ret.temp = &ret_temp;
  def_ret.constant = qcc_false;
  def_ret.type  = NULL;
  ret_temp.ofs = def_ret.ofs;
  ret_temp.scope = NULL;
  ret_temp.size = 3;
  ret_temp.next = NULL;
  for (int i = 0; i < MAX_PARMS; ++i) {
    def_parms[i].temp = NULL;
    def_parms[i].type = NULL;
    def_parms[i].ofs = OFS_PARM0+3*i;
    def_parms[i].name = parmname[i];
    snprintf(parmname[i], sizeof(parmname[i]), "parm%d", i);
  }
  QCC_Clear_Memory(0);
}


static int WriteBodylessFuncs (int handle) {
  int ret = 0;
  for (QCC_def_t *d = pr.def_head.next; d != NULL; d = d->next) {
    if (d->type->type == ev_function && !d->scope) {
      // function parms are ok
      if (!(d->initialized & 1) && d->references>0) {
        SafeWrite(handle, d->name, strlen(d->name)+1);
        ++ret;
      }
    }
  }
  return ret;
}


// marshalled locals remaps all the functions to use the range MAX_REGS onwards for the offset to their locals.
// this function remaps all the locals back into the function.
static void QCC_UnmarshalLocals (void) {
  QCC_def_t *def;
  unsigned int ofs, maxo;
  ofs = numpr_globals;
  maxo = ofs;
  for (def = pr.def_head.next; def != NULL; def = def->next) {
    if (def->ofs >= MAX_REGS) {
      // unmap defs
      def->ofs = def->ofs+ofs-MAX_REGS;
      if (maxo < def->ofs) maxo = def->ofs;
    }
  }
  for (int i = 0; i < numfunctions; ++i) if (functions[i].parm_start == MAX_REGS) functions[i].parm_start = ofs;
  QCC_RemapOffsets(0, numstatements, MAX_REGS, MAX_REGS+maxo-numpr_globals+3, ofs);
  numpr_globals = maxo+3;
  if (numpr_globals > MAX_REGS) QCC_Error(ERR_TOOMANYGLOBALS, "Too many globals are in use to unmarshal all locals");
  if (maxo-ofs) qcc_printf("Total of %d marshalled globals\n", maxo-ofs);
}


static qccbool QCC_WriteData (int crc) {
  QCC_def_t *def;
  QCC_ddef_t *dd;
  dprograms_t progs;
  int h, i;
  qccbool types = qcc_false;
  qccbool warnedunref = qcc_false;
  if (numstatements == 1 && numfunctions == 1 && numglobaldefs == 1 && numfielddefs == 1) {
    qcc_printf("nothing to write\n");
    return qcc_false;
  }
  if (numstatements > MAX_STATEMENTS) {
    QCC_Error(ERR_TOOMANYSTATEMENTS, "Too many statements - %d\nAdd \"MAX_STATEMENTS\" \"%d\" to qcc.cfg", numstatements, (numstatements+32768)&~32767);
  }
  if (strofs > MAX_STRINGS) {
    QCC_Error(ERR_TOOMANYSTRINGS, "Too many strings - %d\nAdd \"MAX_STRINGS\" \"%d\" to qcc.cfg", strofs, (strofs+32768)&~32767);
  }
  QCC_UnmarshalLocals();
  if (bodylessfuncs) qcc_printf("Warning: There are some functions without bodies.\n");
  if (numpr_globals > 65530) {
    qcc_printf("Forcing target to FTE32 due to numpr_globals\n");
    abort(); //FIXME
  } else {
    if (numpr_globals >= 32768) {
      // not much of a different format. Rewrite output to get it working on original executors?
      qcc_printf("An enhanced executor will be required (FTE/QF/KK)\n");
    }
  }
  if (numpr_globals > 65530) {
    qcc_printf("Using 32 bit target due to numpr_globals\n");
    abort(); //FIXME
  }
  //include a type block?
  types = 1/*debugtarget*/;//!!QCC_PR_EatCompConstDefined("TYPES");  //useful for debugging and saving (maybe, anyway...).
  if (sizeof(char *) != sizeof(string_t)) {
    //qcc_typeinfo_t has a char* inside it, which changes size
    qcc_printf("64-bit builds cannot write typeinfo structures\n");
    types = qcc_false;
  }
  //part of how compilation works. This def is always present, and never used.
  def = QCC_PR_GetDef(NULL, "end_sys_globals", NULL, qcc_false, 0, qcc_false);
  if (def) ++def->references;
  def = QCC_PR_GetDef(NULL, "end_sys_fields", NULL, qcc_false, 0, qcc_false);
  if (def) ++def->references;
  // build globals
  for (def = pr.def_head.next; def != NULL; def = def->next) {
    // skip unnamed or immediates
    if (!def->name[0] || strcmp(def->name, "IMMEDIATE") == 0) continue;
    //
    if (def->references <= 0) {
      int wt = (def->constant ? WARN_NOTREFERENCEDCONST : WARN_NOTREFERENCED);
      if (QCC_PR_Warning(wt, strings+def->s_file, def->s_line, "'%s': no references", def->name)) {
        if (!warnedunref) {
          QCC_PR_Note(WARN_NOTREFERENCED, NULL, 0, "You can use the noref prefix or pragma to silence this message.");
          warnedunref = qcc_true;
        }
      }
      if (opt_unreferenced && def->type->type != ev_field) {
        ++optres_unreferenced;
        continue;
      }
    }
    //
    if (def->type->type == ev_function) {
      if (!def->timescalled) {
        // don't warn on 'main()'
        if (def->references <= 1 && strcmp(def->name, "main") != 0) {
          QCC_PR_Warning(WARN_DEADCODE, strings+def->s_file, def->s_line, "%s is never directly called or referenced (spawn function or dead code)", def->name);
        }
      }
      continue;
    } else if (def->type->type == ev_field && def->constant) {
      if (numfielddefs >= MAX_FIELDS) QCC_PR_ParseError(0, "Too many fields. Limit is %u\n", MAX_FIELDS);
      //fprintf(stderr, "field '%s': %d\n", def->name, def->references);
      dd = &fields[numfielddefs];
      ++numfielddefs;
      dd->type = def->type->aux_type->type;
      dd->s_name = QCC_CopyString(def->name);
      dd->ofs = G_INT(def->ofs);
      continue; // no global for field name
    } else if ((def->scope || def->constant) && (def->type->type != ev_string || opt_constant_names_strings)) {
      if (opt_constant_names) {
        if (def->type->type == ev_string) {
          optres_constant_names_strings += strlen(def->name);
        } else {
          optres_constant_names += strlen(def->name);
        }
        continue;
      }
    }
    //if (!def->saved && def->type->type != ev_string) continue;
    dd = &qcc_globals[numglobaldefs];
    ++numglobaldefs;
    if (types) {
      dd->type = def->type-qcc_typeinfo;
    } else {
      dd->type = def->type->type;
    }
    // set various flags
    dd->type &= ~DEF_SAVEGLOBAL;
    if (def->type->type != ev_function) {
      if (def->saved && ((!def->initialized || def->type->type == ev_function) &&
          def->type->type != ev_field &&
          def->scope == NULL)) {
        dd->type |= DEF_SAVEGLOBAL;
      }
    }
    if (def->shared) dd->type |= DEF_SHARED;
    //
    if (opt_locals && def->scope) {
      dd->s_name = 0;
      optres_locals += strlen(def->name);
    } else {
      dd->s_name = QCC_CopyString(def->name);
    }
    dd->ofs = def->ofs;
  }
  /* */
  for (i = 0; i < numglobaldefs; ++i) {
    dd = &qcc_globals[i];
    if (!(dd->type&DEF_SAVEGLOBAL)) continue; // only warn about saved ones
    for (h = 0; h < numglobaldefs; ++h) {
      if (i == h) continue;
      if (dd->ofs == qcc_globals[h].ofs) {
        if (dd->type != qcc_globals[h].type) {
          if (dd->type != ev_vector && qcc_globals[h].type != ev_float) {
            QCC_PR_Warning(0, NULL, 0, "Mismatched union global types (%s and %s)", strings+dd->s_name, strings+qcc_globals[h].s_name);
          }
        }
        // remove the saveglobal flag on the duplicate globals
        qcc_globals[h].type &= ~DEF_SAVEGLOBAL;
      }
    }
  }
  for (i = 1; i < numfielddefs; ++i) {
    dd = &fields[i];
    if (dd->type == ev_vector) continue; // just ignore vectors
    for (h = 1; h < numfielddefs; ++h) {
      if (i == h) continue;
      if (dd->ofs == fields[h].ofs) {
        if (dd->type != fields[h].type) {
          if (fields[h].type != ev_vector) {
            QCC_PR_Warning(0, NULL, 0, "Mismatched union field types (%s and %s)", strings+dd->s_name, strings+fields[h].s_name);
          }
        }
      }
    }
  }
  if (numglobaldefs > MAX_GLOBALS) QCC_Error(ERR_TOOMANYGLOBALS, "Too many globals: %d", numglobaldefs);
  strofs = (strofs+3)&~3;
  if (verbose) {
    qcc_printf("%6i strofs (of %d)\n", strofs, MAX_STRINGS);
    qcc_printf("%6i numstatements (of %d)\n", numstatements, MAX_STATEMENTS);
    qcc_printf("%6i numfunctions (of %d)\n", numfunctions, MAX_FUNCTIONS);
    qcc_printf("%6i numglobaldefs (of %d)\n", numglobaldefs, MAX_GLOBALS);
    qcc_printf("%6i numfielddefs (%d unique) (of %d)\n", numfielddefs, pr.size_fields, MAX_FIELDS);
    qcc_printf("%6i numpr_globals (of %d)\n", numpr_globals, MAX_REGS);
  }
  if (!*destfile) strcpy(destfile, "progs.dat");
  if (verbose) qcc_printf("Writing %s\n", destfile);
  h = SafeOpenWrite(destfile, 16*1024*1024);
  SafeWrite(h, &progs, sizeof(progs));
  //this is a lame way to do it
  while (SafeSeek(h, 0, SEEK_CUR)&3) SafeWrite(h, "\0", 1);
  progs.ofs_strings = SafeSeek(h, 0, SEEK_CUR);
  progs.numstrings = strofs;
  SafeWrite(h, strings, strofs);
  progs.ofs_statements = SafeSeek(h, 0, SEEK_CUR);
  progs.numstatements = numstatements;
  for (i = 0; i < numstatements; ++i) {
    // resize as we go - scaling down
    statements[i].op = PRLittleShort((unsigned short)statements[i].op);
    if (statements[i].a < 0) {
      statements[i].a = PRLittleShort((short)statements[i].a);
    } else {
      statements[i].a = (unsigned short)PRLittleShort((unsigned short)statements[i].a);
    }
    if (statements[i].b < 0) {
      statements[i].b = PRLittleShort((short)statements[i].b);
    } else {
      statements[i].b = (unsigned short)PRLittleShort((unsigned short)statements[i].b);
    }
    if (statements[i].c < 0) {
      statements[i].c = PRLittleShort((short)statements[i].c);
    } else {
      statements[i].c = (unsigned short)PRLittleShort((unsigned short)statements[i].c);
    }
  }
  SafeWrite(h, statements, numstatements*sizeof(QCC_dstatement_t));
  progs.ofs_functions = SafeSeek(h, 0, SEEK_CUR);
  progs.numfunctions = numfunctions;
  for (i = 0; i < numfunctions; ++i) {
    functions[i].first_statement = PRLittleLong(functions[i].first_statement);
    functions[i].parm_start = PRLittleLong(functions[i].parm_start);
    functions[i].s_name = PRLittleLong(functions[i].s_name);
    functions[i].s_file = PRLittleLong(functions[i].s_file);
    functions[i].numparms = PRLittleLong(functions[i].numparms > MAX_PARMS ? MAX_PARMS : functions[i].numparms);
    functions[i].locals = PRLittleLong(functions[i].locals);
    for (int c = 0; c < MAX_PARMS; ++c) functions[i].parm_size[c] = PRLittleLong(functions[i].parm_size[c]);
  }
  SafeWrite(h, functions, numfunctions*sizeof(QCC_dfunction_t));
  progs.ofs_globaldefs = SafeSeek (h, 0, SEEK_CUR);
  progs.numglobaldefs = numglobaldefs;
  for (i = 0; i < numglobaldefs; ++i) {
    qcc_globals[i].type = (unsigned short)PRLittleShort((unsigned short)qcc_globals[i].type);
    qcc_globals[i].ofs = (unsigned short)PRLittleShort((unsigned short)qcc_globals[i].ofs);
    qcc_globals[i].s_name = PRLittleLong(qcc_globals[i].s_name);
  }
  SafeWrite(h, qcc_globals, numglobaldefs*sizeof(QCC_ddef_t));
  progs.ofs_fielddefs = SafeSeek(h, 0, SEEK_CUR);
  progs.numfielddefs = numfielddefs;
  for (i = 0; i < numfielddefs; ++i) {
    fields[i].type = (unsigned short)PRLittleShort((unsigned short)fields[i].type);
    fields[i].ofs = (unsigned short)PRLittleShort((unsigned short)fields[i].ofs);
    fields[i].s_name = PRLittleLong(fields[i].s_name);
  }
  SafeWrite(h, fields, numfielddefs*sizeof(QCC_ddef_t));
  progs.ofs_globals = SafeSeek (h, 0, SEEK_CUR);
  progs.numglobals = numpr_globals;
  for (i = 0; (unsigned)i < numpr_globals; ++i) ((qccint *)qcc_pr_globals)[i] = PRLittleLong(((qccint *)qcc_pr_globals)[i]);
  SafeWrite(h, qcc_pr_globals, numpr_globals*4);
  if (types) {
    for (i = 0; i < numtypeinfos; ++i) {
      if (qcc_typeinfo[i].aux_type) qcc_typeinfo[i].aux_type = (QCC_type_t*)(qcc_typeinfo[i].aux_type-qcc_typeinfo);
      if (qcc_typeinfo[i].next) qcc_typeinfo[i].next = (QCC_type_t*)(qcc_typeinfo[i].next-qcc_typeinfo);
      qcc_typeinfo[i].name = (char *)QCC_CopyDupBackString(qcc_typeinfo[i].name); //???
    }
  }
  //progs.ofsfiles = 0;
  progs.ofslinenums = 0;
  progs.secondaryversion = 0;
  progs.ofsbodylessfuncs = 0;
  progs.numbodylessfuncs = 0;
  progs.ofs_types = 0;
  progs.numtypes = 0;
  progs.version = PROG_EXTENDEDVERSION;
  progs.secondaryversion = PROG_SECONDARYVERSION16;
  progs.ofsbodylessfuncs = SafeSeek(h, 0, SEEK_CUR);
  progs.numbodylessfuncs = WriteBodylessFuncs(h);
  //k8:TODO:write line numbers instead of LNOF
  progs.ofslinenums = SafeSeek(h, 0, SEEK_CUR);
  SafeWrite (h, statement_linenums, numstatements*sizeof(int));
  if (types) {
    progs.ofs_types = SafeSeek(h, 0, SEEK_CUR);
    SafeWrite(h, qcc_typeinfo, sizeof(QCC_type_t)*numtypeinfos);
    progs.numtypes = numtypeinfos;
  } else {
    progs.ofs_types = 0;
    progs.numtypes = 0;
  }
  //progs.ofsfiles = WriteSourceFiles(h, &progs, debugtarget);
  if (verbose) qcc_printf("%6i TOTAL SIZE\n", (int)SafeSeek (h, 0, SEEK_CUR));
  progs.entityfields = pr.size_fields;
  progs.crc = crc;
  // qbyte swap the header and write it out
  for (i = 0; i < sizeof(progs)/4; ++i) ((qccint *)&progs)[i] = PRLittleLong(((qccint *)&progs)[i]);
  SafeSeek(h, 0, SEEK_SET);
  SafeWrite(h, &progs, sizeof(progs));
  SafeClose(h);
  /*
  if (opt_filenames) {
    qcc_printf("Not writing linenumbers file due to conflicting optimisation\n");
  } else {
    unsigned int lnotype = *(unsigned int *)"LNOF";
    unsigned int version = 1;
    StripExtension(destfile);
    strcat(destfile, ".lno");
    if (verbose)
      qcc_printf("Writing %s for debugging\n", destfile);
    h = SafeOpenWrite (destfile, 2*1024*1024);
    SafeWrite (h, &lnotype, sizeof(int));
    SafeWrite (h, &version, sizeof(int));
    SafeWrite (h, &numglobaldefs, sizeof(int));
    SafeWrite (h, &numpr_globals, sizeof(int));
    SafeWrite (h, &numfielddefs, sizeof(int));
    SafeWrite (h, &numstatements, sizeof(int));
    SafeWrite (h, statement_linenums, numstatements*sizeof(int));
    SafeClose (h);
  }
  */
  return qcc_true;
}


QCC_type_t *QCC_PR_NewType (const char *name, int basictype) {
  char *nn = qccHunkDupStr(name);
  if (numtypeinfos >= MAX_TYPEINFOS) QCC_Error(ERR_TOOMANYTYPES, "Too many types");
  memset(&qcc_typeinfo[numtypeinfos], 0, sizeof(QCC_type_t));
  qcc_typeinfo[numtypeinfos].type = basictype;
  qcc_typeinfo[numtypeinfos].name = nn;
  qcc_typeinfo[numtypeinfos].num_parms = 0;
  qcc_typeinfo[numtypeinfos].param = NULL;
  qcc_typeinfo[numtypeinfos].size = type_size[basictype];
  qcc_typeinfo[numtypeinfos].arraysize = 1;
  ++numtypeinfos;
  return &qcc_typeinfo[numtypeinfos-1];
}


/*
==============
PR_BeginCompilation

called before compiling a batch of files, clears the pr struct
==============
*/
static void  QCC_PR_BeginCompilation (void *memory, int memsize) {
  pr.memory = memory;
  pr.max_memory = memsize;
  pr.def_tail = &pr.def_head;
  QCC_PR_ResetErrorScope();
  pr_scope = NULL;
  /*
  numpr_globals = RESERVED_OFS;
  for (i = 0; i < RESERVED_OFS; ++i) pr_global_defs[i] = &def_void;
  */
  type_void = QCC_PR_NewType("void", ev_void);
  type_string = QCC_PR_NewType("string", ev_string);
  type_float = QCC_PR_NewType("float", ev_float);
  type_vector = QCC_PR_NewType("vector", ev_vector);
  type_entity = QCC_PR_NewType("entity", ev_entity);
  type_field = QCC_PR_NewType("__field", ev_field);
  type_function = QCC_PR_NewType("__function", ev_function);
  type_pointer = QCC_PR_NewType("__pointer", ev_pointer);
  type_integer = QCC_PR_NewType("__integer", ev_integer);
  type_floatfield = QCC_PR_NewType("fieldfloat", ev_field);
  type_floatfield->aux_type = type_float;
  type_pointer->aux_type = QCC_PR_NewType("pointeraux", ev_float);
  type_function->aux_type = type_void;
  //type_field->aux_type = type_float;
  type_integer = QCC_PR_NewType("int", ev_integer);
  numpr_globals = RESERVED_OFS;
  //for (i = 0; i < RESERVED_OFS; ++i) pr_global_defs[i] = NULL;
  // link the function type in so state forward declarations match proper type
  pr.types = NULL;
  //type_function->next = NULL;
  pr_error_count = 0;
  pr_warning_count = 0;
  recursivefunctiontype = 0;
  qcc_freeofs = NULL;
}


/*
==============
PR_FinishCompilation

called after all files are compiled to check for errors
Returns qcc_false if errors were detected.
==============
*/
static int QCC_PR_FinishCompilation (void) {
  int errors = qcc_false;
  // check to make sure all functions prototyped have code
  for (QCC_def_t *d = pr.def_head.next; d != NULL; d = d->next) {
    if (d->type->type == ev_function && !d->scope) {
      // function parms are ok
      //f = G_FUNCTION(d->ofs);
      //if (!f || (!f->code && !f->builtin) )
      if (d->initialized == 0) {
        s_file = d->s_file;
        /*
        if (!strncmp(d->name, "Class*", 6)) {
          QCC_PR_EmitClassFromFunction(d, d->name+6);
          pr_scope = NULL;
        } else
        */
        {
          QCC_PR_ParseWarning(ERR_NOFUNC, "function %s was not defined", d->name);
          QCC_PR_ParsePrintDef(ERR_NOFUNC, d);
          bodylessfuncs = qcc_true;
          errors = qcc_true;
        }
        s_file = 0;
        //errors = qcc_true;
      } else if (d->initialized == 2) {
        bodylessfuncs = qcc_true;
      }
    }
  }
  pr_scope = NULL;
  return !errors;
}


#if 0
//=============================================================================
// FIXME: byte swap?
// this is a 16 bit, non-reflected CRC using the polynomial 0x1021
// and the initial and final xor values shown below...  in other words, the
// CCITT standard CRC used by XMODEM
#define CRC_INIT_VALUE  (0xffff)
#define CRC_XOR_VALUE   (0x0000)

static unsigned short QCC_crctable[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};


static inline void QCC_CRC_Init (uint16_t *crcvalue) {
  *crcvalue = CRC_INIT_VALUE;
}


static inline void QCC_CRC_ProcessByte (uint16_t *crcvalue, qbyte data) {
  *crcvalue = (*crcvalue<<8)^QCC_crctable[(*crcvalue>>8)^data];
}


static inline uint16_t QCC_CRC_Value (uint16_t crcvalue) {
  return crcvalue^CRC_XOR_VALUE;
}
#endif



//=============================================================================
/*
============
PR_WriteProgdefs

Writes the global and entity structures out
Returns a crc of the header, to be stored in the progs file for comparison
at load time.
============
*/
#if 0
#define PROGDEFS_MAX_SIZE  (16384)
//write (to file buf) and add to the crc
static void Add (const char *p, unsigned short *crc, char *file) {
  int i = strlen(file);
  if (i+strlen(p)+1 >= PROGDEFS_MAX_SIZE) return;
  for (const char *s = p; *s; ++s, ++i) {
    QCC_CRC_ProcessByte(crc, *s);
    file[i] = *s;
  }
  file[i]='\0';
}
#define ADD(p)  Add(p, &crc, file)


static void Add3 (const char *p, unsigned short *crc, char *file) {
  for (const char *s = p; *s; ++s) QCC_CRC_ProcessByte(crc, *s);
}
#define ADD3(p)  Add3(p, &crc, file)
#define ADD2(p) strncat(file, p, PROGDEFS_MAX_SIZE-1-strlen(file))  //no crc (later changes)
#endif


static unsigned short QCC_PR_WriteProgdefs (const char *filename) {
#if 0
  char file[PROGDEFS_MAX_SIZE];
  const QCC_def_t *d;
  int f, rescnt = 0;
  uint16_t crc;
  file[0] = '\0';
  QCC_CRC_Init(&crc);
  // printf global vars until the first field is defined
  //ADD: crc and dump
  //ADD2: dump but don't crc
  //ADD3: crc but don't dump
  ADD2("/* ");
  ADD2("File generated by D2DQCC, relevant for engine modding only, the generated crc must be the same as your engine expects.");
  ADD2(" */\n\ntypedef struct  globalvars_s {\n");
  ADD2(
    "  qcint pad;\n"
    "  qcint ofs_return[3];\n" // makes it easier with the get globals func
    "  qcint ofs_parm0[3];\n"
    "  qcint ofs_parm1[3];\n"
    "  qcint ofs_parm2[3];\n"
    "  qcint ofs_parm3[3];\n"
    "  qcint ofs_parm4[3];\n"
    "  qcint ofs_parm5[3];\n"
    "  qcint ofs_parm6[3];\n"
    "  qcint ofs_parm7[3];\n");
  ADD3(qcva("  qcint pad[%d];\n", RESERVED_OFS));
  for (d = pr.def_head.next; d != NULL; d = d->next) {
    const char *name = d->name;
    char buf[128];
    if (!strcmp(name, "end_sys_globals")) break;
    if (d->ofs < RESERVED_OFS) continue;
    if (strcmp(name, "IMMEDIATE") == 0) {
      snprintf(buf, sizeof(buf), "_reserved%d", rescnt++);
      name = buf;
    }
    switch (d->type->type) {
      case ev_float:
        ADD(qcva("  qcfloat %s;\n", name));
        break;
      case ev_vector:
        ADD(qcva("  qcvec3 %s;\n", name));
        d = d->next->next->next; // skip the elements
        break;
      case ev_string:
        ADD(qcva("  qcstring %s;\n", name));
        break;
      case ev_function:
        ADD(qcva("  qcfunc %s;\n", name));
        break;
      case ev_entity:
        ADD(qcva("  qcint %s;\n", name));
        break;
      case ev_integer:
        ADD(qcva("  qcint %s;\n", name));
        break;
      default:
        ADD(qcva("  qcint %s;\n", name));
        break;
    }
  }
  ADD("} globalvars_t;\n\n");
  // qcc_printfall fields
  ADD2("typedef struct entvars_s {\n");
  for (d = pr.def_head.next; d != NULL; d = d->next) {
    if (!strcmp(d->name, "end_sys_fields")) break;
    if (d->type->type != ev_field) continue;
    switch (d->type->aux_type->type) {
      case ev_float:
        ADD(qcva("  float %s;\n",d->name));
        break;
      case ev_vector:
        ADD(qcva("  vec3_t %s;\n",d->name));
        d = d->next->next->next; // skip the elements
        break;
      case ev_string:
        ADD(qcva("  string_t %s;\n",d->name));
        break;
      case ev_function:
        ADD(qcva("  func_t %s;\n",d->name));
        break;
      case ev_entity:
        ADD(qcva("  int %s;\n",d->name));
        break;
      case ev_integer:
        ADD(qcva("  int %s;\n",d->name));
        break;
      default:
        ADD(qcva("  int %s;\n",d->name));
        break;
    }
  }
  ADD("} entvars_t;\n\n");
  ///temp
  ADD2("// with this the crc isn't needed for fields.\n"
    "#ifdef FIELDSSTRUCT\n"
    "struct fieldvars_s {\n"
    "  int ofs;\n"
    "  int type;\n"
    "  char *name;\n"
    "} fieldvars[] = {\n");
  f = 0;
  for (d = pr.def_head.next; d != NULL; d = d->next) {
    if (!strcmp(d->name, "end_sys_fields")) break;
    if (d->type->type != ev_field) continue;
    if (f) ADD2(",\n");
    ADD2(qcva("  {%d, %d, \"%s\"}",G_INT(d->ofs), d->type->aux_type->type, d->name));
    f = 1;
  }
  ADD2("\n};\n#endif\n\n");
  //end temp
  ADD2(qcva("#define PROGHEADER_CRC %d\n", crc));
  if (QCC_CheckParm("-progdefs")) {
    qcc_printf("writing %s\n", filename);
    f = SafeOpenWrite(filename, 16384);
    SafeWrite(f, file, strlen(file));
    SafeClose(f);
  }
  if (ForcedCRC) crc = ForcedCRC;
  return crc;
#else
  return 0;
#endif
}


static void QCC_PR_CommandLinePreprocessorOptions (void) {
  for (int i = 1; i < myargc; ++i) {
    CompilerConstant_t *cnst;
    const char *name;
    char *val;
    int p;
    if (strncmp(myargv[i], "-D", 2) == 0) {
      //compiler constant
      name = myargv[i]+2;
      val = strchr(name, '=');
      if (val != NULL) *val++ = '\0';
      cnst = QCC_PR_DefineName(name);
      if (val != NULL) {
        cnst->value = qccHunkDupStr(val);
      }
    } else if (strncmp(myargv[i], "-O", 2) == 0) {
      // optimisations
      p = 0;
      if (myargv[i][2] >= '0' && myargv[i][2] <= '3') {
      } else if (strncmp(myargv[i]+2, "no-", 3) == 0) {
        if (myargv[i][5]) {
          for (p = 0; optimisations[p].enabled; ++p) {
            if ((*optimisations[p].abbrev && !strcasecmp(myargv[i]+5, optimisations[p].abbrev)) ||
                !strcasecmp(myargv[i]+5, optimisations[p].fullname)) {
              *optimisations[p].enabled = qcc_false;
              break;
            }
          }
        }
      } else {
        if (myargv[i][2]) {
          for (p = 0; optimisations[p].enabled; ++p) {
            if ((*optimisations[p].abbrev && !strcasecmp(myargv[i]+2, optimisations[p].abbrev)) ||
                !strcasecmp(myargv[i]+2, optimisations[p].fullname)) {
              *optimisations[p].enabled = qcc_true;
              break;
            }
          }
        }
      }
      if (!optimisations[p].enabled) QCC_PR_Warning(0, NULL, WARN_BADPARAMS, "Unrecognised optimisation parameter (%s)", myargv[i]);
    } else if (strncmp(myargv[i], "-K", 2) == 0) {
      p = 0;
      if (strncmp(myargv[i]+2, "no-", 3) == 0) {
        for (p = 0; compiler_flag[p].enabled; ++p) {
          if (!strcasecmp(myargv[i]+5, compiler_flag[p].abbrev)) {
            *compiler_flag[p].enabled = qcc_false;
            break;
          }
        }
      } else {
        for (p = 0; compiler_flag[p].enabled; ++p) {
          if (!strcasecmp(myargv[i]+2, compiler_flag[p].abbrev)) {
            *compiler_flag[p].enabled = qcc_true;
            break;
          }
        }
      }
      if (!compiler_flag[p].enabled) QCC_PR_Warning(0, NULL, WARN_BADPARAMS, "Unrecognised keyword parameter (%s)", myargv[i]);
    } else if (strncmp(myargv[i], "-f", 2) == 0) {
      p = 0;
      if (!strncasecmp(myargv[i]+2, "no-", 3)) {
        for (p = 0; compiler_flag[p].enabled; ++p) {
          if (!strcasecmp(myargv[i]+5, compiler_flag[p].abbrev)) {
            *compiler_flag[p].enabled = qcc_false;
            break;
          }
        }
      } else {
        for (p = 0; compiler_flag[p].enabled; ++p) {
          if (!strcasecmp(myargv[i]+2, compiler_flag[p].abbrev)) {
            *compiler_flag[p].enabled = qcc_true;
            break;
          }
        }
      }
      if (!compiler_flag[p].enabled) QCC_PR_Warning(0, NULL, WARN_BADPARAMS, "Unrecognised flag parameter (%s)", myargv[i]);
    } else if (strncmp(myargv[i], "-W", 2) == 0) {
      if (!strcasecmp(myargv[i]+2, "all")) memset(qccwarningdisabled, 0, sizeof(qccwarningdisabled));
      else if (!strcasecmp(myargv[i]+2, "none")) memset(qccwarningdisabled, 1, sizeof(qccwarningdisabled));
      else if(!strcasecmp(myargv[i]+2, "error")) pr_werror = qcc_true;
      else if (!strcasecmp(myargv[i]+2, "no-mundane")) {
        // disable mundane performance/efficiency/blah warnings that don't affect code.
        qccwarningdisabled[WARN_SAMENAMEASGLOBAL] = qcc_true;
        qccwarningdisabled[WARN_DUPLICATEDEFINITION] = qcc_true;
        qccwarningdisabled[WARN_CONSTANTCOMPARISON] = qcc_true;
        qccwarningdisabled[WARN_ASSIGNMENTINCONDITIONAL] = qcc_true;
        qccwarningdisabled[WARN_DEADCODE] = qcc_true;
        qccwarningdisabled[WARN_NOTREFERENCEDCONST] = qcc_true;
        qccwarningdisabled[WARN_NOTREFERENCED] = qcc_true;
        qccwarningdisabled[WARN_POINTLESSSTATEMENT] = qcc_true;
        qccwarningdisabled[WARN_ASSIGNMENTTOCONSTANTFUNC] = qcc_true;
        qccwarningdisabled[WARN_BADPRAGMA] = qcc_true; // C specs say that these should be ignored. We're close enough to C that I consider that a valid statement.
        qccwarningdisabled[WARN_IDENTICALPRECOMPILER] = qcc_true;
        qccwarningdisabled[WARN_UNDEFNOTDEFINED] = qcc_true;
        qccwarningdisabled[WARN_FIXEDRETURNVALUECONFLICT] = qcc_true;
        qccwarningdisabled[WARN_EXTRAPRECACHE] = qcc_true;
        qccwarningdisabled[WARN_CORRECTEDRETURNTYPE] = qcc_true;
      } else {
        p = 0;
        if (!strncasecmp(myargv[i]+2, "no-", 3)) {
          for (p = 0; warningnames[p].name; ++p) {
            if (!strcmp(myargv[i]+5, warningnames[p].name)) {
              qccwarningdisabled[warningnames[p].index] = qcc_true;
              break;
            }
          }
        } else {
          for (p = 0; warningnames[p].name; ++p) {
            if (!strcasecmp(myargv[i]+2, warningnames[p].name)) {
              qccwarningdisabled[warningnames[p].index] = qcc_false;
              break;
            }
          }
        }
        if (!warningnames[p].name) QCC_PR_Warning(0, NULL, WARN_BADPARAMS, "Unrecognised warning parameter (%s)", myargv[i]);
      }
    }
  }
}


/*
============
main
============
*/
char *qccmsrc;
char *qccmsrc2;
char qccmfilename[1024];
char qccmprogsdat[1024];
char qccmsourcedir[1024];


static void QCC_SetDefaultProperties (void) {
  int level;
  Hash_InitTable(&compconstantstable, MAX_CONSTANTS, qccHunkAlloc(Hash_BytesForBuckets(MAX_CONSTANTS)));
  ForcedCRC = 0;
  defaultstatic = 0;
  QCC_PR_DefineName("D2DQCC");
  if (QCC_CheckParm("-Oz")) {
    QCC_PR_DefineName("OP_COMP_STATEMENTS");
    QCC_PR_DefineName("OP_COMP_DEFS");
    QCC_PR_DefineName("OP_COMP_FIELDS");
    QCC_PR_DefineName("OP_COMP_FUNCTIONS");
    QCC_PR_DefineName("OP_COMP_STRINGS");
    QCC_PR_DefineName("OP_COMP_GLOBALS");
    QCC_PR_DefineName("OP_COMP_LINES");
    QCC_PR_DefineName("OP_COMP_TYPES");
  }
  if (QCC_CheckParm("-O0")) level = 0;
  else if (QCC_CheckParm("-O1")) level = 1;
  else if (QCC_CheckParm("-O2")) level = 2;
  else if (QCC_CheckParm("-O3")) level = 3;
  else level = -1;
  if (level == -1) {
    for (int i = 0; optimisations[i].enabled; ++i) *optimisations[i].enabled = (optimisations[i].flags&FLAG_ASDEFAULT ? qcc_true : qcc_false);
  } else {
    for (int i = 0; optimisations[i].enabled; ++i) *optimisations[i].enabled = (level >= optimisations[i].optimisationlevel ? qcc_true : qcc_false);
  }
  //enable all warnings
  memset(qccwarningdisabled, 0, sizeof(qccwarningdisabled));
  //play with default warnings.
  qccwarningdisabled[WARN_NOTREFERENCEDCONST] = qcc_true;
  qccwarningdisabled[WARN_MACROINSTRING] = qcc_true;
  //qccwarningdisabled[WARN_ASSIGNMENTTOCONSTANT] = qcc_true;
  qccwarningdisabled[WARN_FIXEDRETURNVALUECONFLICT] = qcc_true;
  qccwarningdisabled[WARN_EXTRAPRECACHE] = qcc_true;
  qccwarningdisabled[WARN_DEADCODE] = qcc_true;
  qccwarningdisabled[WARN_INEFFICIENTPLUSPLUS] = qcc_true;
  qccwarningdisabled[WARN_D2DQCC_SPECIFIC] = qcc_true;
  qccwarningdisabled[WARN_IFSTRING_USED] = qcc_true;
  if (QCC_CheckParm("-nowarn") || QCC_CheckParm("-Wnone")) memset(qccwarningdisabled, 1, sizeof(qccwarningdisabled));
  if (QCC_CheckParm("-Wall")) memset(qccwarningdisabled, 0, sizeof(qccwarningdisabled));
  //Check the command line
  QCC_PR_CommandLinePreprocessorOptions();
  if (QCC_CheckParm("-debug")) {
    // disable any debug optimisations
    for (int i = 0; optimisations[i].enabled; ++i) if (optimisations[i].flags & FLAG_KILLSDEBUGGERS) *optimisations[i].enabled = qcc_false;
  }
}


int qcc_compileactive = qcc_false;


// as part of the quake engine
void QCC_main (int argc, char **argv) {
  extern int pr_bracelevel;
  int p;
#ifndef QCCONLY
  char destfile2[1024], *s2;
#endif
  char *s;

  SetEndian();
  myargc = argc;
  myargv = argv;
  qcc_compileactive = qcc_true;
  MAX_REGS = 1<<17;
  MAX_STRINGS = 1000000;
  MAX_GLOBALS = 1<<17;
  MAX_FIELDS = 1<<12;
  MAX_STATEMENTS = 0x80000;
  MAX_FUNCTIONS = 16384;
  MAX_TYPEINFOS = 16384;
  MAX_CONSTANTS = 2048;
  p = qcc_pf_externs->FileSize("qcc.cfg");
  if (p < 0) p = qcc_pf_externs->FileSize("src/qcc.cfg");
  if (p > 0) {
    s = qccHunkAlloc(p+1);
    s = (char *)qcc_pf_externs->ReadFile("qcc.cfg", s, p);
    for (;;) {
      s = (char *)QCC_COM_Parse(s);
      if (!strcmp(qcc_token, "MAX_REGS")) {
        s = QCC_COM_Parse(s);
        MAX_REGS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_STRINGS")) {
        s = QCC_COM_Parse(s);
        MAX_STRINGS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_GLOBALS")) {
        s = QCC_COM_Parse(s);
        MAX_GLOBALS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_FIELDS")) {
        s = QCC_COM_Parse(s);
        MAX_FIELDS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_STATEMENTS")) {
        s = QCC_COM_Parse(s);
        MAX_STATEMENTS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_FUNCTIONS")) {
        s = QCC_COM_Parse(s);
        MAX_FUNCTIONS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_TYPES")) {
        s = QCC_COM_Parse(s);
        MAX_TYPEINFOS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "MAX_TEMPS")) {
        s = QCC_COM_Parse(s);
        MAX_TEMPS = atoi(qcc_token);
      } else if (!strcmp(qcc_token, "CONSTANTS")) {
        s = QCC_COM_Parse(s);
        MAX_CONSTANTS = atoi(qcc_token);
      } else if (!s) {
        break;
      } else {
        qcc_printf("Bad token in qcc.cfg file\n");
      }
    }
  }
  for (p = 0; compiler_flag[p].enabled; ++p) *compiler_flag[p].enabled = compiler_flag[p].flags&FLAG_ASDEFAULT;
  pr_werror = qcc_false;
  QCC_SetDefaultProperties();
  optres_shortenifnots = 0;
  optres_overlaptemps = 0;
  optres_noduplicatestrings = 0;
  optres_constantarith = 0;
  optres_nonvec_parms = 0;
  optres_constant_names = 0;
  optres_constant_names_strings = 0;
  optres_assignments = 0;
  optres_unreferenced = 0;
  optres_locals = 0;
  optres_dupconstdefs = 0;
  optres_compound_jumps = 0;
  optres_locals_marshalling = 0;
  numtemps = 0;
  QCC_PurgeTemps();
  strings = (void *)qccHunkAlloc(sizeof(char)*MAX_STRINGS);
  strofs = 1;
  statements = (void *)qccHunkAlloc(sizeof(QCC_dstatement_t)*MAX_STATEMENTS);
  numstatements = 0;
  statement_linenums = (void *)qccHunkAlloc(sizeof(int)*MAX_STATEMENTS);
  functions = (void *)qccHunkAlloc(sizeof(QCC_dfunction_t)*MAX_FUNCTIONS);
  numfunctions = 0;
  pr_bracelevel = 0;
  qcc_pr_globals = (void *)qccHunkAlloc(sizeof(float)*MAX_REGS);
  numpr_globals = 0;
  Hash_InitTable(&globalstable, MAX_REGS/2, qccHunkAlloc(Hash_BytesForBuckets(MAX_REGS/2)));
  Hash_InitTable(&localstable, MAX_REGS/2, qccHunkAlloc(Hash_BytesForBuckets(MAX_REGS/2)));
  Hash_InitTable(&floatconstdefstable, MAX_REGS/2+1, qccHunkAlloc(Hash_BytesForBuckets(MAX_REGS/2+1)));
  Hash_InitTable(&stringconstdefstable, MAX_REGS/2, qccHunkAlloc(Hash_BytesForBuckets(MAX_REGS/2)));
  Hash_InitTable(&stringconstdefstable_trans, 1000, qccHunkAlloc(Hash_BytesForBuckets(1000)));
  dotranslate_count = 0;
  //pr_global_defs = (QCC_def_t **)qccHunkAlloc(sizeof(QCC_def_t *) * MAX_REGS);
  qcc_globals = (void *)qccHunkAlloc(sizeof(QCC_ddef_t)*MAX_GLOBALS);
  numglobaldefs = 0;
  fields = (void *)qccHunkAlloc(sizeof(QCC_ddef_t)*MAX_FIELDS);
  numfielddefs = 0;
  memset(pr_immediate_string, 0, sizeof(pr_immediate_string));
  qcc_typeinfo = (void *)qccHunkAlloc(sizeof(QCC_type_t)*MAX_TYPEINFOS);
  numtypeinfos = 0;
  qcc_tempofs = qccHunkAlloc(sizeof(int)*MAX_TEMPS);
  tempsstart = 0;
  bodylessfuncs = 0;
  memset(&pr, 0, sizeof(pr));
#ifdef MAX_EXTRA_PARMS
  memset(&extra_parms, 0, sizeof(extra_parms));
#endif
  if (QCC_CheckParm("-h") || QCC_CheckParm("-help") || QCC_CheckParm("--help")) {
    qcc_printf("qcc looks for progs.src in the current directory.\n");
    qcc_printf("to look in a different directory: qcc -src <directory>\n");
    qcc_printf("-Fwasm causes D2DQCC to dump all asm to qc.asm\n");
    qcc_printf("-O0 to disable optimisations\n");
    qcc_printf("-O1 to optimise for size\n");
    qcc_printf("-O2 to optimise more - some behaviours may change\n");
    qcc_printf("-O3 to optimise lots - experimental or non-future-proof\n");
    qcc_printf("-Oname to enable an optimisation\n");
    qcc_printf("-Ono-name to disable optimisations\n");
    qcc_printf("-Kkeyword to activate keyword\n");
    qcc_printf("-Kno-keyword to disable keyword\n");
    qcc_printf("-Wall to give a stupid number of warnings\n");
    qcc_printf("-Fautoproto to enable automatic prototyping\n");
    qcc_printf("-Fsubscope to make locals specific to their subscope\n");
    qcc_compileactive = qcc_false;
    return;
  }

  if (opt_locals_marshalling) {
    qcc_printf(
      "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
      "Locals marshalling might be buggy. Use with caution\n"
      "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  }

  p = QCC_CheckParm("-src");
  if (p && p < argc-1) {
    strcpy(qccmsourcedir, argv[p+1]);
    strcat(qccmsourcedir, "/");
    qcc_printf("Source directory: %s\n", qccmsourcedir);
  } else {
    *qccmsourcedir = '\0';
  }
  QCC_InitData();
  QCC_PR_BeginCompilation((void *)qccHunkAlloc(0x100000), 0x100000);
  if (!numsourcefiles) {
    p = QCC_CheckParm("-qc");
    if (!p || p >= argc-1 || argv[p+1][0] == '-') p = QCC_CheckParm("-srcfile");
    if (p && p < argc-1) {
      snprintf(qccmprogsdat, sizeof(qccmprogsdat), "%s", argv[p+1]);
    } else {
      // look for a preprogs.src... :o)
      snprintf(qccmprogsdat, sizeof(qccmprogsdat), "preprogs.src");
      if (qcc_pf_externs->FileSize(qccmprogsdat) <= 0) snprintf(qccmprogsdat, sizeof(qccmprogsdat), "progs.src");
    }
    numsourcefiles = 0;
    strcpy(sourcefileslist[numsourcefiles++], qccmprogsdat);
    currentsourcefile = 0;
  } else if (currentsourcefile == numsourcefiles) {
    // no more
    qcc_compileactive = qcc_false;
    numsourcefiles = 0;
    currentsourcefile = 0;
    return;
  }
  if (currentsourcefile) qcc_printf("-------------------------------------\n");
  snprintf(qccmprogsdat, sizeof(qccmprogsdat), "%s%s", qccmsourcedir, sourcefileslist[currentsourcefile++]);
  qcc_printf("Source file: %s\n", qccmprogsdat);
  if (QCC_LoadFile(qccmprogsdat, (void *)&qccmsrc) == -1) return;
  if (QCC_CheckParm("-wasm")) writeasm = 1;
#ifdef QCC_WRITEASM
  if (writeasm) {
    asmfile = fopen("qc.asm", "wb");
    if (!asmfile) QCC_Error(ERR_INTERNAL, "Couldn't open file for asm output.");
  }
#endif
  newstylesource = qcc_false;
  while (*qccmsrc && isspace(*qccmsrc)) ++qccmsrc;
  pr_file_p = QCC_COM_Parse(qccmsrc);
  if (QCC_CheckParm("-qc")) {
    strcpy(destfile, qccmprogsdat);
    StripExtension(destfile);
    strcat(destfile, ".qco");
    p = QCC_CheckParm ("-o");
    if (!p || p >= argc-1 || argv[p+1][0] == '-') {
      if (p && p < argc-1) snprintf(destfile, sizeof(destfile), "%s%s", qccmsourcedir, argv[p+1]);
    }
    goto newstyle;
  }
  if (*qcc_token == '#') {
newstyle:
    newstylesource = qcc_true;
    StartNewStyleCompile();
    return;
  }

  pr_file_p = qccmsrc;
  QCC_PR_LexWhitespace();
  qccmsrc = pr_file_p;

  s = qccmsrc;
  pr_file_p = qccmsrc;
  QCC_PR_SimpleGetToken();
  strcpy(qcc_token, pr_token);
  qccmsrc = pr_file_p;

  if (!qccmsrc) QCC_Error(ERR_NOOUTPUT, "No destination filename.  qcc -help for info.");
  strcpy(destfile, qcc_token);

#ifndef QCCONLY
  p = 0;
  s2 = strcpy(destfile2, destfile);
  if (!strncmp(s2, "./", 2)) {
    s2 += 2;
  } else {
    while (!strncmp(s2, "../", 3)) { s2 += 3; ++p; }
  }
  strcpy(qccmfilename, qccmsourcedir);
  for (s = qccmfilename+strlen(qccmfilename); p && s >= qccmfilename; --s) {
    if (*s == '/' || *s == '\\') {
      *(s+1) = '\0';
      --p;
    }
  }
  snprintf(destfile, sizeof(destfile), "%s", s2);
  while (p > 0) {
    memmove(destfile+3, destfile, strlen(destfile)+1);
    destfile[0] = '.';
    destfile[1] = '.';
    destfile[2] = '/';
    --p;
  }
#endif
  qcc_printf("outputfile: %s\n", destfile);
  pr_dumpasm = qcc_false;
  currentchunk = NULL;
}


void new_QCC_ContinueCompile (void) {
  if (setjmp(pr_parse_abort)) {
    //if (pr_error_count != 0)
    {
      QCC_Error(ERR_PARSEERRORS, "Errors have occured");
      return;
    }
    QCC_PR_SkipToSemicolon();
    if (pr_token_type == TT_EOF) return;
  }
  if (pr_token_type == TT_EOF) {
    if (pr_error_count) QCC_Error(ERR_PARSEERRORS, "Errors have occured");
    QCC_FinishCompile();
    PostCompile();
    if (!PreCompile()) return;
    QCC_main(myargc, myargv);
    return;
  }
  pr_scope = NULL; // outside all functions
  QCC_PR_ParseDefs(0);
}


// called between exe frames - won't loose net connection (is the theory)...
void QCC_ContinueCompile (void) {
  char *s, *s2;
  currentchunk = NULL;
  if (!qcc_compileactive) return; //HEY!
  if (newstylesource) {
    new_QCC_ContinueCompile();
    return;
  }
  qccmsrc = QCC_COM_Parse(qccmsrc);
  if (!qccmsrc) {
    QCC_FinishCompile();
    PostCompile();
    if (!PreCompile()) return;
    QCC_main(myargc, myargv);
    return;
  }
  s = qcc_token;
  strcpy(qccmfilename, qccmsourcedir);
  for (;;) {
    if (!strncmp(s, "..\\", 3) || !strncmp(s, "../", 3)) {
      s2 = qccmfilename+strlen(qccmfilename)-2;
      while (s2 >= qccmfilename) {
        if (*s2 == '/' || *s2 == '\\') { s2[1] = '\0'; break; }
        --s2;
      }
      if (s2>=qccmfilename) { s += 3; continue; }
    }
    if (strncmp(s, ".\\", 2) == 0 || strncmp(s, "./", 2) == 0) { s += 2; continue; }
    break;
  }
  strcat(qccmfilename, s);
  qcc_printf("compiling %s\n", qccmfilename);
  QCC_LoadFile(qccmfilename, (void *)&qccmsrc2);
  if (!QCC_PR_CompileFile(qccmsrc2, qccmfilename)) QCC_Error(ERR_PARSEERRORS, "Errors have occured\n");
}


void QCC_FinishCompile (void) {
  qccbool donesomething;
  int crc;
  currentchunk = NULL;
  if (setjmp(pr_parse_abort)) QCC_Error(ERR_INTERNAL, "internal error");
  if (!QCC_PR_FinishCompilation()) QCC_Error(ERR_PARSEERRORS, "compilation errors");
  if (pr_werror && pr_warning_count != 0) QCC_Error(ERR_PARSEERRORS, "compilation errors");
  // write progdefs.h
  crc = QCC_PR_WriteProgdefs("progdefs.h");
  // write data file
  donesomething = QCC_WriteData(crc);
  if (donesomething) {
    if (verbose) {
      qcc_printf("Compile Complete\n\n");
      if (optres_shortenifnots) qcc_printf("optres_shortenifnots %d\n", optres_shortenifnots);
      if (optres_overlaptemps) qcc_printf("optres_overlaptemps %d\n", optres_overlaptemps);
      if (optres_noduplicatestrings) qcc_printf("optres_noduplicatestrings %d\n", optres_noduplicatestrings);
      if (optres_constantarith) qcc_printf("optres_constantarith %d\n", optres_constantarith);
      if (optres_nonvec_parms) qcc_printf("optres_nonvec_parms %d\n", optres_nonvec_parms);
      if (optres_constant_names) qcc_printf("optres_constant_names %d\n", optres_constant_names);
      if (optres_constant_names_strings) qcc_printf("optres_constant_names_strings %d\n", optres_constant_names_strings);
      if (optres_assignments) qcc_printf("optres_assignments %d\n", optres_assignments);
      if (optres_unreferenced) qcc_printf("optres_unreferenced %d\n", optres_unreferenced);
      if (optres_locals) qcc_printf("optres_locals %d\n", optres_locals);
      if (optres_dupconstdefs) qcc_printf("optres_dupconstdefs %d\n", optres_dupconstdefs);
      if (optres_compound_jumps) qcc_printf("optres_compound_jumps %d\n", optres_compound_jumps);
      if (optres_locals_marshalling) qcc_printf("optres_locals_marshalling %d\n", optres_locals_marshalling);
      qcc_printf("numtemps %d\n", numtemps);
    }
    qcc_printf("%d warnings\n", pr_warning_count);
  }
  qcc_compileactive = qcc_false;
  //
  QCC_Clear_Memory(1);
}


static void StartNewStyleCompile (void) {
  if (setjmp(pr_parse_abort)) {
    if (++pr_error_count > MAX_ERRORS) return;
    if (setjmp(pr_parse_abort)) return;
    QCC_PR_SkipToSemicolon();
    if (pr_token_type == TT_EOF) return;
  }
  compilingfile = qccmprogsdat;
  pr_file_p = qccmsrc;
  s_file = s_file2 = QCC_CopyString(compilingfile);
  pr_source_line = 0;
  QCC_PR_NewLine(qcc_false);
  QCC_PR_Lex(); // read first token
}
