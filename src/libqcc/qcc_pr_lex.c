/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <time.h>

#include "qcc.h"


extern char qccmsourcedir[];

static void QCC_PR_ConditionCompilation (void);
static qccbool QCC_PR_UndefineName (const char *name);
static const char *QCC_PR_EatCompConstString (const char *def);

static int QCC_PR_LexInteger (void);
static void QCC_PR_LexString (void);

static void *errorscope;

qccbool defaultnoref;
qccbool defaultstatic;
int ForcedCRC;
int recursivefunctiontype;

const char *compilingfile;
int pr_source_line;
/*const*/ char *pr_file_p;
static const char *pr_line_start;   // start of current source line
int pr_bracelevel;
char pr_token[QCC_MAX_TOKEN_SIZE+1];
token_type_t pr_token_type;
QCC_type_t *pr_immediate_type;
QCC_eval_t pr_immediate;
char pr_immediate_string[8192];
int pr_error_count;
int pr_warning_count;
CompilerConstant_t *CompilerConstant;
qccbool type_inlinefunction;


static const struct {
  const char *old;
  const char *new;
} pr_premap[] = {
  {"(+)", "|="},
  {"(-)", "&~="},
  {"->", "."},
};


// longer symbols must be before a shorter partial match
static const char *pr_punctuation[] = {
  "(+)",
  "(-)",
  "&&",
  "||",
  "&~=",
  "<=",
  ">=",
  "==",
  "!=",
  "/=",
  "*=",
  "+=",
  "-=",
  "|=",
  "++",
  "--",
  "->",
  "::",
  "[[",
  "]]",
  ";",
  ",",
  "!",
  "*",
  "/",
  "(",
  ")",
  "-",
  "+",
  "=",
  "[",
  "]",
  "{",
  "}",
  "...",
  "..",
  ".",
  "<<",
  "<",
  ">>",
  ">" ,
  "?",
  "#" ,
  "@",
  "&" ,
  "|",
  "^",
  ":",
};


// simple types.  function types are dynamically allocated
QCC_type_t *type_void;// = {ev_void/*, &def_void*/};
QCC_type_t *type_string;// = {ev_string/*, &def_string*/};
QCC_type_t *type_float;// = {ev_float/*, &def_float*/};
QCC_type_t *type_vector;// = {ev_vector/*, &def_vector*/};
QCC_type_t *type_entity;// = {ev_entity/*, &def_entity*/};
QCC_type_t *type_field;// = {ev_field/*, &def_field*/};
QCC_type_t *type_function;// = {ev_function/*, &def_function*/,NULL,&type_void};
// type_function is a void() function used for state defs
QCC_type_t *type_pointer;// = {ev_pointer/*, &def_pointer*/};
QCC_type_t *type_integer;// = {ev_integer/*, &def_integer*/};

QCC_type_t *type_floatfield;// = {ev_field/*, &def_field*/, NULL, &type_float};

QCC_def_t def_ret, def_parms[MAX_PARMS];


#include "qcc_pr_preproc.c"


/*
==============
PR_NewLine

Call at start of file and when *pr_file_p == '\n'
==============
*/
void QCC_PR_NewLine (qccbool incomment) {
  ++pr_source_line;
  pr_line_start = pr_file_p;
  if (incomment) return; // no constants if in a comment
  while (*pr_file_p && *pr_file_p != '\n' && isspace(*pr_file_p)) ++pr_file_p;
  QCC_PR_Preprocessor();
}


/*
==============
PR_LexString

Parses a quoted string
==============
*/
static void QCC_PR_LexString (void) {
  int len = 0;
  char *end;
  const char *cnst;
  int texttype = 0;
  ++pr_file_p; // skip quote
  for (;;) {
    int c = (unsigned char)(*pr_file_p), d;
    ++pr_file_p;
    if (!c) QCC_PR_ParseError(ERR_EOF, "EOF inside quote");
    if (c == '\n') QCC_PR_ParseError(ERR_INVALIDSTRINGIMMEDIATE, "newline inside quote");
    if (c == '\\') {
      // escaped char
      c = (unsigned char)(*pr_file_p);
      ++pr_file_p;
      if (!c) QCC_PR_ParseError(ERR_EOF, "EOF inside quote");
      switch (c) {
        case 'n': c = '\n'; break;
        case 'r': c = '\r'; break;
        case '"': c = '"'; break;
        case 't': c = '\t'; break;
        case 'a': c = '\a'; break;
        case 'v': c = '\v'; break;
        case 'f': c = '\f'; break;
        case 'e': c = '\x1b'; break;
        case 's': case 'b': texttype ^= 128; continue;
        case '[': c = 16; break;
        case ']': c = 17; break;
        case '{':
          c = 0;
          while ((d = *pr_file_p++) != '}') {
            c = c*10+d-'0';
            if (d < '0' || d > '9' || c > 255) QCC_PR_ParseError(ERR_BADCHARACTERCODE, "Bad character code");
          }
          break;
        case '<': c = 29; break;
        case '-': c = 30; break;
        case '>': c = 31; break;
        case 'x': case 'X':
          c = 0;
          d = (unsigned char)*pr_file_p++;
          if (d >= '0' && d <= '9') c += d-'0';
          else if (d >= 'A' && d <= 'F') c += d-'A'+10;
          else if (d >= 'a' && d <= 'f') c += d-'a'+10;
          else QCC_PR_ParseError(ERR_BADCHARACTERCODE, "Bad character code");
          c *= 16;
          d = (unsigned char)*pr_file_p++;
          if (d >= '0' && d <= '9') c += d-'0';
          else if (d >= 'A' && d <= 'F') c += d-'A'+10;
          else if (d >= 'a' && d <= 'f') c += d-'a'+10;
          else QCC_PR_ParseError(ERR_BADCHARACTERCODE, "Bad character code");
          break;
        case '\\': c = '\\'; break;
        case '\'': c = '\''; break;
        //FIXME
        case '0' ... '9':
          c = 18+c-'0';
          break;
        case '\r': QCC_PR_ParseError(0, "DIE!");
        case '\n': ++pr_source_line; break;
        default: QCC_PR_ParseError(ERR_INVALIDSTRINGIMMEDIATE, "Unknown escape char %c", c);
      }
    } else if (c == '"') {
      if (len >= sizeof(pr_immediate_string)-1) {
        QCC_Error(ERR_INVALIDSTRINGIMMEDIATE, "String length exceeds %d", sizeof(pr_immediate_string)-1);
      }
      while (*pr_file_p && *pr_file_p <= ' ') {
        if (*pr_file_p == '\n') {
          ++pr_file_p;
          QCC_PR_NewLine(qcc_false);
        } else {
          ++pr_file_p;
        }
      }
      if (*pr_file_p == '"') {
        // have annother go
        ++pr_file_p;
        continue;
      }
      pr_token[len] = 0;
      pr_token_type = TT_IMMED;
      pr_immediate_type = type_string;
      strcpy (pr_immediate_string, pr_token);
      return;
    } else if (c == '#') {
      for (end = pr_file_p; ; ++end) {
        if (isspace(*end) <= ' ') break;
        if (strchr("()+-*/\\|&=^~[]\"{};:,.#", *end)) break;
      }
      c = *end;
      *end = '\0';
      cnst = QCC_PR_EatCompConstString(pr_file_p);
      *end = c;
      c = '#'; // undo
      if (cnst) {
        QCC_PR_ParseWarning(WARN_MACROINSTRING, "Macro expansion in string");
        if (len+strlen(cnst) >= sizeof(pr_token)-1) QCC_Error(ERR_INVALIDSTRINGIMMEDIATE, "String length exceeds %d", sizeof(pr_token)-1);
        strcpy(pr_token+len, cnst);
        len += strlen(cnst);
        pr_file_p = end;
        continue;
      }
    } else {
      c |= texttype;
    }
    pr_token[len] = c;
    ++len;
    if (len >= sizeof(pr_token)-1) QCC_Error(ERR_INVALIDSTRINGIMMEDIATE, "String length exceeds %d", sizeof(pr_token)-1);
  }
}


static inline int digit (int c, int base) {
  if (c >= 'a' && c <= 'z') c -= 32; // convert to upper case
  if (c < '0' || (c > '9' && c < 'A') || c > 'Z') return -1;
  if (c > '9') c -= 7; // normalize
  c -= '0';
  return (c >= 0 && c < base ? c : -1);
}


/*
==============
PR_LexInteger
==============
*/
static int QCC_PR_LexInteger (void) {
  int base = 10, len = 0, c = *pr_file_p;
  if (pr_file_p[0] == '0' && (pr_file_p[1] == 'x' || pr_file_p[1] == 'X')) {
    pr_token[0] = '0';
    pr_token[1] = 'x';
    len = 2;
    base = 16;
    c = *(pr_file_p += 2);
  }
  if (digit(c, base) < 0) QCC_Error(ERR_NOTANUMBER, "Invalid number");
  do {
    if (len >= sizeof(pr_token)-1) QCC_Error(ERR_NOTANUMBER, "Number too long");
    pr_token[len++] = c;
    c = *(++pr_file_p);
  } while (c && digit(c, base) >= 0);
  if (c && (isalpha(c) || c == '_' || c == '.')) QCC_Error(ERR_NOTANUMBER, "Invalid number");
  pr_token[len] = 0;
  return strtol(pr_token, NULL, base);
}


/*
==============
PR_LexNumber
==============
*/
static void QCC_PR_LexNumber (void) {
  int tokenlen = 0, num = 0, base = 10, sign = 1, c;
  if (*pr_file_p == '-') {
    sign = -1;
    ++pr_file_p;
    pr_token[tokenlen++] = '-';
  }
  if (pr_file_p[0] == '0' && (pr_file_p[1] == 'x' || pr_file_p[1] == 'X')) {
    pr_file_p += 2;
    base = 16;
    pr_token[tokenlen++] = '0';
    pr_token[tokenlen++] = 'x';
  }
  pr_immediate_type = NULL;
  while ((c = *pr_file_p)) {
    if (digit(c, base) >= 0) {
      if (tokenlen >= sizeof(pr_token)-1) QCC_Error(ERR_NOTANUMBER, "Number too long");
      pr_token[tokenlen++] = c;
      num = num*base+digit(c, base);
      ++pr_file_p;
      continue;
    }
    if (c == '.') {
      if (base != 10) QCC_Error(ERR_NOTANUMBER, "No hex floats yet");
      if (tokenlen >= sizeof(pr_token)-1) QCC_Error(ERR_NOTANUMBER, "Number too long");
      pr_token[tokenlen++] = c;
      ++pr_file_p;
      pr_immediate_type = type_float;
      for (;;) {
        c = *pr_file_p++;
        if (c >= '0' && c <= '9') {
          if (tokenlen >= sizeof(pr_token)-1) QCC_Error(ERR_NOTANUMBER, "Number too long");
          pr_token[tokenlen++] = c;
          continue;
        }
        if (c != 'f') --pr_file_p;
        break;
      }
      pr_token[tokenlen++] = 0;
      pr_immediate._float = (float)atof(pr_token);
      return;
    }
    if (c == 'i') {
      if (tokenlen >= sizeof(pr_token)-1) QCC_Error(ERR_NOTANUMBER, "Number too long");
      pr_token[tokenlen++] = c;
      pr_token[tokenlen++] = 0;
      ++pr_file_p;
      pr_immediate_type = type_integer;
      pr_immediate._int = num*sign;
      return;
    }
    break;
  }
  pr_token[tokenlen++] = 0;
  if (c && (isalpha(c) || c == '_')) QCC_Error(ERR_NOTANUMBER, "Invalid number");
  if (!pr_immediate_type) pr_immediate_type = (flag_assume_integer ? type_integer : type_float);
  if (pr_immediate_type == type_integer) {
    pr_immediate_type = type_integer;
    pr_immediate._int = num*sign;
  } else {
    pr_immediate_type = type_float;
    // at this point, we know there's no '.' in it, so the NaN bug shouldn't happen
    // and we cannot use atof on tokens like 0xabc, so use num*sign, it SHOULD be safe
    //pr_immediate._float = atof(pr_token);
    pr_immediate._float = ((float)num)*sign;
  }
}


/*
==============
PR_LexFloat
==============
*/
static float QCC_PR_LexFloat (void) {
  int len = 0, wasdot = 0, c = *pr_file_p;
  do {
    if (c == '.') if (wasdot++) QCC_Error(ERR_NOTANUMBER, "Invalid float number");
    if (len >= sizeof(pr_token)-1) QCC_Error(ERR_NOTANUMBER, "Number too long");
    pr_token[len++] = c;
    c = *(++pr_file_p);
  } while (c && ((c >= '0' && c <= '9') || c == '.'));
  pr_token[len] = 0;
  return (float)atof(pr_token);
}


/*
==============
PR_LexVector

Parses a single quoted vector
==============
*/
static void QCC_PR_LexVector (void) {
  ++pr_file_p;
  if (*pr_file_p == '\\') {
    // extended character constant
    pr_token_type = TT_IMMED;
    pr_immediate_type = type_float;
    ++pr_file_p;
    switch (*pr_file_p) {
      case 'n': pr_immediate._float = '\n'; break;
      case 'r': pr_immediate._float = '\r'; break;
      case 't': pr_immediate._float = '\t'; break;
      case '\'': pr_immediate._float = '\''; break;
      case '\"': pr_immediate._float = '\"'; break;
      case '\\': pr_immediate._float = '\\'; break;
      default: QCC_PR_ParseError(ERR_INVALIDVECTORIMMEDIATE, "Bad character constant");
    }
    if (*pr_file_p != '\'') QCC_PR_ParseError(ERR_INVALIDVECTORIMMEDIATE, "Bad character constant");
    ++pr_file_p;
    return;
  }
  if (pr_file_p[1] == '\'') {
    // character constant
    pr_token_type = TT_IMMED;
    pr_immediate_type = type_float;
    pr_immediate._float = pr_file_p[0];
    pr_file_p += 2;
    return;
  }
  pr_token_type = TT_IMMED;
  pr_immediate_type = type_vector;
  QCC_PR_LexWhitespace ();
  for (int i = 0 ; i < 3; ++i) {
    pr_immediate.vector[i] = QCC_PR_LexFloat();
    QCC_PR_LexWhitespace();
    if (*pr_file_p == '\'' && i == 1) {
      if (i < 2) QCC_PR_ParseWarning(WARN_D2DQCC_SPECIFIC, "Bad vector");
      for (++i; i < 3 ; ++i) pr_immediate.vector[i] = 0;
      break;
    }
  }
  if (*pr_file_p != '\'') QCC_PR_ParseError(ERR_INVALIDVECTORIMMEDIATE, "Bad vector");
  ++pr_file_p;
}


/*
==============
PR_LexId

Parses an identifier
==============
*/
static void QCC_PR_LexId (void) {
  int len = 0, c = *pr_file_p;
  pr_token_type = TT_ID;
  do {
    if (len >= sizeof(pr_token)-1) QCC_Error(ERR_IDTOOLONG, "Identifier too long");
    pr_token[len++] = c;
    c = *(++pr_file_p);
  } while ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_' || (c >= '0' && c <= '9'));
  pr_token[len] = 0;
}


/*
==============
PR_LexDelimiter
==============
*/
static void QCC_PR_LexDelimiter (void) {
  pr_token_type = TT_DELIM;
  for (size_t f = 0; f < sizeof(pr_punctuation)/sizeof(pr_punctuation[0]); ++f) {
    const char *p = pr_punctuation[f];
    int len = strlen(p);
    if (strncmp(p, pr_file_p, len) == 0) {
      const char *tk = p;
      for (size_t c = 0; c < sizeof(pr_premap)/sizeof(pr_premap[0]); ++c) {
        if (strcmp(p, pr_premap[c].old) == 0) { tk = pr_premap[c].new; break; }
      }
      strcpy(pr_token, tk);
      if (p[0] == '{') ++pr_bracelevel; else if (p[0] == '}') --pr_bracelevel;
      pr_file_p += len;
      return;
    }
  }
  QCC_PR_ParseError(ERR_UNKNOWNPUCTUATION, "Unknown punctuation");
}


/*
==============
PR_LexWhitespace
==============
*/
void QCC_PR_LexWhitespace (void) {
  int c;
  while ((c = (unsigned char)(*pr_file_p))) {
    // skip whitespace
    if (c <= 32) {
      ++pr_file_p;
      if (c == '\n') QCC_PR_NewLine(qcc_false);
      continue;
    }
    // skip comments
    if (c == '/') {
      switch (pr_file_p[1]) {
        case '/': // skip c++ comments
          while (*pr_file_p && *pr_file_p != '\n') ++pr_file_p;
          if (*pr_file_p) ++pr_file_p; // don't break on eof
          QCC_PR_NewLine(qcc_false);
          continue;
        case '*': // skip c comments
          pr_file_p += 2;
          while (pr_file_p[0] != '*' || pr_file_p[1] != '/') {
            if (pr_file_p[0] == 0) { QCC_PR_ParseError(0, "EOF inside comment"); return; }
            if (*pr_file_p++ == '\n') QCC_PR_NewLine(qcc_true);
          }
          pr_file_p += 2;
          continue;
      }
    }
    break; // a real character has been found
  }
}


//============================================================================
// just parses text, returning qcc_false if an eol is reached
qccbool QCC_PR_SimpleGetToken (void) {
  int c, i;
  pr_token[0] = 0;
  // skip whitespace
  while ((c = (unsigned char)*pr_file_p) <= ' ') {
    if (c == '\n' || c == 0) return qcc_false;
    ++pr_file_p;
  }
  if (pr_file_p[0] == '/') {
    if (pr_file_p[1] == '/') {
      // comment alert
      while (*pr_file_p && *pr_file_p != '\n') ++pr_file_p;
      return qcc_false;
    }
    if (pr_file_p[1] == '*') return qcc_false;
  }
  i = 0;
  while ((c = (unsigned char)*pr_file_p) > ' ' && strchr(",;()\x5d", c) == NULL) {
    if (i >= sizeof(pr_token)-1) QCC_PR_ParseError(0, "Simple token too long");
    pr_token[i++] = c;
    ++pr_file_p;
  }
  pr_token[i] = 0;
  return (i != 0);
}


/*
==============
PR_Lex

Sets pr_token, pr_token_type, and possibly pr_immediate and pr_immediate_type
==============
*/
void QCC_PR_Lex (void) {
  int c;
again:
  pr_token[0] = 0;
  if (pr_file_p == NULL) {
    if (QCC_PR_UnInclude()) goto again; // tail call emulation
    pr_token_type = TT_EOF;
    return;
  }
  QCC_PR_LexWhitespace();
  if (pr_file_p == NULL) {
    if (QCC_PR_UnInclude()) goto again; // tail call emulation
    pr_token_type = TT_EOF;
    return;
  }
  c = *pr_file_p;
  if (!c) {
    if (QCC_PR_UnInclude()) goto again; // tail call emulation
    pr_token_type = TT_EOF;
    return;
  }
  // handle quoted strings as a unit
  if (c == '\"') { QCC_PR_LexString(); return; }
  // handle quoted vectors as a unit
  if (c == '\'') { QCC_PR_LexVector(); return; }
  // if the first character is a valid identifier, parse until a non-id character is reached
  if (c == '0' && (pr_file_p[1] == 'x' || pr_file_p[1] == 'X')) {
    pr_token_type = TT_IMMED;
    QCC_PR_LexNumber();
    return;
  }
  if (((c == '.' || c == '-') && pr_file_p[1] >= '0' && pr_file_p[1] <= '9') || (c >= '0' && c <= '9')) {
    pr_token_type = TT_IMMED;
    QCC_PR_LexNumber();
    return;
  }
  if (c == '#') {
    if (pr_file_p[1] != '-' && !(pr_file_p[1] >= '0' || pr_file_p[1] <= '9')) {
      //hash and not number
      //fprintf(stderr, "hashnum\n===\n%s\n===\n", pr_file_p);
      ++pr_file_p;
      if (!QCC_PR_EatCompConst()) {
        if (!QCC_PR_SimpleGetToken()) strcpy(pr_token, "unknown");
        QCC_PR_ParseError(ERR_CONSTANTNOTDEFINED, "Explicit precompiler usage when not defined %s", pr_token);
      } else {
        if (pr_token_type == TT_EOF) QCC_PR_Lex();
      }
      return;
    }
  }
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_') {
    if (!QCC_PR_EatCompConst()) {
      // look for a macro
      QCC_PR_LexId();
    } else if (pr_token_type == TT_EOF) {
      if (QCC_PR_UnInclude()) {
        QCC_PR_Lex();
        return;
      }
      pr_token_type = TT_EOF;
    }
    return;
  }
  // parse symbol strings until a non-symbol is found
  QCC_PR_LexDelimiter();
}


//=============================================================================
qccbool PR_Lex_IsTypeName (const char *tk) {
  return
    !strcmp("string", pr_token) ||
    !strcmp("float", pr_token) ||
    !strcmp("entity", pr_token) ||
    !strcmp("vector", pr_token) ||
    !strcmp("int", pr_token);
}


//=============================================================================
void QCC_PR_ParsePrintDef (int type, QCC_def_t *def) {
  if (qccwarningdisabled[type]) return;
  if (def->s_file) qcc_printf("%s:%d:    %s  is defined here\n", strings+def->s_file, def->s_line, def->name);
}


static void QCC_PR_PrintScope (void) {
  if (pr_scope) {
    if (errorscope != pr_scope) qcc_printf("in function %s (line %d),\n", pr_scope->name, pr_scope->s_line);
    errorscope = pr_scope;
  } else {
    if (errorscope) qcc_printf("at global scope,\n");
    errorscope = NULL;
  }
}


void QCC_PR_ResetErrorScope (void) {
  errorscope = NULL;
}


/*
============
PR_ParseError

Aborts the current file load
============
*/
void QCC_PR_ParseError (int errortype, const char *error, ...) {
  va_list argptr;
  char string[1024];
  va_start(argptr,error);
  vsnprintf(string,sizeof(string)-1, error,argptr);
  va_end(argptr);
  qcc_set_badfile_name(strings+s_file, pr_source_line);
  QCC_PR_PrintScope();
  qcc_printf("%s:%d: error: %s\n", strings + s_file, pr_source_line, string);
  longjmp(pr_parse_abort, 1);
}


void QCC_PR_ParseErrorPrintDef (int errortype, QCC_def_t *def, const char *error, ...) {
  va_list argptr;
  char string[1024];
  va_start(argptr,error);
  vsnprintf(string,sizeof(string)-1, error,argptr);
  va_end(argptr);
  qcc_set_badfile_name(strings+s_file, pr_source_line);
  QCC_PR_PrintScope();
  qcc_printf("%s:%d: error: %s\n", strings + s_file, pr_source_line, string);
  QCC_PR_ParsePrintDef(WARN_ERROR, def);
  longjmp(pr_parse_abort, 1);
}


void QCC_PR_ParseWarning (int type, const char *error, ...) {
  va_list argptr;
  char string[1024];
  if (type < ERR_PARSEERRORS && qccwarningdisabled[type]) return;
  va_start(argptr,error);
  vsnprintf(string,sizeof(string)-1, error,argptr);
  va_end(argptr);
  QCC_PR_PrintScope();
  if (type >= ERR_PARSEERRORS) {
    qcc_printf("%s:%d: error: %s\n", strings + s_file, pr_source_line, string);
    ++pr_error_count;
  } else {
    qcc_printf("%s:%d: warning: %s\n", strings + s_file, pr_source_line, string);
    ++pr_warning_count;
  }
}


void QCC_PR_Note (int type, const char *file, int line, const char *error, ...) {
  va_list argptr;
  char string[1024];
  if (qccwarningdisabled[type]) return;
  va_start(argptr,error);
  vsnprintf(string,sizeof(string)-1, error,argptr);
  va_end(argptr);
  QCC_PR_PrintScope();
  if (file) {
    qcc_printf("%s:%d: note: %s\n", file, line, string);
  } else {
    qcc_printf("note: %s\n", string);
  }
}


qccbool QCC_PR_Warning (int type, const char *file, int line, const char *error, ...) {
  va_list argptr;
  char string[1024];
  if (qccwarningdisabled[type]) return qcc_false;
  va_start(argptr,error);
  vsnprintf(string,sizeof(string)-1, error,argptr);
  va_end(argptr);
  QCC_PR_PrintScope();
  if (file) {
    qcc_printf("%s:%d: warning: %s\n", file, line, string);
  } else {
    qcc_printf("warning: %s\n", string);
  }
  ++pr_warning_count;
  return qcc_true;
}


/*
=============
PR_Expect

Issues an error if the current token isn't equal to string
Gets the next token
=============
*/
void QCC_PR_Expect (const char *string) {
  if (strcmp(string, pr_token) != 0) QCC_PR_ParseError(ERR_EXPECTED, "expected %s, found %s", string, pr_token);
  QCC_PR_Lex();
}


/*
=============
PR_Check

Returns qcc_true and gets the next token if the current token equals string
Returns qcc_false and does nothing otherwise
=============
*/
qccbool QCC_PR_EatDelim (const char *string) {
  if (pr_token_type != TT_DELIM) return qcc_false;
  if (strcmp(string, pr_token) != 0) return qcc_false;
  QCC_PR_Lex();
  return qcc_true;
}


qccbool QCC_PR_EatImmediate (const char *string) {
  if (pr_token_type != TT_IMMED) return qcc_false;
  if (strcmp(string, pr_token) != 0) return qcc_false;
  QCC_PR_Lex();
  return qcc_true;
}


qccbool QCC_PR_EatId (const char *string) {
  if (pr_token_type != TT_ID) return qcc_false;
  if (strcmp(string, pr_token) != 0) return qcc_false;
  QCC_PR_Lex();
  return qcc_true;
}


qccbool QCC_PR_EatKeyword (const char *string) {
  if (strcmp(string, pr_token) != 0) return qcc_false;
  QCC_PR_Lex();
  return qcc_true;
}


/*
============
PR_ParseName

Checks to see if the current token is a valid name
============
*/
char *QCC_PR_ParseName (void) {
  static char ident[MAX_NAME];
  char *ret;
  if (pr_token_type != TT_ID) {
    if (pr_token_type == TT_EOF) {
      QCC_PR_ParseError(ERR_EOF, "unexpected EOF");
    } else {
      QCC_PR_ParseError(ERR_NOTANAME, "\"%s\" - not a name", pr_token);
    }
  }
  if (strlen(pr_token) >= MAX_NAME-1) QCC_PR_ParseError(ERR_NAMETOOLONG, "name too long");
  strcpy(ident, pr_token);
  QCC_PR_Lex();
  ret = qccHunkDupStr(ident);
  return ret;
}


/*
============
PR_FindType

Returns a preexisting complex type that matches the parm, or allocates
a new one and copies it out.
============
*/

//0 if same
//QCC_type_t *QCC_PR_NewType (char *name, int basictype);
int qcc_typecmp (const QCC_type_t *a, const QCC_type_t *b) {
  if (a == b) return 0;
  if (!a || !b) return 1; //different (^ and not both null)
  if (a->type != b->type) return 1;
  if (a->num_parms != b->num_parms) return 1;
  if (a->size != b->size) return 1;
  //if (strcmp(a->name, b->name)) return 1; //This isn't 100% clean.
  if (qcc_typecmp(a->aux_type, b->aux_type)) return 1;
  if (a->param || b->param) {
    a = a->param;
    b = b->param;
    while (a || b) {
      if (qcc_typecmp(a, b)) return 1;
      a = a->next;
      b = b->next;
    }
  }
  return 0;
}


static QCC_type_t *QCC_PR_DuplicateType (QCC_type_t *in) {
  QCC_type_t *out, *ip, *plast;
  if (!in) return NULL;
  out = QCC_PR_NewType(in->name, in->type);
  out->aux_type = QCC_PR_DuplicateType(in->aux_type);
  // copy params
  plast = out->param = NULL;
  for (ip = in->param; ip != NULL; ip = ip->next) {
    QCC_type_t *op = QCC_PR_DuplicateType(ip);
    if (plast != NULL) plast->next = op; else out->param = op;
    plast = op;
  }
  //
  out->num_parms = in->num_parms;
  out->ofs = in->ofs;
  out->size = in->size;
  out->arraysize = in->arraysize;
  out->name = in->name;
  return out;
}


const char *QCC_TypeName (QCC_type_t *type) {
  static char buffer[2][512];
  static int op;
  char *ret;
  op++;
  ret = buffer[op&1];
  if (type->type == ev_field) {
    type = type->aux_type;
    *ret++ = '.';
  }
  *ret = 0;
  if (type->type == ev_function) {
    qccbool varg = type->num_parms < 0;
    int args = type->num_parms;
    if (args < 0) args = -(args+1);
    strcat(ret, type->aux_type->name);
    strcat(ret, " (");
    type = type->param;
    while (type) {
      if (args <= 0) strcat(ret, "optional ");
      --args;
      strcat(ret, type->name);
      type = type->next;
      if (type || varg) strcat(ret, ", ");
    }
    if (varg) strcat(ret, "...");
    strcat(ret, ")");
  } else {
    strcpy(ret, type->name);
  }
  return buffer[op&1];
}


QCC_type_t *QCC_PR_FindType (QCC_type_t *type) {
  for (int t = 0; t < numtypeinfos; ++t) if (qcc_typecmp(&qcc_typeinfo[t], type) == 0) return &qcc_typeinfo[t];
  QCC_Error(ERR_INTERNAL, "Error with type");
  return type;
}


QCC_type_t *QCC_TypeForName (const char *name) {
  for (int i = 0; i < numtypeinfos; ++i) if (!strcmp(qcc_typeinfo[i].name, name)) return &qcc_typeinfo[i];
  return NULL;
}


/*
============
PR_SkipToSemicolon

For error recovery, also pops out of nested braces
============
*/
void QCC_PR_SkipToSemicolon (void) {
  do {
    if (!pr_bracelevel && QCC_PR_EatDelim(";")) return;
    QCC_PR_Lex();
  } while (pr_token_type != TT_EOF);
}


/*
============
PR_ParseType

Parses a variable type, including field and functions types
============
*/
#ifdef MAX_EXTRA_PARMS
char pr_parm_names[MAX_PARMS+MAX_EXTRA_PARMS][MAX_NAME];
#else
char pr_parm_names[MAX_PARMS][MAX_NAME];
#endif


// expects a '(' to have already been parsed
QCC_type_t *QCC_PR_ParseFunctionType (int newtype, QCC_type_t *returntype) {
  QCC_type_t *ftype, *ptype, *nptype;
  char *name;
  int definenames = !recursivefunctiontype;
  int optional = 0, numparms = 0;
  /* */
  ++recursivefunctiontype;
  ftype = QCC_PR_NewType(type_function->name, ev_function);
  ftype->aux_type = returntype; // return type
  ftype->num_parms = 0;
  ptype = NULL;
  if (!QCC_PR_EatDelim(")")) {
    do {
      if (ftype->num_parms >= MAX_PARMS+MAX_EXTRA_PARMS) {
        QCC_PR_ParseError(ERR_TOOMANYTOTALPARAMETERS, "Too many parameters. Sorry. (limit is %d)\n", MAX_PARMS+MAX_EXTRA_PARMS);
      }
      if (QCC_PR_EatDelim("...")) {
        if (optional) numparms = optional-1;
        ftype->num_parms = -numparms-1;
        break;
      }
      if (QCC_PR_EatKeyword("optional")) {
        if (!optional) optional = numparms+1;
      } else if (optional) {
        QCC_PR_ParseWarning(WARN_MISSINGOPTIONAL, "optional not specified on all optional args\n");
      }
      nptype = QCC_PR_ParseType(qcc_true, qcc_false);
      if (nptype->type == ev_void) break;
      if (!ptype) {
        ptype = nptype;
        ftype->param = ptype;
      } else {
        ptype->next = nptype;
        ptype = ptype->next;
      }
      // process thing like 'string...'
      if (QCC_PR_EatDelim("...")) {
        //fprintf(stderr, "typed vaarg: [%s] %d\n", nptype->name, numparms);
        if (optional) numparms = optional-1;
        ftype->num_parms = -numparms-1;
        break;
      }
      //type->name = "FUNC PARAMETER";
      if (strcmp(pr_token, ",") != 0 && strcmp(pr_token, ")") != 0) {
        name = QCC_PR_ParseName();
        if (definenames) strcpy(pr_parm_names[numparms], name);
      } else if (definenames) {
        strcpy(pr_parm_names[numparms], "");
      }
      ++numparms;
      ftype->num_parms = (optional ? optional-1 : numparms);
    } while (QCC_PR_EatDelim(","));
    QCC_PR_Expect(")");
  }
  --recursivefunctiontype;
  if (newtype) return ftype;
  return QCC_PR_FindType(ftype);
}


QCC_type_t *QCC_PR_PointerType (QCC_type_t *pointsto) {
  QCC_type_t  *ptype, *e;
  ptype = QCC_PR_NewType("ptr", ev_pointer);
  ptype->aux_type = pointsto;
  e = QCC_PR_FindType (ptype);
  if (e == ptype) {
    char name[128];
    snprintf(name, sizeof(name), "ptr to %s", pointsto->name);
    e->name = strdup(name);
  }
  return e;
}


/* newtype=qcc_true: creates a new type always
 * silentfail=qcc_true: function is permitted to return NULL if it was not given a type, otherwise never returns NULL
 */
QCC_type_t *QCC_PR_ParseType (int newtype, qccbool silentfail) {
  QCC_type_t *newt, *type;
  const char *name;
  int i;
  type_inlinefunction = qcc_false; // doesn't really matter so long as its not from an inline function type
  if (QCC_PR_EatDelim("..")) {
    // so we don't end up with the user specifying '. .vector blah' (hexen2 added the .. token for array ranges)
    newt = QCC_PR_NewType("FIELD TYPE", ev_field);
    newt->aux_type = QCC_PR_ParseType(qcc_false, qcc_false);
    newt->size = newt->aux_type->size;
    newt = QCC_PR_FindType(newt);
    type = QCC_PR_NewType("FIELD TYPE", ev_field);
    type->aux_type = newt;
    type->size = type->aux_type->size;
    if (newtype) return type;
    return QCC_PR_FindType(type);
  }
  if (QCC_PR_EatDelim(".")) {
    newt = QCC_PR_NewType("FIELD TYPE", ev_field);
    newt->aux_type = QCC_PR_ParseType(qcc_false, qcc_false);
    newt->size = newt->aux_type->size;
    if (newtype) return newt;
    return QCC_PR_FindType(newt);
  }
  name = QCC_PR_EatCompConstString(pr_token);
  type = NULL;
  for (i = 0; i < numtypeinfos; ++i) {
    if (!strcmp(qcc_typeinfo[i].name, name)) {
      type = &qcc_typeinfo[i];
      break;
    }
  }
  if (i == numtypeinfos) {
    if (!*name) return NULL;
    if (silentfail) return NULL;
    QCC_PR_ParseError(ERR_NOTATYPE, "\"%s\" is not a type", name);
  }
  QCC_PR_Lex();
  while (QCC_PR_EatDelim("*")) type = QCC_PointerTypeTo(type);
  if (QCC_PR_EatDelim("(")) {
    // this is followed by parameters: must be a function
    //fprintf(stderr, "QCC_PR_ParseFunctionType: '%s'\n", name);
    type_inlinefunction = qcc_true;
    type = QCC_PR_ParseFunctionType(newtype, type);
  } else {
    if (newtype) type = QCC_PR_DuplicateType(type);
  }
  return type;
}
