typedef struct {
  QCC_def_t *p, *l;
} xlocsave;


static inline void qcc_init_subscoped_locals (xlocsave *sve) {
  sve->p = pr.prevlocvars;
  sve->l = pr.localvars;
  pr.prevlocvars = pr.localvars;
}


static void qcc_cleanup_subscoped_locals (xlocsave *sve) {
  for (QCC_def_t *e2 = pr.localvars; e2 != sve->l; e2 = e2->nextlocal) {
    if (!e2->subscoped_away) {
      Hash_RemoveData(&localstable, e2->name, e2);
      e2->subscoped_away = qcc_true;
    }
  }
  pr.prevlocvars = sve->p;
  pr.localvars = sve->l;
}


/*FIXME: is 'break' inside while in switch will break out of switch? */
static void QCC_PR_ParseBlock (qccbool nosublocs) {
  xlocsave sve;
  if (!nosublocs) qcc_init_subscoped_locals(&sve);
  while (!QCC_PR_EatDelim("}")) QCC_PR_ParseStatement(qcc_false);
  if (!nosublocs) qcc_cleanup_subscoped_locals(&sve);
}


static void QCC_PR_ParseFor (void) {
  int continues, breaks;
  int old_numstatements;
  int numtemp, i;
  QCC_def_t *e;
  QCC_dstatement_t *patch1, *patch2, *patch3;
  int linenum[32];
  QCC_dstatement_t temp[sizeof(linenum)/sizeof(linenum[0])];
  xlocsave sve;
  /* */
  qcc_init_subscoped_locals(&sve);
  continues = num_continues;
  breaks = num_breaks;
  QCC_PR_Expect("(");
  if (!QCC_PR_EatDelim(";")) {
    for (;;) {
      QCC_def_t *vdec = NULL;
      if (pr_token_type == TT_ID && PR_Lex_IsTypeName(pr_token)) vdec = QCC_PR_ParseDefs(1); // no commas!
      if (vdec != NULL) {
        // restore variable name
        if (isalnum(pr_token[0])) QCC_PR_IncludeChunk(" ", qcc_true, NULL);
        QCC_PR_IncludeChunk(pr_token, qcc_true, NULL);
        if (isalnum(pr_token[0])) QCC_PR_IncludeChunk(" ", qcc_true, NULL);
        pr_token_type = TT_ID;
        strcpy(pr_token, vdec->name);
      }
      QCC_FreeTemp(QCC_PR_Expression(TOP_PRIORITY, EXPR_DISALLOW_COMMA));
      if (QCC_PR_EatDelim(";")) break;
      QCC_PR_Expect(",");
    }
  }
  patch2 = &statements[numstatements];
  if (!QCC_PR_EatDelim(";")) {
    conditional = 1;
    e = QCC_PR_Expression(TOP_PRIORITY, 0);
    conditional = 0;
    QCC_PR_Expect(";");
  } else {
    e = NULL;
  }
  if (!QCC_PR_EatDelim(")")) {
    old_numstatements = numstatements;
    QCC_FreeTemp(QCC_PR_Expression(TOP_PRIORITY, 0));
    numtemp = numstatements-old_numstatements;
    if (numtemp > sizeof(linenum)/sizeof(linenum[0])) QCC_PR_ParseError(ERR_TOOCOMPLEX, "Update expression too large");
    numstatements = old_numstatements;
    for (i = 0; i < numtemp; ++i) {
      linenum[i] = statement_linenums[numstatements+i];
      temp[i] = statements[numstatements+i];
    }
    QCC_PR_Expect(")");
  } else {
    numtemp = 0;
  }
  if (e) {
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IFNOT_I], e, 0, &patch1));
  } else {
    patch1 = NULL;
  }
  if (!QCC_PR_EatDelim(";")) QCC_PR_ParseStatement(qcc_true); //don't give the hanging ';' warning
  patch3 = &statements[numstatements];
  for (i = 0; i < numtemp; ++i) {
    statement_linenums[numstatements] = linenum[i];
    statements[numstatements++] = temp[i];
  }
  QCC_PR_SimpleStatement(OP_GOTO, patch2-&statements[numstatements], 0, 0, qcc_false);
  if (patch1) patch1->b = &statements[numstatements]-patch1;
  if (breaks != num_breaks) {
    for (i = breaks; i < num_breaks; ++i) {
      patch1 = &statements[pr_breaks[i]];
      statements[pr_breaks[i]].a = &statements[numstatements]-patch1;
    }
    num_breaks = breaks;
  }
  if (continues != num_continues) {
    for (i = continues; i < num_continues; ++i) {
      patch1 = &statements[pr_continues[i]];
      statements[pr_continues[i]].a = patch3-patch1;
    }
    num_continues = continues;
  }
  qcc_cleanup_subscoped_locals(&sve);
}


static void QCC_PR_ParseReturn (void) {
  QCC_def_t *e, *e2;
  if (QCC_PR_EatDelim(";")) {
    if (pr_scope->type->aux_type->type != ev_void) {
      QCC_PR_ParseWarning(WARN_MISSINGRETURNVALUE, "\'%s\' should return %s", pr_scope->name, pr_scope->type->aux_type->name);
    }
    //QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_DONE], 0, 0, NULL));
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_RETURN], 0, 0, NULL));
    return;
  }
  e = QCC_PR_Expression(TOP_PRIORITY, 0);
  e2 = QCC_SupplyConversion(e, pr_scope->type->aux_type->type, qcc_true);
  if (e != e2) {
    QCC_PR_ParseWarning(WARN_CORRECTEDRETURNTYPE, "\'%s\' returned %s, expected %s, conversion supplied", pr_scope->name, e->type->name, pr_scope->type->aux_type->name);
    e = e2;
  }
  QCC_PR_Expect (";");
  if (pr_scope->type->aux_type->type != e->type->type) {
    QCC_PR_ParseWarning(WARN_WRONGRETURNTYPE, "\'%s\' returned %s, expected %s", pr_scope->name, e->type->name, pr_scope->type->aux_type->name);
  }
  QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_RETURN], e, 0, NULL));
}


static void QCC_PR_ParseWhile (void) {
  QCC_def_t *e;
  QCC_dstatement_t *patch1, *patch2, *patch3;
  int continues = num_continues;
  int breaks = num_breaks;
  QCC_PR_Expect("(");
  patch2 = &statements[numstatements];
  conditional = 1;
  e = QCC_PR_Expression(TOP_PRIORITY, 0);
  conditional = 0;
  if (((e->constant && !e->temp) || !strcmp(e->name, "IMMEDIATE")) && opt_compound_jumps) {
    ++optres_compound_jumps;
    if (!G_INT(e->ofs)) {
      QCC_PR_ParseWarning(0, "while(0)?");
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, &patch1));
    } else {
      patch1 = NULL;
    }
  } else {
    if (e->constant && !e->temp) {
      if (!G_FLOAT(e->ofs)) QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, &patch1)); else patch1 = NULL;
    } else {
      int opc;
      if (!qcc_typecmp(e->type, type_string)) opc = OP_IFNOT_S;
      else if (!qcc_typecmp(e->type, type_float)) opc = OP_IFNOT_F;
      else opc = OP_IFNOT_I;
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[opc], e, 0, &patch1));
    }
  }
  QCC_PR_Expect(")"); // after the line number is noted
  QCC_PR_ParseStatement(qcc_false);
  QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], NULL, 0, &patch3));
  patch3->a = patch2-patch3;
  if (patch1) {
    if (patch1->op == OP_GOTO) {
      patch1->a = &statements[numstatements]-patch1;
    } else {
      patch1->b = &statements[numstatements]-patch1;
    }
  }
  if (breaks != num_breaks) {
    for (int i = breaks; i < num_breaks; ++i) {
      patch1 = &statements[pr_breaks[i]];
      statements[pr_breaks[i]].a = &statements[numstatements]-patch1; // jump to after the return-to-top goto
    }
    num_breaks = breaks;
  }
  if (continues != num_continues) {
    for (int i = continues; i < num_continues; ++i) {
      patch1 = &statements[pr_continues[i]];
      statements[pr_continues[i]].a = patch2-patch1; // jump back to top
    }
    num_continues = continues;
  }
}


static void QCC_PR_ParseDo (void) {
  QCC_def_t *e;
  QCC_dstatement_t *patch1, *patch2;
  int continues = num_continues;
  int breaks = num_breaks;
  patch1 = &statements[numstatements];
  QCC_PR_ParseStatement(qcc_false);
  QCC_PR_Expect("while");
  QCC_PR_Expect("(");
  conditional = 1;
  e = QCC_PR_Expression(TOP_PRIORITY, 0);
  conditional = 0;
  if (e->constant && !e->temp) {
    if (G_FLOAT(e->ofs)) {
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], NULL, 0, &patch2));
      patch2->a = patch1-patch2;
    }
  } else {
    int opc;
    if (!qcc_typecmp(e->type, type_string)) opc = OP_IF_S;
    else if (!qcc_typecmp(e->type, type_float)) opc = OP_IF_F;
    else opc = OP_IF_I;
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[opc], e, NULL, &patch2));
    patch2->b = patch1-patch2;
  }
  QCC_PR_Expect(")");
  QCC_PR_Expect(";");
  if (breaks != num_breaks) {
    for (int i = breaks; i < num_breaks; ++i) {
      patch2 = &statements[pr_breaks[i]];
      statements[pr_breaks[i]].a = &statements[numstatements]-patch2;
    }
    num_breaks = breaks;
  }
  if (continues != num_continues) {
    for (int i = continues; i < num_continues; ++i) {
      patch2 = &statements[pr_continues[i]];
      statements[pr_continues[i]].a = patch1-patch2;
    }
    num_continues = continues;
  }
}


static void QCC_PR_ParseLocal (void) {
  //if (locals_end != numpr_globals)  //is this breaking because of locals?
  //  QCC_PR_ParseWarning("local vars after temp vars\n");
  QCC_PR_ParseDefs(0);
  locals_end = numpr_globals;
}


static void QCC_PR_ParseIf (void) {
  int opc;
  QCC_def_t *e;
  QCC_dstatement_t *patch1, *patch2;
  qccbool negate = QCC_PR_EatKeyword("not");
  QCC_PR_Expect("(");
  conditional = 1;
  e = QCC_PR_Expression(TOP_PRIORITY, 0);
  conditional = 0;
  //negate = (negate != 0);
  if (!qcc_typecmp(e->type, type_string)) {
    opc = (negate ? OP_IF_S : OP_IFNOT_S);
  } else if (!qcc_typecmp(e->type, type_float)) {
    opc = (negate ? OP_IF_F : OP_IFNOT_F);
  } else {
    opc = (negate ? OP_IF_I : OP_IFNOT_I);
  }
  QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[opc], e, 0, &patch1));
  QCC_PR_Expect(")"); // close bracket is after we save the statement to mem (so debugger does not show the if statement as being on the line after)
  QCC_PR_ParseStatement(qcc_false);
  if (QCC_PR_EatKeyword("else")) {
    opc = statements[numstatements-1].op;
    // the last statement of the if was a return, so we don't need the goto at the end
    if ((opc == OP_RETURN || opc == OP_DONE || opc == OP_GOTO) && opt_compound_jumps &&
        !QCC_AStatementJumpsTo(numstatements, patch1-statements, numstatements)) {
      //QCC_PR_ParseWarning(0, "optimised the else");
      ++optres_compound_jumps;
      patch1->b = &statements[numstatements]-patch1;
      QCC_PR_ParseStatement(qcc_false);
    } else {
      //QCC_PR_ParseWarning(0, "using the else");
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, &patch2));
      patch1->b = &statements[numstatements]-patch1;
      QCC_PR_ParseStatement(qcc_false);
      patch2->a = &statements[numstatements]-patch2;
      if (QCC_PR_StatementBlocksMatch(patch1+1, patch2-patch1, patch2+1, &statements[numstatements]-patch2)) {
        QCC_PR_ParseWarning(0, "Two identical blocks each side of an else");
      }
    }
  } else {
    patch1->b = &statements[numstatements]-patch1;
  }
}


static void QCC_PR_ParseSwitch (void) {
  int i;
  QCC_def_t *e, *e2;
  QCC_dstatement_t *patch1, *patch2, *patch3;
  int oldst, defaultcase = -1;
  temp_t *et;
  QCC_type_t *switchtype;
  int breaks = num_breaks;
  int cases = num_cases;
  QCC_PR_Expect("(");
  conditional = 1;
  e = QCC_PR_Expression(TOP_PRIORITY, 0);
  conditional = 0;
  if (e == &def_ret) {
    // copy it out, so our hack just below doesn't crash us
    /*
    if (e->type->type == ev_vector) e = QCC_PR_Statement(pr_opcodes+OP_STORE_V, e, QCC_GetTemp(type_vector), NULL);
    else e = QCC_PR_Statement(pr_opcodes+OP_STORE_F, e, QCC_GetTemp(type_float), NULL);
    if (e == &def_ret) QCC_Error(ERR_INTERNAL, "internal error: switch: e == &def_ret"); // this shouldn't be happening
    */
    et = NULL;
  } else {
    et = e->temp;
    e->temp = NULL; // so noone frees it until we finish this loop
  }
  // expands
  //switch (CONDITION) {
  //  case 1:
  //    break;
  //  case 2:
  //  default:
  //    break;
  //}
  //to
  // x = CONDITION, goto start
  // l1:
  //  goto end
  // l2:
  // def:
  //  goto end
  //  goto end      P1
  // start:
  //  if (x == 1) goto l1;
  //  if (x == 2) goto l2;
  //  goto def
  // end:

  // x is emitted in an opcode, stored as a register that we cannot access later
  // it should be possible to nest these
  switchtype = e->type;
  QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], e, 0, &patch1));
  QCC_PR_Expect(")"); // close bracket is after we save the statement to mem (so debugger does not show the if statement as being on the line after)
  oldst = numstatements;
  QCC_PR_ParseStatement(qcc_false);
  // this is so that a missing goto at the end of your switch doesn't end up in the jumptable again
  if (oldst == numstatements || !QCC_StatementIsAJump(numstatements-1, numstatements-1)) {
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, &patch2)); // the P1 statement/the theyforgotthebreak statement.
    //QCC_PR_ParseWarning(0, "emitted goto");
  } else {
    patch2 = NULL;
    //QCC_PR_ParseWarning(0, "No goto");
  }
  patch1->a = &statements[numstatements]-patch1; // the goto start part
  if (e == &def_ret) e->type = switchtype; // set it back to the type it was actually meant to be
  for (int i = cases; i < num_cases; ++i) {
    if (!pr_casesdef[i]) {
      if (defaultcase >= 0) QCC_PR_ParseError(ERR_MULTIPLEDEFAULTS, "Duplicated default case");
      defaultcase = i;
    } else {
      if (pr_casesdef[i]->type->type != e->type->type) {
        if (e->type->type == ev_integer && pr_casesdef[i]->type->type == ev_float) {
          pr_casesdef[i] = QCC_MakeIntConst((int)qcc_pr_globals[pr_casesdef[i]->ofs]);
        } else {
          QCC_PR_ParseWarning(WARN_SWITCHTYPEMISMATCH, "switch case type mismatch");
        }
      }
      if (pr_casesdef2[i]) {
        QCC_def_t *e3;
        if (pr_casesdef2[i]->type->type != e->type->type) {
          if (e->type->type == ev_integer && pr_casesdef[i]->type->type == ev_float) {
            pr_casesdef2[i] = QCC_MakeIntConst((int)qcc_pr_globals[pr_casesdef2[i]->ofs]);
          } else {
            QCC_PR_ParseWarning(WARN_SWITCHTYPEMISMATCH, "switch caserange type mismatch");
          }
        }
        if (e->type->type == ev_float) {
          e2 = QCC_PR_Statement(&pr_opcodes[OP_GE_F], e, pr_casesdef[i], NULL);
          e3 = QCC_PR_Statement(&pr_opcodes[OP_LE_F], e, pr_casesdef2[i], NULL);
          e2 = QCC_PR_Statement(&pr_opcodes[OP_AND_F], e2, e3, NULL);
          QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IF_I], e2, 0, &patch3));
          patch3->b = &statements[pr_cases[i]]-patch3;
        } else if (e->type->type == ev_integer) {
          e2 = QCC_PR_Statement(&pr_opcodes[OP_GE_I], e, pr_casesdef[i], NULL);
          e3 = QCC_PR_Statement(&pr_opcodes[OP_LE_I], e, pr_casesdef2[i], NULL);
          e2 = QCC_PR_Statement(&pr_opcodes[OP_AND_I], e2, e3, NULL);
          QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IF_I], e2, 0, &patch3));
          patch3->b = &statements[pr_cases[i]]-patch3;
        } else {
          QCC_PR_ParseWarning(WARN_SWITCHTYPEMISMATCH, "switch caserange MUST be a float or integer");
        }
      } else {
        if (!pr_casesdef[i]->constant || G_INT(pr_casesdef[i]->ofs)) {
          switch (e->type->type) {
            case ev_float: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_F], e, pr_casesdef[i], NULL); break;
            case ev_entity: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_E], e, pr_casesdef[i], &patch1); break; // why???
            case ev_vector: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_V], e, pr_casesdef[i], &patch1); break;
            case ev_string: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_S], e, pr_casesdef[i], &patch1); break;
            case ev_function: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_FNC], e, pr_casesdef[i], &patch1); break;
            case ev_field: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_FNC], e, pr_casesdef[i], &patch1); break;
            case ev_integer: e2 = QCC_PR_Statement(&pr_opcodes[OP_EQ_I], e, pr_casesdef[i], &patch1); break;
            default: QCC_PR_ParseError(ERR_BADSWITCHTYPE, "Bad switch type"); break;
          }
          QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IF_I], e2, 0, &patch3));
        } else {
          if (e->type->type == ev_string) QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IFNOT_S], e, 0, &patch3));
          else if (e->type->type == ev_float) QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IFNOT_F], e, 0, &patch3));
          else QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IFNOT_I], e, 0, &patch3));
        }
        patch3->b = &statements[pr_cases[i]]-patch3;
      }
    }
  }
  if (defaultcase >= 0) {
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, &patch3));
    patch3->a = &statements[pr_cases[defaultcase]]-patch3;
  }
  num_cases = cases;
  patch3 = &statements[numstatements];
  if (patch2) patch2->a = patch3-patch2; // set P1 jump
  if (breaks != num_breaks) {
    for (i = breaks; i < num_breaks; ++i) {
      patch2 = &statements[pr_breaks[i]];
      patch2->a = patch3-patch2;
    }
    num_breaks = breaks;
  }
  if (et) {
    e->temp = et;
    QCC_FreeTemp(e);
  }
}


static void QCC_PR_ParseCase (void) {
  if (num_cases >= max_cases) {
    max_cases += 8;
    pr_cases = realloc(pr_cases, sizeof(*pr_cases)*max_cases);
    pr_casesdef = realloc(pr_casesdef, sizeof(*pr_casesdef)*max_cases);
    pr_casesdef2 = realloc(pr_casesdef2, sizeof(*pr_casesdef2)*max_cases);
  }
  pr_cases[num_cases] = numstatements;
  pr_casesdef[num_cases] = QCC_PR_Expression(TOP_PRIORITY, EXPR_DISALLOW_COMMA);
  if (QCC_PR_EatDelim("..")) {
    pr_casesdef2[num_cases] = QCC_PR_Expression(TOP_PRIORITY, EXPR_DISALLOW_COMMA);
    if (pr_casesdef[num_cases]->constant && pr_casesdef2[num_cases]->constant &&
        !pr_casesdef[num_cases]->temp && !pr_casesdef2[num_cases]->temp) {
      if (G_FLOAT(pr_casesdef[num_cases]->ofs) >= G_FLOAT(pr_casesdef2[num_cases]->ofs)) {
        QCC_PR_ParseError(ERR_CASENOTIMMEDIATE, "Caserange statement uses backwards range\n");
      }
    }
  } else {
    pr_casesdef2[num_cases] = NULL;
  }
  if (numstatements != pr_cases[num_cases]) QCC_PR_ParseError(ERR_CASENOTIMMEDIATE, "Case statements may not use formulas\n");
  ++num_cases;
  QCC_PR_Expect(":");
}


static void QCC_PR_ParseDefault (void) {
  if (num_cases >= max_cases) {
    max_cases += 8;
    pr_cases = realloc(pr_cases, sizeof(*pr_cases)*max_cases);
    pr_casesdef = realloc(pr_casesdef, sizeof(*pr_casesdef)*max_cases);
    pr_casesdef2 = realloc(pr_casesdef2, sizeof(*pr_casesdef2)*max_cases);
  }
  pr_cases[num_cases] = numstatements;
  pr_casesdef[num_cases] = NULL;
  pr_casesdef2[num_cases] = NULL;
  ++num_cases;
  QCC_PR_Expect(":");
}


static void QCC_PR_ParseGoto (void) {
  QCC_dstatement_t *patch2;
  if (pr_token_type != TT_ID) {
    QCC_PR_ParseError(ERR_NOLABEL, "invalid label name \"%s\"", pr_token);
    return;
  }
  QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, &patch2);
  QCC_PR_GotoStatement(patch2, pr_token);
  //QCC_PR_ParseWarning("Gotos are evil");
  QCC_PR_Lex();
  QCC_PR_Expect(";");
}


static void QCC_PR_ParseBreak (void) {
  if (num_breaks >= max_breaks) {
    max_breaks += 8;
    pr_breaks = realloc(pr_breaks, sizeof(*pr_breaks)*max_breaks);
  }
  pr_breaks[num_breaks] = numstatements;
  QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, NULL);
  ++num_breaks;
  QCC_PR_Expect(";");
}


static void QCC_PR_ParseContinue (void) {
  if (num_continues >= max_continues) {
    max_continues += 8;
    pr_continues = realloc(pr_continues, sizeof(*pr_continues)*max_continues);
  }
  pr_continues[num_continues] = numstatements;
  QCC_PR_Statement(&pr_opcodes[OP_GOTO], 0, 0, NULL);
  ++num_continues;
  QCC_PR_Expect(";");
}


/*
============
PR_ParseStatement

============
*/
static void QCC_PR_ParseStatement (qccbool nosublocs) {
  QCC_def_t *e;
  int statementstart = pr_source_line;
  if (QCC_PR_EatDelim("{")) { QCC_PR_ParseBlock(nosublocs); return; }
  if (QCC_PR_EatKeyword("return")) { QCC_PR_ParseReturn(); return; }
  if (QCC_PR_EatKeyword("while")) { QCC_PR_ParseWhile(); return; }
  if (QCC_PR_EatKeyword("for")) { QCC_PR_ParseFor(); return; }
  if (QCC_PR_EatKeyword("do")) { QCC_PR_ParseDo(); return; }
  if (QCC_PR_EatKeyword("local")) { QCC_PR_ParseLocal(); return; }
  if (QCC_PR_EatKeyword("if")) { QCC_PR_ParseIf(); return; }
  if (QCC_PR_EatKeyword("switch")) { QCC_PR_ParseSwitch(); return; }
  if (QCC_PR_EatKeyword("goto")) { QCC_PR_ParseGoto(); return; }
  if (QCC_PR_EatKeyword("break")) {
#if 0
    if (!strcmp("(", pr_token)) {
      // make sure it wasn't a call to the break function
      QCC_PR_IncludeChunk("break(", qcc_true, NULL);
      QCC_PR_Lex(); // so it sees the break
    } else
#endif
    {
      QCC_PR_ParseBreak();
      return;
    }
  }
  if (QCC_PR_EatKeyword("continue")) {
#if 0
    if (!strcmp("(", pr_token)) {
      // make sure it wasn't a call to the continue function
      QCC_PR_IncludeChunk("continue(", qcc_true, NULL);
      QCC_PR_Lex(); // so it sees the continue
    } else
#endif
    {
      QCC_PR_ParseContinue();
      return;
    }
  }
  if (QCC_PR_EatKeyword("case")) { QCC_PR_ParseCase(); return; }
  if (QCC_PR_EatKeyword("default")) { QCC_PR_ParseDefault(); return; }

  if (pr_token_type == TT_ID && (PR_Lex_IsTypeName(pr_token) || !strcmp("const", pr_token))) {
    //if (locals_end != numpr_globals) // is this breaking because of locals?
    //  QCC_PR_ParseWarning("local vars after temp vars\n");
    QCC_PR_ParseDefs(0);
    locals_end = numpr_globals;
    return;
  }

  if (QCC_PR_EatDelim(":")) {
    if (pr_token_type != TT_ID) {
      QCC_PR_ParseError(ERR_BADLABELNAME, "invalid label name \"%s\"", pr_token);
      return;
    }
    for (int i = 0; i < num_labels; ++i) {
      if (strncmp(pr_labels[i].name, pr_token, sizeof(pr_labels[num_labels].name)-1) == 0) {
        QCC_PR_ParseWarning(WARN_DUPLICATELABEL, "Duplicate label %s", pr_token);
        QCC_PR_Lex();
        return;
      }
    }
    if (num_labels >= max_labels) {
      max_labels += 8;
      pr_labels = realloc(pr_labels, sizeof(*pr_labels)*max_labels);
    }
    strncpy(pr_labels[num_labels].name, pr_token, sizeof(pr_labels[num_labels].name)-1);
    pr_labels[num_labels].lineno = pr_source_line;
    pr_labels[num_labels].statementno = numstatements;
    ++num_labels;
    //QCC_PR_ParseWarning("Gotos are evil");
    QCC_PR_Lex();
    return;
  }

  if (QCC_PR_EatDelim(";")) {
    int osl = pr_source_line;
    pr_source_line = statementstart;
    if (!expandedemptymacro) QCC_PR_ParseWarning(WARN_POINTLESSSTATEMENT, "Hanging ';'");
    pr_source_line = osl;
    return;
  }

  //qcc_functioncalled = 0;
  qcc_usefulstatement = qcc_false;
  e = QCC_PR_Expression(TOP_PRIORITY, 0);
  expandedemptymacro = qcc_false;
  QCC_PR_Expect(";");

  if (e->type->type != ev_void && !qcc_usefulstatement) {
    int osl = pr_source_line;
    pr_source_line = statementstart;
    QCC_PR_ParseWarning(WARN_POINTLESSSTATEMENT, "Statement without effect");
    pr_source_line = osl;
  }
  QCC_FreeTemp(e);
  //qcc_functioncalled = qcc_false;
}
