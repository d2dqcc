// we are using malloc() here
// this is safe as long as we don't forget to free() everything
// and don't drop errors

//FIXME: rewrite this completely in recursive manner!
static void QCC_CheckForDeadAndMissingReturns (int first, int last, int rettype) {
  uint8_t *ophit; // hitflags
  //
  void trace (int st) {
    if (st < first || st > last) QCC_PR_ParseWarning(0, "%s: internal compiler error (0) %d", pr_scope->name, st);
    while (st >= first && st < last) {
      if (ophit[st-first]) return; // we already visited this place, do nothing more
      ophit[st-first] = 1;
      if (statements[st].op == OP_GOTO) {
        // have GOTO, just follow it
        if (statements[st].sa == 0) QCC_PR_ParseWarning(0, "%s: inifinite loop", pr_scope->name);
        //fprintf(stderr, "sa=%d\n", statements[st].sa);
        st += statements[st].sa;
        if (st < first || st > last) QCC_PR_ParseWarning(0, "%s: internal compiler error (1)", pr_scope->name);
        continue;
      }
      // is this a conditional branch?
      if (is_branch_op(statements[st].op)) {
        // let's trace jump destination
        if (branch_dest(st) == st) QCC_PR_ParseWarning(0, "%s: possible inifinite loop", pr_scope->name);
        trace(branch_dest(st));
        // and continue with this one
      } else {
        // exit?
        switch (statements[st].op) {
          case OP_DONE:
            if (rettype != ev_void) QCC_PR_ParseWarning(WARN_MISSINGRETURN, "%s: not all control paths return a value", pr_scope->name);
            // fallthru
          case OP_RETURN:
            // we are done with this execution path
            return;
        }
      }
      // next statement
      ++st;
    }
  }
  //
  //if (statements[last-1].op == OP_DONE) --last; // don't want the last done
  // check if last statement is GOTO or RETURN for non-void function
  /*
  if (rettype != ev_void) {
    if (statements[last-1].op != OP_RETURN) {
      if (statements[last-1].op != OP_GOTO || (signed int)statements[last-1].a > 0) {
        QCC_PR_ParseWarning(WARN_MISSINGRETURN, "%s: not all control paths return a value", pr_scope->name);
        //return;
      }
    }
  }
  */
  //fprintf(stderr, "QCC_CheckForDeadAndMissingReturns: first=%d; last=%d\n", first, last);
  // now: trace all possible pathes and complain:
  //   * if there is one without RETURN and we are non-void
  //   * if there is some unreachable code left
  if ((ophit = calloc(last-first+1, sizeof(uint8_t))) == NULL) {
    // this is unlikely happens
    QCC_PR_ParseWarning(0, "%s: can't check returns", pr_scope->name);
    return;
  }
  // let's do some recursive tracing
  trace(first);
  if (statements[last-1].op == OP_DONE) --last; // don't want the last done
  // now check if we have some unreachable code
  for (int st = first; st < last; ++st) {
    if (!ophit[st-first]) {
      if (opt_compound_jumps) {
        // we can ignore single statements like these without compound jumps (compound jumps correctly removes all)
        switch (statements[st].op) {
          case OP_GOTO: case OP_DONE: case OP_RETURN: break;
          default:
            QCC_PR_ParseWarning(WARN_UNREACHABLECODE, "%s: contains unreachable code", pr_scope->name);
            st = last; // stop this
            break;
        }
      }
    }
  }
  //
  free(ophit);
}
