typedef QCC_def_t *(*IntrinsicFn) (QCC_def_t *func);


static QCC_def_t *qcc_intrinsic_gettext (QCC_def_t *func) {
  QCC_def_t *d = NULL;
  if (!func->initialized) func->initialized = 3;
  ++func->references;
  if (pr_token_type == TT_IMMED && pr_immediate_type->type == ev_string) {
    d = QCC_MakeTranslateStringConst(pr_immediate_string);
    QCC_PR_Lex();
  } else {
    QCC_PR_ParseErrorPrintDef(ERR_TYPEMISMATCHPARM, func, "_() intrinsic accepts only a string immediate");
  }
  QCC_PR_Expect(")");
  return d;
}


static QCC_def_t *qcc_intrinsic_sizeof (QCC_def_t *func) {
  QCC_type_t *t;
  if (!func->initialized) func->initialized = 3;
  ++func->references;
  t = QCC_PR_ParseType(qcc_false, qcc_false);
  QCC_PR_Expect(")");
  return QCC_MakeIntConst(t->size*4);
}


static const struct {
  const char *name;
  IntrinsicFn fn;
} intrlist[] = {
  {.name="_", .fn=qcc_intrinsic_gettext},
  {.name="sizeof", .fn=qcc_intrinsic_sizeof},
};
