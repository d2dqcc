/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _QCC_VMSTRUCT_H_
#define _QCC_VMSTRUCT_H_

#include <stdint.h>


typedef int QCC_string_t;
typedef int progsnum_t;
typedef int func_t;
typedef int string_t;


typedef struct QGG_GCC_PACKED {
  uint16_t op;
  union {
    struct { uint16_t a, b, c; };
    uint16_t args[3];
    struct { int16_t sa, sb, sc; };
    int16_t sargs[3];
  };
} QCC_dstatement_t;


typedef struct QGG_GCC_PACKED {
  uint16_t type; // if DEF_SAVEGLOBAL bit is set the variable needs to be saved in savegames
  uint16_t ofs;
  QCC_string_t s_name;
} QCC_ddef_t;


#define DEF_SAVEGLOBAL  (1<<15)
#define DEF_SHARED      (1<<14)

typedef struct QGG_GCC_PACKED {
  uint32_t first_statement;  // negative numbers are builtins
  uint32_t parm_start;
  uint32_t locals;       // total ints of parms + locals
  QCC_string_t s_name;
  QCC_string_t s_file;     // source file defined in
  uint32_t numparms;
  uint32_t parm_size[MAX_PARMS]; // was byte
} QCC_dfunction_t;


#define PROG_QTESTVERSION 3
#define PROG_VERSION  6
#define PROG_KKQWSVVERSION 7
#define PROG_EXTENDEDVERSION  7
#define PROG_SECONDARYVERSION16 (*(uint32_t *)"1FTE" ^ *(uint32_t *)"PROG") //something unlikly and still meaningful (to me)
#define PROG_SECONDARYVERSION32 (*(uint32_t *)"1FTE" ^ *(uint32_t *)"32B ") //something unlikly and still meaningful (to me)

typedef struct QGG_GCC_PACKED {
  uint32_t version;
  uint32_t crc;      // check of header file

  uint32_t ofs_statements; //comp 1
  uint32_t numstatements;  // statement 0 is an error

  uint32_t ofs_globaldefs; //comp 2
  uint32_t numglobaldefs;

  uint32_t ofs_fielddefs;  //comp 4
  uint32_t numfielddefs;

  uint32_t ofs_functions;  //comp 8
  uint32_t numfunctions; // function 0 is an empty

  uint32_t ofs_strings;  //comp 16
  uint32_t numstrings;   // first string is a null string

  uint32_t ofs_globals;  //comp 32
  uint32_t numglobals;

  uint32_t entityfields;

  //debug / version 7 extensions
  uint32_t ofslinenums;  //numstatements big //comp 64
  uint32_t ofsbodylessfuncs; //no comp
  uint32_t numbodylessfuncs;

  uint32_t ofs_types;  //comp 128
  uint32_t numtypes;

  uint32_t secondaryversion; //Constant - to say that any version 7 progs are actually ours, not someone else's alterations.
} dprograms_t;
#define standard_dprograms_t_size ((size_t)&((dprograms_t *)NULL)->ofslinenums)


typedef struct {
  char filename[128];
  uint32_t size;
  uint32_t compsize;
  uint32_t compmethod;
  uint32_t ofs;
} includeddatafile_t;


typedef struct QGG_GCC_PACKED typeinfo_s {
  qcc_etype type;
  uint32_t next;
  uint32_t aux_type;
  uint32_t num_parms;
  uint32_t ofs;  //inside a structure.
  uint32_t size;
  string_t  name;
} typeinfo_t;


typedef struct {
  const char *name;
  const char *opname;
  int priority;
  enum { ASSOC_LEFT, ASSOC_RIGHT, ASSOC_RIGHT_RESULT } associative;
  union {
    struct { struct QCC_type_s **type_a, **type_b, **type_c; };
    struct QCC_type_s **types[3];
  };
} QCC_opcode_t;


#endif
