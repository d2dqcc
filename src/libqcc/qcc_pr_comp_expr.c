static QCC_def_t *QCC_PR_ExpressionEx (int priority, int exprflags, QCC_def_t *gotarg0);


/*
============
QCC_ConversionOpcode

return conversion opcode.
zero return value means that no conversion needed.
negative return value means that there is no conversion available.
============
*/
static inline int QCC_ConversionOpcode (const QCC_def_t *var, qcc_etype wanted) {
  //fprintf(stderr, "QCC_ConversionOpcode: from=%d; to=%d\n", var->type->type, wanted);
  // no conversion needed
  if (var->type->type == wanted) return 0;
  if (var->type->type == ev_integer && wanted == ev_function) return 0;
  if (var->type->type == ev_integer && wanted == ev_pointer) return 0;
  // stuff needs converting
  if (var->type->type == ev_pointer && var->type->aux_type) {
    if (var->type->aux_type->type == ev_float && wanted == ev_integer) return OP_CP_FTOI;
    if (var->type->aux_type->type == ev_integer && wanted == ev_float) return OP_CP_ITOF;
  } else {
    if (var->type->type == ev_float && wanted == ev_integer) return OP_CONV_FTOI;
    if (var->type->type == ev_integer && wanted == ev_float) return OP_CONV_ITOF;
    //if (var->type->type == ev_float && wanted == ev_string) return OP_CONV_FTOS;
    //if (var->type->type == ev_integer && wanted == ev_string) return OP_CONV_ITOS;
  }
  // impossible
  return -1;
}


/*
============
QCC_SupplyConversion

emit conversion operators if necessary.
fail if 'fatal' is true and there is no conversion available.
return converted def_t (this can be 'var' if no conversion necessary.
============
*/
static QCC_def_t *QCC_SupplyConversion (QCC_def_t *var, qcc_etype wanted, qccbool fatal) {
  int o;
  //fprintf(stderr, "QCC_SupplyConversion: from=%d; to=%d\n", var->type->type, wanted);
  if ((o = QCC_ConversionOpcode(var, wanted)) == 0) return var; // type already matches
  if (o < 0) {
    if (fatal) {
      QCC_PR_ParseErrorPrintDef(ERR_TYPEMISMATCH, var, "Implicit type mismatch. Needed %s, got %s.", basictypenames[wanted], var->type->name);
    }
    return var;
  }
  return QCC_PR_Statement(&pr_opcodes[o], var, NULL, NULL); // conversion return value
}



/*
============
QCC_canConv

check if we can convert 'from' to the given type.
return 'conversion cost' (wtf?!) or negative value if we can't.
conversion cost of zero means 'no conversion necessary'.
============
*/
static inline int QCC_canConv (const QCC_def_t *from, qcc_etype to) {
  int op;
  //fprintf(stderr, "[%s] f=%d t=%d\n", from->type->name, from->type->type, to);
  if (from->type->type == to) return 0;
  //if (from->type->type == ev_vector && to == ev_float) return 4; // don't want this, use vectors properly
  //
  op = QCC_ConversionOpcode(from, to);
  if (op == 0) return 0;
  if (op > 0) return 4;
  /*
  if (from->type->type == ev_pointer && from->type->aux_type->type == to) return 1;
  if (QCC_ConversionOpcode(from, to) >= 0) return 1;
  */
  //k8: if (from->type->type == ev_integer && to == ev_function) return 1;
  //if (from->type->type == ev_integer && to == ev_string) return 1;
  //if (from->type->type == ev_float && to == ev_string) return 1;
  return -100;
}


/*
============
QCC_PR_ExprCast

process type casting.
============
*/
static QCC_def_t *QCC_PR_ExprCast (QCC_type_t *newtype) {
  QCC_def_t *e, *e2;
  //qcc_etype t;
  // type casting
  QCC_PR_Expect(")");
  e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA);
  //
  if (newtype->type == e->type->type) return e; // ok
  // you may cast from const 0 to any type of same size for free (from either int or float for simplicity)
  if (newtype->size == e->type->size &&
      (e->type->type == ev_integer || e->type->type == ev_float) &&
      e->constant && !G_INT(e->ofs)) {
    //TODO: wtf?!
    QCC_PR_ParseError(0, "Bad type cast: TODO const 0");
  }
  // cast from int->float will convert
  if (newtype->type == ev_float && e->type->type == ev_integer) {
    return QCC_PR_Statement(&pr_opcodes[OP_CONV_ITOF], e, 0, NULL);
  }
  // cast from float->int will convert
  if (newtype->type == ev_integer && e->type->type == ev_float) {
    return QCC_PR_Statement(&pr_opcodes[OP_CONV_FTOI], e, 0, NULL);
  }
  // cast to string
  if (newtype->type == ev_string) {
    // cast from int->string will convert
    if (e->type->type == ev_integer) return QCC_PR_Statement(&pr_opcodes[OP_CONV_ITOS], e, 0, NULL);
    // cast from float->string will convert
    if (e->type->type == ev_float) return QCC_PR_Statement(&pr_opcodes[OP_CONV_FTOS], e, 0, NULL);
    QCC_PR_ParseError(0, "Bad type cast");
  }
  //
  if (
      //pointers
      ((newtype->type == ev_pointer || newtype->type == ev_string || newtype->type == ev_integer) &&
       (e->type->type == ev_pointer || e->type->type == ev_string || e->type->type == ev_integer)) ||
      //ents/classs
      (newtype->type == ev_entity && e->type->type == ev_entity)) {
    // you may freely cast between pointers (and ints, as this is explicit)
    // (strings count as pointers - WARNING: some strings may not be expressable as pointers)
    //direct cast
    e2 = (void *)qccHunkAlloc(sizeof(QCC_def_t));
    memset(e2, 0, sizeof(QCC_def_t));
    e2->type = newtype;
    e2->ofs = e->ofs;
    e2->constant = qcc_true;
    e2->temp = e->temp;
    return e2;
  }
  //
  QCC_PR_ParseError(0, "Bad type cast");
}


static QCC_def_t *QCC_PR_ExprCast2Bool (void) {
  QCC_def_t *e, *e2;
  int opc, stnum;
  QCC_PR_Expect(")");
  e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA);
  e2 = QCC_GetTemp(type_float);
  if (!qcc_typecmp(e->type, type_string)) opc = OP_IF_S;
  else if (!qcc_typecmp(e->type, type_float)) opc = OP_IF_F;
  else opc = OP_IF_I;
  // store 'true' value
  QCC_PR_Statement3(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(1.0f), e2, NULL, qcc_false);
  // generate 'checkjump'
  stnum = numstatements;
  QCC_PR_Statement3(&pr_opcodes[opc], e, NULL, NULL, qcc_false);
  // store 'false' value
  QCC_PR_Statement3(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(0.0f), e2, NULL, qcc_false);
  // fix jump
  statements[stnum].sb = numstatements-stnum;
  // done
  QCC_FreeTemp(e);
  return e2;
}


/*
============
QCC_PR_Term
============
*/
static QCC_def_t *QCC_PR_Term (void) {
  QCC_def_t *e, *e2;
  qcc_etype t;
  char ppmm = 0;
  if (pr_token_type != TT_DELIM) {
    e = QCC_PR_ParseValue(qcc_true);
    // allow method calls only for correct conditions
    if (e->type->type == ev_entity || (e->type->type == ev_field && e->type->aux_type->type == ev_entity)) {
      if (pr_token[0] == ':') {
        // this can be either ":" or "::", nothing else
        e2 = e; // save self
        if (pr_token[1] == ':') {
          // call global function with new 'self'
          QCC_PR_Lex(); // eat "::" token
          e = QCC_PR_Term(); // get function
        } else {
          // call entity field with new 'self'
          // stuff back "." to parse field load
          strcpy(pr_token, "."); // overwrite ":"
          e = QCC_PR_ExpressionEx(1/*dot priority*/, EXPR_DISALLOW_FCALL, e2); // load function
        }
        QCC_PR_Expect("("); // this SHOULD be function call
        qcc_usefulstatement = qcc_true;
        e = QCC_PR_ParseFunctionCall(e, e2);
      }
    }
    return e;
  }
  // '++' or '--'
  if (QCC_PR_EatDelim("++")) ppmm = '+'; else if (QCC_PR_EatDelim("--")) ppmm = '-';
  if (ppmm) {
    qcc_usefulstatement = qcc_true;
    //e = QCC_PR_Term();
    e = QCC_PR_ParseValue(qcc_true);
    if (e->constant) QCC_PR_ParseError(0, "Assignment to constant %s", e->name);
    if (e->temp) QCC_PR_ParseWarning(WARN_ASSIGNMENTTOCONSTANT, "Hey! That's a temp! %c%c operators cannot work on temps!", ppmm, ppmm);
    switch (e->type->type) {
      case ev_integer:
        QCC_PR_Statement3(&pr_opcodes[ppmm == '+' ? OP_ADD_I : OP_SUB_I], e, QCC_MakeIntConst(1), e, qcc_false);
        break;
      case ev_float:
        QCC_PR_Statement3(&pr_opcodes[ppmm == '+' ? OP_ADD_F : OP_SUB_F], e, QCC_MakeFloatConst(1), e, qcc_false);
        break;
      default:
        QCC_PR_ParseError(ERR_BADPLUSPLUSOPERATOR, "%c%c operator on unsupported type", ppmm, ppmm);
        break;
    }
    return e;
  }
  // unary logic not
  if (QCC_PR_EatDelim("!")) {
    int opcode;
    e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA|EXPR_WARN_ABOVE_1);
    switch ((t = e->type->type)) {
      case ev_float: opcode = OP_NOT_F; break;
      case ev_string: opcode = OP_NOT_S; break;
      case ev_entity: opcode = OP_NOT_ENT; break;
      case ev_vector: opcode = OP_NOT_V; break;
      case ev_function: opcode = OP_NOT_FNC; break;
      case ev_integer: opcode = OP_NOT_FNC; break; // functions are integer values too
      case ev_pointer: opcode = OP_NOT_FNC; break; // ...and pointers
      default: QCC_PR_ParseError(ERR_BADNOTTYPE, "type mismatch for !");
    }
    return QCC_PR_Statement(&pr_opcodes[opcode], e, 0, NULL);
  }
  // subexpression or typecast
  if (QCC_PR_EatDelim("(")) {
    qccbool oldcond;
    QCC_type_t *newtype = QCC_PR_ParseType(qcc_false, qcc_true);
    if (newtype) return QCC_PR_ExprCast(newtype);
    if (QCC_PR_EatKeyword("bool")) return QCC_PR_ExprCast2Bool(); // wow, cast to boolean
    // not a type casting, subexpression
    oldcond = conditional;
    conditional = (conditional ? 2 : 0);
    e = QCC_PR_Expression(TOP_PRIORITY, 0);
    QCC_PR_Expect(")");
    conditional = oldcond;
    return e;
  }
  // unary minus
  if (QCC_PR_EatDelim("-")) {
    e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA);
    switch (e->type->type) {
      case ev_float: e2 = QCC_PR_Statement(&pr_opcodes[OP_SUB_F], QCC_MakeFloatConst(0), e, NULL); break;
      case ev_vector: e2 = QCC_PR_Statement(&pr_opcodes[OP_SUB_V], QCC_MakeVectorConst(0, 0, 0), e, NULL); break;
      case ev_integer: e2 = QCC_PR_Statement(&pr_opcodes[OP_SUB_I], QCC_MakeIntConst(0), e, NULL); break;
      default: QCC_PR_ParseError(ERR_BADNOTTYPE, "type mismatch for -"); break;
    }
    return e2;
  }
  // unary plus
  if (QCC_PR_EatDelim("+")) {
    e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA);
    switch (e->type->type) {
      case ev_float:
      case ev_vector:
      case ev_integer:
        e2 = e;
        break;
      default: QCC_PR_ParseError(ERR_BADNOTTYPE, "type mismatch for +"); break;
    }
    return e2;
  }
  // unary address
  if (QCC_PR_EatDelim("&")) {
    int st = numstatements;
    e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA);
    t = e->type->type;
    if (st != numstatements) {
      // woo, something like ent.field?
      if (is_edict_load(statements[numstatements-1].op)) {
        statements[numstatements-1].op = OP_ADDRESS;
        e->type = QCC_PR_PointerType(e->type);
        return e;
      }
      if (is_array_load(statements[numstatements-1].op)) {
        statements[numstatements-1].op = OP_GLOBALADDRESS;
        e->type = QCC_PR_PointerType(e->type);
        return e;
      }
      if (is_pointer_load(statements[numstatements-1].op)) {
        statements[numstatements-1].op = OP_POINTER_ADD;
        e->type = QCC_PR_PointerType(e->type);
        return e;
      }
      // this is a restriction that could be lifted, I just want to make sure that I got all the bits first
      QCC_PR_ParseError(ERR_BADNOTTYPE, "type mismatch for '&' Must be singular expression or field reference");
      return e;
    }
    //QCC_PR_ParseWarning(0, "debug: &global");
    e2 = QCC_PR_Statement(&pr_opcodes[OP_GLOBALADDRESS], e, 0, NULL);
    e2->type = QCC_PR_PointerType(e->type);
    return e2;
  }
  // unary ptrref
  if (QCC_PR_EatDelim("*")) {
    int opcode;
    e = QCC_PR_Expression(UNARY_PRIORITY, EXPR_DISALLOW_COMMA);
    t = e->type->type;
    if (t != ev_pointer) QCC_PR_ParseError(ERR_BADNOTTYPE, "type mismatch for *");
    switch (e->type->aux_type->type) {
      case ev_float: opcode = OP_LOADP_F; break;
      case ev_string: opcode = OP_LOADP_S; break;
      case ev_vector: opcode = OP_LOADP_V; break;
      case ev_entity: opcode = OP_LOADP_ENT; break;
      case ev_field: opcode = OP_LOADP_FLD; break;
      case ev_function: opcode = OP_LOADP_FLD; break;
      case ev_integer: opcode = OP_LOADP_I; break;
      case ev_pointer: opcode = OP_LOADP_I; break;
      default: QCC_PR_ParseError(ERR_BADNOTTYPE, "type mismatch for * (unrecognised type)"); break;
    }
    e2 = QCC_PR_Statement(&pr_opcodes[opcode], e, 0, NULL);
    e2->type = e->type->aux_type;
    return e2;
  }
  // this will raise an error (i hope)
  return QCC_PR_ParseValue(qcc_true);
}


/*
============
QCC_PR_ExprPMPost

process postfix '++' and '--'.
============
*/
static QCC_def_t *QCC_PR_ExprPMPost (QCC_def_t *e, int ppmm) {
  QCC_def_t *e2;
  int opcodef = (ppmm == '+' ? OP_ADD_F : OP_SUB_F);
  int opcodei = (ppmm == '+' ? OP_ADD_I : OP_SUB_I);
  // if the last statement was an ent.float (or something)
  if (((unsigned int)(statements[numstatements-1].op-OP_LOAD_F) < 6 || statements[numstatements-1].op == OP_LOAD_I) &&
      statements[numstatements-1].c == e->ofs) {
    // we have our load
    QCC_def_t *e3;
    // the only inefficiency here is with an extra temp (we can't reuse the original)
    // this is not a problem, as the optimise temps or locals marshalling can clean these up for us
    qcc_usefulstatement = qcc_true;
    // load
    // add to temp / /subsctact from temp
    // store temp to offset
    // return original loaded (which is not at the same offset as the pointer we store to)
    e2 = QCC_GetTemp(type_float);
    e3 = QCC_GetTemp(type_pointer);
    QCC_PR_SimpleStatement(OP_ADDRESS, statements[numstatements-1].a, statements[numstatements-1].b, e3->ofs, qcc_false);
    if (e->type->type == ev_float) {
      QCC_PR_Statement3(&pr_opcodes[opcodef], e, QCC_MakeFloatConst(1), e2, qcc_false);
      QCC_PR_Statement3(&pr_opcodes[OP_STOREP_F], e2, e3, NULL, qcc_false);
    } else if (e->type->type == ev_integer) {
      QCC_PR_Statement3(&pr_opcodes[opcodei], e, QCC_MakeIntConst(1), e2, qcc_false);
      QCC_PR_Statement3(&pr_opcodes[OP_STOREP_I], e2, e3, NULL, qcc_false);
    } else {
      QCC_PR_ParseError(ERR_PARSEERRORS, "%c%c suffix operator results in nonstandard behaviour. Use %c=1 or prefix form instead", ppmm, ppmm, ppmm);
    }
    QCC_FreeTemp(e2);
    QCC_FreeTemp(e3);
  } else if (e->type->type == ev_float) {
    //copy to temp
    //add to original
    //return temp (which == original)
    QCC_PR_ParseWarning(WARN_INEFFICIENTPLUSPLUS, "%c%c suffix operator results in inefficient behaviour. Use %c=1 or prefix form instead", ppmm, ppmm, ppmm);
    qcc_usefulstatement = qcc_true;
    e2 = QCC_GetTemp(type_float);
    QCC_PR_Statement3(&pr_opcodes[OP_STORE_F], e, e2, NULL, qcc_false);
    QCC_PR_Statement3(&pr_opcodes[opcodef], e, QCC_MakeFloatConst(1), e, qcc_false);
    QCC_FreeTemp(e);
    e = e2;
  } else if (e->type->type == ev_integer) {
    QCC_PR_ParseWarning(WARN_INEFFICIENTPLUSPLUS, "%c%c suffix operator results in inefficient behaviour. Use %c=1 or prefix form instead", ppmm, ppmm, ppmm);
    qcc_usefulstatement = qcc_true;
    e2 = QCC_GetTemp(type_integer);
    QCC_PR_Statement3(&pr_opcodes[OP_STORE_I], e, e2, NULL, qcc_false);
    QCC_PR_Statement3(&pr_opcodes[opcodei], e, QCC_MakeIntConst(1), e, qcc_false);
    QCC_FreeTemp(e);
    e = e2;
  } else {
    QCC_PR_ParseError(ERR_PARSEERRORS, "%c%c suffix operator results in nonstandard behaviour. Use %c=1 or prefix form instead", ppmm, ppmm, ppmm);
  }
  return e;
}


static const QCC_opcode_t *find_type_conversion (QCC_def_t *e, QCC_def_t *e2, const QCC_opcode_t *oldop,
  qcc_etype type_a, qcc_etype type_c, int *numcvt)
{
  const QCC_opcode_t *bestop = NULL;
  int numconversions = 32767;
  for (const QCC_opcode_t *op = pr_opcodes; op && op->name; ++op) {
    int c;
    //
    if (op->priority != oldop->priority) continue; // not on this priority level, do not want
    if (strcmp(op->name, oldop->name) != 0) continue; // ugly name, do not want
    //if (!(type_c != ev_void && type_c != (*op->type_c)->type)) -- go on
    if (type_c != ev_void && type_c != (*op->type_c)->type) continue; //??? but do not want anyway
    //
    if (op->associative != ASSOC_LEFT) {
      // assignment
      if (op->type_a == &type_pointer) {
        // ent var
        if (e->type->type != ev_pointer) {
          c = -200; // don't cast to a pointer
        } else if ((*op->type_c)->type == ev_void && op->type_b == &type_pointer && e2->type->type == ev_pointer) {
          c = 0; // generic pointer... FIXME: is this safe? make sure both sides are equivalent
        } else if (e->type->aux_type->type != (*op->type_b)->type) {
          // if e isn't a pointer to a type_b
          c = -200; // don't let the conversion work
        } else {
          c = QCC_canConv(e2, (*op->type_c)->type);
        }
      } else {
        c = QCC_canConv(e2, (*op->type_b)->type);
        // in this case, a is the final assigned value
        if (type_a != (*op->type_a)->type) c = -300; // don't use this op, as we must not change var b's type
      }
    } else {
      // not an assignment
      if (op->type_a == &type_pointer) {
        // ent var
        // if e isn't a pointer to a type_b
        if (e2->type->type != ev_pointer || e2->type->aux_type->type != (*op->type_b)->type) {
          c = -200; // don't let the conversion work
        } else {
          c = 0;
        }
      } else {
        c = QCC_canConv(e, (*op->type_a)->type);
        c += QCC_canConv(e2, (*op->type_b)->type);
      }
    }
    //
    if (c >= 0 && c < numconversions) {
      bestop = op;
      numconversions = c;
      if (c == 0) break; // can't get less conversions than 0...
    }
  }
  //
  if (numcvt != NULL) *numcvt = numconversions;
  return bestop;
}


/*
==============
QCC_PR_Expression

parse expression, generate code.

exprflags:
  EXPR_WARN_ABOVE_1
    emit warning if priority greater than 1
  EXPR_DISALLOW_COMMA
    don't parse commas as expression part (we need this for function calls, for example)

seems that 'simplestore' is qcc_true for simple 'STORE_*', but qcc_false for complex operations like '+='
'conditional':
  bit 0 set: this is conditional expression, warn on assigning
  bit 1 set: this was conditional initially, but now we are in '(...)'
==============
*/
static QCC_def_t *QCC_PR_ExpressionEx (int priority, int exprflags, QCC_def_t *gotarg0) {
  const QCC_opcode_t *op, *bestop;
  int numconversions, bprevst = -1;
  QCC_def_t *e, *e2, *ctemp = NULL;
  qcc_etype type_a, type_c;
  float negfinal = 0.0f;
  int islogand = 0;
  //
  if (priority > TOP_PRIORITY+4) priority = TOP_PRIORITY+4; // just in case
  // 0: term
  if (priority == 0) return QCC_PR_Term();
  // parse expression on the lower priority level
  e = (gotarg0 == NULL ? QCC_PR_Expression(priority-1, exprflags) : gotarg0);
  // ternary operator?
  if (priority == TERN_PRIORITY && QCC_PR_EatDelim("?")) {
    QCC_dstatement_t *fromj, *elsej;
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_IFNOT_I], e, NULL, &fromj));
    e = QCC_PR_Expression(TOP_PRIORITY, 0);
    e2 = QCC_GetTemp(e->type);
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[e2->type->size >= 3 ? OP_STORE_V : OP_STORE_F], e, e2, NULL));
    // e2 can be stomped upon until its reused anyway
    QCC_UnFreeTemp(e2);
    QCC_PR_Expect(":");
    QCC_PR_Statement(&pr_opcodes[OP_GOTO], NULL, NULL, &elsej);
    fromj->b = &statements[numstatements]-fromj;
    e = QCC_PR_Expression(TOP_PRIORITY, 0);
    if (qcc_typecmp(e->type, e2->type) != 0) QCC_PR_ParseError(0, "Ternary operator with mismatching types");
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[e2->type->size >= 3 ? OP_STORE_V : OP_STORE_F], e, e2, NULL));
    QCC_UnFreeTemp(e2);
    elsej->a = &statements[numstatements]-elsej;
    return e2;
  }
  // parse on the current priority level
  for (;;) {
    //if (gotarg0) fprintf(stderr, "gotarg0; token:[%s]; nofcall:%d\n", pr_token, exprflags&EXPR_DISALLOW_FCALL);
    // function call?
    if (priority == FUNC_PRIORITY && !(exprflags&EXPR_DISALLOW_FCALL)) {
      if (QCC_PR_EatDelim("(")) {
        qcc_usefulstatement = qcc_true;
        e = QCC_PR_ParseFunctionCall(e, NULL);
        continue;
        //k8:??? continue or return?
      }
    }
    //HACK: negative numbers eats '-', fix this
    if (pr_token_type == TT_IMMED) {
      if (pr_immediate_type->type == ev_float) {
        if (pr_immediate._float < 0) {
          // hehehe... was a minus all along...
          QCC_PR_IncludeChunk(pr_token, qcc_true, NULL);
          strcpy(pr_token, "+"); // two negatives would make a positive
          pr_token_type = TT_DELIM;
        }
      }
    }
    //
    if (pr_token_type != TT_DELIM) QCC_PR_ParseWarning(WARN_UNEXPECTEDPUNCT, "Expected punctuation");
    // find opcode if any
    op = NULL;
    for (const QCC_opcode_t *xop = pr_opcodes; xop->name != NULL; ++xop) {
      if (xop->priority != priority) continue; // not on this priority level
      if (!QCC_PR_EatDelim(xop->name)) continue; // not the given operator
      op = xop; // got it
      if (op->associative != ASSOC_LEFT) {
        // this is assign operator (only assigns have ASSOC_RIGHT)
        // if last statement is an indirect, change it to an address of
        e2 = NULL; // use as flag
        if (!simplestore) {
          // convert edict loads to 'taking pointer' instruction
          if (is_edict_load(statements[numstatements-1].op) && statements[numstatements-1].c == e->ofs) {
            qcc_usefulstatement = qcc_true; // hells why?
            statements[numstatements-1].op = OP_ADDRESS;
            type_pointer->aux_type->type = e->type->type;
            e->type = type_pointer;
          } else
          // if last statement retrieved a value, switch it to retrieve a usable pointer
          if (is_array_load(statements[numstatements-1].op)) {
            statements[numstatements-1].op = OP_GLOBALADDRESS;
            type_pointer->aux_type->type = e->type->type;
            e->type = type_pointer;
          } else
          // convert 'load pointer'
          if (is_pointer_load(statements[numstatements-1].op) && statements[numstatements-1].c == e->ofs) {
            if (!statements[numstatements-1].b) {
              // if the loadp has no offset, remove the instruction and convert the dest of this
              // instruction directly to the pointer's load address
              // this kills the add 0
              e->ofs = statements[numstatements-1].a;
              --numstatements;
              if (e->type->type != ev_pointer) {
                type_pointer->aux_type->type = e->type->type;
                e->type = type_pointer;
              }
            } else {
              // pointer arithmetics
              statements[numstatements-1].op = OP_POINTER_ADD;
              if (e->type->type != ev_pointer) {
                type_pointer->aux_type->type = e->type->type;
                e->type = type_pointer;
              }
            }
          } else
          // string indexing
          if (statements[numstatements-1].op == OP_LOADP_C && e->ofs == statements[numstatements-1].c) {
            //fprintf(stderr, "OPC: %s [%s]\n", op->name, op->opname);
            // now we want to make sure that string = float can't work without it being a dereferenced pointer
            // (we don't want to allow storep_c without dereferece)
            if (op->name[0] != '=') {
              // invalid string indexing operation (alas)
              QCC_PR_ParseErrorPrintDef(0, e, "TODO: '%s' and string indexing", op->name);
            }
            //
            e->type = type_string;
            statements[numstatements-1].op = OP_ADD_SF;
            e2 = QCC_PR_Expression(priority, exprflags);
            //
            if (e2->type->type == ev_float || e2->type->type == ev_integer) {
              //HACK!
              //FIXME: OP_STOREP_C can't index
              op = &pr_opcodes[OP_STOREP_C];
            } else {
              QCC_PR_ParseErrorPrintDef(0, e, "invalid string assign operation");
            }
          }
        }
        if (e2 == NULL) e2 = QCC_PR_Expression(priority, exprflags);
      } else {
        // normal operator (not assign)
        if (is_conditional_priority(op->priority)) {
          // boolean short circuit
          int opc, stn;
          // select opcode
          if (op->name[0] == '&') {
            islogand = 1;
            if (!qcc_typecmp(e->type, type_string)) opc = OP_IFNOT_S;
            else if (!qcc_typecmp(e->type, type_float)) opc = OP_IFNOT_F;
            else opc = OP_IFNOT_I;
          } else {
            islogand = 0;
            if (!qcc_typecmp(e->type, type_string)) opc = OP_IF_S;
            else if (!qcc_typecmp(e->type, type_float)) opc = OP_IF_F;
            else opc = OP_IF_I;
          }
          if (ctemp == NULL) {
            // boolean result is always float
            ctemp = QCC_GetTemp(/*e->type*/type_float);
            // this is the first logic op, so convert first operand to floatbool
            // store 'final result'
            //fprintf(stderr, "new ctemp at %u\n", numstatements);
            negfinal = (islogand ? 1.0f : 0.0f);
            QCC_PR_Statement3(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(1.0f-negfinal), ctemp, NULL, qcc_false);
          } else {
            //fprintf(stderr, " jump at %u\n", numstatements);
          }
          // now jump out of here if we know the result
          stn = numstatements;
          // statement 3 because we don't want to optimise this into if from not ifnot
          QCC_PR_Statement3(&pr_opcodes[opc], e, NULL, NULL, qcc_false);
          // patch chain
          statements[stn].sb = bprevst;
          bprevst = stn;
        }
        e2 = QCC_PR_Expression(priority-1, exprflags);
      }
      // type check
      type_a = e->type->type;
      /*type_b = e2->type->type;*/
      //if (type_a == ev_pointer && type_b == ev_pointer) QCC_PR_ParseWarning(0, "Debug: pointer op pointer");
      if (op->name[0] == '.') {
        // field access gets type from field
        type_c = (e2->type->aux_type ? e2->type->aux_type->type : -1); // -1: not a field
      } else {
        type_c = ev_void;
      }
      // find 'best type conversion'
      bestop = find_type_conversion(e, e2, op, type_a, type_c, &numconversions);
      if (bestop == NULL) {
        // no conversion found
        if (!is_conditional_priority(op->priority)) {
          if (op-pr_opcodes == OP_STOREP_C && e->type->type == ev_string) {
            e2 = QCC_SupplyConversion(e2, ev_float, qcc_true);
            if (e2->type->type != ev_float) {
              //QCC_PR_ParseWarning(WARN_LAXCAST, "type mismatch for %s (%s and %s)", op->name, e->type->name, e2->type->name);
              QCC_PR_ParseError(ERR_TYPEMISMATCH, "type mismatch for %s (%s and %s)", op->name, e->type->name, e2->type->name);
            }
          } else {
            QCC_PR_ParseError(ERR_TYPEMISMATCH, "type mismatch for %s (%s and %s)", op->name, e->type->name, e2->type->name);
          }
        }
      } else {
        if (numconversions > 3) QCC_PR_ParseWarning(WARN_IMPLICITCONVERSION, "Implicit conversion");
        op = bestop;
      }
      //
      //if (type_a == ev_pointer && type_b != e->type->aux_type->type) QCC_PR_ParseError ("type mismatch for %s", op->name);
      //
      if (op->associative != ASSOC_LEFT) {
        // assignment
        qcc_usefulstatement = qcc_true;
        if (priority == ASSIGN_PRIORITY && (e->constant || e->ofs < OFS_PARM0)) {
          if (e->type->type == ev_function) {
            QCC_PR_ParseWarning(WARN_ASSIGNMENTTOCONSTANTFUNC, "Assignment to function %s", e->name);
            QCC_PR_ParsePrintDef(WARN_ASSIGNMENTTOCONSTANTFUNC, e);
          } else {
            QCC_PR_ParseErrorPrintDef(0, e, "Assignment to constant %s", e->name);
          }
          qcc_set_badfile_name(strings+s_file, pr_source_line);
        }
        if (conditional&1) QCC_PR_ParseWarning(WARN_ASSIGNMENTINCONDITIONAL, "Assignment in conditional");
        e = QCC_PR_Statement(op, e2, e, NULL);
      } else {
        if (ctemp == NULL) {
          // not boolop
          e = QCC_PR_Statement(op, e, e2, NULL);
        } else {
          // boolop
          QCC_FreeTemp(e);
          e = e2; // go on
        }
      }
      // field access gets type from field
      if (type_c != ev_void/* && type_c != ev_string*/) e->type = e2->type->aux_type;
      if (priority > 1 && (exprflags&EXPR_WARN_ABOVE_1)) QCC_PR_ParseWarning(0, "You may wish to add brackets after that '!' operator");
      break;
    }
    // operator was (or wasn't) found, code was emited
    if (!op) {
      // th
      if (e == NULL) QCC_PR_ParseError(ERR_INTERNAL, "e == NULL");
      if (QCC_PR_EatDelim("++")) e = QCC_PR_ExprPMPost(e, '+');
      else if (QCC_PR_EatDelim("--")) e = QCC_PR_ExprPMPost(e, '-');
      break; // next token isn't at this priority level
    }
    // continue on this priority level
  }
  // fix patch chain, if any
  if (ctemp != NULL) {
    //fprintf(stderr, "negfinal at %u\n", numstatements);
    int opc, stn;
    // select opcode
    if (islogand) {
      if (!qcc_typecmp(e->type, type_string)) opc = OP_IFNOT_S;
      else if (!qcc_typecmp(e->type, type_float)) opc = OP_IFNOT_F;
      else opc = OP_IFNOT_I;
    } else {
      if (!qcc_typecmp(e->type, type_string)) opc = OP_IF_S;
      else if (!qcc_typecmp(e->type, type_float)) opc = OP_IF_F;
      else opc = OP_IF_I;
    }
    // now jump out of here if we know the result
    stn = numstatements;
    // statement 3 because we don't want to optimise this into if from not ifnot
    QCC_PR_Statement3(&pr_opcodes[opc], e, NULL, NULL, qcc_false);
    // patch chain
    statements[stn].sb = bprevst;
    bprevst = stn;
    // store 'negative final result'
    QCC_PR_Statement3(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(negfinal), ctemp, NULL, qcc_false);
    // drop out 'e' and change it to ctemp
    QCC_FreeTemp(e);
    e = ctemp;
    //fprintf(stderr, "bprevst=%d\n", bprevst);
    // now fix patch chain
    while (bprevst != -1) {
      int pnum = bprevst;
      //fprintf(stderr, " fixing %d (jump to %u)\n", pnum, numstatements);
      bprevst = statements[bprevst].sb;
      statements[pnum].sb = numstatements-pnum;
    }
  }
  //
  if (e == NULL) QCC_PR_ParseError(ERR_INTERNAL, "e == NULL");
  if (!(exprflags&EXPR_DISALLOW_COMMA) && priority == TOP_PRIORITY && QCC_PR_EatDelim(",")) {
    // comma is allowed, and this is expression without side-effects? warn!
    if (!qcc_usefulstatement) QCC_PR_ParseWarning(WARN_POINTLESSSTATEMENT, "Effectless statement");
    QCC_FreeTemp(e);
    qcc_usefulstatement = qcc_false;
    e = QCC_PR_Expression(TOP_PRIORITY, exprflags);
  }
  return e;
}


static QCC_def_t *QCC_PR_Expression (int priority, int exprflags) { return QCC_PR_ExpressionEx(priority, exprflags, NULL); }
