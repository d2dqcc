/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//compile routines
#include <setjmp.h>

#include "qcc.h"


char errorfile[128];
int errorline;
progfuncs_t *qccprogfuncs;
jmp_buf qcccompileerror;

int comp_nump;
char **comp_parms;


void PostCompile (void) {
  if (!qccpersisthunk) qccClearHunk();
  if (asmfile) {
    fclose(asmfile);
    asmfile = NULL;
  }
}


qccbool PreCompile (void) {
  QCC_PR_ResetErrorScope();
  qccClearHunk();
  qcchunk = malloc(qcchunksize = 256*1024*1024);
  while (!qcchunk && qcchunksize > 8*1024*1024) {
    qcchunksize /= 2;
    qcchunk = malloc(qcchunksize);
  }
  qccalloced = 0;
  return (!!qcchunk);
}


qccbool CompileParams (progfuncs_t *progfuncs, int doall, int nump, char **parms) {
  comp_nump = nump;
  comp_parms = parms;
  *errorfile = '\0';
  qccprogfuncs = progfuncs;
  if (setjmp(qcccompileerror)) {
    PostCompile();
    if (*errorfile) {
      qcc_printf("Error in %s on line %d\n", errorfile, errorline);
    }
    return qcc_false;
  }
  if (!PreCompile()) return qcc_false;
  QCC_main(nump, parms);
  while (qcc_compileactive) QCC_ContinueCompile();
  PostCompile();
  return qcc_true;
}


int Comp_Begin (progfuncs_t *progfuncs, int nump, char **parms) {
  comp_nump = nump;
  comp_parms = parms;
  qccprogfuncs = progfuncs;
  *errorfile = '\0';
  if (setjmp(qcccompileerror)) {
    PostCompile();
    return qcc_false;
  }
  if (!PreCompile()) return qcc_false;
  QCC_main(nump, parms);
  return qcc_true;
}


int Comp_Continue (progfuncs_t *progfuncs) {
  qccprogfuncs = progfuncs;
  if (setjmp(qcccompileerror)) {
    PostCompile();
    return qcc_false;
  }
  if (qcc_compileactive) {
    QCC_ContinueCompile();
  } else {
    PostCompile();
    return qcc_false;
  }
  return qcc_true;
}


void qcc_set_badfile_name (const char *fname, int line) {
  strcpy(errorfile, fname);
  errorline = line;
}
