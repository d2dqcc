/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _QCC_H_
#define _QCC_H_

#include <ctype.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


#ifndef QCC_GCC_PRINTF
# define QCC_GCC_PRINTF(x)  __attribute__((format(printf,x,x+1)))
#endif

#ifndef QCC_GCC_NORETURN
# define QCC_GCC_NORETURN  __attribute__((noreturn))
#endif

#ifndef QGG_GCC_PACKED
# define QGG_GCC_PACKED  __attribute__((packed,gcc_struct))
#endif


typedef enum { qcc_false, qcc_true } qccbool;

typedef unsigned char qbyte;
typedef int32_t qccint;


#include "qcc_vmop.h"
#include "qcc_vmstruct.h"
#include "qcc_cmdlib.h"


//this is for testing
#define QCC_WRITEASM


#define QCC_STAT_NOCONV  ((QCC_dstatement_t **)0xffffffff)


typedef struct {
  unsigned char *(*ReadFile) (const char *fname, void *buffer, int len);
  int (*FileSize) (const char *fname);  //-1 if file does not exist
  qccbool (*WriteFile) (const char *name, const void *data, int len);
  int (*printf) (const char *fmt, ...) QCC_GCC_PRINTF(1);
  void (*Sys_Error) (const char *fmt, ...) QCC_GCC_PRINTF(1);
  void (*Abort) (const char *fmt, ...) QCC_GCC_PRINTF(1);

  //small string allocation malloced and freed randomly by the executor. (use malloc if you want)
  void *(*memalloc) (size_t size);
  void (*memfree) (void *mem);
  void *(*memrealloc) (void *ptr, size_t newsize);
} progfuncs_t;


extern progfuncs_t *qccprogfuncs;

extern const char *compilingfile;
extern QCC_string_t s_file, s_file2; // filename for function definition
extern int qcc_compileactive;
extern jmp_buf qcccompileerror;

#define MAXSOURCEFILESLIST  (8)
extern char sourcefileslist[MAXSOURCEFILESLIST][1024];
extern int currentsourcefile;
extern int numsourcefiles;


#define qcc_pf_externs  (qccprogfuncs)
#define qcc_printf     (qcc_pf_externs->printf)
#define qcc_sys_error  (qcc_pf_externs->Sys_Error)
#define qcc_abort      (qcc_pf_externs->Abort)

#define qcc_memalloc  (qcc_pf_externs->memalloc)
#define qcc_memfree   (qcc_pf_externs->memfree)
#define qcc_realloc   (qcc_pf_externs->memrealloc)


//extern int PRHunkMark (progfuncs_t *progfuncs);
//extern void PRHunkFree (progfuncs_t *progfuncs, int mark);
//extern void *PRHunkAlloc (progfuncs_t *progfuncs, int size);
//extern void *PRAddressableAlloc (progfuncs_t *progfuncs, int ammount);

extern char *qcva (const char *text, ...) QCC_GCC_PRINTF(1);
extern void StripExtension (char *path);

//#define edvars(ed) (((edictrun_t*)ed)->fields)  //pointer to the field vars, given an edict

extern void SetEndian (void);
extern short (*PRBigShort) (short l);
extern short (*PRLittleShort) (short l);
extern int (*PRBigLong) (int l);
extern int (*PRLittleLong) (int l);
extern float (*PRBigFloat) (float l);
extern float (*PRLittleFloat) (float l);


extern int Comp_Begin (progfuncs_t *progfuncs, int nump, char **parms);
extern int Comp_Continue (progfuncs_t *progfuncs);

//extern int noextensions;

static inline int QC_strncasecmp (const char *s1, const char *s2, size_t n) { return strncasecmp(s1, s2, n); }
static inline int QC_strcasecmp (const char *s1, const char *s2) { return strcasecmp(s1, s2); }

extern int qccalloced;
extern int qcchunksize;
extern char *qcchunk;
extern int qccpersisthunk;

extern void *qccHunkAlloc (size_t mem);
extern void *qccHunkDupStr (const char *str);
extern void qccClearHunk (void);

extern short (*PRBigShort) (short l);
extern short (*PRLittleShort) (short l);
extern int (*PRBigLong) (int l);
extern int (*PRLittleLong) (int l);
extern float (*PRBigFloat) (float l);
extern float (*PRLittleFloat) (float l);


#define MAX_ERRORS (10)

#define MAX_NAME  (256) // chars long

extern int MAX_REGS;
extern int MAX_STRINGS;
extern int MAX_GLOBALS;
extern int MAX_FIELDS;
extern int MAX_STATEMENTS;
extern int MAX_FUNCTIONS;
extern int MAX_TEMPS;

//FIXME
#define MAX_DATA_PATH  (64)

extern int MAX_CONSTANTS;
#define MAXCONSTANTNAMELENGTH   (64)
#define MAXCONSTANTPARAMLENGTH  (32)
#define MAXCONSTANTPARAMS       (32)


#include "hash.h"
extern hashtable_t compconstantstable;
extern hashtable_t globalstable, localstable;


#ifdef QCC_WRITEASM
extern FILE *asmfile;
#endif
//=============================================================================
// offsets are always multiplied by 4 before using
typedef unsigned int gofs_t; // offset in global data block
typedef struct QCC_function_s QCC_function_t;
typedef struct QCC_def_s QCC_def_t;
typedef struct QCC_type_s QCC_type_t;


struct QCC_type_s {
  qcc_etype type;
  QCC_type_t *next;
  // function types are more complex
  QCC_type_t *aux_type; // return type or field type
  QCC_type_t *param; // function param types (linked by 'next')
  int num_parms; // -1 = variable args
  //int funcnum; // funcdef number+1; ofs can be set to 0 to indicate that there is no literal for it yet; see functions[]
  //struct QCC_type_s *parm_types[MAX_PARMS]; // only [num_parms] allocated
  unsigned int ofs; // inside a structure
  unsigned int size;
  unsigned int arraysize;
  char *name;
};

extern int qcc_typecmp (const QCC_type_t *a, const QCC_type_t *b); // 0 if same


typedef struct temp_s {
  gofs_t ofs;
  QCC_def_t *scope;
#ifdef QCC_WRITEASM
  QCC_def_t *lastfunc;
#endif
  struct temp_s *next;
  qccbool used;
  unsigned int size;
} temp_t;

extern void QCC_PurgeTemps (void);


// not written
struct QCC_def_s {
  QCC_type_t *type;
  char *name;
  QCC_def_t *next;
  QCC_def_t *nextlocal; // provides a chain of local variables for the opt_locals_marshalling optimisation
  gofs_t ofs;
  //int vecofs; // offset for vector part (valid only for ev_vector)
  QCC_def_t *scope; // function the var was defined in, or NULL
  QCC_function_t *func; // function definition
  // arrays and structs create multiple globaldef objects providing different types at the
  // different parts of the single object (struct), or alternative names (vectors).
  // this allows us to correctly set the const type based upon how its initialised.
  QCC_def_t *deftail;
  int initialized; // 1 when a declaration included "= immediate"
  int constant; // 1 says we can use the value over and over again
  int references;
  int timescalled; // part of the opt_stripfunctions optimisation
  int s_file;
  int s_line;
  int arraysize;
  QCC_def_t *ownerclass; // can be NULL
  qccbool shared;
  qccbool saved;
  qccbool isstatic;
  qccbool subscoped_away;
  temp_t *temp;
};


//=============================================================================
typedef union QCC_eval_s {
  QCC_string_t string;
  float _float;
  float vector[3];
  func_t function;
  int _int;
  union QCC_eval_s *ptr;
} QCC_eval_t;

const extern unsigned int type_size[];

extern QCC_type_t
  *type_void,
  *type_string,
  *type_float,
  *type_vector,
  *type_entity,
  *type_field,
  *type_function,
  *type_pointer,
  *type_integer,
  *type_floatfield;

struct QCC_function_s {
  int builtin; // if non 0, call an internal function
  int code; // first statement
  char *file; // source file with definition
  int file_line;
  int funcnum; // in functions[]
  QCC_def_t *def;
  unsigned int parm_ofs[MAX_PARMS]; // always contiguous, right?
};


//
// output generated by prog parsing
//
typedef struct {
  char *memory;
  int max_memory;
  int current_memory;
  QCC_type_t *types;
  QCC_def_t def_head; // unused head of linked list
  QCC_def_t *def_tail; // add new defs after this and move it
  QCC_def_t *localvars; // chain of variables which need to be pushed and stuff.
  QCC_def_t *prevlocvars;
  int size_fields;
} QCC_pr_info_t;

extern  QCC_pr_info_t pr;


typedef struct {
  char name[MAXCONSTANTNAMELENGTH];
  char *value;
  char params[MAXCONSTANTPARAMS][MAXCONSTANTPARAMLENGTH];
  int numparams;
  qccbool used;
  qccbool inside;
  int namelen;
} CompilerConstant_t;

extern CompilerConstant_t *CompilerConstant;


//============================================================================
extern qccbool pr_dumpasm;

typedef enum {
  TT_EOF,       // end of file reached
  TT_ID,        // identifier
  TT_DELIM,     // code punctuation
  TT_IMMED,     // string, float, vector
} token_type_t;

#define QCC_MAX_TOKEN_SIZE  (8191) // without trailing zero
extern char pr_token[QCC_MAX_TOKEN_SIZE+1];
extern token_type_t pr_token_type;
extern QCC_type_t *pr_immediate_type;
extern QCC_eval_t pr_immediate;

extern qccbool flag_assume_integer;

extern qccbool opt_overlaptemps;
extern qccbool opt_shortenifnots;
extern qccbool opt_nonvec_parms;
extern qccbool opt_constant_names;
extern qccbool opt_unreferenced;
extern qccbool opt_locals;
extern qccbool opt_constant_names_strings;
extern qccbool opt_compound_jumps;
extern qccbool opt_locals_marshalling;
extern qccbool opt_vectorcalls;

extern int optres_shortenifnots;
extern int optres_overlaptemps;
extern int optres_noduplicatestrings;
extern int optres_constantarith;
extern int optres_nonvec_parms;
extern int optres_constant_names;
extern int optres_assignments;
extern int optres_unreferenced;
extern int optres_locals;
extern int optres_dupconstdefs;
extern int optres_constant_names_strings;
extern int optres_compound_jumps;
extern int optres_locals_marshalling;

extern qccbool CompileParams (progfuncs_t *progfuncs, int doall, int nump, char **parms);

extern void QCC_PR_PrintStatement (QCC_dstatement_t *s);

// reads the next token into pr_token and classifies its type
extern void QCC_PR_Lex (void);
extern qccbool PR_Lex_IsTypeName (const char *tk);

extern QCC_type_t *QCC_PR_NewType (const char *name, int basictype);
extern QCC_type_t *QCC_PointerTypeTo(QCC_type_t *type);
extern QCC_def_t *QCC_PR_ParseDefs (qccbool nocommas);
extern QCC_type_t *QCC_PR_ParseType (int newtype, qccbool silentfail);

extern qccbool type_inlinefunction;

extern QCC_type_t *QCC_TypeForName (const char *name);
extern QCC_type_t *QCC_PR_ParseFunctionType (int newtype, QCC_type_t *returntype);
extern char *QCC_PR_ParseName (void);
extern CompilerConstant_t *QCC_PR_DefineName (const char *name);

extern void QCC_RemapOffsets (unsigned int firststatement, unsigned int laststatement, unsigned int min, unsigned int max, unsigned int newmin);

extern int QCC_PR_IntConstExpr (void);

extern qccbool QCC_PR_EatImmediate (const char *string);
extern qccbool QCC_PR_EatDelim (const char *string);
extern qccbool QCC_PR_EatId (const char *string);
extern void QCC_PR_Expect (const char *string);
extern qccbool QCC_PR_EatKeyword (const char *string);
extern void QCC_PR_ParseError (int errortype, const char *error, ...) QCC_GCC_NORETURN QCC_GCC_PRINTF(2);
extern void QCC_PR_ParseWarning (int warningtype, const char *error, ...) QCC_GCC_PRINTF(2);
extern qccbool QCC_PR_Warning (int type, const char *file, int line, const char *error, ...) QCC_GCC_PRINTF(4);
extern void QCC_PR_Note (int type, const char *file, int line, const char *error, ...) QCC_GCC_PRINTF(4);
extern void QCC_PR_ParsePrintDef (int warningtype, QCC_def_t *def);
extern void QCC_PR_ParseErrorPrintDef (int errortype, QCC_def_t *def, const char *error, ...) QCC_GCC_NORETURN QCC_GCC_PRINTF(3);

extern int QCC_WarningForName (const char *name);

//QccMain.c must be changed if this is changed.
enum {
  WARN_DEBUGGING,
  WARN_ERROR,
  WARN_NOTREFERENCED,
  WARN_NOTREFERENCEDCONST,
  WARN_CONFLICTINGRETURNS,
  WARN_TOOFEWPARAMS,
  WARN_TOOMANYPARAMS,
  WARN_UNEXPECTEDPUNCT,
  WARN_ASSIGNMENTTOCONSTANT,
  WARN_ASSIGNMENTTOCONSTANTFUNC,
  WARN_MISSINGRETURNVALUE,
  WARN_WRONGRETURNTYPE,
  WARN_CORRECTEDRETURNTYPE,
  WARN_POINTLESSSTATEMENT,
  WARN_MISSINGRETURN,
  WARN_DUPLICATEDEFINITION,
  WARN_UNDEFNOTDEFINED,
  WARN_PRECOMPILERMESSAGE,
  WARN_TOOMANYPARAMETERSFORFUNC,
  WARN_STRINGTOOLONG,
  WARN_BADPRAGMA,
  WARN_HANGINGSLASHR,
  WARN_NOTDEFINED,
  WARN_NOTCONSTANT,
  WARN_SWITCHTYPEMISMATCH,
  WARN_CONFLICTINGUNIONMEMBER,
  WARN_ENUMFLAGS_NOTINTEGER,
  WARN_ENUMFLAGS_NOTBINARY,
  WARN_DUPLICATELABEL,
  WARN_DUPLICATEMACRO,
  WARN_ASSIGNMENTINCONDITIONAL,
  WARN_MACROINSTRING,
  WARN_BADPARAMS,
  WARN_IMPLICITCONVERSION,
  WARN_FIXEDRETURNVALUECONFLICT,
  WARN_EXTRAPRECACHE,
  WARN_NOTPRECACHED,
  WARN_DEADCODE,
  WARN_UNREACHABLECODE,
  WARN_NOTSTANDARDBEHAVIOUR,
  WARN_INEFFICIENTPLUSPLUS,
  WARN_DUPLICATEPRECOMPILER,
  WARN_IDENTICALPRECOMPILER,
  WARN_D2DQCC_SPECIFIC, //extension that only D2DQCC will have a clue about
  WARN_IFSTRING_USED,
  WARN_LAXCAST, //some errors become this with a compiler flag
  WARN_UNDESIRABLECONVENTION,
  WARN_SAMENAMEASGLOBAL,
  WARN_CONSTANTCOMPARISON,
  WARN_UNSAFEFUNCTIONRETURNTYPE,
  WARN_MISSINGOPTIONAL,

  ERR_PARSEERRORS, // caused by qcc_pr_parseerror being called

  // these are definetely my fault...
  ERR_INTERNAL,
  ERR_TOOCOMPLEX,
  ERR_BADOPCODE,
  ERR_TOOMANYSTATEMENTS,
  ERR_TOOMANYSTRINGS,
  ERR_BADTARGETSWITCH,
  ERR_TOOMANYTYPES,
  ERR_TOOMANYPAKFILES,
  ERR_PRECOMPILERCONSTANTTOOLONG,
  ERR_MACROTOOMANYPARMS,
  ERR_TOOMANYFRAMEMACROS,

  // limitations, some are imposed by compiler, some arn't
  ERR_TOOMANYGLOBALS,
  ERR_TOOMANYGOTOS,
  ERR_TOOMANYBREAKS,
  ERR_TOOMANYCONTINUES,
  ERR_TOOMANYCASES,
  ERR_TOOMANYLABELS,
  ERR_TOOMANYOPENFILES,
  ERR_TOOMANYPARAMETERSVARARGS,
  ERR_TOOMANYTOTALPARAMETERS,

  // these are probably yours, or qcc being fussy
  ERR_KEYWORDDISABLED,
  ERR_BADEXTENSION,
  ERR_BADIMMEDIATETYPE,
  ERR_NOOUTPUT,
  ERR_NOTAFUNCTION,
  ERR_FUNCTIONWITHVARGS,
  ERR_BADHEX,
  ERR_UNKNOWNPUCTUATION,
  ERR_EXPECTED,
  ERR_NOTANAME,
  ERR_NAMETOOLONG,
  ERR_NOFUNC,
  ERR_COULDNTOPENFILE,
  ERR_NOTFUNCTIONTYPE,
  ERR_TOOFEWPARAMS,
  ERR_TOOMANYPARAMS,
  ERR_CONSTANTNOTDEFINED,
  ERR_BADFRAMEMACRO,
  ERR_TYPEMISMATCH,
  ERR_TYPEMISMATCHREDEC,
  ERR_TYPEMISMATCHPARM,
  ERR_TYPEMISMATCHARRAYSIZE,
  ERR_UNEXPECTEDPUNCTUATION,
  ERR_NOTACONSTANT,
  ERR_REDECLARATION,
  ERR_INITIALISEDLOCALFUNCTION,
  ERR_NOTDEFINED,
  ERR_ARRAYNEEDSSIZE,
  ERR_ARRAYNEEDSBRACES,
  ERR_TOOMANYINITIALISERS,
  ERR_TYPEINVALIDINSTRUCT,
  ERR_NOSHAREDLOCALS,
  ERR_TYPEWITHNONAME,
  ERR_BADARRAYSIZE,
  ERR_NONAME,
  ERR_SHAREDINITIALISED,
  ERR_UNKNOWNVALUE,
  ERR_BADARRAYINDEXTYPE,
  ERR_NOVALIDOPCODES,
  ERR_MEMBERNOTVALID,
  ERR_BADPLUSPLUSOPERATOR,
  ERR_BADNOTTYPE,
  ERR_BADTYPECAST,
  ERR_MULTIPLEDEFAULTS,
  ERR_CASENOTIMMEDIATE,
  ERR_BADSWITCHTYPE,
  ERR_BADLABELNAME,
  ERR_NOLABEL,
  ERR_THINKTIMETYPEMISMATCH,
  ERR_STATETYPEMISMATCH,
  ERR_BADBUILTINIMMEDIATE,
  ERR_PARAMWITHNONAME,
  ERR_BADPARAMORDER,
  ERR_ILLEGALCONTINUES,
  ERR_ILLEGALBREAKS,
  ERR_ILLEGALCASES,
  ERR_NOTANUMBER,
  ERR_WRONGSUBTYPE,
  ERR_EOF,
  ERR_NOPRECOMPILERIF,
  ERR_NOENDIF,
  ERR_HASHERROR,
  ERR_NOTATYPE,
  ERR_TOOMANYPACKFILES,
  ERR_INVALIDVECTORIMMEDIATE,
  ERR_INVALIDSTRINGIMMEDIATE,
  ERR_BADCHARACTERCODE,
  ERR_BADPARMS,
  ERR_IDTOOLONG,
  ERR_WERROR,

  WARN_MAX
};


#define FLAG_KILLSDEBUGGERS  (1<<0)
#define FLAG_ASDEFAULT       (1<<1)
#define FLAG_MIDCOMPILE      (1<<2) // option can be changed mid-compile with the special pragma


typedef struct {
  qccbool *enabled;
  const char *abbrev;
  int optimisationlevel;
  int flags;  // bit 0: kills debuggers; bit 1: applied as default (see FLAG_xxx)
  const char *fullname;
  const char *description;
  const void *guiinfo;
} optimisations_t;
extern optimisations_t optimisations[];


typedef struct {
  qccbool *enabled;
  int flags; // bit 1: applied as default (see FLAG_xxx)
  const char *abbrev;
  const char *fullname;
  const char *description;
  const void *guiinfo;
} compiler_flag_t;
extern compiler_flag_t compiler_flag[];

extern qccbool qccwarningdisabled[WARN_MAX];

extern jmp_buf pr_parse_abort; // longjump with this on parse error
extern int pr_source_line;
extern /*const*/ char *pr_file_p; //FIXME: leaks memory?

extern void *QCC_PR_Malloc (int size);


extern QCC_def_t *pr_scope;
extern int pr_error_count, pr_warning_count;

extern void QCC_PR_NewLine (qccbool incomment);
extern QCC_def_t *QCC_PR_GetDef (QCC_type_t *type, const char *name, QCC_def_t *scope, qccbool allocate, int arraysize, qccbool saved);

extern void QCC_PR_PrintDefs (void);

extern void QCC_PR_SkipToSemicolon (void);

#define MAX_EXTRA_PARMS  (128)

#ifdef MAX_EXTRA_PARMS
extern char pr_parm_names[MAX_PARMS+MAX_EXTRA_PARMS][MAX_NAME];
extern QCC_def_t *extra_parms[MAX_EXTRA_PARMS];
#else
extern char pr_parm_names[MAX_PARMS][MAX_NAME];
#endif
extern qccbool pr_trace;

#define G_FLOAT(o)     (qcc_pr_globals[o])
#define G_INT(o)       (*(int *)&qcc_pr_globals[o])
#define G_VECTOR(o)    (&qcc_pr_globals[o])
#define G_STRING(o)    (strings + *(QCC_string_t *)&qcc_pr_globals[o])
//#define G_FUNCTION(o)  (*(func_t *)&qcc_pr_globals[o])


extern qccbool QCC_PR_CompileFile (char *string, const char *filename);
extern void QCC_PR_ResetErrorScope (void);

extern qccbool pr_dumpasm;
extern QCC_string_t s_file; // filename for function definition
extern QCC_def_t def_ret, def_parms[MAX_PARMS];

extern void QCC_PR_EmitClassFromFunction (QCC_def_t *scope, const char *tname);

extern void PostCompile (void);
extern qccbool PreCompile (void);

//=============================================================================
extern char pr_immediate_string[8192];

extern float *qcc_pr_globals;
extern unsigned int numpr_globals;

extern char *strings;
extern int strofs;

extern QCC_dstatement_t *statements;
extern int numstatements;
extern int *statement_linenums;

extern QCC_dfunction_t *functions;
extern int numfunctions;

extern QCC_ddef_t *qcc_globals;
extern int numglobaldefs;

extern QCC_def_t *activetemps;

extern QCC_ddef_t *fields;
extern int numfielddefs;

extern QCC_type_t *qcc_typeinfo;
extern int numtypeinfos;
extern int MAX_TYPEINFOS;

extern int ForcedCRC;
extern qccbool defaultnoref;
extern qccbool defaultstatic;

extern int *qcc_tempofs;
//extern int qcc_functioncalled;  //unuse temps if this is true - don't want to reuse the same space.

extern int tempsstart;
extern int numtemps;

typedef char PATHSTRING[MAX_DATA_PATH];

extern int QCC_CopyString (const char *str);


typedef struct qcc_cachedsourcefile_s {
  char filename[128];
  int size;
  char *file;
  enum {FT_CODE, FT_DATA} type; // quakec source file or not
  struct qcc_cachedsourcefile_s *next;
} qcc_cachedsourcefile_t;
extern qcc_cachedsourcefile_t *qcc_sourcefile;


extern const char *QCC_TypeName (QCC_type_t *type);
extern void QCC_PR_IncludeChunk (char *data, qccbool duplicate, const char *filename);
extern void QCC_PR_IncludeChunkEx(char *data, qccbool duplicate, const char *filename, CompilerConstant_t *cnst);
extern qccbool QCC_PR_UnInclude (void);

extern void QCC_main (int argc, char **argv);
extern void QCC_ContinueCompile (void);
extern void QCC_FinishCompile (void);

extern void qcc_set_badfile_name (const char *fname, int line);

extern qccbool QCC_Include (const char *filename);
extern void QCC_PR_LexWhitespace (void);
extern qccbool QCC_PR_SimpleGetToken (void);

extern qccbool expandedemptymacro;
extern struct qcc_includechunk_s *currentchunk;
extern int recursivefunctiontype;
extern struct freeoffset_s *qcc_freeofs;

extern QCC_type_t *QCC_PR_FindType (QCC_type_t *type);
extern QCC_type_t *QCC_PR_PointerType (QCC_type_t *pointsto);

extern int QCC_CheckParm (const char *check);
extern CompilerConstant_t *QCC_PR_EatCompConstDefined (const char *def);

extern void QCC_Clear_Memory (int dofree);

extern hashtable_t floatconstdefstable;
extern hashtable_t stringconstdefstable;
extern hashtable_t stringconstdefstable_trans;
extern int dotranslate_count;


#endif
