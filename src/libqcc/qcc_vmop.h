/*
 * QuakeC VM opcodes (and some offsets) definitions
 * this file is shared by the execution and compiler
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _QCC_VMOP_H_
#define _QCC_VMOP_H_

#ifdef __cplusplus
extern "C" {
#endif


typedef enum {
  ev_void,      // 0
  ev_string,    // 1
  ev_float,     // 2
  ev_vector,    // 3
  ev_entity,    // 4
  ev_field,     // 5
  ev_function,  // 6
  ev_pointer,   // 7
  ev_integer    // 8
} qcc_etype;


enum {
  OFS_NULL      = 0,
  OFS_RETURN    = 1,
  // leave 3 ofs for each parm to hold vectors
  OFS_PARM0     = 4,
  OFS_PARM1     = 7,
  OFS_PARM2     = 10,
  OFS_PARM3     = 13,
  OFS_PARM4     = 16,
  OFS_PARM5     = 19,
  OFS_PARM6     = 22,
  OFS_PARM7     = 25,
  OFS_PARMTHIS  = 28,
  RESERVED_OFS  = 29,
  //
  MAX_PARMS = 8
};


typedef enum qcvm_opcode_e {
  OP_DONE,

  OP_MUL_F,
  OP_MUL_V,
  OP_MUL_FV, // NOTE: the float operands must NOT be at the same locations: A != C
  OP_MUL_VF, // and here: B != C

  OP_DIV_F,

  OP_ADD_F,
  OP_ADD_V,

  OP_SUB_F,
  OP_SUB_V,

  OP_EQ_F,
  OP_EQ_V,
  OP_EQ_S,
  OP_EQ_E,
  OP_EQ_FNC,

  OP_NE_F,
  OP_NE_V,
  OP_NE_S,
  OP_NE_E,
  OP_NE_FNC,

  OP_LE_F,
  OP_GE_F,
  OP_LT_F,
  OP_GT_F,

  OP_LOAD_F,
  OP_LOAD_V,
  OP_LOAD_S,
  OP_LOAD_ENT,
  OP_LOAD_FLD,
  OP_LOAD_FNC,

  OP_ADDRESS,

  OP_STORE_F,
  OP_STORE_V,
  OP_STORE_S,
  OP_STORE_ENT,
  OP_STORE_FLD,
  OP_STORE_FNC,

  OP_STOREP_F,
  OP_STOREP_V,
  OP_STOREP_S,
  OP_STOREP_ENT,
  OP_STOREP_FLD,
  OP_STOREP_FNC,

  OP_RETURN,
  OP_NOT_F,
  OP_NOT_V,
  OP_NOT_S,
  OP_NOT_ENT,
  OP_NOT_FNC,
  OP_IF_I,
  OP_IFNOT_I,
  OP_CALL0,
  OP_CALL1,
  OP_CALL2,
  OP_CALL3,
  OP_CALL4,
  OP_CALL5,
  OP_CALL6,
  OP_CALL7,
  OP_CALL8,
  OP_STATE,
  OP_GOTO,
  OP_AND_F,
  OP_OR_F,

  OP_BITAND_F,
  OP_BITOR_F,

  OP_FETCH_GBL_F,
  OP_FETCH_GBL_V,
  OP_FETCH_GBL_S,
  OP_FETCH_GBL_E,
  OP_FETCH_GBL_FNC,

  OP_CONV_ITOF,
  OP_CONV_FTOI,
  OP_CP_ITOF,
  OP_CP_FTOI,

  OP_LOAD_I,
  OP_STOREP_I,
  OP_STOREP_IF,
  OP_STOREP_FI,

  OP_LE_I,
  OP_GE_I,
  OP_LT_I,
  OP_GT_I,
  OP_EQ_I,
  OP_EQ_IF,
  OP_EQ_FI,
  OP_NE_I,

  OP_BITSET,
  OP_BITCLR,

  OP_BOUNDCHECK,

  OP_STORE_I,
  OP_STORE_IF,
  OP_STORE_FI,

  OP_ADD_I,
  OP_ADD_FI,
  OP_ADD_IF,

  OP_SUB_I,
  OP_SUB_FI,
  OP_SUB_IF,

  OP_BITAND_I,
  OP_BITOR_I,

  OP_MUL_I,
  OP_DIV_I,

  // note that this will be generated on 'if (s == "")' and so on, not directly
  OP_IFNOT_S,
  OP_IF_S,

  OP_XOR_I,
  OP_RSHIFT_I,
  OP_LSHIFT_I,

  OP_IF_F,
  OP_IFNOT_F,

  OP_LOADA_F,
  OP_LOADA_V,
  OP_LOADA_S,
  OP_LOADA_ENT,
  OP_LOADA_FLD,
  OP_LOADA_FNC,
  OP_LOADA_I,

  OP_GLOBALADDRESS,
  OP_POINTER_ADD, // 32-bit pointers

  OP_STORE_P,
  //OP_LOAD_P,

  OP_LOADP_F,
  OP_LOADP_V,
  OP_LOADP_S,
  OP_LOADP_ENT,
  OP_LOADP_FLD,
  OP_LOADP_FNC,
  OP_LOADP_I,

  //-------------------------------------
  //string manipulation.
  OP_ADD_SF,  //(char*)c = (char*)a + (float)b
  OP_SUB_S,   //(float)c = (char*)a - (char*)b
  OP_STOREP_C,//(float)c = *(char*)b = (float)a
  OP_LOADP_C, //(float)c = *(char*)
  //-------------------------------------

  OP_BITAND_IF,
  OP_BITOR_IF,
  OP_BITAND_FI,
  OP_BITOR_FI,

  OP_BITSETP,
  OP_BITCLRP,

  // the rest are added
  // mostly they are various different ways of adding two vars with conversions
  OP_MULSTORE_F,
  OP_MULSTORE_V,
  OP_MULSTOREP_F,
  OP_MULSTOREP_V,

  OP_DIVSTORE_F,
  OP_DIVSTOREP_F,

  OP_ADDSTORE_F,
  OP_ADDSTORE_V,
  OP_ADDSTOREP_F,
  OP_ADDSTOREP_V,

  OP_SUBSTORE_F,
  OP_SUBSTORE_V,
  OP_SUBSTOREP_F,
  OP_SUBSTOREP_V,

  OP_LE_IF,
  OP_GE_IF,
  OP_LT_IF,
  OP_GT_IF,

  OP_LE_FI,
  OP_GE_FI,
  OP_LT_FI,
  OP_GT_FI,

  OP_MUL_IF,
  OP_MUL_FI,
  OP_MUL_VI,
  OP_MUL_IV,
  OP_DIV_IF,
  OP_DIV_FI,
  OP_AND_I,
  OP_OR_I,
  OP_AND_IF,
  OP_OR_IF,
  OP_AND_FI,
  OP_OR_FI,
  OP_NE_IF,
  OP_NE_FI,

  OP_CONV_FTOS,
  OP_CONV_ITOS,
  OP_CONV_VTOS,
  OP_CONV_ETOS,

  OP_NUMREALOPS,

  // these ops are emulated out, always, and are only present in the compiler
  OP_BITSET_I,
  OP_BITSETP_I,

  OP_MULSTORE_I,
  OP_DIVSTORE_I,
  OP_ADDSTORE_I,
  OP_SUBSTORE_I,
  OP_MULSTOREP_I,
  OP_DIVSTOREP_I,
  OP_ADDSTOREP_I,
  OP_SUBSTOREP_I,

  OP_MULSTORE_IF,
  OP_MULSTOREP_IF,
  OP_DIVSTORE_IF,
  OP_DIVSTOREP_IF,
  OP_ADDSTORE_IF,
  OP_ADDSTOREP_IF,
  OP_SUBSTORE_IF,
  OP_SUBSTOREP_IF,

  OP_MULSTORE_FI,
  OP_MULSTOREP_FI,
  OP_DIVSTORE_FI,
  OP_DIVSTOREP_FI,
  OP_ADDSTORE_FI,
  OP_ADDSTOREP_FI,
  OP_SUBSTORE_FI,
  OP_SUBSTOREP_FI,
} qcvm_opcode;


#ifdef __cplusplus
}
#endif
#endif
