/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "qcc.h"
#include "hash.h"


static const char *basictypenames[] = {
  "void",
  "string",
  "float",
  "vector",
  "entity",
  "field",
  "function",
  "pointer",
  "integer",
};


qccbool flag_assume_integer;        // is that an integer or a float? qcc says float. but we support int too, so maybe we want that instead?

qccbool opt_overlaptemps;           // reduce numpr_globals by reuse of temps: when they are not needed they are freed for reuse
qccbool opt_shortenifnots;          // if(!var) is made an IF rather than NOT IFNOT
qccbool opt_nonvec_parms;           // store_f instead of store_v on function calls, where possible
qccbool opt_constant_names;         // take out the defs and name strings of constants
qccbool opt_constant_names_strings; // removes the defs of strings too (plays havok with multiprogs)
qccbool opt_unreferenced;           // strip defs that are not referenced
qccbool opt_locals = 0;             // strip out the names of locals and immediates
qccbool opt_compound_jumps;         // jumps to jump statements jump to the final point
qccbool opt_locals_marshalling;     // make the local vars of all functions occupy the same globals
qccbool opt_vectorcalls;            // vectors can be packed into 3 floats, which can yield lower numpr_globals, but cost two more statements per call
qccbool opt_simplifiedifs;          // if (f != 0) -> if (f). if (f == 0) -> ifnot (f)

//these are the results of the opt_. The values are printed out when compilation is compleate, showing effectivness.
int optres_shortenifnots;
int optres_assignments;
int optres_overlaptemps;
int optres_noduplicatestrings;
int optres_constantarith;
int optres_nonvec_parms;
int optres_constant_names;
int optres_constant_names_strings;
int optres_unreferenced;
int optres_locals;
int optres_dupconstdefs;
int optres_compound_jumps;
int optres_locals_marshalling;

qccbool simplestore;
qccbool expandedemptymacro;

QCC_pr_info_t pr;
//QCC_def_t   **pr_global_defs/*[MAX_REGS]*/; // to find def for a global variable

//keeps track of how many funcs are called while parsing a statement
//int qcc_functioncalled;

//========================================

QCC_def_t *pr_scope; // the function being parsed, or NULL
qccbool pr_dumpasm;
QCC_string_t s_file, s_file2;      // filename for function definition

unsigned int      locals_start;   // for tracking local variables vs temps
unsigned int      locals_end;   // for tracking local variables vs temps

jmp_buf   pr_parse_abort;   // longjump with this on parse error

qccbool qcc_usefulstatement;

int max_breaks;
int max_continues;
int max_cases;
int num_continues;
int num_breaks;
int num_cases;
int *pr_breaks;
int *pr_continues;
int *pr_cases;
QCC_def_t **pr_casesdef;
QCC_def_t **pr_casesdef2;

typedef struct {
  int statementno;
  int lineno;
  char name[256];
} gotooperator_t;

int max_labels;
int max_gotos;
gotooperator_t *pr_labels;
gotooperator_t *pr_gotos;
int num_gotos;
int num_labels;

QCC_def_t *extra_parms[MAX_EXTRA_PARMS];

static int conditional;
temp_t *functemps;    // floats/strings/funcs/ents...

static int constchecks;
static int varchecks;
static int typechecks;


typedef struct freeoffset_s {
  struct freeoffset_s *next;
  gofs_t ofs;
  unsigned int size;
} freeoffset_t;

freeoffset_t *qcc_freeofs;


////////////////////////////////////////////////////////////////////////////////
#include "qcc_pr_comp_operators.c"


////////////////////////////////////////////////////////////////////////////////
static QCC_def_t *QCC_SupplyConversion (QCC_def_t *var, qcc_etype wanted, qccbool fatal);

static void QCC_PR_ParseStatement (qccbool nosublocs);
static QCC_def_t *QCC_PR_Statement (const QCC_opcode_t *op, QCC_def_t *var_a, QCC_def_t *var_b, QCC_dstatement_t **outstatement);
static QCC_dstatement_t *QCC_PR_SimpleStatement (int op, int var_a, int var_b, int var_c, int force);
static int QCC_AStatementJumpsTo (int targ, int first, int last);
static qccbool QCC_StatementIsAJump (int stnum, int notifdest);
static QCC_def_t *QCC_PR_DummyDef (QCC_type_t *type, const char *name, QCC_def_t *scope, int arraysize, unsigned int ofs, int referable, qccbool saved);

static gofs_t QCC_GetFreeOffsetSpace (unsigned int size);

static void QCC_WriteAsmFunction (QCC_def_t *sc, unsigned int firststatement, gofs_t firstparm);


enum {
  EXPR_WARN_ABOVE_1 = 2,
  EXPR_DISALLOW_COMMA = 4,
  EXPR_DISALLOW_FCALL = 8
};

static QCC_def_t *QCC_PR_Expression (int priority, int exprflags);


////////////////////////////////////////////////////////////////////////////////
static inline qccbool is_branch_op (int op) {
  switch (op) {
    case OP_IF_I: case OP_IFNOT_I:
    case OP_IF_S: case OP_IFNOT_S:
    case OP_IF_F: case OP_IFNOT_F:
    case OP_GOTO:
      return qcc_true;
  }
  return qcc_false;
}


static inline int branch_dest (int opofs) {
  return opofs+statements[opofs].sargs[statements[opofs].op == OP_GOTO ? 0 : 1];
}


////////////////////////////////////////////////////////////////////////////////
static inline qccbool OpAssignsToC (unsigned int op) {
  // calls, switches and cases DON'T
  if (pr_opcodes[op].type_c == &type_void) return qcc_false;
  if (op >= OP_BITSET && op <= OP_BITCLRP) return qcc_false;
  /*if (op >= OP_STORE_I && op <= OP_STORE_FI) return qcc_false; <- add STOREP_*?*/
  if (op == OP_STOREP_C/* || op == OP_LOADP_C*/) return qcc_false;
  if (op >= OP_MULSTORE_F && op <= OP_SUBSTOREP_V) return qcc_false;
  return qcc_true;
}


/*
static qccbool OpAssignsToB (unsigned int op) {
  if (op >= OP_BITSET && op <= OP_BITCLRP) return qcc_true;
  if (op >= OP_STORE_I && op <= OP_STORE_FI) return qcc_true;
  if (op == OP_STOREP_C || op == OP_LOADP_C) return qcc_true;
  if (op >= OP_MULSTORE_F && op <= OP_SUBSTOREP_V) return qcc_true;
  if (op >= OP_STORE_F && op <= OP_STOREP_FNC) return qcc_true;
  return qcc_false;
}
*/


// no emulated opcodes
static inline qccbool QCC_OPCodeValid (const QCC_opcode_t *op) {
  return (op-pr_opcodes < OP_NUMREALOPS);
}


//===========================================================================
static QCC_def_t *QCC_MakeDummyIntConst (void) {
  QCC_def_t *cn;
  // allocate a new one
  cn = (void *)qccHunkAlloc(sizeof(QCC_def_t));
  cn->next = NULL;
  pr.def_tail->next = cn;
  pr.def_tail = cn;
  cn->type = type_integer;
  cn->name = "IMMEDIATE";
  cn->constant = qcc_true;
  cn->initialized = 1;
  cn->scope = NULL; // always share immediates
  cn->arraysize = 1;
  // copy the immediate to the global area
  cn->ofs = QCC_GetFreeOffsetSpace(type_size[type_integer->type]);
  G_INT(cn->ofs) = 666;
  return cn;
}


static QCC_def_t *QCC_MakeIntConst (int value) {
  QCC_def_t *cn;
  // check for a constant with the same value
  for (cn = pr.def_head.next; cn != NULL; cn = cn->next) {
    ++varchecks;
    if (!cn->initialized) continue;
    if (!cn->constant) continue;
    ++constchecks;
    if (cn->type != type_integer) continue;
    ++typechecks;
    if (G_INT(cn->ofs) == value) return cn;
  }
  // allocate a new one
  cn = (void *)qccHunkAlloc(sizeof(QCC_def_t));
  cn->next = NULL;
  pr.def_tail->next = cn;
  pr.def_tail = cn;
  cn->type = type_integer;
  cn->name = "IMMEDIATE";
  cn->constant = qcc_true;
  cn->initialized = 1;
  cn->scope = NULL; // always share immediates
  cn->arraysize = 1;
  if (!value) {
    G_INT(cn->ofs) = 0;
  } else {
    // copy the immediate to the global area
    cn->ofs = QCC_GetFreeOffsetSpace(type_size[type_integer->type]);
    G_INT(cn->ofs) = value;
  }
  return cn;
}


static QCC_def_t *QCC_MakeVectorConst (float a, float b, float c) {
  QCC_def_t *cn;
  // check for a constant with the same value
  for (cn = pr.def_head.next; cn != NULL; cn = cn->next) {
    ++varchecks;
    if (!cn->initialized) continue;
    if (!cn->constant) continue;
    ++constchecks;
    if (cn->type != type_vector) continue;
    ++typechecks;
    if (G_FLOAT(cn->ofs+0) == a && G_FLOAT(cn->ofs+1) == b && G_FLOAT(cn->ofs+2) == c) return cn;
  }
  // allocate a new one
  cn = (void *)qccHunkAlloc(sizeof(QCC_def_t));
  cn->next = NULL;
  pr.def_tail->next = cn;
  pr.def_tail = cn;
  cn->type = type_vector;
  cn->name = "IMMEDIATE";
  cn->constant = qcc_true;
  cn->initialized = 1;
  cn->scope = NULL; // always share immediates
  cn->arraysize = 1;
  // copy the immediate to the global area
  cn->ofs = QCC_GetFreeOffsetSpace(type_size[type_vector->type]);
  G_FLOAT(cn->ofs+0) = a;
  G_FLOAT(cn->ofs+1) = b;
  G_FLOAT(cn->ofs+2) = c;
  return cn;
}


static QCC_def_t *QCC_MakeFloatConst (float value) {
  union QGG_GCC_PACKED {
    float f;
    int i;
  } fi;
  fi.f = value;
  QCC_def_t *cn = Hash_GetKey(&floatconstdefstable, fi.i);
  if (cn) return cn;
  // allocate a new one
  cn = (void *)qccHunkAlloc(sizeof(QCC_def_t));
  cn->next = NULL;
  pr.def_tail->next = cn;
  pr.def_tail = cn;
  cn->s_file = s_file;
  cn->s_line = pr_source_line;
  cn->type = type_float;
  cn->name = "IMMEDIATE";
  cn->constant = qcc_true;
  cn->initialized = 1;
  cn->scope = NULL; // always share immediates
  cn->arraysize = 1;
  // copy the immediate to the global area
  cn->ofs = QCC_GetFreeOffsetSpace(type_size[type_integer->type]);
  Hash_AddKey(&floatconstdefstable, fi.i, cn, qccHunkAlloc(sizeof(hashtable_bucket_t)));
  G_FLOAT(cn->ofs) = value;
  return cn;
}


static QCC_def_t *QCC_MakeStringConstInternal (const char *value, qccbool translate) {
  int string;
  QCC_def_t *cn = Hash_Get(translate?&stringconstdefstable_trans:&stringconstdefstable, value);
  if (cn) return cn;
  // allocate a new one
  if (translate) {
    char buf[128];
    snprintf(buf, sizeof(buf), "dotranslate_%d", ++dotranslate_count);
    cn = (void *)qccHunkAlloc(sizeof(QCC_def_t)+strlen(buf)+1);
    cn->name = (char *)(cn+1);
    strcpy(cn->name, buf);
  } else {
    cn = (void *)qccHunkAlloc(sizeof(QCC_def_t));
    cn->name = "IMMEDIATE";
  }
  cn->next = NULL;
  pr.def_tail->next = cn;
  pr.def_tail = cn;
  cn->type = type_string;
  cn->constant = qcc_true;
  cn->initialized = 1;
  cn->scope = NULL; // always share immediates
  cn->arraysize = 1;
  // copy the immediate to the global area
  cn->ofs = QCC_GetFreeOffsetSpace(type_size[type_integer->type]);
  string = QCC_CopyString(value);
  Hash_Add((translate ? &stringconstdefstable_trans : &stringconstdefstable), strings+string, cn, qccHunkAlloc(sizeof(hashtable_bucket_t)));
  G_INT(cn->ofs) = string;
  return cn;
}


static QCC_def_t *QCC_MakeStringConst (const char *value) {
  return QCC_MakeStringConstInternal(value, qcc_false);
}


static QCC_def_t *QCC_MakeTranslateStringConst (const char *value) {
  return QCC_MakeStringConstInternal(value, qcc_true);
}


//===========================================================================
//assistant functions. This can safly be bipassed with the old method for more complex things.
static gofs_t QCC_GetFreeOffsetSpace (unsigned int size) {
  int ofs;
  if (opt_locals_marshalling) {
    freeoffset_t *fofs, *prev;
    for (fofs = qcc_freeofs, prev = NULL; fofs != NULL; fofs = fofs->next) {
      if (fofs->size == size) {
        if (prev) prev->next = fofs->next; else qcc_freeofs = fofs->next;
        return fofs->ofs;
      }
      prev = fofs;
    }
    for (fofs = qcc_freeofs, prev = NULL; fofs != NULL; fofs = fofs->next) {
      if (fofs->size > size) {
        fofs->size -= size;
        fofs->ofs += size;
        return fofs->ofs-size;
      }
      prev = fofs;
    }
  }
  ofs = numpr_globals;
  numpr_globals += size;
  if (numpr_globals >= MAX_REGS) {
    if (!opt_overlaptemps || !opt_locals_marshalling) {
      QCC_Error(ERR_TOOMANYGLOBALS, "numpr_globals exceeded MAX_REGS - you'll need to use more optimisations");
    } else {
      QCC_Error(ERR_TOOMANYGLOBALS, "numpr_globals exceeded MAX_REGS");
    }
  }
  return ofs;
}


static void QCC_FreeOffset (gofs_t ofs, unsigned int size) {
  freeoffset_t *fofs;
  /*
  if (ofs+size == numpr_globals) {
    //FIXME: is this a bug?
    //k8: this SHOULD be a bug, i think: we need to allocate the
    //    whole chunk of 'globals', or some writes will miss
    //numpr_globals -= size;
    return;
  }
  */
  for (fofs = qcc_freeofs; fofs != NULL; fofs = fofs->next) {
    //FIXME: if this means the last block becomes free, free them all.
    if (fofs->ofs == ofs+size) {
      fofs->ofs -= size;
      fofs->size += size;
      return;
    }
    if (fofs->ofs+fofs->size == ofs) {
      fofs->size += size;
      return;
    }
  }
  fofs = qccHunkAlloc(sizeof(freeoffset_t));
  fofs->next = qcc_freeofs;
  fofs->ofs = ofs;
  fofs->size = size;
  qcc_freeofs = fofs;
  return;
}


static QCC_def_t *QCC_GetTemp (QCC_type_t *type) {
  QCC_def_t *var_c = (void *)qccHunkAlloc(sizeof(QCC_def_t));
  memset(var_c, 0, sizeof(QCC_def_t));
  var_c->type = type;
  var_c->name = "temp";
  if (opt_overlaptemps || opt_locals_marshalling) {
    temp_t *t = NULL; // always allocate new one for opt_locals_marshalling
    if (opt_overlaptemps) {
      // don't exceed; this lets us allocate a huge block, and still be able to compile smegging big funcs
      for (t = functemps; t; t = t->next) if (!t->used && t->size == type->size) break;
      if (t && t->scope && t->scope != pr_scope) QCC_Error(ERR_INTERNAL, "Internal error temp has scope not equal to current scope");
      if (t) optres_overlaptemps += t->size;
    }
    if (t == NULL) {
      // allocate new one
      t = qccHunkAlloc(sizeof(temp_t));
      t->size = type->size;
      t->next = functemps;
      functemps = t;
      t->ofs = QCC_GetFreeOffsetSpace(t->size);
      numtemps += t->size;
    }
    var_c->ofs = t->ofs;
    var_c->temp = t;
    t->lastfunc = pr_scope;
  } else {
    // we're not going to reallocate any temps so allocate permanently
    var_c->ofs = QCC_GetFreeOffsetSpace(type->size);
    numtemps += type->size;
  }
  var_c->s_file = s_file;
  var_c->s_line = pr_source_line;
  if (var_c->temp) var_c->temp->used = qcc_true;
  return var_c;
}


// nothing else references this temp.
static void QCC_FreeTemp (QCC_def_t *t) {
  if (t && t->temp) t->temp->used = qcc_false;
}


static void QCC_UnFreeTemp (QCC_def_t *t) {
  if (t->temp) t->temp->used = qcc_true;
}


// we've just parsed a statement
// we can gaurentee that any used temps are now not used
// this is just a debugging check, can be turned off
static void QCC_FreeTemps (void) {
  if (def_ret.temp && def_ret.temp->used) {
    QCC_PR_ParseWarning(WARN_DEBUGGING, "Return value still in use in %s", pr_scope->name);
    def_ret.temp->used = qcc_false;
  }
  for (temp_t *t = functemps; t != NULL; t = t->next) {
    if (t->used && !pr_error_count) {
      // don't print this after an error jump out
      QCC_PR_ParseWarning(WARN_DEBUGGING, "Temp was used in %s", pr_scope->name);
      t->used = qcc_false;
    }
  }
}


void QCC_PurgeTemps (void) {
  functemps = NULL;
}


//temps that are still in use over a function call can be considered dodgy.
//we need to remap these to locally defined temps, on return from the function so we know we got them all.
static void QCC_LockActiveTemps (void) {
  for (temp_t *t = functemps; t != NULL; t = t->next) if (t->used) t->scope = pr_scope;
}


static void QCC_LockTemp (QCC_def_t *d) {
  if (d->temp && d->temp->used) d->temp->scope = pr_scope;
}


static void QCC_RemapLockedTemp (temp_t *t, int firststatement, int laststatement) {
#ifdef QCC_WRITEASM
  char buffer[128];
#endif
  QCC_def_t *def;
  int newofs;
  QCC_dstatement_t *st;
  int i;
  newofs = 0;
  for (i = firststatement, st = &statements[i]; i < laststatement; ++i, ++st) {
    if (pr_opcodes[st->op].type_a && st->a == t->ofs) {
      if (!newofs) {
        newofs = QCC_GetFreeOffsetSpace(t->size);
        numtemps += t->size;
        def = QCC_PR_DummyDef(type_float, NULL, pr_scope, t->size, newofs, qcc_false, qcc_false);
        def->nextlocal = pr.localvars;
        def->constant = qcc_false;
#ifdef QCC_WRITEASM
        snprintf(buffer, sizeof(buffer), "locked_%d", t->ofs);
        def->name = qccHunkDupStr(buffer);
#endif
        pr.localvars = def;
      }
      st->a = newofs;
    }
    if (pr_opcodes[st->op].type_b && st->b == t->ofs) {
      if (!newofs) {
        newofs = QCC_GetFreeOffsetSpace(t->size);
        numtemps += t->size;
        def = QCC_PR_DummyDef(type_float, NULL, pr_scope, t->size, newofs, qcc_false, qcc_false);
        def->nextlocal = pr.localvars;
        def->constant = qcc_false;
#ifdef QCC_WRITEASM
        snprintf(buffer, sizeof(buffer), "locked_%d", t->ofs);
        def->name = qccHunkDupStr(buffer);
#endif
        pr.localvars = def;
      }
      st->b = newofs;
    }
    if (pr_opcodes[st->op].type_c && st->c == t->ofs) {
      if (!newofs) {
        newofs = QCC_GetFreeOffsetSpace(t->size);
        numtemps += t->size;
        def = QCC_PR_DummyDef(type_float, NULL, pr_scope, t->size, newofs, qcc_false, qcc_false);
        def->nextlocal = pr.localvars;
        def->constant = qcc_false;
#ifdef QCC_WRITEASM
        snprintf(buffer, sizeof(buffer), "locked_%d", t->ofs);
        def->name = qccHunkDupStr(buffer);
#endif
        pr.localvars = def;
      }
      st->c = newofs;
    }
  }
}


static void QCC_RemapLockedTemps (int firststatement, int laststatement) {
  for (temp_t *t = functemps; t != NULL; t = t->next) {
    if (t->scope || opt_locals_marshalling) {
      QCC_RemapLockedTemp(t, firststatement, laststatement);
      t->scope = NULL;
      t->lastfunc = NULL;
    }
  }
}


static void QCC_fprintfLocals (FILE *f, gofs_t paramstart, gofs_t paramend) {
  int i = 0;
  for (const QCC_def_t *var = pr.localvars; var != NULL; var = var->nextlocal) {
    if (var->ofs >= paramstart && var->ofs < paramend) continue;
    if (var->arraysize != 1) {
      fprintf(f, "local %s %s[%d]; /* at %d */\n", QCC_TypeName(var->type), var->name, var->arraysize, var->ofs);
    } else {
      fprintf(f, "local %s %s; /* at %d */\n", QCC_TypeName(var->type), var->name, var->ofs);
    }
  }
  for (const temp_t *t = functemps; t != NULL; t = t->next, ++i) {
    if (t->lastfunc == pr_scope) {
      fprintf(f, "local %s temp_%d; /* at %d */\n", (t->size == 1 ? "float" : "vector"), i, t->ofs);
    }
  }
}


#ifdef QCC_WRITEASM
static const char *QCC_VarAtOffset (unsigned int ofs, unsigned int size) {
  static char message[1024];
  QCC_def_t *var;
  //check the temps
  temp_t *t;
  int i;
  for (t = functemps, i = 0; t != NULL; t = t->next, ++i) {
    if (ofs >= t->ofs && ofs < t->ofs+t->size) {
      if (size < t->size) {
        snprintf(message, sizeof(message), "temp_%i_%c", t->ofs, 'x'+(ofs-t->ofs)%3);
      } else {
        snprintf(message, sizeof(message), "temp_%d", t->ofs);
      }
      return message;
    }
  }
  for (var = pr.localvars; var != NULL; var = var->nextlocal) {
    if (var->scope && var->scope != pr_scope) continue; // this should be an error
    if (ofs >= var->ofs && ofs < var->ofs+var->type->size) {
      if (*var->name) {
        if (!strcmp(var->name, "IMMEDIATE")) continue; // continue, don't get bogged down by multiple bits of code
        if (size < var->type->size) {
          snprintf(message, sizeof(message), "%s_%c", var->name, 'x'+(ofs-var->ofs)%3);
        } else {
          snprintf(message, sizeof(message), "%s", var->name);
        }
        return message;
      }
    }
  }
  for (var = pr.def_head.next; var != NULL; var = var->next) {
    if (var->scope && var->scope != pr_scope) continue;
    if (ofs >= var->ofs && ofs < var->ofs+var->type->size) {
      if (*var->name) {
        if (!strcmp(var->name, "IMMEDIATE")) {
          switch (var->type->type) {
            case ev_string:
              snprintf(message, sizeof(message), "\"%.1020s\"", &strings[((const int *)qcc_pr_globals)[var->ofs]]);
              return message;
            case ev_integer:
              snprintf(message, sizeof(message), "%d", ((const int *)qcc_pr_globals)[var->ofs]);
              return message;
            case ev_float:
              snprintf(message, sizeof(message), "%f", qcc_pr_globals[var->ofs]);
              return message;
            case ev_vector:
              snprintf(message, sizeof(message), "'%f %f %f'", qcc_pr_globals[var->ofs], qcc_pr_globals[var->ofs+1], qcc_pr_globals[var->ofs+2]);
              return message;
            default:
              snprintf(message, sizeof(message), "IMMEDIATE");
              return message;
          }
        }
        if (size < var->type->size) {
          snprintf(message, sizeof(message), "%s_%c", var->name, 'x'+(ofs-var->ofs)%3);
        } else {
          snprintf(message, sizeof(message), "%s", var->name);
        }
        return message;
      }
    }
  }
  if (size >= 3) {
    if (ofs >= OFS_RETURN && ofs < OFS_PARM0) snprintf(message, sizeof(message), "return");
    else if (ofs >= OFS_PARM0 && ofs < OFS_PARM7) snprintf(message, sizeof(message), "parm%d", (ofs-OFS_PARM0)/3);
    else if (ofs == OFS_PARMTHIS) snprintf(message, sizeof(message), "parmthis");
    else snprintf(message, sizeof(message), "offset_%d", ofs);
  } else {
    if (ofs >= OFS_RETURN && ofs < OFS_PARM0) snprintf(message, sizeof(message), "return_%c", 'x'+ofs-OFS_RETURN);
    else if (ofs >= OFS_PARM0 && ofs < OFS_PARM7) snprintf(message, sizeof(message), "parm%i_%c", (ofs-OFS_PARM0)/3, 'x'+(ofs-OFS_PARM0)%3);
    else if (ofs == OFS_PARMTHIS) snprintf(message, sizeof(message), "parmthis_shit");
    else snprintf(message, sizeof(message), "offset_%d", ofs);
  }
  return message;
}
#endif


#include "qcc_pr_comp_primstat.c"


/*
============
PR_ParseImmediate

Looks for a preexisting constant
============
*/
static QCC_def_t *QCC_PR_ParseImmediate (void) {
  QCC_def_t *cn;
  if (pr_immediate_type == type_float) {
    cn = QCC_MakeFloatConst(pr_immediate._float);
    QCC_PR_Lex();
    return cn;
  }
  if (pr_immediate_type == type_integer) {
    cn = QCC_MakeIntConst(pr_immediate._int);
    QCC_PR_Lex();
    return cn;
  }
  if (pr_immediate_type == type_string) {
    char tmp[8192];
    if (strlen(pr_immediate_string) > sizeof(tmp)-1) QCC_PR_ParseError(ERR_INVALIDSTRINGIMMEDIATE, "string too long");
    snprintf(tmp, sizeof(tmp), "%s", pr_immediate_string);
    for (;;) {
      QCC_PR_Lex();
      if (pr_token_type == TT_IMMED && pr_token_type == TT_IMMED) {
        if (strlen(tmp)+strlen(pr_immediate_string) > sizeof(tmp)-1) {
          QCC_PR_ParseError(ERR_INVALIDSTRINGIMMEDIATE, "string too long");
        } else {
          strcat(tmp, pr_immediate_string);
        }
      } else {
        break;
      }
    }
    cn = QCC_MakeStringConst(tmp);
    return cn;
  }
  // check for a constant with the same value
  // FIXME: hashtable
  for (cn = pr.def_head.next; cn != NULL; cn = cn->next) {
    if (!cn->initialized) continue;
    if (!cn->constant) continue;
    if (cn->type != pr_immediate_type) continue;
    if (pr_immediate_type == type_string) {
      if (strcmp(G_STRING(cn->ofs), pr_immediate_string) == 0) {
        QCC_PR_Lex();
        return cn;
      }
    } else if (pr_immediate_type == type_float) {
      if (G_FLOAT(cn->ofs) == pr_immediate._float) {
        QCC_PR_Lex();
        return cn;
      }
    } else if (pr_immediate_type == type_integer) {
      if (G_INT(cn->ofs) == pr_immediate._int) {
        QCC_PR_Lex();
        return cn;
      }
    } else if (pr_immediate_type == type_vector) {
      if ((G_FLOAT(cn->ofs) == pr_immediate.vector[0]) &&
          (G_FLOAT(cn->ofs+1) == pr_immediate.vector[1]) &&
          (G_FLOAT(cn->ofs+2) == pr_immediate.vector[2])) {
        QCC_PR_Lex();
        return cn;
      }
    } else {
      QCC_PR_ParseError(ERR_BADIMMEDIATETYPE, "weird immediate type");
    }
  }
  // allocate a new one
  cn = (void *)qccHunkAlloc(sizeof(QCC_def_t));
  cn->next = NULL;
  pr.def_tail->next = cn;
  pr.def_tail = cn;
  cn->type = pr_immediate_type;
  cn->name = "IMMEDIATE";
  cn->constant = qcc_true;
  cn->initialized = 1;
  cn->scope = NULL; // always share immediates
  // copy the immediate to the global area
  cn->ofs = QCC_GetFreeOffsetSpace(type_size[pr_immediate_type->type]);
  if (pr_immediate_type == type_string) pr_immediate.string = QCC_CopyString(pr_immediate_string);
  memcpy (qcc_pr_globals+cn->ofs, &pr_immediate, 4*type_size[pr_immediate_type->type]);
  QCC_PR_Lex();
  return cn;
}


// warning, the func could have no name set if it's a field call
static QCC_def_t *QCC_PR_GenerateFunctionCall (QCC_def_t *func, QCC_def_t *arglist[], int argcount, QCC_def_t *newself) {
  QCC_def_t *d, *oldret, *oself;
  QCC_type_t *t;
  QCC_dstatement_t *st;
  /* */
  ++func->timescalled;
  t = func->type;
  if (t->type != ev_function) QCC_PR_ParseErrorPrintDef(ERR_NOTAFUNCTION, func, "not a function");
  // copy the arguments to the global parameter variables
  if (newself != NULL) {
    // we're entering OO code with a different self
    // eg: other::touch(self)
    // note: we will allocate 'self' if there is none
    d = QCC_PR_GetDef(type_entity, "self", NULL, qcc_true, 1, qcc_false);
    oself = QCC_GetTemp(type_entity);
    // oself = self
    QCC_PR_SimpleStatement(OP_STORE_ENT, d->ofs, oself->ofs, 0, qcc_false);
    /*
    // self = other
    QCC_PR_SimpleStatement(OP_STORE_ENT, statements[laststatement-1].a, d->ofs, 0, qcc_false);
    */
    QCC_PR_Statement(&pr_opcodes[OP_STORE_ENT], newself, d, QCC_STAT_NOCONV);
    // if the args refered to self, update them to refer to oself instead (as self is now set to 'other')
    for (int i = 0; i < argcount; ++i) if (arglist[i]->ofs == d->ofs) arglist[i] = oself;
  } else {
    // regular func call
    oself = NULL;
    d = NULL;
  }
  // write the arguments
  for (int i = 0; i < argcount; ++i) {
    d = (i >= MAX_PARMS ? extra_parms[i-MAX_PARMS] : &def_parms[i]);
    if (arglist[i]->type->size > 1 || !opt_nonvec_parms) {
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_V], arglist[i], d, QCC_STAT_NOCONV));
    } else {
      d->type = arglist[i]->type;
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_F], arglist[i], d, QCC_STAT_NOCONV));
      ++optres_nonvec_parms;
    }
  }
  // if the return value was in use, save it off now, so that it doesn't get clobbered
  oldret = (def_ret.temp->used ? QCC_GetTemp(def_ret.type) : NULL);
  // can free temps used for arguments now
  if (oldret && !def_ret.temp->used) {
    QCC_FreeTemp(oldret);
    oldret = NULL;
  } else if (def_ret.temp->used) {
    if (def_ret.type->size == 3) {
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_V], &def_ret, oldret, QCC_STAT_NOCONV));
    } else {
      QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_F], &def_ret, oldret, QCC_STAT_NOCONV));
    }
    QCC_UnFreeTemp(oldret);
    QCC_UnFreeTemp(&def_ret);
    //k8:wtf?! QCC_PR_ParseWarning(WARN_FIXEDRETURNVALUECONFLICT, "Return value conflict - output is inefficient");
  } else {
    oldret = NULL;
  }
  // we dont need to lock the local containing the function index because its thrown away after the call anyway
  // (if a function is called in the argument list then it'll be locked as part of that call)
  QCC_FreeTemp(func);
  QCC_LockActiveTemps(); // any temps before are likely to be used with the return value
  QCC_UnFreeTemp(func);
  // generate the call
  if (argcount > MAX_PARMS) {
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_CALL0+MAX_PARMS], func, 0, &st));
  } else if (argcount) {
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_CALL0+argcount], func, 0, &st));
  } else {
    QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_CALL0], func, 0, &st));
  }
  // restore the class owner
  if (oself) {
    QCC_PR_SimpleStatement(OP_STORE_ENT, oself->ofs, d->ofs, 0, qcc_false);
    QCC_FreeTemp(oself);
  }
  //
  if (oldret) {
    if (oldret->temp && !oldret->temp->used) QCC_PR_ParseWarning(0, "Ret was freed\n");
    // if we preserved the ofs_ret global, restore it here
    d = QCC_GetTemp(t->aux_type);
    if (t->aux_type->size == 3) {
      QCC_FreeTemp(QCC_PR_Statement(pr_opcodes+OP_STORE_V, &def_ret, d, QCC_STAT_NOCONV));
    } else {
      QCC_FreeTemp(QCC_PR_Statement(pr_opcodes+OP_STORE_F, &def_ret, d, QCC_STAT_NOCONV));
    }
    if (def_ret.type->size == 3) {
      QCC_FreeTemp(QCC_PR_Statement(pr_opcodes+OP_STORE_V, oldret, &def_ret, QCC_STAT_NOCONV));
    } else {
      QCC_FreeTemp(QCC_PR_Statement(pr_opcodes+OP_STORE_F, oldret, &def_ret, QCC_STAT_NOCONV));
    }
    QCC_FreeTemp(oldret);
    QCC_UnFreeTemp(&def_ret);
    QCC_UnFreeTemp(d);
    return d;
  }
  def_ret.type = t->aux_type;
  if (def_ret.temp->used) QCC_PR_ParseWarning(WARN_FIXEDRETURNVALUECONFLICT, "Return value conflict -- output is inefficient");
  def_ret.temp->used = qcc_true;
  return &def_ret;
}


#include "qcc_pr_comp_intrinsics.c"


/*
============
PR_ParseFunctionCall

warning, the func could have no name set if it's a field call
opening paren should be eaten
============
*/
static QCC_def_t *QCC_PR_ParseFunctionCall (QCC_def_t *func, QCC_def_t *newself) {
  QCC_def_t *e, *d;
  QCC_type_t *t, *p;
  int extraparms = qcc_false;
  int np, arg;
  QCC_def_t *param[MAX_PARMS+MAX_EXTRA_PARMS];
  /* */
  ++func->timescalled;
  t = func->type;
  if (t->type != ev_function) QCC_PR_ParseErrorPrintDef(ERR_NOTAFUNCTION, func, "not a function");
  // intrinsics
  // these base functions have variable arguments. I would check for (...) args too,
  // but that might be used for extended builtin functionality.
  // (this code wouldn't compile otherwise)
  if (!t->num_parms) {
    for (size_t f = 0; f < sizeof(intrlist)/sizeof(intrlist[0]); ++f) {
      if (strcmp(func->name, intrlist[f].name) == 0) return intrlist[f].fn(func);
    }
  }
  // so it's not an intrinsic
  // copy the arguments to the global parameter variables
  arg = 0;
  if (t->num_parms < 0) {
    extraparms = qcc_true;
    np = -t->num_parms-1;
  } else {
    np = t->num_parms;
  }
  // any temps referenced to build the parameters don't need to be locked
  if (!QCC_PR_EatDelim(")")) {
    int dont_advance = 0;
    p = t->param;
    do {
      if (extraparms && arg >= MAX_PARMS) {
        QCC_PR_ParseErrorPrintDef(ERR_TOOMANYPARAMETERSVARARGS, func, "More than %d parameters on varargs function", MAX_PARMS);
      } else if (arg >= MAX_PARMS+MAX_EXTRA_PARMS) {
        QCC_PR_ParseErrorPrintDef(ERR_TOOMANYTOTALPARAMETERS, func, "More than %d parameters", MAX_PARMS+MAX_EXTRA_PARMS);
      }
      if (!extraparms && arg >= t->num_parms && !p) {
        QCC_PR_ParseWarning(WARN_TOOMANYPARAMETERSFORFUNC, "too many parameters");
        QCC_PR_ParsePrintDef(WARN_TOOMANYPARAMETERSFORFUNC, func);
      }
      // with vectorcalls, we store the vector into the args as individual floats
      // this allows better reuse of vector constants
      // copy it into the offset now, because we can
      if (opt_vectorcalls && pr_token_type == TT_IMMED && pr_immediate_type == type_vector &&
          arg < MAX_PARMS && !def_parms[arg].temp->used) {
        e = &def_parms[arg];
        e->ofs = OFS_PARM0+0;
        QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(pr_immediate.vector[0]), e, QCC_STAT_NOCONV));
        e->ofs = OFS_PARM0+1;
        QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(pr_immediate.vector[1]), e, QCC_STAT_NOCONV));
        e->ofs = OFS_PARM0+2;
        QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_F], QCC_MakeFloatConst(pr_immediate.vector[2]), e, QCC_STAT_NOCONV));
        e->ofs = OFS_PARM0;
        QCC_PR_Lex();
      } else {
        e = QCC_PR_Expression(TOP_PRIORITY, EXPR_DISALLOW_COMMA);
      }
      if (arg >= MAX_PARMS) {
        if (!extra_parms[arg-MAX_PARMS]) {
          d = (QCC_def_t *)qccHunkAlloc(sizeof(QCC_def_t));
          d->name = "extra parm";
          d->ofs = QCC_GetFreeOffsetSpace(3);
          extra_parms[arg-MAX_PARMS] = d;
        }
        d = extra_parms[arg-MAX_PARMS];
      } else {
        d = &def_parms[arg];
      }
      if (p) {
        /*then:
          if (e->type->type != ev_integer && p->type != ev_function)
          if (e->type->type != ev_function && p->type != ev_integer)
          if ( e->type->type != p->type )
        */
        if (qcc_typecmp(e->type, p)) {
          if (p->type == ev_string) {
            // convert number to string
            if (e->type->type == ev_integer) e = QCC_PR_Statement(&pr_opcodes[OP_CONV_ITOS], e, NULL, NULL);
            else if (e->type->type == ev_float) e = QCC_PR_Statement(&pr_opcodes[OP_CONV_FTOS], e, NULL, NULL);
            else if (e->type->type == ev_vector) e = QCC_PR_Statement(&pr_opcodes[OP_CONV_VTOS], e, NULL, NULL);
            else if (e->type->type == ev_entity) e = QCC_PR_Statement(&pr_opcodes[OP_CONV_ETOS], e, NULL, NULL);
          } else if (p->type == ev_integer && e->type->type == ev_float) {
            // convert float -> int... is this a constant?
            //fprintf(stderr, "(%d)FCALL: f->i\n", arg);
            e = QCC_PR_Statement(&pr_opcodes[OP_CONV_FTOI], e, NULL, NULL);
            //fprintf(stderr, "(%d)FCALL: f->i: done\n", arg);
          } else if (p->type == ev_float && e->type->type == ev_integer) {
            // convert int -> float... is this a constant?
            //fprintf(stderr, "(%d)FCALL: i->f\n", arg);
            e = QCC_PR_Statement(&pr_opcodes[OP_CONV_ITOF], e, NULL, NULL);
            //fprintf(stderr, "(%d)FCALL: i->f: done\n", arg);
          } else if (p->type == ev_function && e->type->type == ev_integer && e->constant && !((qccint *)qcc_pr_globals)[e->ofs]) {
            // you're allowed to use int 0 to pass a null function pointer
            // this is basically because __NULL__ is defined as ~0 (int 0)
          }
        }
        d->type = p;
        //fprintf(stderr, "fcall: num_parms=%d; arg=%d; np=%d; p=%p; p->next=%p; da=%d\n", t->num_parms, arg, np, p, p->next, dont_advance);
        if (t->num_parms < 0) {
          // handle special case: typed vaargs
          if (!dont_advance) {
            if (arg == np && p->next == NULL) dont_advance = 1;
          }
          if (!dont_advance) p = p->next;
        } else {
          p = p->next;
        }
      } else {
        // a vector copy will copy everything
        d->type = type_void;
      }
      param[arg] = e;
      /*
      if (e->type->size > 1) {
        QCC_PR_Statement(&pr_opcodes[OP_STORE_V], e, d, (QCC_dstatement_t **)0xffffffff);
      } else {
        QCC_PR_Statement(&pr_opcodes[OP_STORE_F], e, d, (QCC_dstatement_t **)0xffffffff);
      }
      */
      ++arg;
    } while (QCC_PR_EatDelim(","));
    if (t->num_parms != -1 && arg < np) QCC_PR_ParseWarning(WARN_TOOFEWPARAMS, "too few parameters on call to %s", func->name);
    QCC_PR_Expect(")");
  } else if (np) {
    QCC_PR_ParseWarning(WARN_TOOFEWPARAMS, "%s: Too few parameters", func->name);
    QCC_PR_ParsePrintDef(WARN_TOOFEWPARAMS, func);
  }
  return QCC_PR_GenerateFunctionCall(func, param, arg, newself);
}


QCC_type_t *QCC_PointerTypeTo (QCC_type_t *type) {
  QCC_type_t *newtype = QCC_PR_NewType("ptr", ev_pointer);
  newtype->aux_type = type;
  return newtype;
}


/*
============
PR_ParseValue

Returns the global ofs for the current token
============
*/
static QCC_def_t *QCC_PR_ParseValue (qccbool allowarrayassign) {
  QCC_def_t *d, *tmp, *idx;
  QCC_type_t *t;
  char *name;
  // if the token is an immediate, allocate a constant for it
  if (pr_token_type == TT_IMMED) return QCC_PR_ParseImmediate();
  name = QCC_PR_ParseName();
  // look through the defs
  d = QCC_PR_GetDef(NULL, name, pr_scope, qcc_false, 0, qcc_false);
  if (!d) {
    // intrinsics? any old function with no args will do
    int intrfound = 0;
    for (size_t f = 0; f < sizeof(intrlist)/sizeof(intrlist[0]); ++f) {
      if (strcmp(name, intrlist[f].name) == 0) { intrfound = 1; break; }
    }
    if (!intrfound) QCC_PR_ParseError(ERR_UNKNOWNVALUE, "Unknown value \"%s\"", name);
    d = QCC_PR_GetDef(type_function, name, NULL, qcc_true, 1, qcc_false);
    d->initialized = 0;
  }
  //
  t = d->type;
  idx = NULL;
  //if ((t->type == ev_vector || (t->type == ev_field && t->aux_type->type == ev_vector)) && d->arraysize == 1) goto skipit;
  //
  if (d->arraysize == 1 && t->type != ev_pointer) goto skipit;
  //
  for (;;) {
    if (QCC_PR_EatDelim("[")) {
      tmp = QCC_PR_Expression(TOP_PRIORITY, 0);
      QCC_PR_Expect("]");
      //fprintf(stderr, "%d idx=%p, tmp=%p\n", t->type, idx, tmp);
      if (!idx && t->type == ev_pointer && d->arraysize == 1) t = t->aux_type;
      /*
      if (t->type == ev_string && d->arraysize == 1) {
        //QCC_PR_ParseErrorPrintDef(0, d, "TODO: no string indexing yet");
        //fprintf(stderr, "indexing string\n");
        d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_C], d, tmp, NULL);
        / *
        QCC_def_t *res;
        tmp = QCC_SupplyConversion(tmp, ev_float, qcc_true);
        res = QCC_GetTemp(type_float);
        QCC_PR_SimpleStatement(OP_LOADP_C, d->ofs, tmp->ofs, res->ofs, qcc_false);
        QCC_FreeTemp(tmp);
        d = res;
        * /
        continue;
      }
      */
      //
      if (!idx && d->type->type == ev_pointer) {
        // no bounds checks
      } else if (tmp->constant) {
        int i;
        if (tmp->type->type == ev_integer) i = G_INT(tmp->ofs);
        else if (tmp->type->type == ev_float) i = G_FLOAT(tmp->ofs);
        else abort(); //k8:!!!
        if (i < 0 || i >= (!idx ? d->arraysize : t->arraysize)) QCC_PR_ParseErrorPrintDef(0, d, "(constant) array index out of bounds");
      } else {
        tmp = QCC_SupplyConversion(tmp, ev_integer, qcc_true);
        QCC_PR_SimpleStatement(OP_BOUNDCHECK, tmp->ofs, (!idx ? d->arraysize : t->arraysize), 0, qcc_false);
      }
      // don't multiply by type size if the instruction/emulation will do that instead
      // k8: was (idx || QCC_OPCodeValid(&pr_opcodes[OP_LOADA_F]))
      if (t->size != 1 && idx != 0) {
        if (tmp->type->type == ev_float) {
          tmp = QCC_PR_Statement(&pr_opcodes[OP_MUL_F], tmp, QCC_MakeFloatConst(t->size), NULL);
        } else {
          tmp = QCC_PR_Statement(&pr_opcodes[OP_MUL_I], tmp, QCC_MakeIntConst(t->size), NULL);
        }
      }
      idx = (idx ? QCC_PR_Statement(&pr_opcodes[OP_ADD_I], idx, QCC_SupplyConversion(tmp, ev_integer, qcc_true), NULL) : tmp);
      continue;
    }
    //
    if (t->type == ev_pointer && QCC_PR_EatDelim(".")) {
      if (!idx && t->type == ev_pointer && d->arraysize == 1) t = t->aux_type;
      for (t = t->param; t; t = t->next) if (QCC_PR_EatId(t->name)) break;
      if (!t) QCC_PR_ParseError(0, "'%s' is not a member", pr_token);
      tmp = QCC_MakeIntConst(t->ofs);
      idx = (idx ? QCC_PR_Statement(&pr_opcodes[OP_ADD_I], idx, tmp, NULL) : tmp);
      continue;
    }
    //
    break;
  }
  //
skipit:
  if (idx != NULL) {
    if (d->type->type == ev_pointer) {
      switch (t->type) {
        case ev_pointer:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_I], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_float:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_F], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_integer:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_I], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_string:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_S], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_vector:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_V], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_entity:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_ENT], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_field:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_FLD], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_function:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_FNC], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        default:
          QCC_PR_ParseError(ERR_NOVALIDOPCODES, "No op available");
      }
      d->type = t;
    } else {
      // don't care about assignments. the code can convert an OP_LOADA_F to an OP_ADDRESS on assign
      // source type is a struct, or its an array, or something that can otherwise be directly accessed
      switch (t->type) {
        case ev_pointer:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_I], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_float:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_F], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_integer:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_I], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_string:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_S], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_vector:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_V], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_entity:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_ENT], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_field:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_FLD], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        case ev_function:
          d = QCC_PR_Statement(&pr_opcodes[OP_LOADA_FNC], d, QCC_SupplyConversion(idx, ev_integer, qcc_true), NULL);
          break;
        default:
          QCC_PR_ParseError(ERR_NOVALIDOPCODES, "No op available");
      }
      d->type = t;
    }
  } else {
    // no indexing
    t = d->type;
    if (t->type == ev_vector || (t->type == ev_field && t->aux_type->type == ev_vector)) {
      if (QCC_PR_EatDelim(".")) {
        // member access
        char *name = alloca(strlen(d->name)+4), ech;
        // vector.[xyz]
        if (QCC_PR_EatId("x")) ech = 'x';
        else if (QCC_PR_EatId("y")) ech = 'y';
        else if (QCC_PR_EatId("z")) ech = 'z';
        else QCC_PR_ParseError(0, "'%s' is not a vector member", pr_token);
        sprintf(name, "%s.%c", d->name, ech);
        //fprintf(stderr, "accesing vector member: [%s]\n", name);
        d = QCC_PR_GetDef(type_float, name, pr_scope, qcc_false, 0, qcc_false);
        if (d == NULL) QCC_PR_ParseError(ERR_UNKNOWNVALUE, "WTF?!! (00) [%s]", name);
      } else if (QCC_PR_EatDelim("[")) {
        // array-like vector indexing
        tmp = QCC_PR_Expression(TOP_PRIORITY, 0);
        QCC_PR_Expect("]");
      }
    } else if (t->type == ev_string || (t->type == ev_field && t->aux_type->type == ev_string)) {
      if (QCC_PR_EatDelim("[")) {
        // string indexing
        tmp = QCC_PR_Expression(TOP_PRIORITY, 0);
        QCC_PR_Expect("]");
        d = QCC_PR_Statement(&pr_opcodes[OP_LOADP_C], d, tmp, NULL);
      }
    }
  }
  //
  return d;
}


#include "qcc_pr_comp_expr.c"


int QCC_PR_IntConstExpr (void) {
  QCC_def_t *def = QCC_PR_Expression(TOP_PRIORITY, 0);
  if (def->constant) {
    ++def->references;
    if (def->type->type == ev_integer) return G_INT(def->ofs);
    if (def->type->type == ev_float) {
      int i = G_FLOAT(def->ofs);
      if ((float)i == G_FLOAT(def->ofs)) return i;
    }
  }
  QCC_PR_ParseError(ERR_NOTACONSTANT, "Value is not an integer constant");
  return qcc_true;
}


static void QCC_PR_GotoStatement (QCC_dstatement_t *patch2, const char *labelname) {
  if (num_gotos >= max_gotos) {
    max_gotos += 8;
    pr_gotos = realloc(pr_gotos, sizeof(*pr_gotos)*max_gotos);
  }
  strncpy(pr_gotos[num_gotos].name, labelname, sizeof(pr_gotos[num_gotos].name)-1);
  pr_gotos[num_gotos].lineno = pr_source_line;
  pr_gotos[num_gotos].statementno = patch2 - statements;
  ++num_gotos;
}


static qccbool QCC_PR_StatementBlocksMatch (const QCC_dstatement_t *p1, int p1count, const QCC_dstatement_t *p2, int p2count) {
  if (p1count != p2count) return qcc_false;
  while (p1count-- > 0) {
    if (p1->op != p2->op) return qcc_false;
    if (p1->a != p2->a) return qcc_false;
    if (p1->b != p2->b) return qcc_false;
    if (p1->c != p2->c) return qcc_false;
    ++p1;
    ++p2;
  }
  return qcc_true;
}


#include "qcc_pr_comp_statements.c"


//FIXME: CONDITIONAL JUMPS TOO?
static qccbool QCC_FuncJumpsTo (int first, int last, int statement) {
  for (int st = first; st < last; ++st) {
    if (is_branch_op(statements[st].op)) {
      int pc = branch_dest(st);
      if (pc == statement) {
        if (st != first) {
          if (statements[st-1].op == OP_RETURN) break;
          if (statements[st-1].op == OP_DONE) break;
          return qcc_true;
        }
      }
    }
  }
  return qcc_false;
}


static void QCC_CompoundJumps (int first, int last) {
  // jumps to jumps are reordered so they become jumps to the final target
  for (int st = first; st < last; ++st) {
    if (is_branch_op(statements[st].op)) {
      int argn = (statements[st].op == OP_GOTO ? 0 : 1);
      int dest = branch_dest(st);
      /*
      int dest = st+(signed int)statements[st].args[argn];
      */
      if (statements[st].op == OP_GOTO && (statements[dest].op == OP_RETURN || statements[dest].op == OP_DONE)) {
        // goto leads to return: copy the command out to remove the goto
        statements[st] = statements[dest];
        ++optres_compound_jumps;
      } else {
        int infloop = 1000;
        // traverse 'goto' chain
        while (statements[dest].op == OP_GOTO) {
          if (infloop-- == 0) { QCC_PR_ParseWarning(0, "Infinite loop detected"); break; }
          statements[st].sargs[argn] = statements[dest].sa+dest-st;
          dest = st+statements[st].sargs[argn];
          ++optres_compound_jumps;
        }
      }
    }
  }
}


//QCC_CheckForDeadAndMissingReturns()
#include "qcc_pr_comp_retcheck.c"


// only the unconditionals
static inline qccbool QCC_StatementIsAJump (int stnum, int notifdest) {
  switch (statements[stnum].op) {
    case OP_RETURN: case OP_DONE: return qcc_true;
    case OP_GOTO: return (branch_dest(stnum) != notifdest);
  }
  return qcc_false;
}


static int QCC_AStatementJumpsTo (int targ, int first, int last) {
  for (int st = first; st < last; ++st) {
    if (is_branch_op(statements[st].op) && branch_dest(st) == targ) return qcc_true;
  }
  // assume it's used if it is labeled
  for (int st = 0; st < num_labels; ++st) if (pr_labels[st].statementno == targ) return qcc_true;
  return qcc_false;
}


void QCC_RemapOffsets (unsigned int firststatement, unsigned int laststatement, unsigned int min,
                              unsigned int max, unsigned int newmin)
{
  QCC_dstatement_t *st;
  unsigned int i;
  for (i = firststatement, st = &statements[i]; i < laststatement; ++i, ++st) {
    for (int c = 0; c < 3; ++c) {
      if (pr_opcodes[st->op].types[c] && st->args[c] >= min && st->args[c] < max) st->args[c] = st->args[c]-min+newmin;
    }
  }
}


static void QCC_Marshal_Locals (int first, int laststatement) {
  QCC_def_t *local;
  unsigned int newofs;
  // clear these after each function. we arn't overlapping them so why do we need to keep track of them?
  //if (!opt_overlaptemps) {
  //  temp_t *t;
  //  for (t = functemps; t; t = t->next)
  //    QCC_FreeOffset(t->ofs, t->size);
  //  functemps = NULL;
  //}
  // nothing to marshal
  if (!pr.localvars) {
    locals_start = locals_end = 0;/*numpr_globals;*/
    return;
  }
  if (!opt_locals_marshalling) {
    pr.localvars = pr.prevlocvars = NULL;
    return;
  }
  // initial backwards bounds
  locals_start = MAX_REGS;
  locals_end = 0;
  newofs = MAX_REGS; // this is a handy place to put it. :)
  // the params need to be in the order that they were allocated
  // so we allocate in a backwards order
  for (local = pr.localvars; local; local = local->nextlocal) {
    if (local->constant) continue;
    newofs += local->type->size*local->arraysize;
    if (local->arraysize > 1) ++newofs;
  }
  locals_start = MAX_REGS;
  locals_end = newofs;
  optres_locals_marshalling += newofs-MAX_REGS;
  for (local = pr.localvars; local; local = local->nextlocal) {
    if (local->constant) continue;
    if (((const int *)qcc_pr_globals)[local->ofs]) QCC_PR_ParseError(ERR_INTERNAL, "Marshall of a set value");
    newofs -= local->type->size*local->arraysize;
    if (local->arraysize > 1) --newofs;
    QCC_RemapOffsets(first, laststatement, local->ofs, local->ofs+local->type->size*local->arraysize, newofs);
    QCC_FreeOffset(local->ofs, local->type->size*local->arraysize);
    local->ofs = newofs;
  }
  pr.localvars = pr.prevlocvars = NULL;
}


#ifdef QCC_WRITEASM
static void QCC_WriteAsmFunction (QCC_def_t *sc, unsigned int firststatement, gofs_t firstparm) {
  unsigned int i, p;
  gofs_t o;
  QCC_type_t *type;
  QCC_def_t *param;
  if (!asmfile) return;
  type = sc->type;
  fprintf(asmfile, "%s(", QCC_TypeName(type->aux_type));
  p = type->num_parms;
  for (o = firstparm, i = 0, type = type->param; i < p; ++i, type = type->next) {
    if (i) fprintf(asmfile, ", ");
    for (param = pr.localvars; param; param = param->nextlocal) if (param->ofs == o) break;
    if (param) {
      fprintf(asmfile, "%s %s /* at %d */", QCC_TypeName(type), param->name, o);
    } else {
      fprintf(asmfile, "%s /* at %d */", QCC_TypeName(type), o);
    }
    o += type->size;
  }
  fprintf(asmfile, ") %s = asm {\n", sc->name);
  QCC_fprintfLocals(asmfile, firstparm, o);
  for (i = firststatement; i < (unsigned int)numstatements; ++i) {
    fprintf(asmfile, "  %-7s ", pr_opcodes[statements[i].op].opname);
    if (pr_opcodes[statements[i].op].type_a != &type_void) {
      if (pr_opcodes[statements[i].op].type_a) {
        fprintf(asmfile, "%s", QCC_VarAtOffset(statements[i].a, (*pr_opcodes[statements[i].op].type_a)->size));
      } else {
        fprintf(asmfile, "%d", statements[i].a);
      }
      if (pr_opcodes[statements[i].op].type_b != &type_void) {
        if (pr_opcodes[statements[i].op].type_b) {
          fprintf(asmfile, ", %s", QCC_VarAtOffset(statements[i].b, (*pr_opcodes[statements[i].op].type_b)->size));
        } else {
          fprintf(asmfile, ", %d", statements[i].b);
        }
        if (pr_opcodes[statements[i].op].type_c != &type_void && pr_opcodes[statements[i].op].associative == ASSOC_LEFT) {
          if (pr_opcodes[statements[i].op].type_c) {
            fprintf(asmfile, ", %s", QCC_VarAtOffset(statements[i].c, (*pr_opcodes[statements[i].op].type_c)->size));
          } else {
            fprintf(asmfile, ", %d", statements[i].c);
          }
        }
      } else {
        if (pr_opcodes[statements[i].op].type_c != &type_void) {
          if (pr_opcodes[statements[i].op].type_c) {
            fprintf(asmfile, ", %s", QCC_VarAtOffset(statements[i].c, (*pr_opcodes[statements[i].op].type_c)->size));
          } else {
            fprintf(asmfile, ", %d", statements[i].c);
          }
        }
      }
    }
    fprintf(asmfile, "; /*%d*/\n", statement_linenums[i]);
  }
  fprintf(asmfile, "}\n\n");
}
#endif


/*
============
PR_ParseImmediateStatements

Parse a function body
============
*/
static QCC_function_t *QCC_PR_ParseImmediateStatements (QCC_type_t *type) {
  QCC_function_t *f;
  QCC_def_t *defs[MAX_PARMS+MAX_EXTRA_PARMS], *e2;
  QCC_type_t *parm;
  qccbool needsdone = qcc_false;
  freeoffset_t *oldfofs;
  conditional = 0;
  expandedemptymacro = qcc_false;
  f = (void *)qccHunkAlloc(sizeof(QCC_function_t));
  if (type->num_parms < 0) QCC_PR_ParseError(ERR_FUNCTIONWITHVARGS, "QC function with variable arguments and function body");
  f->builtin = 0;
  // define the parms
  locals_start = locals_end = numpr_globals;
  oldfofs = qcc_freeofs;
  qcc_freeofs = NULL;
  parm = type->param;
  for (int i = 0 ; i < type->num_parms; ++i) {
    if (!*pr_parm_names[i]) QCC_PR_ParseError(ERR_PARAMWITHNONAME, "Parameter is not named");
    defs[i] = QCC_PR_GetDef(parm, pr_parm_names[i], pr_scope, qcc_true, 1, qcc_false);
    ++defs[i]->references;
    if (i < MAX_PARMS) {
      f->parm_ofs[i] = defs[i]->ofs;
      if (i > 0 && f->parm_ofs[i] < f->parm_ofs[i-1]) QCC_Error(ERR_BADPARAMORDER, "bad parm order");
      if (i > 0 && f->parm_ofs[i] != f->parm_ofs[i-1]+defs[i-1]->type->size) QCC_Error(ERR_BADPARAMORDER, "parms not packed");
    }
    parm = parm->next;
  }
  if (type->num_parms) locals_start = locals_end = defs[0]->ofs;
  qcc_freeofs = oldfofs;
  f->code = numstatements;
  if (type->num_parms > MAX_PARMS) {
    for (int i = MAX_PARMS; i < type->num_parms; ++i) {
      if (!extra_parms[i-MAX_PARMS]) {
        e2 = (QCC_def_t *)qccHunkAlloc(sizeof(QCC_def_t));
        e2->name = "extra parm";
        e2->ofs = QCC_GetFreeOffsetSpace(3);
        extra_parms[i - MAX_PARMS] = e2;
      }
      extra_parms[i-MAX_PARMS]->type = defs[i]->type;
      if (defs[i]->type->type != ev_vector) {
        QCC_PR_Statement(&pr_opcodes[OP_STORE_F], extra_parms[i-MAX_PARMS], defs[i], NULL);
      } else {
        QCC_PR_Statement(&pr_opcodes[OP_STORE_V], extra_parms[i-MAX_PARMS], defs[i], NULL);
      }
    }
  }
  QCC_RemapLockedTemps(-1, -1);
  QCC_PR_Expect("{");
  // parse regular statements
  while (!QCC_PR_EatDelim("}")) {
    QCC_PR_ParseStatement(qcc_false);
    QCC_FreeTemps();
  }
  QCC_FreeTemps();
  if (f->code == numstatements) {
    needsdone = qcc_true;
  } else if (statements[numstatements-1].op != OP_RETURN && statements[numstatements-1].op != OP_DONE) {
    needsdone = qcc_true;
  }
  if (num_gotos) {
    for (int i = 0; i < num_gotos; ++i) {
      int j;
      for (j = 0; j < num_labels; ++j) {
        if (strcmp(pr_gotos[i].name, pr_labels[j].name) == 0) {
          if (!pr_opcodes[statements[pr_gotos[i].statementno].op].type_a) {
            statements[pr_gotos[i].statementno].a += pr_labels[j].statementno-pr_gotos[i].statementno;
          } else if (!pr_opcodes[statements[pr_gotos[i].statementno].op].type_b) {
            statements[pr_gotos[i].statementno].b += pr_labels[j].statementno-pr_gotos[i].statementno;
          } else {
            statements[pr_gotos[i].statementno].c += pr_labels[j].statementno-pr_gotos[i].statementno;
          }
          break;
        }
      }
      if (j == num_labels) {
        num_gotos = 0;
        QCC_PR_ParseError(ERR_NOLABEL, "Goto statement with no matching label \"%s\"", pr_gotos[i].name);
      }
    }
    num_gotos = 0;
  }
  if (!needsdone) needsdone = QCC_FuncJumpsTo(f->code, numstatements, numstatements);
  // emit an end of statements opcode
  if (needsdone) QCC_PR_Statement(pr_opcodes, 0,0, NULL);
  QCC_CheckForDeadAndMissingReturns(f->code, numstatements, type->aux_type->type);
  if (opt_compound_jumps) QCC_CompoundJumps(f->code, numstatements);
  QCC_RemapLockedTemps(f->code, numstatements);
  locals_end = numpr_globals;
  QCC_Marshal_Locals(f->code, numstatements);
  QCC_WriteAsmFunction(pr_scope, f->code, locals_start);
  if (num_labels) num_labels = 0;
  if (num_continues) {
    num_continues = 0;
    QCC_PR_ParseError(ERR_ILLEGALCONTINUES, "%s: function contains illegal continues", pr_scope->name);
  }
  if (num_breaks) {
    num_breaks = 0;
    QCC_PR_ParseError(ERR_ILLEGALBREAKS, "%s: function contains illegal breaks", pr_scope->name);
  }
  if (num_cases) {
    num_cases = 0;
    QCC_PR_ParseError(ERR_ILLEGALCASES, "%s: function contains illegal cases", pr_scope->name);
  }
  return f;
}


// register a def, and all of it's sub parts.
// only the main def is of use to the compiler.
// the subparts are emitted to the compiler and allow correct saving/loading
// be careful with fields, this doesn't allocated space, so will it allocate fields. It only creates defs at specified offsets.
static QCC_def_t *QCC_PR_DummyDef (QCC_type_t *type, const char *name, QCC_def_t *scope, int arraysize, unsigned int ofs, int referable, qccbool saved) {
  static const char *reserved_words[] = {
    //"builtin",
    "const",
    "for",
    "switch",
    "case",
    "default",
    "goto",
    "break",
    "continue",
    "do",
    "while",
    "if",
    "else",
    "string",
    "float",
    "int",
    "entity",
    "vector",
    "bool",
  };
  char array[64], newname[256];
  QCC_def_t *def, *first = NULL;
  if (name) {
    for (size_t f = 0; f < sizeof(reserved_words)/sizeof(reserved_words[0]); ++f) {
      if (strcmp(name, reserved_words[f]) == 0) {
        QCC_PR_ParseError(ERR_KEYWORDDISABLED, "'%s' keyword used as variable name", name);
      }
    }
  }
  //fprintf(stderr, "arraysize=%d\n", arraysize);
  for (int a = 0; a < arraysize; ++a) {
    if (a == 0) array[0] = 0; else snprintf(array, sizeof(array), "[%d]", a);
    if (name) {
      snprintf(newname, sizeof(newname), "%s%s", name, array);
    } else {
      //*newname = *"";
      newname[0] = 0;
    }
    // allocate a new def
    def = (void *)qccHunkAlloc(sizeof(QCC_def_t));
    memset(def, 0, sizeof(*def));
    def->next = NULL;
    def->arraysize = arraysize;
    if (name) {
      pr.def_tail->next = def;
      pr.def_tail = def;
    }
    if (a > 0) ++def->references;
    def->s_line = pr_source_line;
    def->s_file = s_file;
    if (a) def->initialized = 1;
    def->name = qccHunkDupStr(newname);
    def->type = type;
    def->scope = scope;
    def->saved = saved;
    def->constant = qcc_true; // if (type->type = ev_field)
    if (type->type != ev_function) {
      if (ofs+type->size*a >= MAX_REGS) QCC_Error(ERR_TOOMANYGLOBALS, "MAX_REGS is too small");
      def->ofs = ofs+type->size*a;
    } else {
      def->ofs = 0;
    }
    if (!first) first = def;
    //qcc_printf("Emited %s\n", newname);
    if (arraysize == 1 && (type->type == ev_vector || (type->type == ev_field && type->aux_type->type == ev_vector))) {
      // do the vector thing
      QCC_type_t *vt = (type->type == ev_vector ? type_float : type_floatfield);
      //fprintf(stderr, "creating vector fielddefs: [%s]\n", name);
      // don't warn about unreferenced vector parts
      snprintf(newname, sizeof(newname), "%s.x", name);
      QCC_PR_DummyDef(vt, newname, scope, 1, ofs+type->size*a+0, referable, qcc_false)->references += 1;
      snprintf(newname, sizeof(newname), "%s.y", name);
      QCC_PR_DummyDef(vt, newname, scope, 1, ofs+type->size*a+1, referable, qcc_false)->references += 1;
      snprintf(newname, sizeof(newname), "%s.z", name);
      QCC_PR_DummyDef(vt, newname, scope, 1, ofs+type->size*a+2, referable, qcc_false)->references += 1;
    }
    first->deftail = pr.def_tail;
  }
  //
  if (referable) {
    // anything above needs to be left in, and so warning about not using it is just going to pee people off
    if (!Hash_Get(&globalstable, "end_sys_fields")) ++first->references;
    if (arraysize <= 1 && first->type->type != ev_field) first->constant = qcc_false;
    if (scope) {
      Hash_Add(&localstable, first->name, first, qccHunkAlloc(sizeof(hashtable_bucket_t)));
    } else {
      Hash_Add(&globalstable, first->name, first, qccHunkAlloc(sizeof(hashtable_bucket_t)));
    }
    if (!scope && asmfile) fprintf(asmfile, "%s %s; /* at %d */\n", QCC_TypeName(first->type), first->name, first->ofs);
  }
  return first;
}


/*
============
PR_GetDef

If type is NULL, it will match any type
If allocate is qcc_true, a new def will be allocated if it can't be found
============
*/
QCC_def_t *QCC_PR_GetDef (QCC_type_t *type, const char *name, QCC_def_t *scope, qccbool allocate, int arraysize, qccbool saved) {
  int ofs;
  QCC_def_t *def;
  //char element[MAX_NAME];
  //unsigned int i;
  QCC_def_t *foundstatic = NULL;
  //fprintf(stderr, "QCC_PR_GetDef: [%s] scope=%p; allocate=%d\n", name, scope, allocate);
  if (scope) {
    def = Hash_Get(&localstable, name);
    while (def != NULL) {
      if (def->scope && def->scope != scope) {
        def = Hash_GetNext(&localstable, name, def);
        continue; // in a different function
      }
      if (type && qcc_typecmp(def->type, type)) {
        QCC_PR_ParseErrorPrintDef(ERR_TYPEMISMATCHREDEC, def, "Type mismatch on redeclaration of %s. %s, should be %s", name, QCC_TypeName(type), QCC_TypeName(def->type));
      } if (def->arraysize != arraysize && arraysize) {
        QCC_PR_ParseErrorPrintDef(ERR_TYPEMISMATCHARRAYSIZE, def, "Array sizes for redecleration of %s do not match", name);
      }
      if (allocate) {
        //HACK!HACK!HACK!
        //fprintf(stderr, "LALLOCATE: [%s] %d\n", name, (def->scope == scope && pr_subscopedlocals));
        qccbool fuckme = qcc_false;
        if (def->scope == scope) {
          // check if this definition is just a shadowed local
          //fprintf(stderr, "lv=%p; plv=%p\n", pr.localvars, pr.prevlocvars);
          for (QCC_def_t *e = pr.localvars; e != pr.prevlocvars; e = e->nextlocal) {
            //fprintf(stderr, "[%s] (%s)\n", def->name, e->name);
            if (strcmp(def->name, e->name) == 0) { fuckme = qcc_true; break; }
          }
        }
        if (!fuckme) { def = NULL; break; }
        //
        QCC_PR_ParseWarning(WARN_DUPLICATEDEFINITION, "%s duplicate definition ignored", name);
        QCC_PR_ParsePrintDef(WARN_DUPLICATEDEFINITION, def);
        //if (!scope) QCC_PR_ParsePrintDef(def);
      }
      return def;
    }
  }
  def = Hash_Get(&globalstable, name);
  while (def != NULL) {
    if (def->scope && def->scope != scope) {
      def = Hash_GetNext(&globalstable, name, def);
      continue; // in a different function
    }
    //
    if (def->isstatic && def->s_file != s_file) {
      // warn? or would that be pointless?
      foundstatic = def;
      def = Hash_GetNext(&globalstable, name, def);
      continue; // in a different function
    }
    //
    if (type && qcc_typecmp(def->type, type)) {
      if (!pr_scope) {
        QCC_PR_ParseErrorPrintDef(ERR_TYPEMISMATCHREDEC, def, "Type mismatch on redeclaration of %s. %s, should be %s", name, QCC_TypeName(type), QCC_TypeName(def->type));
      }
    }
    if (def->arraysize != arraysize && arraysize) {
      QCC_PR_ParseErrorPrintDef(ERR_TYPEMISMATCHARRAYSIZE, def, "Array sizes for redecleration of %s do not match", name);
    }
    if (allocate && scope) {
      if (pr_scope) {
        // warn? or would that be pointless?
        def = Hash_GetNext(&globalstable, name, def);
        continue; // in a different function
      }
      QCC_PR_ParseWarning(WARN_DUPLICATEDEFINITION, "%s duplicate definition ignored", name);
      QCC_PR_ParsePrintDef(WARN_DUPLICATEDEFINITION, def);
      //if (!scope) QCC_PR_ParsePrintDef(def);
    }
    return def;
  }
  // do we want to try case insensitive too?
  if (Hash_Get != &Hash_Get && !allocate) {
    if (scope) {
      def = Hash_Get(&localstable, name);
      while (def != NULL) {
        if (def->scope && def->scope != scope) {
          def = Hash_GetNext(&localstable, name, def);
          continue; // in a different function
        }
        if (type && qcc_typecmp(def->type, type)) {
          QCC_PR_ParseError(ERR_TYPEMISMATCHREDEC, "Type mismatch on redeclaration of %s. %s, should be %s", name, QCC_TypeName(type), QCC_TypeName(def->type));
        }
        if (def->arraysize != arraysize && arraysize) {
          QCC_PR_ParseError(ERR_TYPEMISMATCHARRAYSIZE, "Array sizes for redecleration of %s do not match", name);
        }
        if (allocate && scope) {
          QCC_PR_ParseWarning(WARN_DUPLICATEDEFINITION, "%s duplicate definition ignored", name);
          QCC_PR_ParsePrintDef(WARN_DUPLICATEDEFINITION, def);
          //if (!scope) QCC_PR_ParsePrintDef(def);
        }
        return def;
      }
    }
    def = Hash_Get(&globalstable, name);
    while (def != NULL) {
      if (def->scope && def->scope != scope) {
        def = Hash_GetNext(&globalstable, name, def);
        continue; // in a different function
      }
      if (def->isstatic && def->s_file != s_file) {
        // warn? or would that be pointless?
        foundstatic = def;
        def = Hash_GetNext(&globalstable, name, def);
        continue; // in a different function
      }
      if (type && qcc_typecmp(def->type, type)) {
        if (!pr_scope) {
          QCC_PR_ParseError(ERR_TYPEMISMATCHREDEC, "Type mismatch on redeclaration of %s. %s, should be %s", name, QCC_TypeName(type), QCC_TypeName(def->type));
        }
      }
      if (def->arraysize != arraysize && arraysize) {
        QCC_PR_ParseError(ERR_TYPEMISMATCHARRAYSIZE, "Array sizes for redecleration of %s do not match", name);
      }
      if (allocate && scope) {
        if (pr_scope) {
          //warn? or would that be pointless?
          def = Hash_GetNext(&globalstable, name, def);
          continue; // in a different function
        }
        QCC_PR_ParseWarning(WARN_DUPLICATEDEFINITION, "%s duplicate definition ignored", name);
        QCC_PR_ParsePrintDef(WARN_DUPLICATEDEFINITION, def);
        //if (!scope) QCC_PR_ParsePrintDef(def);
      }
      return def;
    }
  }
  if (foundstatic && !allocate) {
    QCC_PR_ParseWarning(WARN_DUPLICATEDEFINITION, "%s defined static", name);
    QCC_PR_ParsePrintDef(WARN_DUPLICATEDEFINITION, foundstatic);
  }
  if (!allocate) return NULL;
  // allocate new definition
  if (arraysize < 1) QCC_PR_ParseError(ERR_ARRAYNEEDSSIZE, "First declaration of array %s with no size",name);
  if (scope) {
    if (QCC_PR_GetDef(type, name, NULL, qcc_false, arraysize, qcc_false)) {
      QCC_PR_ParseWarning(WARN_SAMENAMEASGLOBAL, "Local \"%s\" defined with name of a global", name);
    }
  }
  //if (type->type == ev_function) fprintf(stderr, "ALLOCATING GLOBAL FOR FUNCTION: [%s]\n", name);
  if (type->type != ev_function) {
    ofs = numpr_globals;
    if (arraysize > 1) {
      // write the array size
      ofs = QCC_GetFreeOffsetSpace(1+(type->size*arraysize));
      ((qccint *)qcc_pr_globals)[ofs] = arraysize-1; // an array needs the size written first: this is a hexen2 opcode thing
      ++ofs;
    } else {
      ofs = QCC_GetFreeOffsetSpace(type->size*arraysize);
    }
  } else {
    // don't allocate global for function
    ofs = 0;
  }
  def = QCC_PR_DummyDef(type, name, scope, arraysize, ofs, qcc_true, saved);
  // fix up fields
  if (type->type == ev_field && allocate != 2) {
    // make arrays of fields work
    unsigned int i;
    for (i = 0; i < type->size*arraysize; ++i) *(qccint *)&qcc_pr_globals[def->ofs+i] = pr.size_fields+i;
    pr.size_fields += i;
  }
  if (scope) {
    def->nextlocal = pr.localvars;
    pr.localvars = def;
  }
  return def;
}


QCC_def_t *QCC_PR_DummyFieldDef (QCC_type_t *type, char *name, QCC_def_t *scope, int arraysize, unsigned int *fieldofs, qccbool saved) {
  int a;
  char array[64];
  char newname[256];
  QCC_def_t *def, *first = NULL;
  unsigned int maxfield, startfield;
  startfield = *fieldofs;
  maxfield = startfield;
  for (a = 0; a < arraysize; ++a) {
    if (a == 0) *array = '\0'; else snprintf(array, sizeof(array), "[%d]", a);
    if (*name) {
      snprintf(newname, sizeof(newname), "%s%s", name, array);
      // allocate a new def
      def = (void *)qccHunkAlloc(sizeof(QCC_def_t));
      memset(def, 0, sizeof(*def));
      def->next = NULL;
      def->arraysize = arraysize;
      pr.def_tail->next = def;
      pr.def_tail = def;
      def->s_line = pr_source_line;
      def->s_file = s_file;
      def->name = qccHunkDupStr(newname);
      def->type = type;
      def->scope = scope;
      def->ofs = QCC_GetFreeOffsetSpace(1);
      ((qccint *)qcc_pr_globals)[def->ofs] = *fieldofs;
      ++fieldofs;
      if (!first) first = def;
    } else {
      def = NULL;
    }
    //qcc_printf("Emited %s\n", newname);
  }
  *fieldofs = maxfield; // final size of the union
  return first;
}


static void QCC_PR_ParseInitializerType (int arraysize, QCC_def_t *def, QCC_type_t *type, int offset) {
  QCC_def_t *tmp;
  int i;
  if (arraysize > 1) {
    //arrays go recursive
    QCC_PR_Expect("{");
    for (i = 0; i < arraysize; ++i) {
      QCC_PR_ParseInitializerType(1, def, type, offset+i*type->size);
      if (!QCC_PR_EatDelim(",")) break;
    }
    QCC_PR_Expect("}");
  } else {
    tmp = NULL;
    if (type->type == ev_function && (pr_token_type == TT_DELIM || strcmp(pr_token, "builtin") == 0)) {
      // begin function special case
      QCC_def_t *parentfunc = pr_scope;
      QCC_function_t *f;
      QCC_dfunction_t *df;
      QCC_type_t *parm;
      ++def->references;
      pr_scope = def;
      if (QCC_PR_EatKeyword("builtin")) {
        f = (void *)qccHunkAlloc(sizeof(QCC_function_t));
        f->builtin = 666;
        locals_start = locals_end = 0;
      } else {
        f = QCC_PR_ParseImmediateStatements(type);
      }
      if (tmp == NULL) {
        int np;
        pr_scope = parentfunc;
        //tmp = QCC_MakeIntConst(numfunctions);
        //fprintf(stderr, "FUNC: [%s]: ofs=%u\n", def->name, def->ofs);
        f->def = def;
        f->funcnum = numfunctions;
        if (def->func != NULL && def->func != f) {
          qcc_printf("FATAL: WTF?!! (000)\n");
          abort();
        } else {
          def->func = f;
        }
        if (def->ofs > 0) {
          // this function was forward-declared and now it is defined, so fix global literal
          G_INT(def->ofs) = f->funcnum;
        }
        if (numfunctions >= MAX_FUNCTIONS) QCC_Error(ERR_INTERNAL, "Too many function defs");
        // fill in the dfunction
        df = &functions[numfunctions++];
        df->first_statement = (f->builtin ? -f->builtin : f->code);
        df->s_name = QCC_CopyString(f->def->name);
        df->s_file = s_file2;
        df->numparms = f->def->type->num_parms;
        if (locals_end == locals_start) locals_end = locals_start = 0;
        df->locals = locals_end-locals_start;
        df->parm_start = locals_start;
        //fprintf(stderr, "df->numparms=%d\n", df->numparms);
        np = df->numparms;
        if (np < 0) np = (np == -1 ? MAX_PARMS : -np-1);
        for (i = 0, parm = type->param; /*i < df->numparms*/parm != NULL; ++i, parm = parm->next) {
          //fprintf(stderr, " i=%d, parm=%p\n", i, parm);
          // k8: void print (string, ...): second parm is NULL (wtf?!)
          if (parm == NULL) {
            if (i != abs(df->numparms)-1) QCC_PR_ParseError(0, "internal compiler error in QCC_PR_ParseInitializerType()");
            df->parm_size[i] = 0;
            break;
          } else {
            df->parm_size[i] = parm->size|(parm->type<<8); // record argument type too
            if (parm->type == ev_field && parm->aux_type) {
              //fprintf(stderr, "arg #%d: field, ofs=%u\n", i, parm->ofs);
              df->parm_size[i] |= parm->aux_type->type<<16;
            }
          }
        }
        while (i < np) df->parm_size[i++] = 0;
        // end function special case
      }
    } else if (type->type == ev_string && QCC_PR_EatId("_")) {
      char trname[128];
      QCC_PR_Expect("(");
      if (pr_token_type != TT_IMMED || pr_immediate_type->type != ev_string) QCC_PR_ParseError(0, "_() intrinsic accepts only a string immediate");
      tmp = QCC_MakeStringConst(pr_immediate_string);
      QCC_PR_Lex();
      QCC_PR_Expect(")");
      if (!pr_scope || def->constant) {
        QCC_def_t *dt;
        snprintf(trname, sizeof(trname), "dotranslate_%d", ++dotranslate_count);
        dt = QCC_PR_DummyDef(type_string, trname, pr_scope, 1, offset, qcc_true, qcc_true);
        dt->references = 1;
        dt->constant = 1;
        dt->initialized = 1;
      }
    } else {
      tmp = QCC_PR_Expression(TOP_PRIORITY, EXPR_DISALLOW_COMMA);
      if (qcc_typecmp(type, tmp->type)) {
        // you can cast from const 0 to anything
        if (tmp->type->size == type->size && (tmp->type->type == ev_integer || tmp->type->type == ev_float) && tmp->constant && !G_INT(tmp->ofs)) {
        } else if (type->type == ev_float && tmp->type->type == ev_integer) {
          // cast from int->float will convert
          tmp = QCC_PR_Statement(&pr_opcodes[OP_CONV_ITOF], tmp, 0, NULL);
        } else if (type->type == ev_integer && tmp->type->type == ev_float) {
          // cast from float->int will convert
          tmp = QCC_PR_Statement(&pr_opcodes[OP_CONV_FTOI], tmp, 0, NULL);
        } else {
          QCC_PR_ParseErrorPrintDef(ERR_BADIMMEDIATETYPE, def, "wrong initializer type");
        }
      }
    }
    //
    if (!pr_scope || def->constant) {
      if (tmp != NULL && !tmp->constant) QCC_PR_ParseErrorPrintDef(ERR_BADIMMEDIATETYPE, def, "initializer is not constant");
      if (def->initialized && tmp != NULL) {
        for (i = 0; (unsigned int)i < type->size; ++i) {
          if (G_INT(offset+i) != G_INT(tmp->ofs+i)) QCC_PR_ParseErrorPrintDef(ERR_REDECLARATION, def, "incompatible redeclaration");
        }
      } else if (tmp != NULL) {
        for (i = 0; (unsigned int)i < type->size; ++i) G_INT(offset+i) = G_INT(tmp->ofs+i);
      }
    } else if (tmp != NULL) {
      QCC_def_t lhs, rhs;
      if (def->initialized) QCC_PR_ParseErrorPrintDef(ERR_REDECLARATION, def, "%s initialised twice", def->name);
      memset(&lhs, 0, sizeof(lhs));
      memset(&rhs, 0, sizeof(rhs));
      ++def->references;
      for (i = 0; (unsigned)i < type->size; ) {
        rhs.type = lhs.type = type_float;
        lhs.ofs = offset+i;
        ++tmp->references;
        rhs.ofs = tmp->ofs+i;
        if (type->size-i >= 3) {
          QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_V], &rhs, &lhs, NULL));
          i += 3;
        } else {
          QCC_FreeTemp(QCC_PR_Statement(&pr_opcodes[OP_STORE_F], &rhs, &lhs, NULL));
          ++i;
        }
      }
    }
    if (tmp != NULL) QCC_FreeTemp(tmp);
  }
}


void QCC_PR_ParseInitializerDef (QCC_def_t *def, qccbool isvar, qccbool isconst) {
  def->constant = (isconst || (!isvar && !pr_scope));
  QCC_PR_ParseInitializerType(def->arraysize, def, def->type, def->ofs);
  if (!def->initialized) def->initialized = 1;
}


/*
================
QCC_PR_ParseEnum

Parse enum definition
================
*/
static void QCC_PR_ParseEnum (void) {
  int iv = 0;
  float fv = 0.0f;
  int isint = QCC_PR_EatKeyword("int");
  if (!isint) QCC_PR_EatKeyword("float");
  QCC_PR_Expect("{");
  for (;;) {
    QCC_def_t *def;
    char *name = QCC_PR_ParseName();
    if (QCC_PR_EatDelim("=")) {
      if (pr_token_type != TT_IMMED &&
          ((isint && pr_immediate_type->type != ev_integer) ||
           (!isint && pr_immediate_type->type != ev_float))) {
        def = QCC_PR_GetDef(NULL, QCC_PR_ParseName(), NULL, qcc_false, 0, qcc_false);
        if (def) {
          if (!def->constant) {
            QCC_PR_ParseError(ERR_NOTANUMBER, "enum - %s is not a constant", def->name);
          } else {
            if (isint) iv = G_INT(def->ofs); else fv = G_FLOAT(def->ofs);
          }
        } else {
          QCC_PR_ParseError(ERR_NOTANUMBER, "enum - not a number");
        }
      } else {
        if (isint) iv = pr_immediate._int; else fv = pr_immediate._float;
        QCC_PR_Lex();
      }
    }
    def = (isint ? QCC_MakeIntConst(iv) : QCC_MakeFloatConst(fv));
    Hash_Add(&globalstable, name, def, qccHunkAlloc(sizeof(hashtable_bucket_t)));
    if (isint) ++iv; else ++fv;
    if (QCC_PR_EatDelim("}")) break;
    QCC_PR_Expect(",");
    if (QCC_PR_EatDelim("}")) break; // accept trailing comma
  }
  QCC_PR_Expect(";");
}


/*
================
QCC_PR_ParseEnumFlags

Parse enumflags definition
================
*/
static void QCC_PR_ParseEnumFlags (void) {
  int iv = 1;
  float fv = 1.0f;
  int isint = QCC_PR_EatKeyword("int");
  if (!isint) QCC_PR_EatKeyword("float");
  QCC_PR_Expect("{");
  for (;;) {
    int bits = 0;
    QCC_def_t *def;
    char *name = QCC_PR_ParseName();
    if (QCC_PR_EatDelim("=")) {
      if (pr_token_type != TT_IMMED &&
          ((isint && pr_immediate_type->type != ev_integer) ||
           (!isint && pr_immediate_type->type != ev_float))) {
        def = QCC_PR_GetDef(NULL, QCC_PR_ParseName(), NULL, qcc_false, 0, qcc_false);
        if (def) {
          if (!def->constant) {
            QCC_PR_ParseError(ERR_NOTANUMBER, "enumflags - %s is not a constant", def->name);
          } else {
            if (isint) iv = G_INT(def->ofs); else fv = G_FLOAT(def->ofs);
          }
        } else {
          QCC_PR_ParseError(ERR_NOTANUMBER, "enumflags - not a number");
        }
      } else {
        if (isint) iv = pr_immediate._int; else fv = pr_immediate._float;
        QCC_PR_Lex();
      }
    }
    if (isint) {
      int i = iv;
      while (i) {
        if (((i>>1)<<1) != i) ++bits;
        i >>= 1;
      }
      if (bits > 1) QCC_PR_ParseWarning(WARN_ENUMFLAGS_NOTBINARY, "enumflags - value %d not a single bit", (int)iv);
      def = QCC_MakeIntConst(iv);
      Hash_Add(&globalstable, name, def, qccHunkAlloc(sizeof(hashtable_bucket_t)));
      iv *= 2;
    } else {
      int i = (int)fv;
      if (i != fv) {
        QCC_PR_ParseWarning(WARN_ENUMFLAGS_NOTINTEGER, "enumflags - %f not an integer", fv);
      } else {
        while (i) {
          if (((i>>1)<<1) != i) ++bits;
          i >>= 1;
        }
        if (bits > 1) QCC_PR_ParseWarning(WARN_ENUMFLAGS_NOTBINARY, "enumflags - value %d not a single bit", (int)fv);
      }
      def = QCC_MakeFloatConst(fv);
      Hash_Add(&globalstable, name, def, qccHunkAlloc(sizeof(hashtable_bucket_t)));
      fv *= 2;
    }
    if (QCC_PR_EatDelim("}")) break;
    QCC_PR_Expect(",");
  }
  QCC_PR_Expect(";");
}


/*
================
PR_ParseDefs

Called at the outer layer and when a local statement is hit
'nocommas': don't process 2nd and other defs
================
*/
QCC_def_t *QCC_PR_ParseDefs (qccbool nocommas) {
  char *name;
  QCC_type_t *type;
  QCC_def_t *def, *d, *res = NULL;
  qccbool shared = qcc_false;
  qccbool isstatic = defaultstatic;
  qccbool externfnc = qcc_false;
  qccbool isconstant = qcc_false;
  qccbool isvar = qcc_false;
  qccbool noref = defaultnoref;
  qccbool nosave = qcc_true;
  qccbool allocatenew = qcc_true;
  qccbool inlinefunction = qcc_false;
  qccbool nonconst = qcc_false;
  int arraysize;
  int count = 0;

  void QCC_PR_ParseArrayDef (void) {
    char *oldprfile = pr_file_p;
    arraysize = 0;
    if (QCC_PR_EatDelim("]")) {
      QCC_PR_Expect("=");
      QCC_PR_Expect("{");
      QCC_PR_Lex();
      ++arraysize;
      for (;;) {
        if (pr_token_type == TT_EOF) break;
        if (QCC_PR_EatDelim(",")) ++arraysize;
        if (QCC_PR_EatDelim("}")) break;
        QCC_PR_Lex();
      }
      pr_file_p = oldprfile;
      QCC_PR_Lex();
    } else {
      arraysize = QCC_PR_IntConstExpr();
      QCC_PR_Expect("]");
    }
    if (arraysize < 1) {
      QCC_PR_ParseError (ERR_BADARRAYSIZE, "Definition of array (%s) size is not of a numerical value", name);
      arraysize = 0; // grrr...
    }
  }

  if (!nocommas) {
    while (QCC_PR_EatDelim(";")) ;

    if (QCC_PR_EatKeyword("enum")) { QCC_PR_ParseEnum(); return NULL; }
    if (QCC_PR_EatKeyword("enumflags")) { QCC_PR_ParseEnumFlags(); return NULL; }

    if (QCC_PR_EatKeyword("typedef")) {
      type = QCC_PR_ParseType(qcc_true, qcc_false);
      if (!type) QCC_PR_ParseError(ERR_NOTANAME, "typedef found unexpected tokens");
      type->name = QCC_CopyString(pr_token)+strings;
      QCC_PR_Lex();
      QCC_PR_Expect(";");
      return NULL;
    }

    // parse type flags
    if (QCC_PR_EatKeyword("const")) isconstant = noref = qcc_true;
    //
    while (QCC_PR_EatDelim("[[")) {
      for (;;) {
        if (QCC_PR_EatKeyword("shared")) {
          shared = qcc_true;
          if (pr_scope) QCC_PR_ParseError(ERR_NOSHAREDLOCALS, "Cannot have shared locals");
        }
        else if (QCC_PR_EatKeyword("extern")) externfnc = qcc_true;
        else if (QCC_PR_EatKeyword("const")) isconstant = noref = qcc_true;
        else if (QCC_PR_EatKeyword("nonconst")) { isconstant = qcc_false; isvar = nonconst = qcc_true; }
        else if (QCC_PR_EatKeyword("var")) { isvar = qcc_true; isconstant = qcc_false; nonconst = qcc_true; }
        else if (!pr_scope && QCC_PR_EatKeyword("static")) isstatic = qcc_true;
        else if (!pr_scope && QCC_PR_EatKeyword("nonstatic")) isstatic = qcc_false;
        else if (QCC_PR_EatKeyword("noref")) noref = qcc_true;
        else if (QCC_PR_EatKeyword("save")) nosave = qcc_false;
        else if (QCC_PR_EatKeyword("nosave")) nosave = qcc_true;
        else QCC_PR_ParseError(0, "invalid attribute: '%s'", pr_token);
        if (QCC_PR_EatDelim("]]")) break;
        QCC_PR_Expect(",");
        if (QCC_PR_EatDelim("]]")) break;
      }
    }
  }

  type = QCC_PR_ParseType(qcc_false, (nocommas ? qcc_true : qcc_false));
  if (type == NULL) return NULL; // ignore

  inlinefunction = type_inlinefunction;

  if (nocommas && (type->type == ev_function || type->type == ev_field)) {
    QCC_PR_ParseError(0, "function or field declaration is not allowed here");
  }

  if (externfnc && type->type != ev_function) {
    qcc_printf("Only functions may be defined as external (yet)\n");
    externfnc = qcc_false;
  }

  //if (pr_scope && type->type == ev_field) QCC_PR_ParseError("Fields must be global");
  do {
    if (count++ == 1 && nocommas) break;
    //
    if (QCC_PR_EatDelim(";")) {
      QCC_PR_ParseError(ERR_TYPEWITHNONAME, "type with no name");
      name = NULL;
    } else {
      name = QCC_PR_ParseName();
      //fprintf(stderr, "type: %s; name: %s\n", type->name, name);
    }
    // check for an array
    if (QCC_PR_EatDelim("[")) {
      if (nocommas) QCC_PR_ParseError(0, "array declaration is not allowed here");
      QCC_PR_ParseArrayDef();
    } else {
      arraysize = 1;
    }
    //fprintf(stderr, "nonc: %d [%s]\n", nonconst, name);
    // names consists of [A-Z0-9_] are consts and norefs
    if (type->type == ev_field) {
      //noref = qcc_true; // assume that all fields are referenced
    } else if (!nocommas && arraysize == 1 && !nonconst) {
      qccbool isup = qcc_true;
      for (const char *n = name; *n; ++n) {
        if (!isdigit(*n) && !(*n >= 'A' && *n <= 'Z') && *n != '_') { isup = qcc_false; break; }
      }
      if (isup) isconstant = noref = qcc_true;
    }
    // function def
    if (QCC_PR_EatDelim("(")) {
      if (nocommas) QCC_PR_ParseError(0, "function declaration is not allowed here");
      if (inlinefunction) {
        QCC_PR_ParseWarning(WARN_UNSAFEFUNCTIONRETURNTYPE, "Function returning function. Is this what you meant? (suggestion: use typedefs)");
      }
      inlinefunction = qcc_false;
      type = QCC_PR_ParseFunctionType(qcc_false, type);
    }
    //
    def = QCC_PR_GetDef(type, name, pr_scope, allocatenew, arraysize, !nosave);
    if (!def) QCC_PR_ParseError(ERR_NOTANAME, "%s is not part of someshit", name);
    if (noref) ++def->references;
    if (!def->initialized && shared) {
      // shared count as initiialised
      def->shared = shared;
      def->initialized = qcc_true;
    }
    if (externfnc) def->initialized = 2;
    if (isstatic) {
      if (def->s_file == s_file) {
        def->isstatic = isstatic;
      } else { //if (type->type != ev_function && defaultstatic)  // functions don't quite consitiute a definition
        QCC_PR_ParseErrorPrintDef(ERR_REDECLARATION, def, "can't redefine non-static as static");
      }
    }
    // check for an initialization
    if (type->type == ev_function && pr_scope) {
      if (QCC_PR_EatDelim("=") || QCC_PR_EatDelim("{")) QCC_PR_ParseError(ERR_INITIALISEDLOCALFUNCTION, "local functions may not be initialised");
      d = def;
      while (d != def->deftail) {
        d = d->next;
        d->initialized = 1; // fake function
        //k8:WTF?! G_FUNCTION(d->ofs) = 0;
      }
      continue;
    }
    if (!nocommas &&
        ((type->type == ev_function && (strchr("\x7b[:", *pr_token) || strcmp(pr_token, "builtin") == 0)) ||
         QCC_PR_EatDelim("="))) {
      // this is an initialisation (or a function)
      if (def->shared) QCC_PR_ParseError(ERR_SHAREDINITIALISED, "shared values may not be assigned an initial value");
      /*
      if (def->initialized == 1) {
        if (def->type->type == ev_function) {
          i = G_FUNCTION(def->ofs);
          df = &functions[i];
          QCC_PR_ParseErrorPrintDef(ERR_REDECLARATION, def, "%s redeclared, prev instance is in %s", name, strings+df->s_file);
        } else {
          QCC_PR_ParseErrorPrintDef(ERR_REDECLARATION, def, "%s redeclared", name);
        }
      }
      */
      QCC_PR_ParseInitializerDef(def, isvar, isconstant);
    } else {
      if (type->type == ev_function && isvar) {
        isconstant = !isvar;
        def->initialized = 1;
      }
      if (type->type == ev_field) {
        if (isconstant) def->constant = 2; // special flag on fields, 2, makes the pointer obtained from them also constant
        else if (isvar) def->constant = 0;
        else def->constant = 1;
      } else {
        def->constant = isconstant;
      }
    }
    res = def;
    d = def;
    while (d != def->deftail) {
      d = d->next;
      d->constant = def->constant;
      d->initialized = def->initialized;
    }
    if (nocommas) break;
  } while (QCC_PR_EatDelim(","));
  //
  if (!nocommas) {
    if (type->type == ev_function) {
      // semicolon after function definition is optional
      QCC_PR_EatDelim(";");
    } else {
      if (!QCC_PR_EatDelim(";")) QCC_PR_ParseWarning(WARN_UNDESIRABLECONVENTION, "Missing semicolon at end of definition");
    }
  }
  //
  return res;
}


/*
============
PR_CompileFile

compiles the 0 terminated text, adding defintions to the pr structure
============
*/
qccbool QCC_PR_CompileFile (char *string, const char *filename) {
  jmp_buf oldjb;
  if (!pr.memory) QCC_Error(ERR_INTERNAL, "PR_CompileFile: Didn't clear");
  compilingfile = filename;
  s_file = s_file2 = QCC_CopyString(filename);
  pr_file_p = string;
  pr_source_line = 0;
  memcpy(&oldjb, &pr_parse_abort, sizeof(oldjb));
  if (setjmp(pr_parse_abort)) {
    // dont count it as error
  } else {
    //clock up the first line
    QCC_PR_NewLine(qcc_false);
    QCC_PR_Lex(); // read first token
  }
  while (pr_token_type != TT_EOF) {
    if (setjmp(pr_parse_abort)) {
      if (++pr_error_count > MAX_ERRORS) {
        memcpy(&pr_parse_abort, &oldjb, sizeof(oldjb));
        return qcc_false;
      }
      num_continues = 0;
      num_breaks = 0;
      num_cases = 0;
      QCC_PR_SkipToSemicolon();
      if (pr_token_type == TT_EOF) {
        memcpy(&pr_parse_abort, &oldjb, sizeof(oldjb));
        return qcc_false;
      }
    }
    pr_scope = NULL; // outside all functions
    QCC_PR_ParseDefs(0);
  }
  memcpy(&pr_parse_abort, &oldjb, sizeof(oldjb));
  return (pr_error_count == 0);
}


qccbool QCC_Include (const char *filename) {
  char *newfile;
  char fname[512];
  char *opr_file_p;
  QCC_string_t os_file, os_file2;
  int opr_source_line;
  const char *ocompilingfile;
  struct qcc_includechunk_s *oldcurrentchunk;
  extern struct qcc_includechunk_s *currentchunk;

  ocompilingfile = compilingfile;
  os_file = s_file;
  os_file2 = s_file2;
  opr_source_line = pr_source_line;
  opr_file_p = pr_file_p;
  oldcurrentchunk = currentchunk;

  strcpy(fname, filename);
  QCC_LoadFile(fname, (void*)&newfile);
  currentchunk = NULL;
  pr_file_p = newfile;
  QCC_PR_CompileFile(newfile, fname);
  currentchunk = oldcurrentchunk;

  compilingfile = ocompilingfile;
  s_file = os_file;
  s_file2 = os_file2;
  pr_source_line = opr_source_line;
  pr_file_p = opr_file_p;

  //QCC_PR_IncludeChunk(newfile, qcc_false, fname);

  return qcc_true;
}


void QCC_Clear_Memory (int dofree) {
  if (dofree) {
    if (pr_breaks) free(pr_breaks);
    if (pr_continues) free(pr_continues);
    if (pr_cases) free(pr_cases);
    if (pr_casesdef) free(pr_casesdef);
    if (pr_casesdef2) free(pr_casesdef2);
    if (pr_gotos) free(pr_gotos);
    if (pr_labels) free(pr_labels);
  }
  max_breaks = num_breaks = 0;
  pr_breaks = NULL;
  max_cases = num_cases = 0;
  pr_cases = NULL;
  pr_casesdef = pr_casesdef2 = NULL;
  max_continues = num_continues = 0;
  pr_continues = NULL;
  max_gotos = num_gotos = 0;
  pr_gotos = NULL;
  max_labels = num_labels = 0;
  pr_labels = NULL;
}
