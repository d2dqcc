/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>

#include "hash.h"


// hash init assumes we get clean memory
void Hash_InitTable (hashtable_t *table, uint32_t numbucks, void *mem) {
  table->numbuckets = numbucks;
  table->bucket = (hashtable_bucket_t **)mem;
}


/*
uint32_t Hash_Key (const char *name, uint32_t modulus) {
  uint32_t key = 0;
  for (; *name; ++name) key += (key<<3)+(key>>28)+((unsigned char)name[0]);
  return (key%modulus);
}
*/


uint32_t Hash_Key (const char *name, uint32_t modulus) {
  size_t len = strlen(name);
  uint32_t hash = 0;
  for (size_t i = 0; i < len; ++i) {
    hash += (unsigned char)(name[i]);
    hash += (hash<<10);
    hash ^= (hash>>6);
  }
  hash += (hash<<3);
  hash ^= (hash>>11);
  hash += (hash<<15);
  return hash%modulus;
}


void *Hash_Get (const hashtable_t *table, const char *name) {
  for (const hashtable_bucket_t *buck = table->bucket[Hash_Key(name, table->numbuckets)]; buck != NULL; buck = buck->next) {
    if (strcmp(name, buck->key.string) == 0) return buck->data;
  }
  return NULL;
}


void *Hash_GetKey (const hashtable_t *table, uint32_t key) {
  for (hashtable_bucket_t *buck = table->bucket[key%table->numbuckets]; buck != NULL; buck = buck->next) {
    if (buck->key.value == key) return buck->data;
  }
  return NULL;
}


/* does *NOT* support items that are added with two names */
void *Hash_GetNextKey (const hashtable_t *table, uint32_t key, void *old) {
  hashtable_bucket_t *buck;
  for (buck = table->bucket[key%table->numbuckets]; buck != NULL; buck = buck->next) if (buck->data == old) break; // found the old one
  if (buck == NULL) return NULL;
  // don't return old
  for (buck = buck->next; buck != NULL; buck = buck->next) if (buck->key.value == key) return buck->data;
  return NULL;
}


/* does *NOT* support items that are added with two names */
void *Hash_GetNext (const hashtable_t *table, const char *name, void *old) {
  const hashtable_bucket_t *buck;
  for (buck = table->bucket[Hash_Key(name, table->numbuckets)]; buck != NULL; buck = buck->next) {
    if (buck->data == old) break; // found the old one
  }
  if (buck == NULL) return NULL;
  // don't return old
  for (buck = buck->next; buck != NULL; buck = buck->next) if (strcmp(name, buck->key.string) == 0) return buck->data;
  return NULL;
}


void *Hash_Add (hashtable_t *table, const char *name, void *data, hashtable_bucket_t *buck) {
  uint32_t bucknum = Hash_Key(name, table->numbuckets);
  buck->data = data;
  buck->key.string = name;
  buck->next = table->bucket[bucknum];
  table->bucket[bucknum] = buck;
  return buck;
}


void *Hash_AddKey (hashtable_t *table, uint32_t key, void *data, hashtable_bucket_t *buck) {
  uint32_t bucknum = key%table->numbuckets;
  buck->data = data;
  buck->key.value = key;
  buck->next = table->bucket[bucknum];
  table->bucket[bucknum] = buck;
  return buck;
}


static inline int hash_remove_ex (hashtable_t *table, uint32_t bucknum, int (*cmpfn)(const hashtable_bucket_t *buck)) {
  hashtable_bucket_t *buck = table->bucket[bucknum];
  if (cmpfn(buck)) {
    table->bucket[bucknum] = buck->next;
    return 1;
  }
  for (; buck->next != NULL; buck = buck->next) {
    if (cmpfn(buck->next)) {
      buck->next = buck->next->next;
      return 1;
    }
  }
  return 0;
}


void Hash_Remove (hashtable_t *table, const char *name) {
  hash_remove_ex(table, Hash_Key(name, table->numbuckets), ({
    int cmp (const hashtable_bucket_t *buck) {
      return (strcmp(name, buck->key.string) == 0);
    }
    cmp;
  }));
}


void Hash_RemoveData (hashtable_t *table, const char *name, void *data) {
  hash_remove_ex(table, Hash_Key(name, table->numbuckets), ({
    int cmp (const hashtable_bucket_t *buck) {
      return (buck->data == data && strcmp(name, buck->key.string) == 0);
    }
    cmp;
  }));
}


void Hash_RemoveKey (hashtable_t *table, uint32_t key) {
  hash_remove_ex(table, key%table->numbuckets, ({
    int cmp (const hashtable_bucket_t *buck) {
      return (buck->key.value == key);
    }
    cmp;
  }));
}
