/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _QCC_HASH_H__
#define _QCC_HASH_H__

#include <stdint.h>

#define Hash_BytesForBuckets(b)  (sizeof(hashtable_bucket_t *)*(b))


typedef struct hashtable_bucket_s {
  void *data;
  union {
    const char *string;
    uint32_t value;
  } key;
  struct hashtable_bucket_s *next;
} hashtable_bucket_t;


typedef struct hashtable_s {
  uint32_t numbuckets;
  hashtable_bucket_t **bucket;
} hashtable_t;


extern uint32_t Hash_Key (const char *name, uint32_t modulus);

extern void Hash_InitTable (hashtable_t *table, uint32_t numbucks, void *mem); // mem must be 0 filled. (memset(mem, 0, size))

extern void *Hash_Get (const hashtable_t *table, const char *name);
extern void *Hash_GetKey (const hashtable_t *table, uint32_t key);
extern void *Hash_GetNext (const hashtable_t *table, const char *name, void *old);
extern void *Hash_GetNextKey (const hashtable_t *table, uint32_t key, void *old);

extern void *Hash_Add (hashtable_t *table, const char *name, void *data, hashtable_bucket_t *buck);
extern void *Hash_AddKey (hashtable_t *table, uint32_t key, void *data, hashtable_bucket_t *buck);

extern void Hash_Remove (hashtable_t *table, const char *name);
extern void Hash_RemoveData (hashtable_t *table, const char *name, void *data);
extern void Hash_RemoveKey (hashtable_t *table, uint32_t key);


#endif
