/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// cmdlib.c
#include <ctype.h>

#include "qcc.h"


#define PATHSEPERATOR   '/'

// set these before calling CheckParm
int myargc;
char **myargv;

char qcc_token[1024];
int qcc_eof;


const unsigned int type_size[13] = {
  1, //void
  sizeof(string_t)/4, //string
  1, //float
  3, //vector
  1, //entity
  1, //field
  sizeof(func_t)/4, //function
  1, //pointer (its an int index)
  1, //integer
  1, //fixme: how big should a variant be?
};


/*
============================================================================

          BYTE ORDER FUNCTIONS

============================================================================
*/
short (*PRBigShort) (short l);
short (*PRLittleShort) (short l);
int (*PRBigLong) (int l);
int (*PRLittleLong) (int l);
float (*PRBigFloat) (float l);
float (*PRLittleFloat) (float l);


static short QCC_SwapShort (short l) {
  qbyte b1, b2;
  b1 = l&255;
  b2 = (l>>8)&255;
  return (b1<<8)+b2;
}


static short QCC_Short (short l) {
  return l;
}


static int QCC_SwapLong (int l) {
  qbyte b1, b2, b3, b4;
  b1 = (qbyte)l;
  b2 = (qbyte)(l>>8);
  b3 = (qbyte)(l>>16);
  b4 = (qbyte)(l>>24);
  return ((int)b1<<24) + ((int)b2<<16) + ((int)b3<<8) + b4;
}


static int QCC_Long (int l) {
  return l;
}


static float QCC_SwapFloat (float l) {
  union {qbyte b[4]; float f;} in, out;
  in.f = l;
  out.b[0] = in.b[3];
  out.b[1] = in.b[2];
  out.b[2] = in.b[1];
  out.b[3] = in.b[0];
  return out.f;
}


static float QCC_Float (float l) {
  return l;
}


void SetEndian (void) {
  union {qbyte b[2]; unsigned short s;} ed;
  ed.s = 255;
  if (ed.b[0] == 255) {
    PRBigShort = QCC_SwapShort;
    PRLittleShort = QCC_Short;
    PRBigLong = QCC_SwapLong;
    PRLittleLong  = QCC_Long;
    PRBigFloat = QCC_SwapFloat;
    PRLittleFloat = QCC_Float;
  } else {
    PRBigShort = QCC_Short;
    PRLittleShort = QCC_SwapShort;
    PRBigLong = QCC_Long;
    PRLittleLong = QCC_SwapLong;
    PRBigFloat = QCC_Float;
    PRLittleFloat = QCC_SwapFloat;
  }
}


/*
==============
COM_Parse

Parse a token out of a string
==============
*/
char *QCC_COM_Parse (char *data) {
  int len = 0, c;
  qcc_token[0] = 0;
  if (!data) return NULL;
  // skip whitespace
skipwhite:
  while ((c = (unsigned char)(*data)) <= ' ') {
    if (c == 0) {
      qcc_eof = qcc_true;
      return NULL; // end of file;
    }
    ++data;
  }
  // skip // comments
  if (c == '/' && data[1] == '/') {
    while (*data && *data != '\n') ++data;
    goto skipwhite;
  }
  // skip /* comments
  if (c == '/' && data[1] == '*') {
    while (data[1] && (data[0] != '*' || data[1] != '/')) ++data;
    data += 2;
    goto skipwhite;
  }
  // handle quoted strings specially
  if (c == '\"') {
    ++data;
    for (;;) {
      c = (unsigned char)(*data);
      ++data;
      if (c == '\\' && *data == '\"') {
        //allow C-style string escapes
        c = (unsigned char)(*data);
        ++data;
      } else if (c == '\\' && *data == '\\') {
        // \ is now a special character so it needs to be marked up using itself
        c = (unsigned char)(*data);
        ++data;
      } else if (c == '\\' && *data == 'n') {
        // and do new lines while we're at it.
        c = '\n';
        ++data;
      } else if (c == '\"') {
        qcc_token[len] = 0;
        return data;
      } else if (c == '\0' || c == '\n') {
        qcc_token[len] = 0;
        return data;
      }
      qcc_token[len] = c;
      ++len;
    }
  }
  // parse single characters
  if (strchr("{}()':,", c)) {
    qcc_token[len] = c;
    ++len;
    qcc_token[len] = 0;
    return data+1;
  }
  // parse a regular word
  do {
    qcc_token[len] = c;
    ++data;
    ++len;
    c = (unsigned int)(*data);
    if (strchr("{}()'\":,", c)) break;
  } while (c > 32);
  qcc_token[len] = 0;
  return data;
}


//more C tokens...
char *QCC_COM_Parse2 (char *data) {
  int len = 0, c;
  len = 0;
  qcc_token[0] = 0;
  if (!data) return NULL;
  // skip whitespace
skipwhite:
  while ((c = (unsigned char)(*data)) <= ' ') {
    if (c == 0) {
      qcc_eof = qcc_true;
      return NULL; // end of file;
    }
    ++data;
  }
  // skip // comments
  if (c == '/' && data[1] == '/') {
    while (*data && *data != '\n') ++data;
    goto skipwhite;
  }
  // handle quoted strings specially
  if (c == '\"') {
    ++data;
    for (;;) {
      c = (unsigned int)(*data);
      ++data;
      if (c == '\\' && *data == '\"') {
        //allow C-style string escapes
        c = (unsigned int)(*data);
        ++data;
      } else if (c == '\\' && *data == '\\') {
        // \ is now a special character so it needs to be marked up using itself
        c = (unsigned int)(*data);
        ++data;
      } else if (c == '\\' && *data == 'n') {
        // and do new lines while we're at it.
        c = '\n';
        ++data;
      } else if (c == '\"' || c == '\0') {
        qcc_token[len] = c;
      }
      ++len;
    }
  }
  // parse numbers
  if (c >= '0' && c <= '9') {
    if (c == '0' && data[1] == 'x') {
      //parse hex
      qcc_token[0] = '0';
      c = 'x';
      len = 1;
      ++data;
      for (;;) {
        //parse regular number
        qcc_token[len] = c;
        ++data;
        ++len;
        c = (unsigned int)(*data);
        if ((c < '0'|| c > '9') && (c < 'a'||c > 'f') && (c < 'A'||c > 'F') && c != '.') break;
      }
    } else {
      for (;;) {
        //parse regular number
        qcc_token[len] = c;
        ++data;
        ++len;
        c = (unsigned int)(*data);
        if ((c < '0'|| c > '9') && c != '.') break;
      }
    }
    qcc_token[len] = 0;
    return data;
  }
  // parse words
  //TODO: allow '$'?
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_') {
    do {
      qcc_token[len] = c;
      ++data;
      ++len;
      c = (unsigned int)(*data);
    } while ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_');
    qcc_token[len] = 0;
    return data;
  }
  qcc_token[len] = c;
  ++len;
  qcc_token[len] = 0;
  return data+1;
}


char *qcva (const char *text, ...) {
  va_list argptr;
  static char msg[2048];
  va_start(argptr,text);
  vsnprintf(msg,sizeof(msg)-1, text,argptr);
  va_end(argptr);
  return msg;
}


/*
=============================================================================

            MISC FUNCTIONS

=============================================================================
*/

/*
=================
Error

For abnormal program terminations
=================
*/
void QCC_Error (int errortype, const char *error, ...) {
  va_list argptr;
  char msg[2048];
  va_start(argptr,error);
  vsnprintf(msg,sizeof(msg)-1, error,argptr);
  va_end(argptr);
  qcc_printf("\n************ ERROR ************\n%s\n", msg);
  qcc_set_badfile_name(strings+s_file, pr_source_line);
  numsourcefiles = 0;
  longjmp(qcccompileerror, 1);
}


/*
=================
CheckParm

Checks for the given parameter in the program's command line arguments
Returns the argument number (1 to argc-1) or 0 if not present
=================
*/
int QCC_CheckParm (const char *check) {
  for (int i = 1; i < myargc; ++i) if (!QC_strcasecmp(check, myargv[i])) return i;
  return 0;
}



void DefaultExtension (char *path, const char *extension) {
  char *src;
  // if path doesn't have a .EXT, append extension (extension should include the .)
  src = path+strlen(path)-1;
  while (*src != PATHSEPERATOR && src != path) {
    if (*src == '.') return; // it has an extension
    --src;
  }
  strcat(path, extension);
}


void DefaultPath (char *path, const char *basepath) {
  char temp[4096];
  if (path[0] == PATHSEPERATOR) return; // absolute path location
  strcpy(temp, path);
  strcpy(path, basepath);
  strcat(path, temp);
}


void StripFilename (char *path) {
  int length = strlen(path)-1;
  while (length > 0 && path[length] != PATHSEPERATOR) --length;
  path[length] = 0;
}


/*
====================
Extract file parts
====================
*/
void ExtractFilePath (const char *path, char *dest) {
  const char *src = path+strlen(path)-1;
  // back up until a \ or the start
  while (src != path && *(src-1) != PATHSEPERATOR) --src;
  memmove(dest, path, src-path);
  dest[src-path] = 0;
}


void ExtractFileBase (const char *path, char *dest) {
  const char *src = path+strlen(path)-1;
  // back up until a \ or the start
  while (src != path && *(src-1) != PATHSEPERATOR) --src;
  while (*src && *src != '.') *dest++ = *src++;
  *dest = 0;
}


void ExtractFileExtension (const char *path, char *dest) {
  const char *src = path+strlen(path)-1;
  // back up until a . or the start
  while (src != path && *(src-1) != '.') --src;
  if (src == path) { *dest = 0; return; } // no extension
  memmove(dest, src, strlen(src)+1);
}


/*
==============
ParseNum / ParseHex
==============
*/
long ParseHex (const char *hex) {
  const char *str = hex;
  long num = 0;
  while (*str) {
    num <<= 4;
    if (*str >= '0' && *str <= '9') num += *str-'0';
    else if (*str >= 'a' && *str <= 'f') num += 10 + *str-'a';
    else if (*str >= 'A' && *str <= 'F') num += 10 + *str-'A';
    else QCC_Error(ERR_BADHEX, "Bad hex number: %s", hex);
    ++str;
  }
  return num;
}


long ParseNum (const char *str) {
  char *endptr;
  long res;
  /*if (str[0] == '$') return ParseHex(str+1);*/ //k8
  if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X')) return ParseHex(str+2);
  /*return atol(str);*/
  res = strtol(str, &endptr, 10); /*k8: fuck octals */
  if (*str && !endptr[0]) QCC_Error(ERR_BADHEX, "Bad number: %s", str);
  return res;
}


//buffer size and max size are different. buffer is bigger.

#define MAXQCCFILES  (3)
struct {
  char name[512];
  char *buff;
  int buffsize;
  int ofs;
  int maxofs;
} qccfile[MAXQCCFILES];


int SafeOpenWrite (const char *filename, int maxsize) {
  for (int i = 0; i < MAXQCCFILES; ++i) {
    if (!qccfile[i].buff) {
      strcpy(qccfile[i].name, filename);
      qccfile[i].buffsize = maxsize;
      qccfile[i].maxofs = 0;
      qccfile[i].ofs = 0;
      qccfile[i].buff = malloc(qccfile[i].buffsize);
      return i;
    }
  }
  QCC_Error(ERR_TOOMANYOPENFILES, "Too many open files on file %s", filename);
  return -1;
}


void ResizeBuf (int hand, int newsize) {
  //int wasmal = qccfile[hand].buffismalloc;
  char *nb;
  if (qccfile[hand].buffsize >= newsize) return; //already big enough
  nb = qcc_realloc(qccfile[hand].buff, newsize);
  if (nb == NULL) abort(); //k8:FIXME
  qccfile[hand].buff = nb;
  qccfile[hand].buffsize = newsize;
}


void SafeWrite (int hand, const void *buf, long count) {
  if (count > 0) {
    if (qccfile[hand].ofs+count >= qccfile[hand].buffsize) ResizeBuf(hand, qccfile[hand].ofs+count+(64*1024));
    memmove(&qccfile[hand].buff[qccfile[hand].ofs], buf, count);
    qccfile[hand].ofs += count;
    if (qccfile[hand].ofs > qccfile[hand].maxofs) qccfile[hand].maxofs = qccfile[hand].ofs;
  }
}


//FIXME: always return new offset
int SafeSeek (int hand, int ofs, int mode) {
  if (mode == SEEK_CUR) return qccfile[hand].ofs;
  ResizeBuf(hand, ofs+1024);
  qccfile[hand].ofs = ofs;
  if (qccfile[hand].ofs > qccfile[hand].maxofs) qccfile[hand].maxofs = qccfile[hand].ofs;
  return 0;
}


void SafeClose (int hand) {
  qcc_pf_externs->WriteFile(qccfile[hand].name, qccfile[hand].buff, qccfile[hand].maxofs);
  free(qccfile[hand].buff);
  qccfile[hand].buff = NULL;
}


qcc_cachedsourcefile_t *qcc_sourcefile;

long QCC_LoadFile (const char *filename, void **bufferptr) {
  char *mem;
  int len = qcc_pf_externs->FileSize(filename);
  if (len < 0) {
    QCC_Error(ERR_COULDNTOPENFILE, "Couldn't open file %s", filename);
    return -1;
  }
  mem = qccHunkAlloc(sizeof(qcc_cachedsourcefile_t)+len+2);
  ((qcc_cachedsourcefile_t *)mem)->next = qcc_sourcefile;
  qcc_sourcefile = (qcc_cachedsourcefile_t *)mem;
  qcc_sourcefile->size = len;
  mem += sizeof(qcc_cachedsourcefile_t);
  strcpy(qcc_sourcefile->filename, filename);
  qcc_sourcefile->file = mem;
  qcc_sourcefile->type = FT_CODE;
  qcc_pf_externs->ReadFile(filename, mem, len+2);
  mem[len] = '\n';
  mem[len+1] = '\0';
  *bufferptr = mem;
  return len;
}


void StripExtension (char *path) {
  int length = strlen(path)-1;
  while (length > 0 && path[length] != '.') {
    --length;
    if (path[length] == PATHSEPERATOR) return; // no extension
  }
  if (length) path[length] = 0;
}
