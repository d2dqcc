/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//compile routines
#include <setjmp.h>

#include "qcc.h"


int qccalloced;
int qcchunksize;
char *qcchunk;
int qccpersisthunk;


void *qccHunkAlloc (size_t mem) {
  mem = (mem+7)&~7;
  qccalloced += mem;
  if (qccalloced > qcchunksize) QCC_Error(ERR_INTERNAL, "Compile hunk was filled");
  memset(qcchunk+qccalloced-mem, 0, mem);
  return qcchunk+qccalloced-mem;
}


void qccClearHunk (void) {
  if (qcchunk) {
    free(qcchunk);
    qcchunk = NULL;
  }
}


void *qccHunkDupStr (const char *str) {
  void *res;
  int len;
  if (str == NULL) str = "";
  len = strlen(str)+1;
  res = qccHunkAlloc(len);
  memcpy(res, str, len);
  return res;
}


