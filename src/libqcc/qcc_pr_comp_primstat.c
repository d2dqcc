static inline qccbool is_edict_load (int op) {
  switch (op) {
    case OP_LOAD_F:
    case OP_LOAD_V:
    case OP_LOAD_S:
    case OP_LOAD_ENT:
    case OP_LOAD_FLD:
    case OP_LOAD_FNC:
    case OP_LOAD_I:
    //case OP_LOAD_P:
      return qcc_true;
  }
  return qcc_false;
}


static inline qccbool is_array_load (int op) {
  switch (op) {
    case OP_LOADA_F:
    case OP_LOADA_V:
    case OP_LOADA_S:
    case OP_LOADA_ENT:
    case OP_LOADA_FLD:
    case OP_LOADA_FNC:
    case OP_LOADA_I:
      return qcc_true;
  }
  return qcc_false;
}


static inline qccbool is_pointer_load (int op) {
  switch (op) {
    case OP_LOADP_F:
    case OP_LOADP_V:
    case OP_LOADP_S:
    case OP_LOADP_ENT:
    case OP_LOADP_FLD:
    case OP_LOADP_FNC:
    case OP_LOADP_I:
      return qcc_true;
  }
  return qcc_false;
}




/*
static const char *get_typename (qcc_etype tp) {
  static char buf[128];
  if (tp >= 0 && tp < sizeof(basictypenames)/sizeof(basictypenames[0])) return basictypenames[tp];
  snprintf(buf, sizeof(buf), "<badtype:%d>", tp);
  return buf;
}


static void dump_type (const QCC_type_t *t, const char *msg) {
  if (t == NULL) { fprintf(stderr, " %stype: <not defined>\n", msg); return; }
  fprintf(stderr, " %stype: %s", msg, get_typename(t->type));
  if (t->name) fprintf(stderr, " (%s)", t->name);
  fputc('\n', stderr);
  if (t->type == ev_void) return;
  if (t->type == ev_function) {
    fprintf(stderr, "  argc : %d\n", t->num_parms);
  }
  fprintf(stderr, "  ofs  : %u\n", t->ofs);
  fprintf(stderr, "  size : %u\n", t->size);
  fprintf(stderr, "  asize: %u\n", t->arraysize);
}


static void dump_deft (const QCC_def_t *d) {
  if (d == NULL) return;
  fprintf(stderr, " d(%p): '%s'", d, d->name);
  if (d->s_file) fprintf(stderr, " is defined here: %s:%d", strings+d->s_file, d->s_line);
  fputc('\n', stderr);
  fprintf(stderr, " ofs: %u\n", d->ofs);
  fprintf(stderr, " temp: %p\n", d->temp);
  if (d->type) {
    dump_type(d->type, "");
    if (d->func != NULL) fprintf(stderr, "  funcnum: %d\n", d->func->funcnum);
    dump_type(d->type->aux_type, "aux ");
    if (d->type->param) {
      int cnt = 0;
      for (const QCC_type_t *t = d->type->param; t != NULL; t = t->next) {
        char buf[128];
        snprintf(buf, sizeof(buf), "param#%d ", cnt++);
        dump_type(t, buf);
      }
    }
  }
}
*/


static inline void fix_func_idx (QCC_def_t *func) {
  if (!func || !func->type || func->type->type != ev_function) return;
  if (func->ofs == 0) {
    // never called, create index literal
    QCC_def_t *tmp;
    if (func->func) {
      // defined
      tmp = QCC_MakeIntConst(func->func->funcnum);
    } else {
      // prototyped, not defined; can't merge literal
      tmp = QCC_MakeDummyIntConst();
    }
    func->ofs = tmp->ofs;
    QCC_FreeTemp(tmp);
  }
}


/*
============
QCC_PR_SimpleStatement

Emits a primitive statement, returning the var it places it's value in
============
*/
static QCC_dstatement_t *QCC_PR_SimpleStatement (int op, int var_a, int var_b, int var_c, int force) {
  QCC_dstatement_t *statement;
  if (!force && !QCC_OPCodeValid(pr_opcodes+op)) {
    QCC_PR_ParseError(ERR_BADEXTENSION, "Opcode \"%s|%s\" not valid for target\n", pr_opcodes[op].name, pr_opcodes[op].opname);
  }
  statement_linenums[numstatements] = pr_source_line;
  statement = &statements[numstatements];
  ++numstatements;
  statement->op = op;
  statement->a = var_a;
  statement->b = var_b;
  statement->c = var_c;
  return statement;
}


static void QCC_PR_Statement3 (const QCC_opcode_t *op, QCC_def_t *var_a, QCC_def_t *var_b, QCC_def_t *var_c, int force) {
  QCC_dstatement_t *statement;
  if (!force && !QCC_OPCodeValid(op)) {
    //outputversion = op->extension;
    //if (noextensions)
    QCC_PR_ParseError(ERR_BADEXTENSION, "Opcode \"%s|%s\" not valid for target\n", op->name, op->opname);
  }
  fix_func_idx(var_a);
  fix_func_idx(var_b);
  fix_func_idx(var_c);
  statement = &statements[numstatements];
  ++numstatements;
  statement_linenums[statement-statements] = pr_source_line;
  statement->op = op-pr_opcodes;
  statement->a = (var_a ? var_a->ofs : 0);
  statement->b = (var_b ? var_b->ofs : 0);
  statement->c = (var_c ? var_c->ofs : 0);
}


/*
============
PR_Statement

Emits a primitive statement, returning the var it places it's value in
============
*/
static QCC_def_t *QCC_PR_Statement (const QCC_opcode_t *op, QCC_def_t *var_a, QCC_def_t *var_b, QCC_dstatement_t **outstatement) {
  QCC_dstatement_t *statement;
  QCC_def_t *var_c = NULL;
  //
  fix_func_idx(var_a);
  fix_func_idx(var_b);
  /*
  if (op-pr_opcodes >= OP_CALL0 && op-pr_opcodes <= OP_CALL8) {
    fprintf(stderr, "CALL%d: var_a=%p, var_b=%p, outstatement=%p\n", op-pr_opcodes-OP_CALL0, var_a, var_b, outstatement);
    dump_deft(var_a);
    dump_deft(var_b);
  }
  */
  //
  if (outstatement == QCC_STAT_NOCONV) {
    outstatement = NULL;
  } else if (op->priority != -1 && !is_conditional_priority(op->priority)) {
    if (op->associative != ASSOC_LEFT) {
      //fprintf(stderr, "(%d)NOTLEFT[%s], var_a=%p, var_b=%p\n", op->associative, op->name, var_a, var_b);
      if (op->type_a != &type_pointer) var_b = QCC_SupplyConversion(var_b, (*op->type_a)->type, qcc_false);
    } else {
      if (var_a) var_a = QCC_SupplyConversion(var_a, (*op->type_a)->type, qcc_false);
      if (var_b) var_b = QCC_SupplyConversion(var_b, (*op->type_b)->type, qcc_false);
    }
  }
  if (var_a) { ++var_a->references; QCC_FreeTemp(var_a); }
  if (var_b) { ++var_b->references; QCC_FreeTemp(var_b); }
  if (var_a != NULL && var_b != NULL) {
    if (var_a->type->type == ev_entity && var_b->type->type == ev_entity &&
        var_a->type != var_b->type && strcmp(var_a->type->name, var_b->type->name) != 0) {
      QCC_PR_ParseWarning(0, "Implicit cast from '%s' to '%s'", var_a->type->name, var_b->type->name);
    }
  }
  // maths operators
  {
    // optimize constant expressions
    if (var_a && var_a->constant) {
      if (var_b && var_b->constant) {
        QCC_def_t *nd;
        // both are constants
        // improve some of the maths
        switch (op-pr_opcodes) {
          case OP_LOADA_F:
          case OP_LOADA_V:
          case OP_LOADA_S:
          case OP_LOADA_ENT:
          case OP_LOADA_FLD:
          case OP_LOADA_FNC:
          case OP_LOADA_I:
            nd = (void *)qccHunkAlloc(sizeof(QCC_def_t));
            memset (nd, 0, sizeof(QCC_def_t));
            nd->type = var_a->type;
            nd->ofs = var_a->ofs+G_INT(var_b->ofs);
            nd->temp = var_a->temp;
            nd->constant = qcc_true;
            nd->name = var_a->name;
            return nd;
        case OP_BITOR_F:
          ++optres_constantarith;
          return QCC_MakeFloatConst((float)((int)G_FLOAT(var_a->ofs)|(int)G_FLOAT(var_b->ofs)));
        case OP_BITAND_F:
          ++optres_constantarith;
          return QCC_MakeFloatConst((float)((int)G_FLOAT(var_a->ofs)&(int)G_FLOAT(var_b->ofs)));
        case OP_MUL_F:
          ++optres_constantarith;
          return QCC_MakeFloatConst(G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs));
        case OP_DIV_F:
          if (G_FLOAT(var_b->ofs) == 0.0f) QCC_Error(0, "division by zero");
          ++optres_constantarith;
          return QCC_MakeFloatConst(G_FLOAT(var_a->ofs)/G_FLOAT(var_b->ofs));
        case OP_ADD_F:
          ++optres_constantarith;
          return QCC_MakeFloatConst(G_FLOAT(var_a->ofs)+G_FLOAT(var_b->ofs));
        case OP_SUB_F:
          ++optres_constantarith;
          return QCC_MakeFloatConst(G_FLOAT(var_a->ofs)-G_FLOAT(var_b->ofs));
        case OP_BITOR_I:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)|G_INT(var_b->ofs));
        case OP_BITAND_I:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)&G_INT(var_b->ofs));
        case OP_MUL_I:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)*G_INT(var_b->ofs));
        case OP_DIV_I:
          if (G_INT(var_b->ofs) == 0.0f) QCC_Error(0, "division by zero");
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)/G_INT(var_b->ofs));
        case OP_ADD_I:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)+G_INT(var_b->ofs));
        case OP_SUB_I:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)-G_INT(var_b->ofs));
        case OP_AND_F:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)&&G_INT(var_b->ofs));
        case OP_OR_F:
          ++optres_constantarith;
          return QCC_MakeIntConst(G_INT(var_a->ofs)||G_INT(var_b->ofs));
        case OP_MUL_V: // mul_f is actually a dot-product
          ++optres_constantarith;
          return QCC_MakeFloatConst(
            G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs+0)+
            G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs+1)+
            G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs+2));
        case OP_MUL_FV:
          ++optres_constantarith;
          return QCC_MakeVectorConst(
            G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs+0),
            G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs+1),
            G_FLOAT(var_a->ofs)*G_FLOAT(var_b->ofs+2));
        case OP_MUL_VF:
          ++optres_constantarith;
          return QCC_MakeVectorConst(
            G_FLOAT(var_a->ofs+0)*G_FLOAT(var_b->ofs),
            G_FLOAT(var_a->ofs+1)*G_FLOAT(var_b->ofs),
            G_FLOAT(var_a->ofs+2)*G_FLOAT(var_b->ofs));
        case OP_ADD_V:
          ++optres_constantarith;
          return QCC_MakeVectorConst(
            G_FLOAT(var_a->ofs+0)+G_FLOAT(var_b->ofs+0),
            G_FLOAT(var_a->ofs+1)+G_FLOAT(var_b->ofs+1),
            G_FLOAT(var_a->ofs+2)+G_FLOAT(var_b->ofs+2));
        case OP_SUB_V:
          ++optres_constantarith;
          return QCC_MakeVectorConst(
            G_FLOAT(var_a->ofs+0)-G_FLOAT(var_b->ofs+0),
            G_FLOAT(var_a->ofs+1)-G_FLOAT(var_b->ofs+1),
            G_FLOAT(var_a->ofs+2)-G_FLOAT(var_b->ofs+2));
        }
      } else {
        //a is const, b is not
        switch (op-pr_opcodes) {
          case OP_CONV_FTOI:
            ++optres_constantarith;
            return QCC_MakeIntConst(G_FLOAT(var_a->ofs));
          case OP_CONV_ITOF:
            ++optres_constantarith;
            return QCC_MakeFloatConst(G_INT(var_a->ofs));
          case OP_BITOR_F:
          case OP_OR_F:
          case OP_ADD_F:
            if (G_FLOAT(var_a->ofs) == 0) {
              ++optres_constantarith;
              QCC_UnFreeTemp(var_b);
              return var_b;
            }
            break;
          case OP_MUL_F:
            if (G_FLOAT(var_a->ofs) == 1) {
              ++optres_constantarith;
              QCC_UnFreeTemp(var_b);
              return var_b;
            }
            break;
          case OP_BITAND_F:
          case OP_AND_F:
            if (G_FLOAT(var_a->ofs) != 0) {
              ++optres_constantarith;
              QCC_UnFreeTemp(var_b);
              return var_b;
            }
            break;
          case OP_BITOR_I:
          case OP_OR_I:
          case OP_ADD_I:
            if (G_INT(var_a->ofs) == 0) {
              ++optres_constantarith;
              QCC_UnFreeTemp(var_b);
              return var_b;
            }
            break;
          case OP_MUL_I:
            if (G_INT(var_a->ofs) == 1) {
              ++optres_constantarith;
              QCC_UnFreeTemp(var_b);
              return var_b;
            }
            break;
          case OP_BITAND_I:
          case OP_AND_I:
            if (G_INT(var_a->ofs) != 0) {
              ++optres_constantarith;
              QCC_UnFreeTemp(var_b);
              return var_b;
            }
            break;
        }
      }
    } else if (var_b && var_b->constant) {
      QCC_def_t *nd;
      // b is const, a is not
      switch (op-pr_opcodes) {
        case OP_LOADA_F:
        case OP_LOADA_V:
        case OP_LOADA_S:
        case OP_LOADA_ENT:
        case OP_LOADA_FLD:
        case OP_LOADA_FNC:
        case OP_LOADA_I:
          nd = (void *)qccHunkAlloc (sizeof(QCC_def_t));
          memset (nd, 0, sizeof(QCC_def_t));
          nd->type = var_a->type;
          nd->ofs = var_a->ofs+G_INT(var_b->ofs);
          nd->temp = var_a->temp;
          nd->constant = qcc_false;
          nd->name = var_a->name;
          return nd;
        case OP_BITOR_F:
        case OP_OR_F:
        case OP_SUB_F:
        case OP_ADD_F:
          if (G_FLOAT(var_b->ofs) == 0) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
          break;
        case OP_DIV_F:
        case OP_MUL_F:
          if (G_FLOAT(var_b->ofs) == 1) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
          break;
        // no bitand_f, I don't trust the casts
        case OP_AND_F:
          if (G_FLOAT(var_b->ofs) != 0) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
          break;
        case OP_BITOR_I:
        case OP_OR_I:
        case OP_SUB_I:
        case OP_ADD_I:
          if (G_INT(var_b->ofs) == 0) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
          break;
        case OP_DIV_I:
        case OP_MUL_I:
          if (G_INT(var_b->ofs) == 1) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
          break;
        case OP_BITAND_I:
          if (G_INT(var_b->ofs) == 0xffffffff) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
        case OP_AND_I:
          if (G_INT(var_b->ofs) == 0) {
            ++optres_constantarith;
            QCC_UnFreeTemp(var_a);
            return var_a;
          }
          break;
      }
    }
  }
  /* */
  switch (op-pr_opcodes) {
    case OP_LOADA_F:
    case OP_LOADA_V:
    case OP_LOADA_S:
    case OP_LOADA_ENT:
    case OP_LOADA_FLD:
    case OP_LOADA_FNC:
    case OP_LOADA_I:
      break;
    case OP_AND_F:
      if (var_a->ofs == var_b->ofs) QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Parameter offsets for && are the same");
      if (var_a->constant || var_b->constant) QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Result of comparison is constant");
      break;
    case OP_OR_F:
      if (var_a->ofs == var_b->ofs) QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Parameters for || are the same");
      if (var_a->constant || var_b->constant) QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Result of comparison is constant");
      break;
    case OP_EQ_F:
    case OP_EQ_S:
    case OP_EQ_E:
    case OP_EQ_FNC:
      //if (opt_shortenifnots)
      //  if (var_b->constant && ((qccint *)qcc_pr_globals)[var_b->ofs]==0) // (a == 0) becomes (!a)
      //    op = &pr_opcodes[(op - pr_opcodes) - OP_EQ_F + OP_NOT_F];
    case OP_EQ_V:
    case OP_NE_F:
    case OP_NE_V:
    case OP_NE_S:
    case OP_NE_E:
    case OP_NE_FNC:
    case OP_LE_F:
    case OP_GE_F:
    case OP_LT_F:
    case OP_GT_F:
      if ((var_a->constant && var_b->constant && !var_a->temp && !var_b->temp) || var_a->ofs == var_b->ofs) {
        QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Result of comparison is constant");
      }
      break;
    case OP_IF_S:
    case OP_IFNOT_S:
    case OP_IF_F:
    case OP_IFNOT_F:
    case OP_IF_I:
    case OP_IFNOT_I:
      //if (var_a->type->type == ev_function && !var_a->temp)
      //  QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Result of comparison is constant");
      if (var_a->constant && !var_a->temp) QCC_PR_ParseWarning(WARN_CONSTANTCOMPARISON, "Result of comparison is constant");
      break;
    default:
      break;
  }
  /* */
  if (numstatements) {
    // optimise based on last statement
    if (op-pr_opcodes == OP_IFNOT_I) {
      if (opt_shortenifnots && var_a &&
          (statements[numstatements-1].op == OP_NOT_F || statements[numstatements-1].op == OP_NOT_FNC ||
           statements[numstatements-1].op == OP_NOT_ENT)) {
        if (statements[numstatements-1].c == var_a->ofs) {
          static QCC_def_t nvara;
          op = (statements[numstatements-1].op == OP_NOT_F ? &pr_opcodes[OP_IF_F] : &pr_opcodes[OP_IF_I]);
          --numstatements;
          QCC_FreeTemp(var_a);
          memcpy(&nvara, var_a, sizeof(nvara));
          nvara.ofs = statements[numstatements].a;
          var_a = &nvara;
          ++optres_shortenifnots;
        }
      }
    } else if (op-pr_opcodes == OP_IFNOT_F) {
      if (opt_shortenifnots && var_a && statements[numstatements-1].op == OP_NOT_F) {
        if (statements[numstatements-1].c == var_a->ofs) {
          static QCC_def_t nvara;
          op = &pr_opcodes[OP_IF_F];
          --numstatements;
          QCC_FreeTemp(var_a);
          memcpy(&nvara, var_a, sizeof(nvara));
          nvara.ofs = statements[numstatements].a;
          var_a = &nvara;
          ++optres_shortenifnots;
        }
      }
    } else if (op-pr_opcodes == OP_IFNOT_S) {
      if (opt_shortenifnots && var_a && statements[numstatements-1].op == OP_NOT_S) {
        if (statements[numstatements-1].c == var_a->ofs) {
          static QCC_def_t nvara;
          op = &pr_opcodes[OP_IF_S];
          --numstatements;
          QCC_FreeTemp(var_a);
          memcpy(&nvara, var_a, sizeof(nvara));
          nvara.ofs = statements[numstatements].a;
          var_a = &nvara;
          ++optres_shortenifnots;
        }
      }
    } else if ((unsigned int)(op-pr_opcodes-OP_STORE_F) < 6 || op-pr_opcodes == OP_STORE_P) {
      // remove assignments if what should be assigned is the 3rd operand of the previous statement?
      // don't if it's a call, callH, switch or case
      // && var_a->ofs >RESERVED_OFS)
      // c = a*b is performed in one operation rather than two
      if (OpAssignsToC(statements[numstatements-1].op) && var_a && var_a->ofs == statements[numstatements-1].c) {
        if (var_a->type->type == var_b->type->type) {
          if (var_a->temp) {
            statement = &statements[numstatements-1];
            statement->c = var_b->ofs;
            if (var_a->type->type != var_b->type->type) QCC_PR_ParseWarning(0, "store type mismatch");
            ++var_b->references;
            --var_a->references;
            QCC_FreeTemp(var_a);
            ++optres_assignments;
            simplestore = qcc_true;
            QCC_UnFreeTemp(var_b);
            return var_b;
          }
        }
      }
    }
  }
  simplestore = qcc_false;
  statement = &statements[numstatements];
  ++numstatements;
  // missing opcode emulation
  if (!QCC_OPCodeValid(op)) {
    switch (op-pr_opcodes) {
      case OP_BITSET_I:
        op = &pr_opcodes[OP_BITOR_I];
        var_c = var_b;
        var_b = var_a;
        var_a = var_c;
        var_c = var_a;
        break;
      case OP_SUBSTOREP_FI:
      case OP_SUBSTOREP_IF:
      case OP_ADDSTOREP_FI:
      case OP_ADDSTOREP_IF:
      case OP_MULSTOREP_FI:
      case OP_MULSTOREP_IF:
      case OP_DIVSTOREP_FI:
      case OP_DIVSTOREP_IF:
      case OP_MULSTOREP_V:
      case OP_SUBSTOREP_V:
      case OP_ADDSTOREP_V:
      case OP_SUBSTOREP_F:
      case OP_ADDSTOREP_F:
      case OP_MULSTOREP_F:
      case OP_DIVSTOREP_F:
      case OP_BITSETP_I:
        QCC_UnFreeTemp(var_a);
        QCC_UnFreeTemp(var_b);
        // don't chain these... this expansion is not the same
        {
          int st;
          int need_lock = qcc_false;
          for (st = numstatements-2; st >= 0; --st) {
            if (statements[st].op == OP_ADDRESS && statements[st].c == var_b->ofs) break;
            if (statements[st].op >= OP_CALL0 && statements[st].op <= OP_CALL8) need_lock = qcc_true;
            if (statements[st].c == var_b->ofs) QCC_PR_ParseWarning(0, "Temp-reuse may have broken your %s", op->name);
          }
          if (st < 0) QCC_PR_ParseError(ERR_INTERNAL, "XSTOREP_F: pointer was not generated from previous statement");
          var_c = QCC_GetTemp(*op->type_c);
          if (need_lock) QCC_LockTemp(var_c); // that temp needs to be preserved over calls
          statement_linenums[statement-statements] = statement_linenums[st];
          statement->op = OP_ADDRESS;
          statement->a = statements[st].a;
          statement->b = statements[st].b;
          statement->c = statements[st].c;
          statement_linenums[st] = pr_source_line;
          statements[st].op = ((*op->type_c)->type == ev_vector ? OP_LOAD_V : OP_LOAD_F);
          statements[st].a = statements[st].a;
          statements[st].b = statements[st].b;
          statements[st].c = var_c->ofs;
        }
        statement = &statements[numstatements];
        ++numstatements;
        statement_linenums[statement-statements] = pr_source_line;
        switch (op-pr_opcodes) {
          case OP_SUBSTOREP_V: statement->op = OP_SUB_V; break;
          case OP_ADDSTOREP_V: statement->op = OP_ADD_V; break;
          case OP_MULSTOREP_V: statement->op = OP_MUL_VF; break;
          case OP_SUBSTOREP_F: statement->op = OP_SUB_F; break;
          case OP_SUBSTOREP_IF: statement->op = OP_SUB_IF; break;
          case OP_SUBSTOREP_FI: statement->op = OP_SUB_FI; break;
          case OP_ADDSTOREP_IF: statement->op = OP_ADD_IF; break;
          case OP_ADDSTOREP_FI: statement->op = OP_ADD_FI; break;
          case OP_MULSTOREP_IF: statement->op = OP_MUL_IF; break;
          case OP_MULSTOREP_FI: statement->op = OP_MUL_FI; break;
          case OP_DIVSTOREP_IF: statement->op = OP_DIV_IF; break;
          case OP_DIVSTOREP_FI: statement->op = OP_DIV_FI; break;
          case OP_ADDSTOREP_F: statement->op = OP_ADD_F; break;
          case OP_MULSTOREP_F: statement->op = OP_MUL_F; break;
          case OP_DIVSTOREP_F: statement->op = OP_DIV_F; break;
          case OP_BITSETP_I: statement->op = OP_BITOR_I; break;
          default: // no way will this be hit...
            QCC_PR_ParseError(ERR_INTERNAL, "opcode invalid 3 times %d", op-pr_opcodes);
        }
        statement->a = (var_c ? var_c->ofs : 0);
        statement->b = (var_a ? var_a->ofs : 0);
        statement->c = var_c->ofs;
        var_b = var_b; // this is the ptr
        QCC_FreeTemp(var_a);
        var_a = var_c; // this is the value
        op = &pr_opcodes[((*op->type_c)->type == ev_vector ? OP_STOREP_V : OP_STOREP_F)];
        QCC_FreeTemp(var_c);
        var_c = NULL;
        QCC_FreeTemp(var_b);
        statement = &statements[numstatements];
        ++numstatements;
        break;
      default:
        QCC_PR_ParseError(ERR_BADEXTENSION, "Opcode \"%s|%s\" not valid for target", op->name, op->opname);
        break;
    }
  }
  if (outstatement) *outstatement = statement;
  statement_linenums[statement-statements] = pr_source_line;
  statement->op = op-pr_opcodes;
  statement->a = (var_a ? var_a->ofs : 0);
  statement->b = (var_b ? var_b->ofs : 0);
  if (var_c != NULL) {
    statement->c = var_c->ofs;
  } else if (op->type_c == &type_void || op->associative == ASSOC_RIGHT || op->type_c == NULL) {
    var_c = NULL;
    statement->c = 0; // ifs, gotos, and assignments don't need vars allocated
  } else {
    // allocate result space
    var_c = QCC_GetTemp(*op->type_c);
    statement->c = var_c->ofs;
    if (op->type_b == &type_field) {
      var_c->name = var_b->name;
      var_c->s_file = var_b->s_file;
      var_c->s_line = var_b->s_line;
    }
  }
  if (is_edict_load(op-pr_opcodes)) {
    if (var_b->constant == 2) var_c->constant = qcc_true;
  }
  if (!var_c) {
    if (var_a) QCC_UnFreeTemp(var_a);
    return var_a;
  }
  return var_c;
}
