/*
 * QuakeC compiler for Doom2D:TL!, based on FTEQCC
 * Copyright (C) 1996-1997  Id Software, Inc.
 * Also copyright  FrikQCC and FTEQCC authors
 * Copyright (C) 2013  Ketmar Dark
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// qcc_cmdlib.h
#ifndef _QCC_CMDLIB_H_
#define _QCC_CMDLIB_H_


// the dec offsetof macro doesn't work very well...
#define myoffsetof(type,identifier)  ((size_t)&((type *)NULL)->identifier)


// set these before calling CheckParm
extern int myargc;
extern char **myargv;

extern char qcc_token[1024];
extern int qcc_eof;


extern int QCC_filelength (int handle);
extern int QCC_tell (int handle);

extern void QCC_Error (int errortype, const char *error, ...) QCC_GCC_PRINTF(2);
extern int CheckParm (const char *check);

extern int SafeOpenWrite (const char *filename, int maxsize);
extern int SafeOpenRead (const char *filename);
extern void SafeRead (int handle, void *buffer, long count);
extern void SafeWrite (int handle, const void *buffer, long count);
extern void SafeClose(int handle);
extern int SafeSeek (int hand, int ofs, int mode);
extern void *SafeMalloc (long size);

extern long QCC_LoadFile (const char *filename, void **bufferptr);
extern void QCC_SaveFile (const char *filename, void *buffer, long count);

extern void DefaultExtension (char *path, const char *extension);
extern void DefaultPath (char *path, const char *basepath);
extern void StripFilename (char *path);
extern void StripExtension (char *path);

extern void ExtractFilePath (const char *path, char *dest);
extern void ExtractFileBase (const char *path, char *dest);
extern void ExtractFileExtension (const char *path, char *dest);

extern long ParseNum (const char *str);

extern /*const*/ char *QCC_COM_Parse (/*const*/ char *data);
extern /*const*/ char *QCC_COM_Parse2 (/*const*/ char *data);


#endif
