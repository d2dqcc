//for compiler constants and file includes.
typedef struct qcc_includechunk_s {
  struct qcc_includechunk_s *prev;
  const char *filename;
  char *currentdatapoint;
  int currentlinenumber;
  CompilerConstant_t *cnst;
} qcc_includechunk_t;
qcc_includechunk_t *currentchunk;


void QCC_PR_IncludeChunkEx (char *data, qccbool duplicate, const char *filename, CompilerConstant_t *cnst) {
  qcc_includechunk_t *chunk = qccHunkAlloc(sizeof(qcc_includechunk_t));
  chunk->prev = currentchunk;
  currentchunk = chunk;
  chunk->currentdatapoint = pr_file_p;
  chunk->currentlinenumber = pr_source_line;
  chunk->cnst = cnst;
  if (cnst) ++cnst->inside;
  if (duplicate) {
    char *fn = qccHunkDupStr(data);
    pr_file_p = fn;
  } else {
    pr_file_p = data;
  }
}


void QCC_PR_IncludeChunk (char *data, qccbool duplicate, const char *filename) {
  QCC_PR_IncludeChunkEx(data, duplicate, filename, NULL);
}


qccbool QCC_PR_UnInclude (void) {
  if (!currentchunk) return qcc_false;
  if (currentchunk->cnst) --currentchunk->cnst->inside;
  pr_file_p = currentchunk->currentdatapoint;
  pr_source_line = currentchunk->currentlinenumber;
  currentchunk = currentchunk->prev;
  return qcc_true;
}


//also meant to include it.
void QCC_FindBestInclude (char *newfile, const char *currentfile, const char *rootpath) {
  char fullname[1024];
  int doubledots;
  char *end = fullname;
  if (!*newfile) return;
  doubledots = 0;
  /*count how far up we need to go*/
  while (!strncmp(newfile, "../", 3) || !strncmp(newfile, "..\\", 3)) {
    newfile += 3;
    ++doubledots;
  }
  currentfile += strlen(rootpath); // could this be bad?
  strcpy(fullname, rootpath);
  end = fullname+strlen(end);
  if (*fullname && end[-1] != '/') {
    strcpy(end, "/");
    end = end+strlen(end);
  }
  strcpy(end, currentfile);
  end = end+strlen(end);
  while (end > fullname) {
    --end;
    // stop at the slash, unless we're meant to go further
    if (*end == '/' || *end == '\\') {
      if (!doubledots) { ++end; break; }
      --doubledots;
    }
  }
  strcpy(end, newfile);
  QCC_Include(fullname);
}


int ParsePreprocessorIf (void) {
  CompilerConstant_t *c;
  int eval = 0;
  qccbool notted = qcc_false;
  /*skip whitespace*/
  while (*pr_file_p && *pr_file_p <= ' ' && *pr_file_p != '\n') ++pr_file_p;
  if (*pr_file_p == '!') {
    pr_file_p++;
    notted = qcc_true;
    while (*pr_file_p && *pr_file_p <= ' ' && *pr_file_p != '\n') ++pr_file_p;
  }
  if (!QCC_PR_SimpleGetToken()) {
    if (*pr_file_p == '(') {
      ++pr_file_p;
      eval = ParsePreprocessorIf();
      while (*pr_file_p == ' ' || *pr_file_p == '\t') ++pr_file_p;
      if (*pr_file_p != ')') QCC_PR_ParseError(ERR_EXPECTED, "unclosed bracket condition\n");
      ++pr_file_p;
    } else {
      QCC_PR_ParseError(ERR_EXPECTED, "expected bracket or constant\n");
    }
  } else if (!strcmp(pr_token, "defined")) {
    while (*pr_file_p == ' ' || *pr_file_p == '\t') ++pr_file_p;
    if (*pr_file_p != '(') {
      QCC_PR_ParseError(ERR_EXPECTED, "no opening bracket after defined\n");
    } else {
      ++pr_file_p;
      QCC_PR_SimpleGetToken();
      eval = !!QCC_PR_EatCompConstDefined(pr_token);
      while (*pr_file_p == ' ' || *pr_file_p == '\t') ++pr_file_p;
      if (*pr_file_p != ')') QCC_PR_ParseError(ERR_EXPECTED, "unclosed defined condition\n");
      ++pr_file_p;
    }
  } else {
    c = QCC_PR_EatCompConstDefined(pr_token);
    eval = (c ? atoi(c->value) : atoi(pr_token));
  }
  if (notted) eval = !eval;
  QCC_PR_SimpleGetToken();
  if (!strcmp(pr_token, "||")) eval = ParsePreprocessorIf() || eval;
  else if (!strcmp(pr_token, "&&")) eval = ParsePreprocessorIf() && eval;
  else if (!strcmp(pr_token, "<=")) eval = (eval <= ParsePreprocessorIf());
  else if (!strcmp(pr_token, ">=")) eval = (eval >= ParsePreprocessorIf());
  else if (!strcmp(pr_token, "<")) eval = (eval < ParsePreprocessorIf());
  else if (!strcmp(pr_token, ">")) eval = (eval > ParsePreprocessorIf());
  else if (!strcmp(pr_token, "!=")) eval = (eval != ParsePreprocessorIf());
  return eval;
}


static int striscmd (const char *s, const char *cmd) {
  while (*s && *cmd && *s == *cmd) { ++s; ++cmd; }
  return (!cmd[0] && (!s[0] || isspace(s[0])));
}


static inline char *skip_to_eol (char *s) {
  while (*s && *s != '\n') ++s;
  return s;
}


/*
==============
QCC_PR_Preprocessor
==============

Runs preprocessor stage
*/
static qccbool QCC_PR_Preprocessor (void) {
  char msg[1024];
  int ifmode, a;
  static int ifs = 0;
  int level; // #if level
  qccbool eval = qcc_false;
  char *directive;
  /* */
  if (*pr_file_p != '#') return qcc_false;
  //so `#    define` works
  for (directive = pr_file_p+1; *directive; ++directive) {
    if (*directive == '\r' || *directive == '\n') QCC_PR_ParseError(ERR_UNKNOWNPUCTUATION, "Hanging # with no directive\n");
    if (!isspace(*directive)) break;
  }
  /* */
  if (striscmd(directive, "define")) {
    pr_file_p = directive;
    QCC_PR_ConditionCompilation();
    pr_file_p = skip_to_eol(pr_file_p);
  } else if (striscmd(directive, "undef")) {
    pr_file_p = directive+5;
    while (*pr_file_p && *pr_file_p <= ' ') ++pr_file_p;
    QCC_PR_SimpleGetToken();
    QCC_PR_UndefineName(pr_token);
    //QCC_PR_ConditionCompilation();
    pr_file_p = skip_to_eol(pr_file_p);
  } else if (!strncmp(directive, "if", 2)) {
    int originalline = pr_source_line;
    pr_file_p = directive+2;
    if (striscmd(pr_file_p, "def")) {
      ifmode = 0;
      pr_file_p += 3;
    } else if (striscmd(pr_file_p, "ndef")) {
      ifmode = 1;
      pr_file_p += 4;
    } else {
      if (directive[2] && !isspace(directive[2])) QCC_PR_ParseError(0, "bad \"#if\" type");
      ifmode = 2;
    }
    /* */
    if (ifmode == 2) {
      eval = ParsePreprocessorIf();
      if (*pr_file_p != '\n' && *pr_file_p != '\0') QCC_PR_ParseError(ERR_NOENDIF, "junk on the end of #if line");
    } else {
      QCC_PR_SimpleGetToken();
      //if (!strcmp(pr_token, "COOP_MODE")) eval = qcc_false;
      if (QCC_PR_EatCompConstDefined(pr_token)) eval = qcc_true;
      if (ifmode == 1) eval = (eval ? qcc_false : qcc_true);
    }
    /* */
    pr_file_p = skip_to_eol(pr_file_p);
    level = 1;
    /* */
    if (eval) {
      ifs += 1;
    } else {
      for (;;) {
        while (*pr_file_p && (*pr_file_p == ' ' || *pr_file_p == '\t')) ++pr_file_p;
        if (!*pr_file_p) {
          pr_source_line = originalline;
          QCC_PR_ParseError(ERR_NOENDIF, "#if with no endif");
        }
        if (*pr_file_p == '#') {
          ++pr_file_p;
          while (*pr_file_p == ' ' || *pr_file_p == '\t') ++pr_file_p;
          if (striscmd(pr_file_p, "endif")) --level;
          else if (striscmd(pr_file_p, "if")) ++level;
          else if (striscmd(pr_file_p, "else") && level == 1) {
            ifs += 1;
            pr_file_p = skip_to_eol(pr_file_p);
            break;
          }
        }
        pr_file_p = skip_to_eol(pr_file_p);
        if (level <= 0) break;
        ++pr_file_p; // next line
        ++pr_source_line;
      }
    }
  } else if (striscmd(directive, "else")) {
    int originalline = pr_source_line;
    ifs -= 1;
    level = 1;
    pr_file_p = skip_to_eol(pr_file_p);
    for (;;) {
      while (*pr_file_p && (*pr_file_p == ' ' || *pr_file_p == '\t')) ++pr_file_p;
      if (!*pr_file_p) {
        pr_source_line = originalline;
        QCC_PR_ParseError(ERR_NOENDIF, "#if with no endif");
      }
      if (*pr_file_p == '#') {
        ++pr_file_p;
        while (*pr_file_p==' ' || *pr_file_p == '\t') ++pr_file_p;
        if (striscmd(pr_file_p, "endif")) --level;
        if (striscmd(pr_file_p, "if")) ++level;
        if (striscmd(pr_file_p, "else") && level == 1) {
          ifs += 1;
          break;
        }
      }
      pr_file_p = skip_to_eol(pr_file_p);
      if (level <= 0) break;
      ++pr_file_p; // go off the end
      ++pr_source_line;
    }
  } else if (striscmd(directive, "endif")) {
    pr_file_p = skip_to_eol(pr_file_p);
    if (ifs <= 0) QCC_PR_ParseError(ERR_NOPRECOMPILERIF, "unmatched #endif"); else ifs -= 1;
  } else if (striscmd(directive, "eof")) {
    pr_file_p = NULL;
    return qcc_true;
  } else if (striscmd(directive, "error")) {
    pr_file_p = directive+5;
    for (a = 0; a < sizeof(msg)-1 && pr_file_p[a] != '\n' && pr_file_p[a] != '\0'; ++a) msg[a] = pr_file_p[a];
    msg[a] = '\0';
    // read on until the end of the line, yes, I KNOW we are going to register an error,
    // and not properly leave this function tree, but...
    pr_file_p = skip_to_eol(pr_file_p);
    QCC_PR_ParseError(ERR_HASHERROR, "#Error: %s", msg);
  } else if (striscmd(directive, "warning")) {
    pr_file_p = directive+7;
    for (a = 0; a < 1023 && pr_file_p[a] != '\n' && pr_file_p[a] != '\0'; ++a) msg[a] = pr_file_p[a];
    msg[a-1] = '\0';
    pr_file_p = skip_to_eol(pr_file_p);
    QCC_PR_ParseWarning(WARN_PRECOMPILERMESSAGE, "#warning: %s", msg);
  } else if (striscmd(directive, "message")) {
    pr_file_p = directive+7;
    for (a = 0; a < 1023 && pr_file_p[a] != '\n' && pr_file_p[a] != '\0'; ++a) msg[a] = pr_file_p[a];
    msg[a-1] = '\0';
    pr_file_p = skip_to_eol(pr_file_p);
    qcc_printf("#message: %s\n", msg);
  } else if (striscmd(directive, "forcecrc")) {
    pr_file_p = directive+8;
    ForcedCRC = QCC_PR_LexInteger();
    ++pr_file_p;
    for (a = 0; a < sizeof(msg)-1 && pr_file_p[a] != '\n' && pr_file_p[a] != '\0'; ++a) msg[a] = pr_file_p[a];
    msg[a-1] = '\0';
    pr_file_p = skip_to_eol(pr_file_p);
  } else if (striscmd(directive, "includelist")) {
    pr_file_p = directive+11;
    while (*pr_file_p <= ' ') ++pr_file_p;
    for (;;) {
      QCC_PR_LexWhitespace();
      if (!QCC_PR_SimpleGetToken()) {
        if (!*pr_file_p) {
          QCC_Error(ERR_EOF, "eof in includelist");
        } else {
          ++pr_file_p;
          ++pr_source_line;
        }
        continue;
      }
      if (!strcmp(pr_token, "#endlist")) break;
      QCC_FindBestInclude(pr_token, compilingfile, qccmsourcedir);
      pr_file_p = skip_to_eol(pr_file_p);
    }
    pr_file_p = skip_to_eol(pr_file_p);
  } else if (striscmd(directive, "include")) {
    char sm;
    pr_file_p = directive+7;
    while (*pr_file_p <= ' ') ++pr_file_p;
    msg[0] = '\0';
    if (*pr_file_p == '\"') sm = '\"';
    else if (*pr_file_p == '<') sm = '>';
    else {
      QCC_PR_ParseError(0, "Not a string literal (on a #include)");
      sm = 0;
    }
    ++pr_file_p;
    a = 0;
    while (*pr_file_p != sm) {
      if (*pr_file_p == '\n') {
        QCC_PR_ParseError(0, "#include continued over line boundry\n");
        break;
      }
      msg[a++] = *pr_file_p;
      ++pr_file_p;
    }
    msg[a] = 0;
    QCC_FindBestInclude(msg, compilingfile, qccmsourcedir);
    ++pr_file_p;
    while (*pr_file_p != '\n' && *pr_file_p != '\0' && *pr_file_p <= ' ') ++pr_file_p;
    pr_file_p = skip_to_eol(pr_file_p);
  } else if (striscmd(directive, "output")) {
    extern char destfile[1024];
    pr_file_p = directive+6;
    while (*pr_file_p <= ' ') ++pr_file_p;
    QCC_PR_LexString();
    strcpy(destfile, pr_token);
    qcc_printf("Outputfile: %s\n", destfile);
    ++pr_file_p;
    for (a = 0; a < sizeof(msg)-1 && pr_file_p[a] != '\n' && pr_file_p[a] != '\0'; ++a) msg[a] = pr_file_p[a];
    msg[a-1] = '\0';
    pr_file_p = skip_to_eol(pr_file_p);
  } else if (striscmd(directive, "pragma")) {
    pr_file_p = directive+6;
    while (*pr_file_p && *pr_file_p <= ' ') ++pr_file_p;
    qcc_token[0] = '\0';
    //read on until the end of the line
    for (a = 0; *pr_file_p != '\n' && *pr_file_p != '\0'; ++pr_file_p) {
      if ((*pr_file_p == ' ' || *pr_file_p == '\t' || *pr_file_p == '(') && !*qcc_token) {
        msg[a] = '\0';
        strcpy(qcc_token, msg);
        a=0;
        continue;
      }
      msg[a++] = *pr_file_p;
    }
    msg[a] = '\0';
    for (char *end = msg+a-1; end >= msg && *end <= ' '; --end) *end = '\0';
    if (!*qcc_token) {
      strcpy(qcc_token, msg);
      msg[0] = '\0';
    }
    for (char *end = msg+a-1; end>=msg && *end <= ' '; --end) *end = '\0';
    if (!QC_strcasecmp(qcc_token, "DONT_COMPILE_THIS_FILE")) {
      while (*pr_file_p) {
        pr_file_p = skip_to_eol(pr_file_p);
        if (*pr_file_p == '\n') {
          ++pr_file_p;
          QCC_PR_NewLine(qcc_false);
        }
      }
    } else if (!strncmp(qcc_token, "forcecrc", 8)) {
      ForcedCRC = atoi(msg);
    } else if (!strncmp(qcc_token, "noref", 8)) {
      defaultnoref = atoi(msg);
    } else if (!strncmp(qcc_token, "defaultstatic", 13)) {
      defaultstatic = atoi(msg);
    } else if (!strncmp(qcc_token, "wrasm", 5)) {
      qccbool on = atoi(msg);
      if (asmfile && !on) {
        fclose(asmfile);
        asmfile = NULL;
      }
      if (!asmfile && on) asmfile = fopen("qc.asm", "wb");
    } else if (!strncmp(qcc_token, "sourcefile", 10)) {
      int i;
      QCC_COM_Parse(msg);
      for (i = 0; i < numsourcefiles; ++i) if (!strcmp(sourcefileslist[i], qcc_token)) break;
      if (i == numsourcefiles && numsourcefiles < MAXSOURCEFILESLIST) strcpy(sourcefileslist[numsourcefiles++], qcc_token);
    } else if (!QC_strcasecmp(qcc_token, "PROGS_SRC")) {
      //doesn't make sence, but silenced if you are switching between using a certain precompiler app used with CuTF.
    } else if (!QC_strcasecmp(qcc_token, "PROGS_DAT")) {
      extern char destfile[1024];
#ifndef QCCONLY
      extern char qccmfilename[1024];
      int p;
      char *s, *s2;
#endif
      QCC_COM_Parse(msg);
#ifndef QCCONLY
      p = 0;
      s2 = qcc_token;
      if (!strncmp(s2, "./", 2)) {
        s2 += 2;
      } else {
        while (!strncmp(s2, "../", 3)) { s2 += 3; ++p; }
      }
      strcpy(qccmfilename, qccmsourcedir);
      for (s = qccmfilename+strlen(qccmfilename); p && s >= qccmfilename; --s) {
        if (*s == '/' || *s == '\\') {
          *(s+1) = '\0';
          --p;
        }
      }
      snprintf(destfile, sizeof(destfile), "%s", s2);
      while (p > 0) {
        memmove(destfile+3, destfile, strlen(destfile)+1);
        destfile[0] = '.';
        destfile[1] = '.';
        destfile[2] = '/';
        --p;
      }
#else
      strcpy(destfile, qcc_token);
#endif
      qcc_printf("Outputfile: %s\n", destfile);
    } else if (!QC_strcasecmp(qcc_token, "keyword") || !QC_strcasecmp(qcc_token, "flag")) {
      int st;
      const char *s = QCC_COM_Parse(msg);
      if (!QC_strcasecmp(qcc_token, "enable") || !QC_strcasecmp(qcc_token, "on")) st = 1;
      else if (!QC_strcasecmp(qcc_token, "disable") || !QC_strcasecmp(qcc_token, "off")) st = 0;
      else { QCC_PR_ParseWarning(WARN_BADPRAGMA, "compiler flag state not recognised"); st = -1; }
      if (st < 0) {
        QCC_PR_ParseWarning(WARN_BADPRAGMA, "warning id not recognised");
      } else {
        int f;
        s = QCC_COM_Parse((char *)s); /* it's ok */
        for (f = 0; compiler_flag[f].enabled; ++f) {
          if (!QC_strcasecmp(compiler_flag[f].abbrev, qcc_token)) {
            if (compiler_flag[f].flags&FLAG_MIDCOMPILE) {
              *compiler_flag[f].enabled = st;
            } else {
              QCC_PR_ParseWarning(WARN_BADPRAGMA, "Cannot enable/disable keyword/flag via a pragma");
            }
            break;
          }
        }
        if (!compiler_flag[f].enabled) QCC_PR_ParseWarning(WARN_BADPRAGMA, "keyword/flag not recognised");
      }
    } else if (!QC_strcasecmp(qcc_token, "warning")) {
      int st;
      const char *s = QCC_COM_Parse(msg);
      if (!strcasecmp(qcc_token, "enable") || !strcasecmp(qcc_token, "on")) st = 0;
      else if (!strcasecmp(qcc_token, "disable") || !strcasecmp(qcc_token, "off")) st = 1;
      else if (!strcasecmp(qcc_token, "toggle")) st = 2;
      else { QCC_PR_ParseWarning(WARN_BADPRAGMA, "warning state not recognised"); st = -1; }
      if (st >= 0) {
        int wn;
        s = QCC_COM_Parse((char *)s); /* it's ok */
        wn = QCC_WarningForName(qcc_token);
        if (wn < 0) {
          QCC_PR_ParseWarning(WARN_BADPRAGMA, "warning id not recognised");
        } else {
          if (st == 2) {
            // toggle
            qccwarningdisabled[wn] = qcc_true-qccwarningdisabled[wn];
          } else {
            qccwarningdisabled[wn] = st;
          }
        }
      }
    } else {
      QCC_PR_ParseWarning(WARN_BADPRAGMA, "Unknown pragma \'%s\'", qcc_token);
    }
  } else {
    return qcc_false;
  }
  return qcc_true;
}


//===========================
// compiler constants  - dmw
static qccbool QCC_PR_UndefineName (const char *name) {
  CompilerConstant_t *c = Hash_Get(&compconstantstable, name);
  if (!c) {
    QCC_PR_ParseWarning(WARN_UNDEFNOTDEFINED, "Preprocessor constant %s was not defined", name);
    return qcc_false;
  }
  Hash_Remove(&compconstantstable, name);
  return qcc_true;
}


CompilerConstant_t *QCC_PR_DefineName (const char *name) {
  CompilerConstant_t *cnst;
  //if (numCompilerConstants >= MAX_CONSTANTS) QCC_PR_ParseError("Too many compiler constants - %d >= %d", numCompilerConstants, MAX_CONSTANTS);
  if (strlen(name) >= MAXCONSTANTNAMELENGTH || !*name) QCC_PR_ParseError(ERR_NAMETOOLONG, "Compiler constant name length is too long or short");
  cnst = Hash_Get(&compconstantstable, name);
  if (cnst) {
    QCC_PR_ParseWarning(WARN_DUPLICATEDEFINITION, "Duplicate definition for Preprocessor constant %s", name);
    Hash_Remove(&compconstantstable, name);
  }
  cnst = qccHunkAlloc(sizeof(CompilerConstant_t));
  cnst->used = qcc_false;
  cnst->numparams = 0;
  strcpy(cnst->name, name);
  cnst->namelen = strlen(name);
  cnst->value = cnst->name + strlen(cnst->name);
  for (int i = 0; i < MAXCONSTANTPARAMS; ++i) cnst->params[i][0] = '\0';
  Hash_Add(&compconstantstable, cnst->name, cnst, qccHunkAlloc(sizeof(hashtable_bucket_t)));
  return cnst;
}


/*
static void QCC_PR_Undefine (void) {
  QCC_PR_SimpleGetToken();
  QCC_PR_UndefineName(pr_token);
  //QCC_PR_ParseError("%s was not defined.", pr_token);
}
*/


static void QCC_PR_ConditionCompilation (void) {
  char *oldval, *d, *dbuf, *s;
  int quote = qcc_false, dbuflen;
  CompilerConstant_t *cnst;
  /* */
  QCC_PR_SimpleGetToken();
  if (!QCC_PR_SimpleGetToken()) QCC_PR_ParseError(ERR_NONAME, "No name defined for compiler constant");
  cnst = Hash_Get(&compconstantstable, pr_token);
  if (cnst) {
    oldval = cnst->value;
    Hash_Remove(&compconstantstable, pr_token);
  } else {
    oldval = NULL;
  }
  cnst = QCC_PR_DefineName(pr_token);
  if (*pr_file_p == '(') {
    s = pr_file_p+1;
    while (*pr_file_p++) {
      if (*pr_file_p == ',') {
        if (cnst->numparams >= MAXCONSTANTPARAMS) {
          QCC_PR_ParseError(ERR_MACROTOOMANYPARMS, "May not have more than %d parameters to a macro", MAXCONSTANTPARAMS);
        }
        strncpy(cnst->params[cnst->numparams], s, pr_file_p-s);
        cnst->params[cnst->numparams][pr_file_p-s] = '\0';
        ++cnst->numparams;
        ++pr_file_p;
        s = pr_file_p;
      }
      if (*pr_file_p == ')') {
        if (cnst->numparams >= MAXCONSTANTPARAMS) {
          QCC_PR_ParseError(ERR_MACROTOOMANYPARMS, "May not have more than %d parameters to a macro", MAXCONSTANTPARAMS);
        }
        strncpy(cnst->params[cnst->numparams], s, pr_file_p-s);
        cnst->params[cnst->numparams][pr_file_p-s] = '\0';
        ++cnst->numparams;
        ++pr_file_p;
        break;
      }
    }
  } else {
    cnst->numparams = -1;
  }
  s = pr_file_p;
  d = dbuf = NULL;
  dbuflen = 0;
  while (*s == ' ' || *s == '\t') ++s;
  for (;;) {
    if ((d-dbuf)+2 >= dbuflen) {
      int len = d-dbuf;
      dbuflen = (len+128)*2;
      dbuf = qccHunkAlloc(dbuflen);
      memcpy(dbuf, d-len, len);
      d = dbuf+len;
    }
    if (*s == '\\') {
      // read over a newline if necessary
      if (s[1] == '\n') {
        ++s;
        QCC_PR_NewLine(qcc_true);
        *d++ = *s++;
        //if (s[-1] == '\r' && s[0] == '\n') *d++ = *s++;
      }
    } else if (*s == '\n' || *s == '\0') {
      break;
    }
    if (!quote && s[0] == '/' && (s[1] == '/' || s[1] == '*')) break;
    if (*s == '\"') quote = !quote;
    *d = *s;
    ++d;
    ++s;
  }
  *d = '\0';
  cnst->value = dbuf;
  if (oldval) {
    //we always warn if it was already defined
    //we use different warning codes so that -Wno-mundane can be used to ignore identical redefinitions.
    if (strcmp(oldval, cnst->value)) {
      QCC_PR_ParseWarning(WARN_DUPLICATEPRECOMPILER, "Alternate precompiler definition of %s", pr_token);
    } else {
      QCC_PR_ParseWarning(WARN_IDENTICALPRECOMPILER, "Identical precompiler definition of %s", pr_token);
    }
  }
  pr_file_p = s;
}


/* *buffer, *bufferlen and *buffermax should be NULL/0 at the start */
static void QCC_PR_ExpandStrCat (char **buffer, int *bufferlen, int *buffermax, char *newdata, int newlen) {
  int newmax = *bufferlen+newlen;
  if (newmax < *bufferlen) {
    QCC_PR_ParseWarning(ERR_INTERNAL, "out of memory");
    return;
  }
  if (newmax > *buffermax) {
    char *newbuf;
    if (newmax < 64) newmax = 64;
    if (newmax < *bufferlen*2) {
      newmax = *bufferlen*2;
      if (newmax < *bufferlen) {
        /*overflowed?*/
        QCC_PR_ParseWarning(ERR_INTERNAL, "out of memory");
        return;
      }
    }
    newbuf = realloc(*buffer, newmax);
    if (!newbuf) {
      QCC_PR_ParseWarning(ERR_INTERNAL, "out of memory");
      return; /*OOM*/
    }
    *buffer = newbuf;
    *buffermax = newmax;
  }
  memcpy(*buffer + *bufferlen, newdata, newlen);
  *bufferlen += newlen;
  /*no null terminator, remember to cat one if required*/
}


static int QCC_PR_EatCompConst (void) {
  static char retbuf[1024];
  char *oldpr_file_p = pr_file_p;
  int whitestart;
  CompilerConstant_t *c;
  const char *end;
  for (end = pr_file_p; *end; ++end) {
    if (isspace(*end)) break;
    if (strchr("()+-*/|&=^~[]\"{};:,.#", *end)) break;
  }
  strncpy(pr_token, pr_file_p, end-pr_file_p);
  pr_token[end-pr_file_p] = '\0';
  //fprintf(stderr, "tk: [%s]\n", pr_token);
  c = Hash_Get(&compconstantstable, pr_token);
  if (c && !c->inside) {
    pr_file_p = oldpr_file_p+strlen(c->name);
    while (*pr_file_p == ' ' || *pr_file_p == '\t') ++pr_file_p;
    if (c->numparams >= 0) {
      if (*pr_file_p == '(') {
        char *start, *starttok, *buffer;
        int p, buffermax, bufferlen;
        char *paramoffset[MAXCONSTANTPARAMS+1];
        int param = 0, plevel = 0;
        ++pr_file_p;
        while (*pr_file_p == ' ' || *pr_file_p == '\t') ++pr_file_p;
        start = pr_file_p;
        for (;;) {
          // handle strings correctly by ignoring them
          if (*pr_file_p == '\"') {
            do {
              ++pr_file_p;
            } while ((pr_file_p[-1] == '\\' || pr_file_p[0] != '\"') && *pr_file_p && *pr_file_p != '\n');
          }
          if (*pr_file_p == '(') {
            ++plevel;
          } else if (!plevel && (*pr_file_p == ',' || *pr_file_p == ')')) {
            paramoffset[param++] = start;
            start = pr_file_p+1;
            if (*pr_file_p == ')') {
              *pr_file_p = '\0';
              ++pr_file_p;
              break;
            }
            *pr_file_p = '\0';
            ++pr_file_p;
            while (*pr_file_p == ' ' || *pr_file_p == '\t') { ++pr_file_p; ++start; }
            if (param == MAXCONSTANTPARAMS) QCC_PR_ParseError(ERR_TOOMANYPARAMS, "Too many parameters in macro call");
          } else if (*pr_file_p == ')') {
            --plevel;
            ++pr_file_p;
          } else if (*pr_file_p == '\n') {
            ++pr_file_p;
            QCC_PR_NewLine(qcc_false);
          }
          // see that *pr_file_p = '\0' up there? Must ++ BEFORE checking for !*pr_file_p
          if (!*pr_file_p) QCC_PR_ParseError(ERR_EOF, "EOF on macro call");
        }
        if (param < c->numparams) QCC_PR_ParseError(ERR_TOOFEWPARAMS, "Not enough macro parameters");
        paramoffset[param] = start;
        buffer = NULL;
        bufferlen = 0;
        buffermax = 0;
        oldpr_file_p = pr_file_p;
        pr_file_p = c->value;
        for (;;) {
          whitestart = bufferlen;
          starttok = pr_file_p;
          // copy across whitespace
          while (*pr_file_p <= ' ') {
            if (!*pr_file_p) break;
            ++pr_file_p;
          }
          if (starttok != pr_file_p) QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax,   starttok, pr_file_p-starttok);
          if (*pr_file_p == '\"') {
            starttok = pr_file_p;
            do {
              ++pr_file_p;
            } while ((pr_file_p[-1] == '\\' || pr_file_p[0] != '\"') && *pr_file_p && *pr_file_p != '\n');
            if (*pr_file_p == '\"') ++pr_file_p;
            QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax,   starttok, pr_file_p - starttok);
            continue;
          } else if (*pr_file_p == '#') {
            // if you ask for #a##b you will be shot. use #a #b instead, or chain macros.
            if (pr_file_p[1] == '#') {
              //concatinate (strip out whitespace before the token)
              bufferlen = whitestart;
              pr_file_p += 2;
            } else {
              // stringify
              ++pr_file_p;
              pr_file_p = (char *)QCC_COM_Parse2(pr_file_p); // it's safe, this will always return pr_file_p contents
              if (!pr_file_p) break;
              for (p = 0; p < param; ++p) {
                if (!strcmp(qcc_token, c->params[p])) {
                  QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax, "\"", 1);
                  QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax, paramoffset[p], strlen(paramoffset[p]));
                  QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax, "\"", 1);
                  break;
                }
              }
              if (p == param) {
                QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax, "#", 1);
                QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax, qcc_token, strlen(qcc_token));
                //QCC_PR_ParseWarning(0, "Stringification ignored");
              }
              continue; //already did this one
            }
          }
          pr_file_p = (char *)QCC_COM_Parse2(pr_file_p); // it's safe, function will always return pr_file_p contents
          if (!pr_file_p) break;
          for (p = 0; p < param; ++p) {
            if (!strcmp(qcc_token, c->params[p])) {
              QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax, paramoffset[p], strlen(paramoffset[p]));
              break;
            }
          }
          if (p == param) QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax,   qcc_token, strlen(qcc_token));
        }
        for (p = 0; p < param-1; ++p) paramoffset[p][strlen(paramoffset[p])] = ',';
        paramoffset[p][strlen(paramoffset[p])] = ')';
        pr_file_p = oldpr_file_p;
        if (!bufferlen) {
          expandedemptymacro = qcc_true;
        } else {
          QCC_PR_ExpandStrCat(&buffer, &bufferlen, &buffermax,   "\0", 1);
          QCC_PR_IncludeChunkEx(buffer, qcc_true, NULL, c);
        }
        if (buffer != NULL) free(buffer);
      } else {
        QCC_PR_ParseError(ERR_TOOFEWPARAMS, "Macro without argument list");
      }
    } else {
      if (!*c->value) expandedemptymacro = qcc_true;
      QCC_PR_IncludeChunkEx(c->value, qcc_false, NULL, c);
    }
    QCC_PR_Lex();
    return qcc_true;
  }
  /* */
  if (!strncmp(pr_file_p, "__TIME__", 8)) {
    time_t long_time;
    time(&long_time);
    strftime(retbuf, sizeof(retbuf), "\"%H:%M\"", localtime(&long_time));
    pr_file_p = retbuf;
    QCC_PR_Lex(); //translate the macro's value
    pr_file_p = oldpr_file_p+8;
    return qcc_true;
  }
  if (!strncmp(pr_file_p, "__DATE__", 8)) {
    time_t long_time;
    time(&long_time);
    strftime(retbuf, sizeof(retbuf), "\"%a %d %b %Y\"", localtime(&long_time));
    pr_file_p = retbuf;
    QCC_PR_Lex(); //translate the macro's value
    pr_file_p = oldpr_file_p+8;
    return qcc_true;
  }
  if (!strncmp(pr_file_p, "__FILE__", 8)) {
    snprintf(retbuf, sizeof(retbuf), "\"%s\"", strings + s_file);
    pr_file_p = retbuf;
    QCC_PR_Lex(); //translate the macro's value
    pr_file_p = oldpr_file_p+8;
    return qcc_true;
  }
  if (!strncmp(pr_file_p, "__LINE__", 8)) {
    snprintf(retbuf, sizeof(retbuf), "\"%d\"", pr_source_line);
    pr_file_p = retbuf;
    QCC_PR_Lex(); //translate the macro's value
    pr_file_p = oldpr_file_p+8;
    return qcc_true;
  }
  if (!strncmp(pr_file_p, "__FUNC__", 8)) {
    static char retbuf[256];
    snprintf(retbuf, sizeof(retbuf), "\"%s\"",pr_scope->name);
    pr_file_p = retbuf;
    QCC_PR_Lex(); //translate the macro's value
    pr_file_p = oldpr_file_p+8;
    return qcc_true;
  }
  if (!strncmp(pr_file_p, "__NULL__", 8)) {
    snprintf(retbuf, sizeof(retbuf), "0i");
    pr_file_p = retbuf;
    QCC_PR_Lex(); //translate the macro's value
    pr_file_p = oldpr_file_p+8;
    return qcc_true;
  }
  return qcc_false;
}


static const char *QCC_PR_EatCompConstString (const char *def) {
  CompilerConstant_t *c = Hash_Get(&compconstantstable, def);
  if (c) {
    const char *s = QCC_PR_EatCompConstString(c->value);
    return s;
  }
  return def;
}


CompilerConstant_t *QCC_PR_EatCompConstDefined (const char *def) {
  return Hash_Get(&compconstantstable, def);
}


